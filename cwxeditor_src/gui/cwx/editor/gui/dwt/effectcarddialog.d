
module cwx.editor.gui.dwt.effectcarddialog;

import cwx.card;
import cwx.event;
import cwx.imagesize;
import cwx.motion;
import cwx.path;
import cwx.skin;
import cwx.summary;
import cwx.types;
import cwx.usecounter;
import cwx.utils;
import cwx.warning;

import cwx.editor.gui.sound;

import cwx.editor.gui.dwt.abilityview;
import cwx.editor.gui.dwt.absdialog;
import cwx.editor.gui.dwt.cardpane;
import cwx.editor.gui.dwt.centerlayout;
import cwx.editor.gui.dwt.chooser;
import cwx.editor.gui.dwt.commons;
import cwx.editor.gui.dwt.customtable;
import cwx.editor.gui.dwt.customtext;
import cwx.editor.gui.dwt.dmenu;
import cwx.editor.gui.dwt.dprops;
import cwx.editor.gui.dwt.dskin;
import cwx.editor.gui.dwt.dutils;
import cwx.editor.gui.dwt.eventdialog;
import cwx.editor.gui.dwt.imageselect;
import cwx.editor.gui.dwt.keycodeview;
import cwx.editor.gui.dwt.materialselect;
import cwx.editor.gui.dwt.motionview;
import cwx.editor.gui.dwt.radarspinner;
import cwx.editor.gui.dwt.scales;
import cwx.editor.gui.dwt.splitpane;

import std.algorithm : equal, max;
import std.array;
import std.path;
import std.traits;

import org.eclipse.swt.all;

import java.lang.all;

public:

/// 手札カードの設定を行うダイアログ。
class EffectCardDialog(C) : AbsDialog, CardDialog {
private:
	/// アイテムの現在使用回数の設定
	/// (エンジンの仕様上ほとんど意味が無いため現在無効)
	static immutable SetUseCountCur = false;

	int _readOnly = 0;
	Commons _comm;
	Props _prop;
	Summary _summ;
	CWXPath _ucOwner;
	UseCounter _useCounter;
	C _card;

	ImageSelect!(MtType.CARD) _imgPath;
	FixedWidthText!Text _desc;
	GBLimitText _name;
	Button _needSpell;
	Button[EffectType] _effTyp;
	Button[Resist] _res;
	AbilityView _ability;
	static if (is(C:SkillCard)) {
		Spinner _level;
		void calcPrice(int value) { mixin(S_TRACE);
			_price.setMinimum(_prop.looks.skillPrice(value));
			_price.setMaximum(_prop.looks.skillPrice(value));
			_price.setSelection(_prop.looks.skillPrice(value));
		}
		int priceCancel(int oldVal) { mixin(S_TRACE);
			calcPrice(oldVal);
			return oldVal;
		}
		void updateEnabled() { }
	} else static if (is(C:ItemCard)) {
		Spinner _useCount;
		static if (SetUseCountCur) {
			Spinner _useCountCur;
			Button _useCountIsMax;
			void updateUseLimitMax() { mixin(S_TRACE);
				_useCountCur.setMaximum(_useCount.getSelection());
				if (_useCountIsMax.getSelection()) { mixin(S_TRACE);
					_useCountCur.setSelection(_useCount.getSelection());
				}
			}
		}
		void updateEnabled() { }
	} else static if (is(C:BeastCard)) {
		Spinner _useCount;
		void calcPrice(int value) { mixin(S_TRACE);
			_price.setMinimum(_prop.looks.beastPrice);
			_price.setMaximum(_prop.looks.beastPrice);
			_price.setSelection(_prop.looks.beastPrice);
		}
		int priceCancel(int oldVal) { mixin(S_TRACE);
			calcPrice(oldVal);
			return oldVal;
		}
		Button[Status] _invokeCond;
		Button _removeWithUncons;

		@property
		Status[] invokeCond() { mixin(S_TRACE);
			Status[] r;
			foreach (s; EnumMembers!Status) { mixin(S_TRACE);
				auto p = s in _invokeCond;
				if (!p) continue;
				if (p.getSelection()) r ~= s;
			}
			return r;
		}
		void updateEnabled() { mixin(S_TRACE);
			auto sEnbl = !_readOnly && (!_summ || !_summ.legacy || !.equal(invokeCond, [Status.Alive]));
			foreach (b; _invokeCond.byValue()) { mixin(S_TRACE);
				b.setEnabled(sEnbl);
			}
			_removeWithUncons.setEnabled(!_readOnly && (!_summ || !_summ.legacy || !_removeWithUncons.getSelection()));
			_showStyle.setEnabled(!_readOnly && (!_summ || !_summ.legacy || _showStyles[_showStyle.getSelectionIndex()] != ShowStyle.Center));
		}
	} else static assert (0);

	Spinner _price;
	MotionView _motions;
	Composite _useModParent;
	RadarSpinner _useModR;
	Scales _useModS;
	size_t[Enhance] _useModTbl;
	static if (is(C == ItemCard)) {
		Composite _hasModParent;
		RadarSpinner _hasModR;
		Scales _hasModS;
		size_t[Enhance] _hasModTbl;
	}
	Button[CardTarget] _targ;
	Button _one;
	Button _all;
	Composite _oneAllGrp;
	Button[CardVisual] _vis;
	Button[Premium] _prem;
	Scale _sucRate;
	SoundSelect _se1;
	SoundSelect _se2;
	KeyCodeView _keyCodes;
	Text _scenario;
	TextMenuModify _scenarioTM;
	Text _author;
	TextMenuModify _authorTM;
	static if (is(C:BeastCard)) {
		Combo _showStyle;
		ShowStyle[] _showStyles;
	}

	Skin _summSkin;
	@property
	Skin summSkin() { mixin(S_TRACE);
		return _summSkin ? _summSkin : _comm.skin;
	}
	class ResetSource : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			ignoreMod = true;
			scope (exit) ignoreMod = false;
			_scenario.setText(_summ.scenarioName);
			_author.setText(_summ.author);
			_scenarioTM.reset();
			_authorTM.reset();
		}
	}
	class ModSource : ModifyListener {
		override void modifyText(ModifyEvent e) { mixin(S_TRACE);
			refreshWarning();
		}
	}
	void refEventTree(EventTree et) { mixin(S_TRACE);
		if (et.owner is _card || et.owner is null) { mixin(S_TRACE);
			refreshWarning();
		}
	}
	void refreshWarning() { mixin(S_TRACE);
		string[] ws;
		ws ~= .sjisWarnings(_prop.parent, _summ, _name.getText(), _prop.msgs.name);
		if (_name.over) { mixin(S_TRACE);
			ws ~= .tryFormat(_prop.msgs.warningNameLenOver, _prop.looks.nameLimit, _prop.looks.nameLimit / 2);
		}
		ws ~= _imgPath.warnings;
		if (_effTyp[EffectType.None].getSelection()) { mixin(S_TRACE);
			ws ~= _prop.msgs.warningEffectTypeNone;
		}
		ws ~= .sjisWarnings(_prop.parent, _summ, _desc.getText(), _prop.msgs.desc);
		ws ~= .sjisWarnings(_prop.parent, _summ, _scenario.getText(), _prop.msgs.sourceScenario);
		ws ~= .sjisWarnings(_prop.parent, _summ, _author.getText(), _prop.msgs.sourceAuthor);
		ws ~= _motions.warnings;
		foreach (m; _motions.motions) { mixin(S_TRACE);
			if (m.type == MType.VanishTarget && m.element != cast(int) Element.Miracle) { mixin(S_TRACE);
				ws ~= _prop.msgs.warningVanishCast;
				break;
			}
		}
		static if (is(C:BeastCard)) {
			if (_showStyle.getSelectionIndex() != -1 && _showStyles[_showStyle.getSelectionIndex()] != ShowStyle.Center && !_prop.isTargetVersion(_summ, "4")) { mixin(S_TRACE);
				ws ~= _prop.msgs.warningShowStyleForBeastCard;
			}
			if (!_prop.isTargetVersion(_summ, "3")) { mixin(S_TRACE);
				if (!.equal(invokeCond, [Status.Alive])) { mixin(S_TRACE);
					ws ~= _prop.msgs.warningInvocationCondition;
				}
				if (!_removeWithUncons.getSelection()) { mixin(S_TRACE);
					ws ~= _prop.msgs.warningRemoveWithUnconscious;
				}
			}
			if (_invokeCond[Status.Unconscious].getSelection() && _removeWithUncons.getSelection()) { mixin(S_TRACE);
				ws ~= _prop.msgs.warningUnconsciousCondition;
			}
		}
		if (_summ && _summ.legacy && _se1.filePath != "" && !_se1.selectedDefDir) { mixin(S_TRACE);
			ws ~= _prop.msgs.warningNotDefaultSE;
		}
		ws ~= summSkin.warningSE(_prop.parent, _se1.filePath, _summ.legacy, _prop.var.etc.targetVersion) ~ _se1.warnings;
		if (_summ && _summ.legacy && _se2.filePath != "" && !_se2.selectedDefDir) { mixin(S_TRACE);
			ws ~= _prop.msgs.warningNotDefaultSE;
		}
		ws ~= summSkin.warningSE(_prop.parent, _se2.filePath, _summ.legacy, _prop.var.etc.targetVersion) ~ _se2.warnings;
		if (_card && _card.trees.length) { mixin(S_TRACE);
			if (_scenario.getText() != _summ.scenarioName || _author.getText() != _summ.author) { mixin(S_TRACE);
				ws ~= _prop.msgs.diffSource;
			}
		}
		ws ~= _keyCodes.warnings;

		warning = ws;
	}

	class OASelect : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			refreshEnblOneAll();
		}
	}
	class SelEffectType : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			refreshWarning();
		}
	}

	CTabItem constructMain(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(2, false));
		{ mixin(S_TRACE);
			auto comp2 = new Composite(comp, SWT.NONE);
			comp2.setLayoutData(new GridData(GridData.FILL_BOTH));
			comp2.setLayout(zeroMarginGridLayout(1, false));
			{ mixin(S_TRACE);
				auto grp = new Group(comp2, SWT.NONE);
				grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				grp.setLayout(normalGridLayout(2, false));
				grp.setText(_prop.msgs.name);
				_name = new GBLimitText(_prop.looks.monospace,
					_prop.looks.nameLimit, grp, SWT.BORDER | _readOnly);
				mod(_name.widget);
				createTextMenu!Text(_comm, _prop, _name.widget, &catchMod);
				auto gd = new GridData(GridData.FILL_HORIZONTAL);
				gd.widthHint = _name.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
				_name.widget.setLayoutData(gd);
				auto l = new Label(grp, SWT.NONE);
				l.setText(.tryFormat(_prop.msgs.nameLimit, _prop.looks.nameLimit, _prop.looks.nameLimit / 2));
				.listener(_name.widget, SWT.Modify, &refreshWarning);
			}
			{ mixin(S_TRACE);
				auto skin = summSkin;
				_imgPath = new ImageSelect!(MtType.CARD)(comp2, _readOnly, _comm, _prop, _summ,
					_prop.s(_prop.looks.cardSize.width), _prop.s(_prop.looks.cardSize.height), _prop.s(_prop.looks.cardInsets),
					CardImagePosition.TopLeft, true, { mixin(S_TRACE);
						return .toExportedImageNameWithCardName(_prop.parent, _scenario.getText(), _author.getText(), _name.getText());
					});
				mod(_imgPath);
				_imgPath.modEvent ~= &refreshWarning;
				_imgPath.widget.setLayoutData(new GridData(GridData.FILL_BOTH));
				void refImageScale() { mixin(S_TRACE);
					_imgPath.setPreviewSize(_prop.s(_prop.looks.cardSize.width), _prop.s(_prop.looks.cardSize.height), _prop.s(_prop.looks.cardInsets));
				}
				_comm.refImageScale.add(&refImageScale);
				.listener(_imgPath.widget, SWT.Dispose, { mixin(S_TRACE);
					_comm.refImageScale.remove(&refImageScale);
				});
			}
		}
		{ mixin(S_TRACE);
			auto comp2 = new Composite(comp, SWT.NONE);
			comp2.setLayoutData(new GridData(GridData.FILL_VERTICAL));
			comp2.setLayout(zeroMarginGridLayout(1, false));
			{ mixin(S_TRACE);
				auto grp = new Group(comp2, SWT.NONE);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setLayout(normalGridLayout(1, true));
				grp.setText(_prop.msgs.workConditionGroup);
				Button createCheck(string name, bool enabled) {
					auto check = new Button(grp, SWT.CHECK);
					mod(check);
					check.setEnabled(enabled);
					check.setText(name);
					auto gd = new GridData;
					gd.grabExcessVerticalSpace = true;
					check.setLayoutData(gd);
					return check;
				}
				static if (is(C:BeastCard)) {
					_needSpell = createCheck(_prop.msgs.needSpellAndActive, !_readOnly);
				} else {
					_needSpell = createCheck(_prop.msgs.needSpell, !_readOnly);
				}
			}
			{ mixin(S_TRACE);
				auto grp = new Group(comp2, SWT.NONE);
				grp.setText(_prop.msgs.elementProps);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setLayout(normalGridLayout(2, true));
				foreach (i, eff; [EffectType.Physic, EffectType.Magic,
						EffectType.MagicalPhysic, EffectType.PhysicalMagic,
						EffectType.None]) { mixin(S_TRACE);
					auto radio = new Button(grp, SWT.RADIO);
					mod(radio);
					radio.setEnabled(!_readOnly);
					if (2 <= i) { mixin(S_TRACE);
						auto gd = new GridData(GridData.FILL_BOTH);
						gd.horizontalSpan = 2;
						radio.setLayoutData(gd);
					} else { mixin(S_TRACE);
						radio.setLayoutData(new GridData(GridData.FILL_BOTH));
					}
					radio.setText(.tryFormat(_prop.msgs.effectTypeElement, _prop.msgs.effectTypeName(eff)));
					radio.setToolTipText(.replace(_prop.msgs.effectTypeDesc(eff), "&", "&&"));
					radio.addSelectionListener(new SelEffectType);
					_effTyp[eff] = radio;
				}
			}
			{ mixin(S_TRACE);
				auto grp = new Group(comp2, SWT.NONE);
				grp.setText(_prop.msgs.resistProps);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setLayout(normalGridLayout(2, true));
				foreach (res; [Resist.Avoid, Resist.Resist, Resist.Unfail]) { mixin(S_TRACE);
					auto radio = new Button(grp, SWT.RADIO);
					mod(radio);
					radio.setEnabled(!_readOnly);
					radio.setLayoutData(new GridData(GridData.FILL_BOTH));
					radio.setText(_prop.msgs.resistName(res));
					radio.setToolTipText(.replace(_prop.msgs.resistDesc(res), "&", "&&"));
					_res[res] = radio;
				}
			}
			static if (is(C:BeastCard)) {
				{ mixin(S_TRACE);
					auto grp = new Group(comp2, SWT.NONE);
					grp.setText(_prop.msgs.showStyleForBeastCard);
					grp.setLayoutData(new GridData(GridData.FILL_BOTH));
					grp.setLayout(new CenterLayout);
					_showStyle = new Combo(grp, SWT.READ_ONLY | SWT.DROP_DOWN | SWT.BORDER);
					mod(_showStyle);
					_showStyle.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
					foreach (showStyle; [ShowStyle.Invisible, ShowStyle.Center, ShowStyle.FrontOfUser]) { mixin(S_TRACE);
						_showStyle.add(_prop.msgs.showStyleName(showStyle));
						_showStyles ~= showStyle;
					}
					.listener(_showStyle, SWT.Selection, &refreshWarning);
				}
			}
		}
		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.card);
		tab.setControl(comp);
		return tab;
	}
	CTabItem constructDesc(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto top = new Composite(comp, SWT.NONE);
		top.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		top.setLayout(zeroMarginGridLayout(2, false));
		static if (is(C == SkillCard)) {
			{ mixin(S_TRACE);
				auto grp = new Group(top, SWT.NONE);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setLayout(new CenterLayout);
				grp.setText(_prop.msgs.skillLevel);
				auto comp2 = new Composite(grp, SWT.NONE);
				comp2.setLayout(normalGridLayout(2, false));
				_level = new Spinner(comp2, SWT.BORDER | _readOnly);
				initSpinner(_level);
				mod(_level);
				_level.setMaximum(_prop.var.etc.skillLevelMax);
				_level.setMinimum(0);
				auto l = new Label(comp2, SWT.NONE);
				l.setText(.tryFormat(_prop.msgs.rangeHint, 0, _prop.var.etc.skillLevelMax));
			}
		} else static if (is(C == ItemCard) || is(C == BeastCard)) {
			{ mixin(S_TRACE);
				auto grp = new Group(top, SWT.NONE);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setLayout(new CenterLayout);
				grp.setText(_prop.msgs.useCountGroup);
				auto comp2 = new Composite(grp, SWT.NONE);
				static if (SetUseCountCur && is(C:ItemCard)) {
					comp2.setLayout(zeroMarginGridLayout(3, false));
					auto l1 = new Label(comp2, SWT.NONE);
					l1.setText(_prop.msgs.useCountMax);
					_useCount = new Spinner(comp2, SWT.BORDER | _readOnly);
					initSpinner(_useCount);
					mod(_useCount);
					_useCount.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					_useCount.setMaximum(_prop.var.etc.useCountMax);
					_useCount.setMinimum(0);
					.listener(_useCount, SWT.Selection, &updateUseLimitMax);
					auto l2 = new Label(comp2, SWT.NONE);
					l2.setText(.tryFormat(_prop.msgs.useCountRange, _prop.var.etc.useCountMax));
					auto l3 = new Label(comp2, SWT.NONE);
					l3.setText(_prop.msgs.useCountCur);
					_useCountCur = new Spinner(comp2, SWT.BORDER | _readOnly);
					initSpinner(_useCountCur);
					mod(_useCountCur);
					_useCountCur.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					_useCountCur.setMaximum(_prop.var.etc.useCountMax);
					_useCountCur.setMinimum(0);
					_useCountIsMax = new Button(comp2, SWT.CHECK);
					_useCountIsMax.setEnabled(!_readOnly);
					_useCountIsMax.setText(_prop.msgs.useCountIsMax);
					.listener(_useCountIsMax, SWT.Selection, { mixin(S_TRACE);
						_useCountCur.setEnabled(!_readOnly && !_useCountIsMax.getSelection());
						if (_useCountIsMax.getSelection()) { mixin(S_TRACE);
							_useCountCur.setSelection(_useCount.getSelection());
						}
					});
				} else { mixin(S_TRACE);
					comp2.setLayout(normalGridLayout(2, false));
					_useCount = new Spinner(comp2, SWT.BORDER | _readOnly);
					initSpinner(_useCount);
					mod(_useCount);
					_useCount.setMaximum(_prop.var.etc.useCountMax);
					_useCount.setMinimum(0);
					auto l = new Label(comp2, SWT.NONE);
					l.setText(.tryFormat(_prop.msgs.useCountRange, _prop.var.etc.useCountMax));
				}
			}
		} else { mixin(S_TRACE);
			static assert (0);
		}
		{ mixin(S_TRACE);
			auto grp = new Group(top, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_BOTH));
			grp.setLayout(new CenterLayout);
			grp.setText(_prop.msgs.price);
			auto comp2 = new Composite(grp, SWT.NONE);
			comp2.setLayout(normalGridLayout(2, false));
			static if (is(C == SkillCard) || is(C == BeastCard)) {
				_price = new Spinner(comp2, SWT.BORDER | SWT.READ_ONLY);
				initSpinner(_price);
				_price.setMaximum(_prop.var.etc.priceMax);
				static if (is(C == SkillCard)) {
					new SpinnerEdit(_level, &calcPrice, &calcPrice, &priceCancel);
				} else static if (is(C == BeastCard)) {
					new SpinnerEdit(_useCount, &calcPrice, &calcPrice, &priceCancel);
				} else { mixin(S_TRACE);
					static assert (0);
				}
				auto l = new Label(comp2, SWT.NONE);
				l.setText(_prop.msgs.priceAuto);
			} else static if (is(C == ItemCard)) {
				_price = new Spinner(comp2, SWT.BORDER | _readOnly);
				initSpinner(_price);
				mod(_price);
				_price.setMaximum(_prop.var.etc.priceMax);
				_price.setMinimum(0);
				auto l = new Label(comp2, SWT.NONE);
				l.setText(.tryFormat(_prop.msgs.rangeHint, 0, _prop.var.etc.priceMax));
			} else { mixin(S_TRACE);
				static assert (0);
			}
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp, SWT.NONE);
			auto gd = new GridData(GridData.FILL_BOTH);
			gd.horizontalSpan = 2;
			grp.setLayoutData(gd);
			grp.setLayout(new CenterLayout(SWT.HORIZONTAL));
			grp.setText(_prop.msgs.desc);
			auto descComp = new Composite(grp, SWT.NONE);
			descComp.setLayout(new CenterLayout(SWT.NONE, 0));
			_desc = new FixedWidthText!Text(_prop.adjustFont(_prop.looks.cardDescFont(summSkin.legacy)), _prop.looks.cardDescLen, descComp, SWT.BORDER | _readOnly);
			descComp.setLayoutData(_desc.computeTextBaseSize(.max(_prop.looks.cardDescLine(true), _prop.looks.cardDescLine(false))));
			mod(_desc.widget);
			createTextMenu!Text(_comm, _prop, _desc.widget, &catchMod);
			.listener(_desc.widget, SWT.Modify, &refreshWarning);
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp, SWT.NONE);
			auto gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.horizontalSpan = 2;
			grp.setLayoutData(gd);
			grp.setLayout(normalGridLayout(2, false));
			grp.setText(_prop.msgs.source);
			Text createLine(string title, out TextMenuModify tm) { mixin(S_TRACE);
				auto l = new Label(grp, SWT.NONE);
				l.setText(title);
				auto text = new Text(grp, SWT.BORDER | _readOnly);
				mod(text);
				tm = createTextMenu!Text(_comm, _prop, text, &catchMod);
				text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				text.addModifyListener(new ModSource);
				return text;
			}
			_scenario = createLine(_prop.msgs.sourceScenario, _scenarioTM);
			_author = createLine(_prop.msgs.sourceAuthor, _authorTM);

			auto resetSource = new Button(grp, SWT.PUSH);
			resetSource.setEnabled(!_readOnly);
			auto rgd = new GridData(GridData.HORIZONTAL_ALIGN_END);
			rgd.horizontalSpan = 2;
			resetSource.setLayoutData(rgd);
			resetSource.setText(_prop.msgs.resetSource);
			resetSource.addSelectionListener(new ResetSource);
		}

		auto tab = new CTabItem(tabf, SWT.NONE);
		static if (is(C == SkillCard)) {
			tab.setText(_prop.msgs.levelAndDesc);
		} else static if (is(C == ItemCard) || is(C == BeastCard)) {
			tab.setText(_prop.msgs.useCountAndDesc);
		}
		tab.setControl(comp);
		return tab;
	}
	CTabItem constructApt(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		_ability = new AbilityView(_comm, comp, _readOnly);
		_ability.setLayoutData(new GridData(GridData.FILL_BOTH));
		mod(_ability);
		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.apt);
		tab.setControl(comp);
		return tab;
	}
	Composite createModParent(Composite comp, string title) { mixin(S_TRACE);
		auto grp = new Group(comp, SWT.NONE);
		grp.setText(title);
		grp.setLayoutData(new GridData(GridData.FILL_BOTH));
		auto cl = new CenterLayout;
		cl.fillHorizontal = true;
		cl.fillVertical = true;
		grp.setLayout(cl);
		return grp;
	}
	void createMod(Composite parent, ref size_t[Enhance] tbl, ref RadarSpinner useModR, ref Scales useModS) { mixin(S_TRACE);
		int[] values = [];
		if (useModR) { mixin(S_TRACE);
			values = useModR.getValues();
			useModR.dispose();
			useModR = null;
		}
		if (useModS) { mixin(S_TRACE);
			values = useModS.getValues();
			useModS.dispose();
			useModS = null;
		}

		static const Es = [Enhance.Avoid, Enhance.Resist, Enhance.Defense];
		string[] names;
		names.length = Es.length;
		foreach (i, enh; Es) { mixin(S_TRACE);
			tbl[enh] = i;
			names[i] = .tryFormat(_prop.msgs.enhanceBonus, _prop.msgs.enhanceName(enh));
		}

		int stepC = _prop.var.etc.enhanceMax * 2 + 1;
		int min = cast(int) _prop.var.etc.enhanceMax * -1;
		int page = _prop.var.etc.enhanceMax / 2;
		if (_prop.var.etc.radarStyleParams) { mixin(S_TRACE);
			useModR = new RadarSpinner(parent, _readOnly);
			useModR.setRadar(stepC, names, min);
			useModR.antialias = true;
			useModR.borderlines = [0];
			useModR.lineStep = page;
			if (values.length) useModR.setValues(values);
			mod(useModR);
		} else { mixin(S_TRACE);
			useModS = new Scales(parent, _readOnly);
			useModS.setScales(stepC, names, page, min);
			useModS.borderlines = [0];
			if (values.length) useModS.setValues(values);
			mod(useModS);
		}
		parent.layout();
	}
	void initUseMod() { mixin(S_TRACE);
		createMod(_useModParent, _useModTbl, _useModR, _useModS);
	}
	CTabItem constructUseModify(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(2, false));
		{ mixin(S_TRACE);
			_useModParent = createModParent(comp, _prop.msgs.useModify);
			initUseMod();
		}
		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.useBonus);
		tab.setControl(comp);
		return tab;
	}
	static if (is(C == ItemCard)) {
		void initHasMod() { mixin(S_TRACE);
			createMod(_hasModParent, _hasModTbl, _hasModR, _hasModS);
		}
		CTabItem constructHaveModify(CTabFolder tabf) { mixin(S_TRACE);
			auto comp = new Composite(tabf, SWT.NONE);
			comp.setLayout(normalGridLayout(2, false));
			{ mixin(S_TRACE);
				_hasModParent = createModParent(comp, _prop.msgs.haveModify);
				initHasMod();
			}
			auto tab = new CTabItem(tabf, SWT.NONE);
			tab.setText(_prop.msgs.haveBonus);
			tab.setControl(comp);
			return tab;
		}
	}
	CTabItem constructMotion(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, false));
		{ mixin(S_TRACE);
			_motions = new MotionView(_comm, _prop, _summ, _ucOwner, _useCounter, comp, _readOnly);
			mod(_motions);
			_motions.warningEvent ~= &refreshWarning;
			_motions.setLayoutData(new GridData(GridData.FILL_BOTH));
		}
		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setControl(comp);
		tab.setText(_prop.msgs.motion);
		return tab;
	}
	CTabItem constructProps(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, false));
		{ mixin(S_TRACE);
			auto tcomp = new Composite(comp, SWT.NONE);
			tcomp.setLayoutData(new GridData(GridData.FILL_BOTH));
			tcomp.setLayout(zeroMarginGridLayout(2, false));
			{ mixin(S_TRACE);
				auto comp2 = new Composite(tcomp, SWT.NONE);
				auto gd = new GridData(GridData.FILL_BOTH);
				gd.verticalSpan = 2;
				comp2.setLayoutData(gd);
				comp2.setLayout(zeroMarginGridLayout(1, false));
				{ mixin(S_TRACE);
					auto grp = new Group(comp2, SWT.NONE);
					grp.setText(_prop.msgs.effectTarget);
					grp.setLayoutData(new GridData(GridData.FILL_BOTH));
					auto cl = new CenterLayout(SWT.HORIZONTAL, 0);
					cl.fillVertical = true;
					grp.setLayout(cl);
					auto comp3 = new Composite(grp, SWT.NONE);
					comp3.setLayout(normalGridLayout(2, false));
					foreach (t; [CardTarget.None, CardTarget.User,
							CardTarget.Party, CardTarget.Enemy, CardTarget.Both]) { mixin(S_TRACE);
						auto radio = new Button(comp3, SWT.RADIO);
						mod(radio);
						radio.setEnabled(!_readOnly);
						radio.setLayoutData(new GridData(GridData.FILL_BOTH));
						radio.setText(_prop.msgs.cardTargetName(t));
						radio.addSelectionListener(new OASelect);
						_targ[t] = radio;
					}
				}
				{ mixin(S_TRACE);
					auto grp = new Group(comp2, SWT.NONE);
					grp.setText(_prop.msgs.effectRange);
					grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					auto cl = new CenterLayout;
					cl.fillVertical = true;
					grp.setLayout(cl);
					auto comp3 = new Composite(grp, SWT.NONE);
					comp3.setLayout(normalGridLayout(2, false));
					auto one = new Button(comp3, SWT.RADIO);
					mod(one);
					one.setEnabled(!_readOnly);
					one.setLayoutData(new GridData(GridData.FILL_BOTH));
					one.setText(_prop.msgs.cardTargetOne);
					_one = one;
					auto all = new Button(comp3, SWT.RADIO);
					mod(all);
					all.setEnabled(!_readOnly);
					all.setLayoutData(new GridData(GridData.FILL_BOTH));
					all.setText(_prop.msgs.cardTargetAll);
					_all = all;
					_oneAllGrp = grp;
				}
			}
			{ mixin(S_TRACE);
				auto grp = new Group(tcomp, SWT.NONE);
				grp.setText(_prop.msgs.effectVisual);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setLayout(normalGridLayout(2, false));
				foreach (v; [CardVisual.None, CardVisual.Horizontal,
						CardVisual.Reverse, CardVisual.Vertical]) { mixin(S_TRACE);
					auto radio = new Button(grp, SWT.RADIO);
					mod(radio);
					radio.setEnabled(!_readOnly);
					radio.setLayoutData(new GridData(GridData.FILL_BOTH));
					radio.setText(_prop.msgs.cardVisualName(v));
					_vis[v] = radio;
				}
			}
			{ mixin(S_TRACE);
				auto grp = new Group(tcomp, SWT.NONE);
				grp.setText(_prop.msgs.cardPremium);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setLayout(normalGridLayout(1, false));
				foreach (p; [Premium.Normal, Premium.Rare, Premium.Premium]) { mixin(S_TRACE);
					auto radio = new Button(grp, SWT.RADIO);
					mod(radio);
					radio.setEnabled(!_readOnly);
					radio.setLayoutData(new GridData(GridData.FILL_BOTH));
					radio.setText(_prop.msgs.premiumName(p));
					_prem[p] = radio;
				}
			}
		}
		{ mixin(S_TRACE);
			createSuccessRateScale(_prop, comp, _sucRate)
				.setLayoutData(new GridData(GridData.FILL_BOTH));
			mod(_sucRate);
			_sucRate.setEnabled(!_readOnly);
		}
		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.cardProps);
		tab.setControl(comp);
		return tab;
	}
	static if (is(C:BeastCard)) {
		CTabItem constructBehavior(CTabFolder tabf) { mixin(S_TRACE);
			auto comp = new Composite(tabf, SWT.NONE);
			comp.setLayout(normalGridLayout(1, true));

			auto invokeCond = .createStatusPane(_prop, comp, _prop.msgs.invocationCondition, _invokeCond, &mod!Button, SWT.CHECK);
			invokeCond.setLayoutData(new GridData(GridData.FILL_BOTH));

			{ mixin(S_TRACE);
				auto grp = new Group(comp, SWT.NONE);
				grp.setText(_prop.msgs.removalCondition);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				auto cl = new CenterLayout;
				cl.fillHorizontal = true;
				grp.setLayout(cl);
				auto comp2 = new Composite(grp, SWT.NONE);
				comp2.setLayout(zeroMarginGridLayout(1, true));
				_removeWithUncons = new Button(comp2, SWT.CHECK);
				_removeWithUncons.setText(_prop.msgs.removeWithUnconscious);
				.listener(_removeWithUncons, SWT.Selection, &refDataVersion);
			}

			auto hint = .createStatusHint(_prop, comp);
			hint.setLayoutData(new GridData(GridData.FILL_BOTH));
			foreach (b; _invokeCond.byValue()) { mixin(S_TRACE);
				.listener(b, SWT.Selection, &refDataVersion);
			}

			auto tab = new CTabItem(tabf, SWT.NONE);
			tab.setText(_prop.msgs.behaviorOfBeastCard);
			tab.setControl(comp);
			return tab;
		}
	}
	CTabItem constructKeyCode(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto sash = new SplitPane(comp, SWT.HORIZONTAL);
		sash.setLayoutData(new GridData(GridData.FILL_BOTH));
		{ mixin(S_TRACE);
			Button dummy;
			auto grp = .createEffectSoundPanel(sash, _comm, _summ, _prop.msgs.se, _readOnly != SWT.NONE, false, dummy, _se1, _se2);
			mod(_se1);
			_se1.modEvent ~= &refreshWarning;
			_se1.loadedEvent ~= &refreshWarning;
			mod(_se2);
			_se2.modEvent ~= &refreshWarning;
			_se2.loadedEvent ~= &refreshWarning;
		}
		{ mixin(S_TRACE);
			auto grp = new Group(sash, SWT.NONE);
			grp.setText(_prop.msgs.keyCodes);
			grp.setLayout(normalGridLayout(1, true));

			_keyCodes = new KeyCodeView(_comm, _summ, grp, _readOnly, true, false, &catchMod, () => _motions.motions);
			_keyCodes.setLayoutData(new GridData(GridData.FILL_BOTH));
			mod(_keyCodes);
			_keyCodes.modEvent ~= &refreshWarning;
		}
		.setupWeights(sash, _prop.var.etc.seKeyCodeSashL, _prop.var.etc.seKeyCodeSashR);

		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.seAndKeyCode);
		tab.setControl(comp);
		return tab;
	}
	void delCard(CWXPath owner, C c) { mixin(S_TRACE);
		if (_card is c) { mixin(S_TRACE);
			forceCancel();
		}
	}
	void refScenario(Summary summ) { mixin(S_TRACE);
		forceCancel();
	}
	void refSkin() { mixin(S_TRACE);
		_desc.font = _prop.adjustFont(_prop.looks.cardDescFont(summSkin.legacy));
	}
	void refDataVersion() { mixin(S_TRACE);
		updateEnabled();
		refreshWarning();
		_desc.widget.setLayoutData(_desc.computeTextBaseSize(_prop.looks.cardDescLine(_summ && _summ.legacy)));
		_desc.widget.getParent().layout(true);
	}
	class Dispose : DisposeListener {
		override void widgetDisposed(DisposeEvent e) { mixin(S_TRACE);
			static if (is(C:SkillCard)) {
				_comm.refSkill.remove(&updateTitle);
				_comm.delSkill.remove(&delCard);
			} else static if (is(C:ItemCard)) {
				_comm.refItem.remove(&updateTitle);
				_comm.delItem.remove(&delCard);
			} else static if (is(C:BeastCard)) {
				_comm.refBeast.remove(&updateTitle);
				_comm.delBeast.remove(&delCard);
			} else static assert (0);
			_comm.refSkin.remove(&refSkin);
			_comm.refDataVersion.remove(&refDataVersion);
			_comm.refTargetVersion.remove(&refDataVersion);
			_comm.refScenario.remove(&refScenario);
			_comm.refEventTree.remove(&refEventTree);
			_comm.delEventTree.remove(&refEventTree);
			_comm.refRadarStyle.remove(&initUseMod);
			static if (is(C:ItemCard)) {
				_comm.refRadarStyle.remove(&initHasMod);
			}
		}
	}
public:
	this(Commons comm, Props prop, Shell shell, Summary summ, CWXPath ucOwner, UseCounter uc, C card, bool readOnly) { mixin(S_TRACE);
		assert (summ !is null);
		_comm = comm;
		_summ = summ;
		_ucOwner = .renameInfo(ucOwner);
		_useCounter = uc;
		_card = card;
		_prop = prop;
		_readOnly = readOnly ? SWT.READ_ONLY : SWT.NONE;
		if (_readOnly || !_summ || _summ.scenarioPath == "") { mixin(S_TRACE);
			_summSkin = findSkin(_comm, _prop, _summ);
		}
		static if (is(C == SkillCard)) {
			string text = _card ? .tryFormat(_prop.msgs.dlgTitSkill, _card.name) : _prop.msgs.dlgTitNewSkill;
			auto img = _prop.images.skill;
			auto size = _prop.var.skillCardDlg;
		} else static if (is(C == ItemCard)) {
			string text = _card ? .tryFormat(_prop.msgs.dlgTitItem, _card.name) : _prop.msgs.dlgTitNewItem;
			auto img = _prop.images.item;
			auto size = _prop.var.itemCardDlg;
		} else static if (is(C == BeastCard)) {
			string text = _card ? .tryFormat(_prop.msgs.dlgTitBeast, _card.name) : _prop.msgs.dlgTitNewBeast;
			auto img = _prop.images.beast;
			auto size = _prop.var.beastCardDlg;
		} else { mixin(S_TRACE);
			static assert (0);
		}
		super(prop, shell, _readOnly, false, text, img, true, size, true);
		if (!_card) applyEnabled(true);
	}

	@property
	override
	C card() { mixin(S_TRACE);
		return _card;
	}
	@property
	void card(C v) { mixin(S_TRACE);
		_card = v;
	}

	bool openCWXPath(string path, bool shellActivate) { mixin(S_TRACE);
		return _motions.openCWXPath(path, shellActivate);
	}
protected:
	override void setup(Composite area) { mixin(S_TRACE);
		area.setLayout(windowGridLayout(1));
		auto tabf = new CTabFolder(area, SWT.BORDER);

		constructMain(tabf);
		constructDesc(tabf);
		constructApt(tabf);
		constructUseModify(tabf);
		static if (is(C == ItemCard)) {
			constructHaveModify(tabf);
		}
		constructMotion(tabf);
		constructProps(tabf);
		static if (is(C:BeastCard)) {
			constructBehavior(tabf);
		}
		constructKeyCode(tabf);
		tabf.setSelection(0);

		static if (is(C:SkillCard)) {
			_comm.refSkill.add(&updateTitle);
			_comm.delSkill.add(&delCard);
		} else static if (is(C:ItemCard)) {
			_comm.refItem.add(&updateTitle);
			_comm.delItem.add(&delCard);
		} else static if (is(C:BeastCard)) {
			_comm.refBeast.add(&updateTitle);
			_comm.delBeast.add(&delCard);
		} else static assert (0);
		_comm.refSkin.add(&refSkin);
		_comm.refDataVersion.add(&refDataVersion);
		_comm.refTargetVersion.add(&refDataVersion);
		_comm.refScenario.add(&refScenario);
		_comm.refEventTree.add(&refEventTree);
		_comm.delEventTree.add(&refEventTree);
		_comm.refRadarStyle.add(&initUseMod);
		static if (is(C:ItemCard)) {
			_comm.refRadarStyle.add(&initHasMod);
		}
		area.addDisposeListener(new Dispose);

		// Windows Vistaだとタブの横幅が凄いことになったので必要最低限にする。
		scope maxSize = new Point(0, 0);
		foreach (tab; tabf.getItems()) { mixin(S_TRACE);
			scope size = tab.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT);
			if (maxSize.x < size.x) maxSize.x = size.x;
			if (maxSize.y < size.y) maxSize.y = size.y;
		}
		scope rect = tabf.computeTrim(SWT.DEFAULT, SWT.DEFAULT, maxSize.x, maxSize.y);
		auto gd = new GridData(GridData.FILL_BOTH);
		gd.widthHint = rect.width;
		gd.heightHint = rect.height;
		tabf.setLayoutData(gd);

		refCard(_card);

		refDataVersion();
	}
	private void updateTitle(C card) { mixin(S_TRACE);
		if (_card !is card) return;
		static if (is(C:SkillCard)) {
			string text = .tryFormat(_prop.msgs.dlgTitSkill, .tryFormat(_prop.msgs.nameWithID, _card.id, _card.name));
		} else static if (is(C:ItemCard)) {
			string text = .tryFormat(_prop.msgs.dlgTitItem, .tryFormat(_prop.msgs.nameWithID, _card.id, _card.name));
		} else static if (is(C:BeastCard)) {
			string text = .tryFormat(_prop.msgs.dlgTitBeast, .tryFormat(_prop.msgs.nameWithID, _card.id, _card.name));
		} else { mixin(S_TRACE);
			static assert (0);
		}
		getShell().setText(text);
	}
	private void refCard(C card) { mixin(S_TRACE);
		if (_card && _card !is card) return;
		ignoreMod = true;
		scope (exit) ignoreMod = false;
		auto skin = summSkin;
		if (_card) { mixin(S_TRACE);
			_imgPath.images = _card.paths;
			_desc.setText(_card.desc);
			_scenario.setText(_card.scenario);
			_author.setText(_card.author);
			_name.setText(_card.name);
			_needSpell.setSelection(_card.spell);
			_effTyp[_card.effectType].setSelection(true);
			_res[_card.resist].setSelection(true);
			_ability.physical = _card.physical;
			_ability.mental = _card.mental;
			static if (is(C == SkillCard)) {
				_level.setSelection(_card.level);
			}
			static if (is(C == ItemCard)) {
				static if (SetUseCountCur) {
					_useCount.setSelection(_card.useLimitMax);
					_useCountCur.setSelection(_card.useLimit);
					_useCountIsMax.setSelection(_card.useLimit == _card.useLimitMax);
					_useCountCur.setEnabled(!_readOnly && !_useCountIsMax.getSelection());
					updateUseLimitMax();
				} else {
					_useCount.setSelection(_card.useLimit);
				}
			} else static if (is(C == BeastCard)) {
				auto ssi = cast(int).cCountUntil(_showStyles, _card.showStyle);
				_showStyle.select(ssi == -1 ? 0 : ssi);
				_useCount.setSelection(_card.useLimit);
				bool[Status] ss;
				foreach (s; _card.invocationCondition) { mixin(S_TRACE);
					auto p = s in _invokeCond;
					if (p) { mixin(S_TRACE);
						p.setSelection(true);
						ss[s] = true;
					}
				}
				foreach (s, b; _invokeCond) { mixin(S_TRACE);
					if (s !in ss) b.setSelection(false);
				}
				_removeWithUncons.setSelection(_card.removeWithUnconscious);
			}
			static if (is(C == ItemCard)) {
				_price.setSelection(_card.price);
			}
			_motions.motions = _card.motions;
			foreach (e, index; _useModTbl) { mixin(S_TRACE);
				if (_useModR) { mixin(S_TRACE);
					_useModR.setValue(index, _card.enhance(e));
				} else { mixin(S_TRACE);
					_useModS.setValue(index, _card.enhance(e));
				}
			}
			static if (is(C == ItemCard)) {
				foreach (e, index; _hasModTbl) { mixin(S_TRACE);
					if (_hasModR) { mixin(S_TRACE);
						_hasModR.setValue(index, _card.enhanceOwner(e));
					} else { mixin(S_TRACE);
						_hasModS.setValue(index, _card.enhanceOwner(e));
					}
				}
			}
			_targ[_card.target].setSelection(true);
			_one.setSelection(!_card.allRange);
			_all.setSelection(_card.allRange);
			refreshEnblOneAll();
			_vis[_card.visual].setSelection(true);
			_prem[_card.premium].setSelection(true);
			_sucRate.setSelection(_card.successRate + Content.successRate_max);
			_se1.path = _card.soundPath1;
			_se1.volume = _card.volume1;
			_se1.loopCount = _card.loopCount1;
			_se2.path = _card.soundPath2;
			_se2.volume = _card.volume2;
			_se2.loopCount = _card.loopCount2;
			_keyCodes.keyCodes = _card.keyCodes;
			updateTitle(_card);
			refreshWarning();
		} else { mixin(S_TRACE);
			_imgPath.images = [];
			_scenario.setText(_summ.scenarioName);
			_author.setText(_summ.author);
			_effTyp[EffectType.Physic].setSelection(true);
			_res[Resist.Avoid].setSelection(true);
			static if (is(C == SkillCard)) {
				_level.setSelection(1);
			}
			foreach (e, index; _useModTbl) { mixin(S_TRACE);
				if (_useModR) { mixin(S_TRACE);
					_useModR.setValue(index, 0);
				} else { mixin(S_TRACE);
					_useModS.setValue(index, 0);
				}
			}
			static if (is(C == ItemCard)) {
				foreach (e, index; _hasModTbl) { mixin(S_TRACE);
					if (_hasModR) { mixin(S_TRACE);
						_hasModR.setValue(index, 0);
					} else { mixin(S_TRACE);
						_hasModS.setValue(index, 0);
					}
				}
			}
			static if (is(C:BeastCard)) {
				_showStyle.select(cast(int).cCountUntil(_showStyles, ShowStyle.Center));
				foreach (s, b; _invokeCond) { mixin(S_TRACE);
					b.setSelection(s is Status.Alive);
				}
				_removeWithUncons.setSelection(true);
			}
			_targ[CardTarget.None].setSelection(true);
			_one.setSelection(true);
			refreshEnblOneAll();
			_vis[CardVisual.None].setSelection(true);
			_prem[Premium.Normal].setSelection(true);
			_sucRate.setSelection(Content.successRate_max);
			_se1.path = "";
			_se2.path = "";
		}
		static if (is(C == SkillCard)) {
			calcPrice(_level.getSelection());
		} else static if (is(C == BeastCard)) {
			calcPrice(_useCount.getSelection());
		}
	}
	private void refreshEnblOneAll() { mixin(S_TRACE);
		auto enabled = _targ[CardTarget.Party].getSelection() || _targ[CardTarget.Enemy].getSelection() || _targ[CardTarget.Both].getSelection();
		_oneAllGrp.setEnabled(enabled);
		_one.setEnabled(!_readOnly && enabled);
		_all.setEnabled(!_readOnly && enabled);
	}

	override bool apply() { mixin(S_TRACE);
		auto images = _imgPath.materialPath(forceApplying);
		if (images.cancel) return false;
		if (_card) { mixin(S_TRACE);
			_card.paths = images.images;
			_card.desc = wrapReturnCode(_desc.getText());
			_card.name = _name.getText();
		} else { mixin(S_TRACE);
			_card = new C(_prop.sys, _summ.newId!(C), _name.getText(),
				images.images, wrapReturnCode(_desc.getText()));
		}
		_card.spell = _needSpell.getSelection();
		putRadioValue!(EffectType)(_effTyp, &_card.effectType);
		putRadioValue!(Resist)(_res, &_card.resist);
		_card.physical = _ability.physical;
		_card.mental = _ability.mental;
		static if (is(C == SkillCard)) {
			_card.level = _level.getSelection();
		}
		static if (is(C == ItemCard)) {
			_card.useLimitMax = _useCount.getSelection();
			static if (SetUseCountCur) {
				_card.useLimit = _useCountCur.getSelection();
			} else { mixin(S_TRACE);
				_card.useLimit = _card.useLimitMax;
			}
		} else static if (is(C == BeastCard)) {
			_card.showStyle = _showStyles[_showStyle.getSelectionIndex()];
			_card.useLimit = _useCount.getSelection();
			_card.invocationCondition = invokeCond;
			_card.removeWithUnconscious = _removeWithUncons.getSelection();
		}
		static if (is(C == ItemCard)) {
			_card.price = _price.getSelection();
		}
		_card.motions = _motions.motions;
		foreach (e, index; _useModTbl) { mixin(S_TRACE);
			if (_useModR) { mixin(S_TRACE);
				_card.enhance(e, _useModR.getValue(index));
			} else { mixin(S_TRACE);
				_card.enhance(e, _useModS.getValue(index));
			}
		}
		static if (is(C == ItemCard)) {
			foreach (e, index; _hasModTbl) { mixin(S_TRACE);
				if (_hasModR) { mixin(S_TRACE);
					_card.enhanceOwner(e, _hasModR.getValue(index));
				} else { mixin(S_TRACE);
					_card.enhanceOwner(e, _hasModS.getValue(index));
				}
			}
		}
		putRadioValue!(CardTarget)(_targ, &_card.target);
		_card.allRange = _oneAllGrp.isEnabled() && _all.getSelection();
		putRadioValue!(CardVisual)(_vis, &_card.visual);
		putRadioValue!(Premium)(_prem, &_card.premium);
		_card.successRate = cast(int) _sucRate.getSelection() - Content.successRate_max;
		_card.soundPath1 = _se1.path;
		_card.volume1 = _se1.volume;
		_card.loopCount1 = _se1.loopCount;
		_card.soundPath2 = _se2.path;
		_card.volume2 = _se2.volume;
		_card.loopCount2 = _se2.loopCount;
		_card.scenario = _scenario.getText();
		_card.author = _author.getText();
		_card.keyCodes = _keyCodes.keyCodes;

		_comm.refKeyCodes.call();
		return true;
	}
}

alias MaterialSelect!(MtType.SE, Combo, Combo) SoundSelect;

Composite createEffectSoundPanel(Composite parent, Commons comm, Summary summ, string name, bool readOnly, bool isEffectContent, out Button initialEffect, out SoundSelect se1, out SoundSelect se2) { mixin(S_TRACE);
	auto grp = new Group(parent, SWT.NONE);
	grp.setText(name);
	grp.setLayout(normalGridLayout(1, false));
	SoundSelect createSE(string title, bool hasInitialEffect) { mixin(S_TRACE);
		auto comp = new Composite(grp, SWT.NONE);
		comp.setLayoutData(new GridData(GridData.FILL_BOTH));
		comp.setLayout(zeroMarginGridLayout(3, false));

		if (hasInitialEffect) { mixin(S_TRACE);
			initialEffect = new Button(comp, SWT.CHECK);
			initialEffect.setText(comm.prop.msgs.hasInitialEffect);
			auto gd = new GridData;
			gd.horizontalSpan = 3;
			initialEffect.setLayoutData(gd);
		}

		auto se = new SoundSelect(comm, comm.prop, summ, null, readOnly, null, included => [comm.prop.msgs.defaultSelection(comm.prop.msgs.soundNone)]);

		auto l = new CLabel(comp, SWT.NONE);
		auto gdl = new GridData(GridData.FILL_HORIZONTAL);
		gdl.horizontalSpan = 3;
		l.setLayoutData(gdl);
		l.setImage(comm.prop.images.sound);
		l.setText(title);

		se.createDirsCombo(comp).setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		se.createStopButton(comp);
		se.createPlayButton(comp);

		auto gdfl = new GridData(GridData.FILL_HORIZONTAL);
		gdfl.horizontalSpan = 3;
		se.createFileList(comp).setLayoutData(gdfl);
		auto ogd = new GridData(GridData.FILL_HORIZONTAL);
		ogd.horizontalSpan = 3;
		se.createPlayingOptions(comp, false, true).setLayoutData(ogd);

		if (hasInitialEffect) { mixin(S_TRACE);
			se.enabled = false;
			.listener(initialEffect, SWT.Selection, { mixin(S_TRACE);
				se.enabled = initialEffect.getSelection();
			});
		}
		return se;
	}
	se1 = createSE(comm.prop.msgs.se1, isEffectContent);
	se2 = createSE(comm.prop.msgs.se2, false);
	return grp;
}
