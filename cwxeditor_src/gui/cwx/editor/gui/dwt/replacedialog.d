
module cwx.editor.gui.dwt.replacedialog;

import cwx.area;
import cwx.summary;
import cwx.event;
import cwx.coupon;
import cwx.utils;
import cwx.card;
import cwx.motion;
import cwx.flag;
import cwx.usecounter;
import cwx.types;
import cwx.path;
import cwx.background;
import cwx.skin;
import cwx.msgutils;
import cwx.menu;
import cwx.jpy;
import cwx.cab;
import cwx.features;
import cwx.textholder;
import cwx.system;
import cwx.warning;

import cwx.editor.gui.dwt.dprops;
import cwx.editor.gui.dwt.dutils;
import cwx.editor.gui.dwt.centerlayout;
import cwx.editor.gui.dwt.chooser;
import cwx.editor.gui.dwt.customtable;
import cwx.editor.gui.dwt.commons;
import cwx.editor.gui.dwt.dskin;
import cwx.editor.gui.dwt.splitpane;
import cwx.editor.gui.dwt.undo;
import cwx.editor.gui.dwt.dmenu;
import cwx.editor.gui.dwt.chooser;
import cwx.editor.gui.dwt.incsearch;

import core.thread;
static import core.memory;

import std.algorithm : map, uniq, swap, count, filter;
import std.array;
import std.ascii;
import std.conv;
import std.exception;
import std.file;
import std.path;
import std.range;
import std.regex : Regex, regex, RegexMatch, match;
import std.string;
import std.traits;
import std.typecons : Tuple;
import std.utf;
static import std.algorithm;
static import std.regex;

import org.eclipse.swt.all;

import java.lang.all;

private class CWXPathString {
	string scPath;
	CWXPath parent;
	CWXPath path;
	string array;
	this (string scPath, CWXPath parent, CWXPath path, string array) { mixin(S_TRACE);
		this.scPath = scPath;
		this.parent = parent;
		this.path = path;
		this.array = array;
	}
}
private class FilePathString {
	string scPath;
	string array;
	this (string scPath, string array) { mixin(S_TRACE);
		this.scPath = scPath;
		this.array = array;
	}
}

private class FKeyCodesUndo : TUndo!(FKeyCode[]) {
	this (FKeyCode[] old, FKeyCode[] n, void delegate(FKeyCode[]) set) { mixin(S_TRACE);
		super (old.dup, n.dup, set, (FKeyCode[] v) { return v.dup; });
	}
}

private class CouponsUndo : TUndo!(Coupon[]) {
	this (Coupon[] old, Coupon[] n, void delegate(Coupon[]) set) { mixin(S_TRACE);
		super (old.dup, n.dup, set, (Coupon[] v) { return v.dup; });
	}
}

/// 検索と置換を行うダイアログ。
class ReplaceDialog {
private:
	class UndoRepl : UndoArr {
		private void delegate()[] _refCall;
		this (Undo[] array, bool rev, void delegate()[] refCall) { mixin(S_TRACE);
			super (array, rev);
			_refCall = refCall;
		}
		override void undo() { mixin(S_TRACE);
			super.undo();
			refContentText();
			_status.setText(.tryFormat(_prop.msgs.replaceUndo, .formatNum(_result.getItemCount())));
			_comm.inReplaceText = true;
			foreach_reverse (d; _refCall) d();
			_comm.inReplaceText = false;
			_comm.replText.call();
		}
		override void redo() { mixin(S_TRACE);
			super.redo();
			refContentText();
			_status.setText(.tryFormat(_prop.msgs.replaceRedo, .formatNum(_result.getItemCount())));
			_comm.inReplaceText = true;
			foreach (d; _refCall) d();
			_comm.inReplaceText = false;
			_comm.replText.call();
		}
	}

	class RUndo : Undo {
		private CWXPath _path = null;
		private CWXPath _parent = null;
		private string _cwxPath = "";
		private string _filePath = null;
		private Undo[] _uArr;
		this (CWXPath parent, CWXPath path, string cwxPath, Undo[] uArr) { mixin(S_TRACE);
			_parent = parent;
			_path = path;
			_cwxPath = cwxPath;
			_uArr = uArr;
		}
		this (string filePath, Undo[] uArr) { mixin(S_TRACE);
			_filePath = filePath;
			_uArr = uArr;
		}
		void undo() { mixin(S_TRACE);
			size_t dmy = 0;
			_undoAfter.length = 0;
			foreach_reverse (u; _uArr) u.undo();
			foreach (d; _undoAfter) d();
			_undoAfter.length = 0;
			_result.clearAll();
		}
		void redo() { mixin(S_TRACE);
			size_t dmy = 0;
			_undoAfter.length = 0;
			foreach_reverse (u; _uArr) u.redo();
			foreach (d; _undoAfter) d();
			_undoAfter.length = 0;
			_result.clearAll();
		}
		void dispose() { mixin(S_TRACE);
			foreach (u; _uArr) u.dispose();
		}
	}

	void store(string filePath, Undo[] uArr) { mixin(S_TRACE);
		_rUndo ~= new RUndo(null, null, filePath, uArr);
	}
	void store(CWXPath parent, CWXPath path, string cwxPath, Undo[] uArr) { mixin(S_TRACE);
		_rUndo ~= new RUndo(parent, path, cwxPath, uArr);
	}
	void store(CWXPath parent, CWXPath path, string cwxPath, string o, string n, void delegate(string) set) { mixin(S_TRACE);
		_rUndo ~= new RUndo(parent, path, cwxPath, [new StrUndo(o, n, set)]);
	}
	void store(CWXPath parent, CWXPath path, string cwxPath, string[] o, string[] n, void delegate(string[]) set) { mixin(S_TRACE);
		_rUndo ~= new RUndo(parent, path, cwxPath, [new StrArrUndo(o, n, set)]);
	}
	void store(CWXPath parent, CWXPath path, string cwxPath, FKeyCode[] o, FKeyCode[] n, void delegate(FKeyCode[]) set) { mixin(S_TRACE);
		_rUndo ~= new RUndo(parent, path, cwxPath, [new FKeyCodesUndo(o, n, set)]);
	}
	void store(CWXPath parent, CWXPath path, string cwxPath, Coupon[] o, Coupon[] n, void delegate(Coupon[]) set) { mixin(S_TRACE);
		_rUndo ~= new RUndo(parent, path, cwxPath, [new CouponsUndo(o, n, set)]);
	}
	void storeID(User, Id)(CWXPath parent, CWXPath path, User u, Id from, Id to, void delegate(Id) set) { mixin(S_TRACE);
		_rUndo ~= new RUndo(parent, path, null, [new TUndo!Id(from, to, set)]);
	}

	bool _inProc = false;
	bool _inUndo = false;
	Undo[] _rUndo;
	void delegate()[] _after;
	void delegate()[] _undoAfter;
	void delegate()[] _refCall;
	core.thread.Thread _uiThread;

	Commons _comm;
	Props _prop;
	Summary _summ;
	UndoManager _undo;

	Summary _grepSumm = null;
	int _grepCount = -1;
	Skin _grepSkin = null;
	string _grepFile = "";
	bool _inGrep = false;
	void delegate() _sendReloadProps = null;

	bool _cancel = false;

	Shell _win;
	Composite _parent;
	CTabFolder _tabf;
	CTabItem _tabText;
	CTabItem _tabID;
	CTabItem _tabPath;
	CTabItem _tabContents;
	CTabItem _tabCoupon;
	CTabItem _tabUnuse;
	CTabItem _tabError;
	CTabItem _tabGrep;
	CTabItem _lastFind = null;
	Button _find;
	Button _replace;
	Button _close;
	Button _rangeAllCheck;

	bool ignoreMod = false;

	Composite _textGrp1, _textGrp2, _textFromComp, _grepFromComp;
	Combo _from;
	Combo _to;
	IncSearch _fromIncSearch = null;
	IncSearch _toIncSearch = null;

	Combo _idKind;
	Combo _fromID;
	Composite _fromIDComp;
	Spinner _fromIDVal;
	ulong[int] _fromIDTbl;
	Combo _toID;
	Composite _toIDComp;
	Spinner _toIDVal;
	ulong[int] _toIDTbl;
	IncSearch _fromIDIncSearch = null;
	IncSearch _toIDIncSearch = null;

	Combo _fromPath;
	Combo _toPath;
	IncSearch _fromPathIncSearch = null;
	IncSearch _toPathIncSearch = null;

	Combo _grepDir;
	Button _grepSubDir;

	Button _notIgnoreCase;
	Button _useRegex;
	Button _useWildcard;
	Button _exact;
	Button _ignoreReturnCode;
	class SelRegex : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			if (_useRegex.getSelection()) { mixin(S_TRACE);
				_useWildcard.setSelection(false);
			}
		}
	}
	class SelWildcard : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			if (_useWildcard.getSelection()) { mixin(S_TRACE);
				_useRegex.setSelection(false);
			}
		}
	}
	class SelIgnoreReturnCode : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			_comm.refreshToolBar();
		}
	}
	@property
	const
	bool catchMod() { return !ignoreMod; }

	/// 貼紙
	Button _summary;
	/// シナリオ名
	Button _scenario;
	/// 作者名
	Button _author;
	/// シナリオ出現条件
	Button _rCoupon;
	/// メッセージ
	Button _msg;
	/// カード名
	Button _cardName;
	/// カード解説
	Button _cardDesc;
	/// イベントテキスト
	Button _event;
	/// スタート名
	Button _start;
	/// 状態変数名
	Button _varName;
	/// 状態変数値
	Button _varValue;
	/// 式
	Button _expression;
	/// クーポン
	Button _coupon;
	/// ゴシップ
	Button _gossip;
	/// 終了印
	Button _end;
	/// エリア/バトル/パッケージ名
	Button _area;
	/// キーコード
	Button _keyCode;
	/// セル名称
	Button _cellName;
	/// カードグループ
	Button _cardGroup;
	/// ファイル
	Button _file;
	/// コメント
	Button _comment;
	/// JPTXファイル
	Button _jptx;
	/// テキストファイル
	Button _textFile;

	Button[] _noSummText;

	alias Tuple!(string, "name", bool, "isClassic", string, "wsnVer", string, "targVer") VerInfo;
	Combo _targetVer;
	VerInfo[] _verInfos;

	Button _unuseFlag;
	Button _unuseStep;
	Button _unuseVariant;
	Button _unuseArea;
	Button _unuseBattle;
	Button _unusePackage;
	Button _unuseCast;
	Button _unuseSkill;
	Button _unuseItem;
	Button _unuseBeast;
	Button _unuseInfo;
	Button _unuseStart;
	Button _unusePath;

	Button _cCoupon;
	Button _cGossip;
	Button _cEnd;
	Button _cKeyCode;
	Button _cCellName;
	Button _cCardGroup;

	Table _result;
	TableTextEdit _edit;
	Tree _range;

	AddResult[] _results;

	bool[CWXPath] _rangeTable;
	IncSearch _incSearch = null;
	private void incSearch() { mixin(S_TRACE);
		if (!_range.isEnabled()) return;
		.forceFocus(_range, true);
		_incSearch.startIncSearch();
	}

	Composite[CTabItem] _comps;

	ToolItem[CType] _contents;

	Label _status;

	bool _notIgnoreCaseSel;
	bool _exactSel;
	bool _ignoreReturnCodeSel;
	bool _summarySel;
	bool _scenarioSel;
	bool _authorSel;
	bool _msgSel;
	bool _cardNameSel;
	bool _cardDescSel;
	bool _eventSel;
	bool _startSel;
	bool _varNameSel;
	bool _varValueSel;
	bool _expressionSel;
	bool _couponSel;
	bool _gossipSel;
	bool _endSel;
	bool _areaSel;
	bool _keyCodeSel;
	bool _cellNameSel;
	bool _cardGroupSel;
	bool _fileSel;
	bool _commentSel;
	bool _jptxSel;
	bool _textFileSel;
	string _fromText;
	string _toText;

	bool _flagDirOnRange = false;

	bool _resultRedraw = true;
	void resultRedraw(bool val) { mixin(S_TRACE);
		if (!_win || _win.isDisposed()) return;
		if (_resultRedraw !is val) { mixin(S_TRACE);
			_resultRedraw = val;
			_result.setRedraw(val);
		}
	}

	static interface AddResult : Runnable {
		void setData(TableItem itm);
	}

	class AddResultPath : AddResult {
		size_t count = 0;
		string path;
		string desc;
		Summary grepSumm;
		Skin grepSkin;
		void run() { mixin(S_TRACE);
			if (cancel) return;
			if (!_win || _win.isDisposed()) return;
			refResultStatus(cast(int)count, false);
			_results ~= this;
			if (_prop.var.etc.searchResultRealtime) _result.setItemCount(cast(int)_results.length);
		}
		override void setData(TableItem itm) { mixin(S_TRACE);
			auto summ = grepSumm ? grepSumm : _summ;
			auto fullPath = std.path.buildPath(summ.scenarioPath, path);
			if (grepSumm) { mixin(S_TRACE);
				itm.setImage(fimage(fullPath, grepSkin));
			} else { mixin(S_TRACE);
				itm.setImage(fimage(fullPath, _comm.skin));
			}
			string scPath = null;
			string text = encodePath(path);
			itm.setText(0, text);
			if (desc.length) { mixin(S_TRACE);
				itm.setText(2, desc.replace("\n", ""));
				itm.setImage(2, _prop.images.warning);
			}
			if (grepSumm) { mixin(S_TRACE);
				scPath = grepSumm.readOnlyPath != "" ? grepSumm.readOnlyPath : grepSumm.useTemp ? grepSumm.origZipName : grepSumm.scenarioPath;
				text = .tryFormat(_prop.msgs.grepScenario, grepSumm.scenarioName, scPath);
				itm.setText(2, text.replace("\n", ""));
				itm.setImage(2, _prop.images.summary);
			}
			itm.setData(new FilePathString(scPath, path));
		}
	}
	class AddResultCWXPath : AddResult {
		CWXPath parent;
		CWXPath path;
		string cwxPath;
		string desc;
		size_t count = 0;
		Summary grepSumm;
		void run() { mixin(S_TRACE);
			if (cancel) return;
			if (!_win || _win.isDisposed()) return;
			refResultStatus(cast(int)count, false);
			_results ~= this;
			if (_prop.var.etc.searchResultRealtime) _result.setItemCount(cast(int)_results.length);
		}
		override void setData(TableItem itm) { mixin(S_TRACE);
			addResultImpl(itm, grepSumm, parent, path, cwxPath, desc);
		}
	}
	private void addResultImpl(TableItem itm, Summary grepSumm, CWXPath parent, CWXPath path, string cwxPath, string desc = "") {
		string text1, text2;
		Image img1, img2;
		auto includePath = !cast(Content)path && cast(Content)path.cwxParent;
		getPathParams(grepSumm, parent, path, text1, text2, img1, img2, includePath ? path.cwxParent : null);
		itm.setImage(0, img1);
		itm.setText(0, text1);
		itm.setImage(1, img2);
		itm.setText(1, text2);
		if (desc.length) { mixin(S_TRACE);
			itm.setText(2, desc.replace("\n", ""));
			itm.setImage(2, _prop.images.warning);
		}
		string scPath = null;
		if (grepSumm) { mixin(S_TRACE);
			scPath = grepSumm.readOnlyPath != "" ? grepSumm.readOnlyPath : grepSumm.useTemp ? grepSumm.origZipName : grepSumm.scenarioPath;
			itm.setText(2, .tryFormat(_prop.msgs.grepScenario, grepSumm.scenarioName, scPath));
			itm.setImage(2, _prop.images.summary);
		}
		itm.setData(new CWXPathString(scPath, parent, grepSumm ? null : path, cwxPath));
	}
	class AddResultMsg : AddResult {
		string name;
		Image delegate() image;
		size_t count = 0;
		void run() { mixin(S_TRACE);
			if (cancel) return;
			if (!_win || _win.isDisposed()) return;
			refResultStatus(cast(int)count, false);
			_results ~= this;
			if (_prop.var.etc.searchResultRealtime) _result.setItemCount(cast(int)_results.length);
		}
		override void setData(TableItem itm) { mixin(S_TRACE);
			itm.setText(name);
			itm.setImage(image());
		}
	}
	class AddResultUse : AddResult {
		string name;
		uint use;
		Image delegate() image;
		size_t count = 0;
		void run() { mixin(S_TRACE);
			if (cancel) return;
			if (!_win || _win.isDisposed()) return;
			refResultStatus(cast(int)count, false);
			_results ~= this;
			if (_prop.var.etc.searchResultRealtime) _result.setItemCount(cast(int)_results.length);
		}
		override void setData(TableItem itm) { mixin(S_TRACE);
			itm.setText(0, name);
			itm.setImage(0, image());
			itm.setText(1, .text(use));
		}
	}
	class AddResultComment : AddResult {
		CWXPath parent;
		CWXPath path;
		string cwxPath;
		string comment;
		size_t count = 0;
		Summary grepSumm;
		void run() { mixin(S_TRACE);
			if (cancel) return;
			if (!_win || _win.isDisposed()) return;
			refResultStatus(cast(int)count, false);
			_results ~= this;
			if (_prop.var.etc.searchResultRealtime) _result.setItemCount(cast(int)_results.length);
		}
		override void setData(TableItem itm) { mixin(S_TRACE);
			string text1, text2;
			Image img1, img2;
			getPathParams(grepSumm, parent, path, text1, text2, img1, img2, path);
			itm.setImage(0, _prop.images.menu(MenuID.Comment));
			itm.setText(0, .tryFormat(_prop.msgs.commentText, comment.replace("\n", "")));
			itm.setImage(1, img2);
			itm.setText(1, text2);
			string scPath = null;
			if (grepSumm) { mixin(S_TRACE);
				scPath = grepSumm.readOnlyPath != "" ? grepSumm.readOnlyPath : grepSumm.useTemp ? grepSumm.origZipName : grepSumm.scenarioPath;
				itm.setText(2, .tryFormat(_prop.msgs.grepScenario, grepSumm.scenarioName, scPath));
				itm.setImage(2, _prop.images.summary);
			}
			itm.setData(new CWXPathString(scPath, parent, grepSumm ? null : path, cwxPath));
		}
	}
	Display _display;

	class ML : MouseAdapter {
		public override void mouseDoubleClick(MouseEvent e) { mixin(S_TRACE);
			if (_result.isFocusControl() && e.button == 1) { mixin(S_TRACE);
				openPath((e.stateMask & SWT.SHIFT) != 0);
			}
		}
	}
	class KL : KeyAdapter {
		public override void keyPressed(KeyEvent e) { mixin(S_TRACE);
			if (_result.isFocusControl() && .isEnterKey(e.keyCode)) { mixin(S_TRACE);
				openPath((e.stateMask & SWT.SHIFT) != 0);
			}
		}
	}
	private bool _hasID = false;
	private static const ID_AREA = 0;
	private static const ID_BATTLE = 1;
	private static const ID_PACKAGE = 2;
	private static const ID_CAST = 3;
	private static const ID_SKILL = 4;
	private static const ID_ITEM = 5;
	private static const ID_BEAST = 6;
	private static const ID_INFO = 7;
	private static const ID_FLAG = 8;
	private static const ID_STEP = 9;
	private static const ID_VARIANT = 10;
	private static const ID_COUPON = 11;
	private static const ID_GOSSIP = 12;
	private static const ID_COMPLETE_STAMP = 13;
	private static const ID_KEY_CODE = 14;
	private static const ID_CELL_NAME = 15;
	private static const ID_CARD_GROUP = 16;
	private void setupIDsImpl2(T)(T[] arr, Combo combo, Spinner spn, ref ulong[int] tbl, bool clear, IncSearch incSearch) { mixin(S_TRACE);
		ulong[int] tbl2;
		string oldSel = clear ? "" : combo.getText();
		combo.removeAll();
		static if (is(T:cwx.flag.Flag) || is(T:Step) || is(T:cwx.flag.Variant)) {
			auto set = new HashSet!string;
			if (_summ) { mixin(S_TRACE);
				static if (is(T:cwx.flag.Flag)) {
					foreach (key; _summ.useCounter.keys!FlagId) { mixin(S_TRACE);
						if (.icmp(_prop.sys.randomValue, cast(string)key) == 0) continue;
						_hasID = true;
						if (!incSearch.match(cast(string)key)) continue;
						set.add(cast(string)key);
					}
				} else static if (is(T:Step)) {
					foreach (key; _summ.useCounter.keys!StepId) { mixin(S_TRACE);
						if (.icmp(_prop.sys.randomValue, cast(string)key) == 0) continue;
						if (.icmp(_prop.sys.selectedPlayerCardNumber, cast(string)key) == 0) continue;
						_hasID = true;
						if (!incSearch.match(cast(string)key)) continue;
						set.add(cast(string)key);
					}
				} else static if (is(T:cwx.flag.Variant)) {
					foreach (key; _summ.useCounter.keys!VariantId) { mixin(S_TRACE);
						_hasID = true;
						if (!incSearch.match(cast(string)key)) continue;
						set.add(cast(string)key);
					}
				} else static assert (0);
			}
			foreach (i, a; arr) { mixin(S_TRACE);
				auto p = a.path;
				if (p == "") continue;
				_hasID = true;
				if (!incSearch.match(p)) continue;
				combo.add(p);
				set.remove(p);
			}
			bool delegate(string a, string b) cmps;
			if (_prop.var.etc.logicalSort) { mixin(S_TRACE);
				cmps = (a, b) => incmp(a, b) < 0;
			} else { mixin(S_TRACE);
				cmps = (a, b) => icmp(a, b) < 0;
			}
			foreach (p; .sortDlg(set.array(), cmps)) { mixin(S_TRACE);
				if (p == "") continue;
				_hasID = true;
				if (!incSearch.match(p)) continue;
				combo.add(p);
			}
			combo.setText(oldSel);
			if (combo.getText() == "" && combo.getItemCount()) { mixin(S_TRACE);
				combo.select(0);
			}
			spn.setEnabled(false);
		} else static if (is(T:CouponId) || is(T:GossipId) || is(T:CompleteStampId) || is(T:KeyCodeId) || is(T:CellNameId) || is(T:CardGroupId)) {
			string[] arr2;
			static if (is(T:CouponId)) {
				arr2 = .allCoupons(_comm, _summ, CouponComboType.AllCoupons);
			} else static if (is(T:GossipId)) {
				arr2 = .allGossips(_comm, _summ);
			} else static if (is(T:CompleteStampId)) {
				arr2 = .allCompleteStamps(_comm, _summ);
			} else static if (is(T:KeyCodeId)) {
				arr2 = .allKeyCodes(_comm, _summ);
			} else static if (is(T:CellNameId)) {
				arr2 = .allCellNames(_comm, _summ);
			} else static if (is(T:CardGroupId)) {
				arr2 = .allCardGroups(_comm, _summ);
			} else static assert (0);
			foreach (a; arr2) {
				_hasID = true;
				if (!incSearch.match(a)) continue;
				combo.add(a);
			}
			combo.setText(oldSel);
			if (combo.getText() == "" && combo.getItemCount()) { mixin(S_TRACE);
				combo.select(0);
			}
			spn.setEnabled(false);
		} else {
			combo.add(_prop.msgs.defaultSelection(_prop.msgs.replSetID));
			foreach (i, a; arr) { mixin(S_TRACE);
				_hasID = true;
				if (!incSearch.match(a.name)) continue;
				combo.add(to!(string)(a.id) ~ "." ~ a.name);
				tbl2[cast(int)i + 1] = a.id;
			}
			combo.select(1 < combo.getItemCount() ? 1 : 0);
			if (oldSel) { mixin(S_TRACE);
				auto i = combo.indexOf(oldSel);
				if (i >= 0) combo.select(i);
			}
			spn.setEnabled(combo.getSelectionIndex() == 0);
		}
		tbl = tbl2;
	}
	private void setupIDsImpl1(T)(T[] arr, bool clear, bool from, bool to) { mixin(S_TRACE);
		if (from) setupIDsImpl2(arr, _fromID, _fromIDVal, _fromIDTbl, clear, _fromIDIncSearch);
		if (to) setupIDsImpl2(arr, _toID, _toIDVal, _toIDTbl, clear, _toIDIncSearch);
	}
	private void setupIDs(bool clear, bool from, bool to) { mixin(S_TRACE);
		_hasID = false;
		if (!_summ) { mixin(S_TRACE);
			_fromID.removeAll();
			_fromID.setEnabled(false);
			_fromIDVal.setEnabled(false);
			_toID.removeAll();
			_toID.setEnabled(false);
			_toIDVal.setEnabled(false);
			return;
		}
		if (from) _fromID.setEnabled(true);
		if (to) _toID.setEnabled(true);
		switch (_idKind.getSelectionIndex()) {
		case ID_AREA: setupIDsImpl1(_summ.areas, clear, from, to); break;
		case ID_BATTLE: setupIDsImpl1(_summ.battles, clear, from, to); break;
		case ID_PACKAGE: setupIDsImpl1(_summ.packages, clear, from, to); break;
		case ID_CAST: setupIDsImpl1(_summ.casts, clear, from, to); break;
		case ID_SKILL: setupIDsImpl1(_summ.skills, clear, from, to); break;
		case ID_ITEM: setupIDsImpl1(_summ.items, clear, from, to); break;
		case ID_BEAST: setupIDsImpl1(_summ.beasts, clear, from, to); break;
		case ID_INFO: setupIDsImpl1(_summ.infos, clear, from, to); break;
		case ID_FLAG:
			cwx.flag.Flag[] fs;
			.sortedWithPath(_summ.flagDirRoot.allFlags, _prop.var.etc.logicalSort, (cwx.flag.Flag f) { fs ~= f; });
			setupIDsImpl1(fs, clear, from, to);
			break;
		case ID_STEP:
			Step[] fs;
			.sortedWithPath(_summ.flagDirRoot.allSteps, _prop.var.etc.logicalSort, (Step f) { fs ~= f; });
			setupIDsImpl1(fs, clear, from, to);
			break;
		case ID_VARIANT:
			cwx.flag.Variant[] fs;
			.sortedWithPath(_summ.flagDirRoot.allVariants, _prop.var.etc.logicalSort, (cwx.flag.Variant f) { fs ~= f; });
			setupIDsImpl1(fs, clear, from, to);
			break;
		case ID_COUPON: setupIDsImpl1(_summ.useCounter.keys!CouponId, clear, from, to); break;
		case ID_GOSSIP: setupIDsImpl1(_summ.useCounter.keys!GossipId, clear, from, to); break;
		case ID_COMPLETE_STAMP: setupIDsImpl1(_summ.useCounter.keys!CompleteStampId, clear, from, to); break;
		case ID_KEY_CODE: setupIDsImpl1(_summ.useCounter.keys!KeyCodeId, clear, from, to); break;
		case ID_CELL_NAME: setupIDsImpl1(_summ.useCounter.keys!CellNameId, clear, from, to); break;
		case ID_CARD_GROUP: setupIDsImpl1(_summ.useCounter.keys!CardGroupId, clear, from, to); break;
		default: assert (0);
		}
	}
	private void fromIDIncSearch() { mixin(S_TRACE);
		.forceFocus(_fromID, true);
		_fromIDIncSearch.startIncSearch();
	}
	private void toIDIncSearch() { mixin(S_TRACE);
		.forceFocus(_toID, true);
		_toIDIncSearch.startIncSearch();
	}

	private string[] _lastMaterialPaths;
	private string[] allMaterials(bool scenarioOnly) { mixin(S_TRACE);
		if (!_summ) return [];
		return _summ.allMaterials(_comm.skin, _prop.var.etc.ignorePaths, _prop.var.etc.logicalSort, scenarioOnly);
	}
	private void setupPaths() { mixin(S_TRACE);
		bool oldIgnoreMod = ignoreMod;
		ignoreMod = true;
		scope (exit) ignoreMod = oldIgnoreMod;

		_lastMaterialPaths = [""] ~ allMaterials(false);
		setupPathsImpl(true, true);
	}
	private bool _hasPath = false;
	private void setupPathsImpl(bool from, bool to) { mixin(S_TRACE);
		_hasPath = false;
		void setPaths(Combo combo, IncSearch incSearch) { mixin(S_TRACE);
			auto old = combo.getText();
			combo.removeAll();
			foreach (path; _lastMaterialPaths) { mixin(S_TRACE);
				_hasPath = true;
				if (!incSearch.match(path)) continue;
				combo.add(path);
			}
			combo.setText(old);
		}
		if (from) setPaths(_fromPath, _fromPathIncSearch);
		if (to) setPaths(_toPath, _toPathIncSearch);
	}
	class SListener : ShellAdapter {
		override void shellActivated(ShellEvent e) { mixin(S_TRACE);
			setupIDs(false, true, true);
			setupPaths();
			_comm.refreshToolBar();
		}
	}
	class SelID : SelectionAdapter {
		private Spinner _spn;
		this (Spinner spn) { _spn = spn; }
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			auto combo = cast(Combo) e.widget;
			_spn.setEnabled(combo.getSelectionIndex() == 0);
			_comm.refreshToolBar();
		}
	}
	class SelIDKind : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			selIDKind();
		}
	}
	void selIDKind() {
		updateIDCombo();
		setupIDs(true, true, true);
		_prop.var.etc.searchIDKind = _idKind.getSelectionIndex();
		_comm.refreshToolBar();
	}
	@property
	private bool idKindIsString() { mixin(S_TRACE);
		auto index = _idKind.getSelectionIndex();
		return index == ID_FLAG || index == ID_STEP || index == ID_VARIANT || index == ID_COUPON || index == ID_GOSSIP || index == ID_COMPLETE_STAMP || index == ID_KEY_CODE || index == ID_CELL_NAME || index == ID_CARD_GROUP;
	}
	private void updateIDCombo() { mixin(S_TRACE);
		if (idKindIsString) { mixin(S_TRACE);
			if (_fromID) { mixin(S_TRACE);
				_fromID.dispose();
				_toID.dispose();
			}

			_fromID = new Combo(_fromIDComp, SWT.BORDER | SWT.DROP_DOWN);
			_fromID.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
			_toID = new Combo(_toIDComp, SWT.BORDER | SWT.DROP_DOWN);
			_toID.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
		} else { mixin(S_TRACE);
			if (_fromID) { mixin(S_TRACE);
				_fromID.dispose();
				_toID.dispose();
			}
			_fromID = new Combo(_fromIDComp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
			_fromID.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
			_fromID.addSelectionListener(new SelID(_fromIDVal));
			_toID = new Combo(_toIDComp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
			_toID.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
			_toID.addSelectionListener(new SelID(_toIDVal));
		}

		void setupMenu(Combo combo, void delegate() incSearch) { mixin(S_TRACE);
			auto menu = new Menu(_win, SWT.POP_UP);
			combo.setMenu(menu);
			createMenuItem(_comm, menu, MenuID.IncSearch, incSearch, () => _hasID);
			new MenuItem(menu, SWT.SEPARATOR);
			createTextMenu!Combo(_comm, _prop, combo, &catchMod);

			auto index = _idKind.getSelectionIndex();
			if (index == ID_COUPON) { mixin(S_TRACE);
				new MenuItem(menu, SWT.SEPARATOR);
				.createCouponTypeMenu(_comm, combo, true, true);
			} else if (index == ID_KEY_CODE) { mixin(S_TRACE);
				new MenuItem(menu, SWT.SEPARATOR);
				.createKeyCodeTimingMenu(_comm, combo, false, true);
			}
		}

		setupMenu(_fromID, &fromIDIncSearch);
		setupMenu(_toID, &toIDIncSearch);

		_fromIDIncSearch = new IncSearch(_comm, _fromID, () => _hasID);
		_fromIDIncSearch.modEvent ~= () => setupIDs(false, true, false);
		_toIDIncSearch = new IncSearch(_comm, _toID, () => _hasID);
		_toIDIncSearch.modEvent ~= () => setupIDs(false, false, true);

		_fromIDComp.layout();
		_toIDComp.layout();
	}
	private void tabChanged() { mixin(S_TRACE);
		auto sel = _tabf.getSelection();
		if (!sel) return;

		_parent.setRedraw(false);
		scope (exit) _parent.setRedraw(true);

		_prop.var.etc.searchPlan = _tabf.getSelectionIndex();
		if (sel is _tabText || sel is _tabGrep) { mixin(S_TRACE);
			auto comp = _comps[sel is _tabGrep ? _tabGrep : _tabText];
			if (_textGrp1.getParent() !is comp) _textGrp1.setParent(comp);
			if (_textGrp2.getParent() !is comp) _textGrp2.setParent(comp);
			auto fromComp = sel is _tabGrep ? _grepFromComp : _textFromComp;
			if (_from.getParent() !is fromComp) { mixin(S_TRACE);
				_from.setParent(fromComp);
			}
		}
		foreach (tab, comp; _comps) { mixin(S_TRACE);
			auto gd = cast(GridData) comp.getLayoutData();
			if (tab is sel) { mixin(S_TRACE);
				gd.heightHint= SWT.DEFAULT;
			} else { mixin(S_TRACE);
				gd.heightHint= 0;
			}
		}
		_parent.layout(true);
		auto enabled = !(sel is _tabText && _ignoreReturnCode.getSelection()) && sel !is _tabContents && sel !is _tabCoupon && sel !is _tabUnuse && sel !is _tabError;
		_replace.setEnabled(enabled);
		_range.setEnabled(sel !is _tabUnuse && sel !is _tabGrep);
		_rangeAllCheck.setEnabled(enabled);
		if (!_range.getEnabled()) _incSearch.close();
		_comm.refreshToolBar();
	}
	class TSListener : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			tabChanged();
		}
	}
	LCheck[] _checked;
	class LCheck : SelectionAdapter {
		Widget[] buttons;
		private Button _all = null;
		private void setSelection(Widget b, bool s) { mixin(S_TRACE);
			auto button = cast(Button) b;
			if (button) button.setSelection(s);
			auto ti = cast(ToolItem) b;
			if (ti) ti.setSelection(s);
		}
		private bool getSelection(Widget b) { mixin(S_TRACE);
			auto button = cast(Button) b;
			if (button) return button.getSelection();
			auto ti = cast(ToolItem) b;
			if (ti) return ti.getSelection();
			assert (0);
		}
		class AllCheck : SelectionAdapter {
			override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
				foreach (b; buttons) { mixin(S_TRACE);
					setSelection(b, _all.getSelection());
				}
			}
		}
		void check() { mixin(S_TRACE);
			bool checked = true;
			foreach (b; buttons) { mixin(S_TRACE);
				checked &= getSelection(b);
			}
			_all.setSelection(checked);
		}
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			assert (_all);
			check();
		}
		void createAlls(Composite parent, string text) { mixin(S_TRACE);
			_all = new Button(parent, SWT.CHECK);
			_all.setText(text);
			_all.addSelectionListener(new AllCheck);
			check();
		}
	}
	Composite addButtonLine(Composite grp) { mixin(S_TRACE);
		auto comp = new Composite(grp, SWT.NONE);
		comp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		auto rl = new RowLayout(SWT.HORIZONTAL);
		rl.wrap = true;
		rl.pack = false;
		rl.marginLeft = rl.marginLeft.ppis;
		rl.marginRight = rl.marginRight.ppis;
		rl.marginTop = rl.marginTop.ppis;
		rl.marginBottom = rl.marginBottom.ppis;
		rl.spacing = rl.spacing.ppis;
		comp.setLayout(rl);
		return comp;
	}
	void constructText(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto comp2 = new Composite(comp, SWT.NONE);
		auto comp2gl = windowGridLayout(1, true);
		comp2gl.marginWidth = 0;
		comp2gl.marginHeight = 0;
		comp2.setLayout(comp2gl);
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.replText);
			grp.setLayout(normalGridLayout(2, false));
			auto fl = new Label(grp, SWT.NONE);
			fl.setText(_prop.msgs.replFrom);
			_textFromComp = new Composite(grp, SWT.NONE);
			_textFromComp.setLayout(new FillLayout);
			_from = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN);
			_from.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
			auto gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.widthHint = _prop.var.etc.nameWidth;
			_textFromComp.setLayoutData(gd);
			auto lt = new Label(grp, SWT.NONE);
			lt.setText(_prop.msgs.replTo);
			_to = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN);
			_to.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
			_to.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			auto fMenu = new Menu(_win, SWT.POP_UP);
			_from.setMenu(fMenu);
			createMenuItem(_comm, fMenu, MenuID.IncSearch, { mixin(S_TRACE);
				.forceFocus(_from, true);
				_fromIncSearch.startIncSearch();
			}, () => 0 < _prop.var.etc.searchHistories.length);
			new MenuItem(fMenu, SWT.SEPARATOR);
			createTextMenu!Combo(_comm, _prop, _from, &catchMod);

			auto tMenu = new Menu(_win, SWT.POP_UP);
			_to.setMenu(tMenu);
			createMenuItem(_comm, tMenu, MenuID.IncSearch, { mixin(S_TRACE);
				.forceFocus(_to, true);
				_toIncSearch.startIncSearch();
			}, () => 0 < _prop.var.etc.replaceHistories.length);
			new MenuItem(tMenu, SWT.SEPARATOR);
			createTextMenu!Combo(_comm, _prop, _to, &catchMod);

			_fromIncSearch = new IncSearch(_comm, _from, () => 0 < _prop.var.etc.searchHistories.length);
			_fromIncSearch.modEvent ~= &updateFromHistory;
			_toIncSearch = new IncSearch(_comm, _to, () => 0 < _prop.var.etc.replaceHistories.length);
			_toIncSearch.modEvent ~= &updateToHistory;
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			_textGrp1 = grp;
			auto gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.widthHint = _prop.var.etc.searchResultTableWidth;
			grp.setLayoutData(gd);
			grp.setText(_prop.msgs.replCond);
			grp.setLayout(normalGridLayout(3, false));
			_notIgnoreCase = new Button(grp, SWT.CHECK);
			_notIgnoreCase.setText(_prop.msgs.replNotIgnoreCase);
			_exact = new Button(grp, SWT.CHECK);
			_exact.setText(_prop.msgs.replExactMatch);
			_ignoreReturnCode = new Button(grp, SWT.CHECK);
			_ignoreReturnCode.setText(_prop.msgs.replIgnoreReturnCode);
			_ignoreReturnCode.addSelectionListener(new SelIgnoreReturnCode);
			_useWildcard = new Button(grp, SWT.CHECK);
			_useWildcard.setText(_prop.msgs.replWildcard);
			_useWildcard.addSelectionListener(new SelWildcard);
			auto gdw = new GridData;
			gdw.horizontalSpan = 3;
			_useWildcard.setLayoutData(gdw);
			_useRegex = new Button(grp, SWT.CHECK);
			_useRegex.setText(_prop.msgs.replRegExp);
			_useRegex.addSelectionListener(new SelRegex);
			auto gdr = new GridData;
			gdr.horizontalSpan = 3;
			_useRegex.setLayoutData(gdr);
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			_textGrp2 = grp;
			auto gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.widthHint = _prop.var.etc.searchResultTableWidth;
			grp.setLayoutData(gd);
			grp.setText(_prop.msgs.replTextTarget);
			grp.setLayout(zeroGridLayout(1, true));
			auto checked = new LCheck;
			_checked ~= checked;
			{ mixin(S_TRACE);
				auto btns = addButtonLine(grp);
				Button createB(string text, char accr, bool summ = false) { mixin(S_TRACE);
					auto b = new Button(btns, SWT.CHECK);
					b.setText(text ~ "(&" ~ accr ~ ")");
					checked.buttons ~= b;
					b.addSelectionListener(checked);
					if (!summ) _noSummText ~= b;
					return b;
				}
				_summary = createB(_prop.msgs.replTextSummary, '1', true);
				_scenario = createB(_prop.msgs.replTextScenario, '2');
				_author = createB(_prop.msgs.replTextAuthor, '3');
				_msg = createB(_prop.msgs.replTextMessage, '4');
				_cardName = createB(_prop.msgs.replTextCardName, '5');
				_cardDesc = createB(_prop.msgs.replTextCardDesc, '6');
				_event = createB(_prop.msgs.replTextEventText, '7');
				_start = createB(_prop.msgs.replTextStart, '8');
				_varName = createB(_prop.msgs.replTextVariableName, '9');
				_varValue = createB(_prop.msgs.replTextVariableValue, 'A', true);
				_expression = createB(_prop.msgs.replTextExpression, 'B');
				_coupon = createB(_prop.msgs.replTextCoupon, 'D');
				_gossip = createB(_prop.msgs.replTextGossip, 'E');
				_end = createB(_prop.msgs.replTextEndScenario, 'G');
				_area = createB(_prop.msgs.replTextAreaName, 'H');
				_keyCode = createB(_prop.msgs.replTextKeyCode, 'I');
				_cellName = createB(_prop.msgs.replTextCellName, 'K');
				_cardGroup = createB(_prop.msgs.replTextCardGroup, 'L');
				_file = createB(_prop.msgs.replTextFile, 'M');
				_comment = createB(_prop.msgs.replTextComment, 'N');
				_jptx = createB(_prop.msgs.replTextJptx, 'O');
				_textFile = createB(_prop.msgs.replTextTextFile, 'P');
				if (1 < _prop.var.etc.plainTextFileExtensions.length) { mixin(S_TRACE);
					auto txtExts = .join(.map!(ext => "*" ~ ext)(_prop.var.etc.plainTextFileExtensions), ", ");
					_textFile.setToolTipText(.tryFormat(_prop.msgs.replacePlainTextHint, txtExts));
				} else { mixin(S_TRACE);
					_textFile.setToolTipText(_prop.msgs.replacePlainTextHintNoExt);
				}
			}
			auto sep = new Label(grp, SWT.SEPARATOR | SWT.HORIZONTAL);
			sep.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			{ mixin(S_TRACE);
				auto btns = addButtonLine(grp);
				checked.createAlls(btns, _prop.msgs.allCheck);
			}
		}

		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.replForText);
		tab.setControl(comp);
		_tabText = tab;

		auto gd = new GridData(GridData.FILL_BOTH);
		comp2.setLayoutData(gd);
		_comps[tab] = comp2;
	}
	void constructID(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(zeroGridLayout(1, true));
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.replID);
			grp.setLayout(normalGridLayout(3, false));
			{ mixin(S_TRACE);
				auto l = new Label(grp, SWT.NONE);
				l.setText(_prop.msgs.replIDKind);
				_idKind = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
				_idKind.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
				_idKind.add(_prop.msgs.replIDArea);
				_idKind.add(_prop.msgs.replIDBattle);
				_idKind.add(_prop.msgs.replIDPackage);
				_idKind.add(_prop.msgs.replIDCast);
				_idKind.add(_prop.msgs.replIDSkill);
				_idKind.add(_prop.msgs.replIDItem);
				_idKind.add(_prop.msgs.replIDBeast);
				_idKind.add(_prop.msgs.replIDInfo);
				_idKind.add(_prop.msgs.replIDFlag);
				_idKind.add(_prop.msgs.replIDStep);
				_idKind.add(_prop.msgs.replIDVariant);
				_idKind.add(_prop.msgs.replIDCoupon);
				_idKind.add(_prop.msgs.replIDGossip);
				_idKind.add(_prop.msgs.replIDCompleteStamp);
				_idKind.add(_prop.msgs.replIDKeyCode);
				_idKind.add(_prop.msgs.replIDCellName);
				_idKind.add(_prop.msgs.replIDCardGroup);
				_idKind.select(0);
				if (0 <= _prop.var.etc.searchIDKind && _prop.var.etc.searchIDKind < _idKind.getItemCount()) { mixin(S_TRACE);
					_idKind.select(_prop.var.etc.searchIDKind);
				}
				auto gd = new GridData;
				gd.horizontalSpan = 2;
				_idKind.setLayoutData(gd);
				_idKind.addSelectionListener(new SelIDKind);
			}
			{ mixin(S_TRACE);
				auto sep = new Label(grp, SWT.SEPARATOR | SWT.HORIZONTAL);
				auto gd = new GridData(GridData.FILL_HORIZONTAL);
				gd.horizontalSpan = 3;
				sep.setLayoutData(gd);
			}
			void setupID(string text, ref Composite comboComp, ref Spinner spn) { mixin(S_TRACE);
				auto l = new Label(grp, SWT.NONE);
				l.setText(text);
				comboComp = new Composite(grp, SWT.NONE);
				auto cl = new CenterLayout(SWT.HORIZONTAL | SWT.VERTICAL, 0);
				cl.fillHorizontal = true;
				cl.fillVertical = true;
				comboComp.setLayout(cl);
				auto gd = new GridData(GridData.FILL_HORIZONTAL);
				gd.widthHint = _prop.var.etc.nameWidth;
				comboComp.setLayoutData(gd);
				spn = new Spinner(grp, SWT.BORDER);
				initSpinner(spn);
				spn.setMinimum(1);
				spn.setMaximum(_prop.looks.idMax);
			}
			setupID(_prop.msgs.replFrom, _fromIDComp, _fromIDVal);
			setupID(_prop.msgs.replTo, _toIDComp, _toIDVal);
			updateIDCombo();
		}

		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.replForID);
		tab.setControl(comp);
		_tabID = tab;

		auto gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 0;
		comp2.setLayoutData(gd);
		_comps[tab] = comp2;
	}
	void constructPath(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(zeroGridLayout(1, true));
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.replPath);
			grp.setLayout(normalGridLayout(2, false));
			Combo setupPath(string text, ref IncSearch incSearch) { mixin(S_TRACE);
				auto l = new Label(grp, SWT.NONE);
				l.setText(text);
				auto combo = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN);
				combo.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
				auto gd = new GridData(GridData.FILL_HORIZONTAL);
				gd.widthHint = _prop.var.etc.nameWidth;
				combo.setLayoutData(gd);

				auto menu = new Menu(_win, SWT.POP_UP);
				combo.setMenu(menu);
				createMenuItem(_comm, menu, MenuID.IncSearch, { mixin(S_TRACE);
					.forceFocus(combo, true);
					incSearch.startIncSearch();
				}, () => _hasPath);
				new MenuItem(menu, SWT.SEPARATOR);
				createTextMenu!Combo(_comm, _prop, combo, &catchMod);

				incSearch = new IncSearch(_comm, combo, () => _hasPath);

				return combo;
			}
			_fromPath = setupPath(_prop.msgs.replFrom, _fromPathIncSearch);
			_fromPathIncSearch.modEvent ~= () => setupPathsImpl(true, false);
			_toPath = setupPath(_prop.msgs.replTo, _toPathIncSearch);
			_fromPathIncSearch.modEvent ~= () => setupPathsImpl(false, true);
			auto dummy = new Composite(grp, SWT.NONE);
			auto gd = new GridData(GridData.FILL_VERTICAL);
			gd.heightHint = 0;
			gd.widthHint = 0;
			dummy.setLayoutData(gd);
			auto desc = new Label(grp, SWT.NONE);
			desc.setText(_prop.msgs.wildcardDesc);
		}

		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.replForPath);
		tab.setControl(comp);
		_tabPath = tab;

		auto gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 0;
		comp2.setLayoutData(gd);
		_comps[tab] = comp2;
	}
	void constructContents(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(zeroGridLayout(1, true));
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			auto gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.widthHint = _prop.var.etc.searchResultTableWidth;
			grp.setLayoutData(gd);
			grp.setText(_prop.msgs.searchRange);
			grp.setLayout(zeroGridLayout(1, true));
			auto checked = new LCheck;
			_checked ~= checked;

			auto comp3 = new Composite(grp, SWT.NONE);
			comp3.setLayoutData(new GridData(GridData.FILL_BOTH));
			comp3.setLayout(windowGridLayout(1, true));
			auto bar = new ToolBar(comp3, SWT.HORIZONTAL | SWT.FLAT | SWT.WRAP);
			_comm.put(bar);
			bar.setLayoutData(new GridData(GridData.FILL_BOTH));
			foreach (cGrp; EnumMembers!CTypeGroup) { mixin(S_TRACE);
				if (bar.getItemCount()) { mixin(S_TRACE);
					new ToolItem(bar, SWT.SEPARATOR);
				}
				auto cs = CTYPE_GROUP[cGrp];
				foreach (cType; cs) { mixin(S_TRACE);
					auto text = _prop.msgs.contentName(cType);
					auto img = _prop.images.content(cType);
					void delegate() func = null;
					auto ti = createToolItem2(_comm, bar, text, img, func, null, SWT.CHECK);
					_contents[cType] = ti;
					checked.buttons ~= ti;
					ti.addSelectionListener(checked);
				}
			}
			auto sep = new Label(grp, SWT.SEPARATOR | SWT.HORIZONTAL);
			sep.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			auto btns = addButtonLine(grp);
			checked.createAlls(btns, _prop.msgs.allSelect);
		}

		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.replContents);
		tab.setControl(comp);
		_tabContents = tab;

		auto gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 0;
		comp2.setLayoutData(gd);
		_comps[tab] = comp2;
	}
	void constructCoupon(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto comp2 = new Composite(comp, SWT.NONE);
		auto comp2gl = windowGridLayout(1, true);
		comp2gl.marginWidth = 0;
		comp2gl.marginHeight = 0;
		comp2.setLayout(comp2gl);
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			auto gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.widthHint = _prop.var.etc.searchResultTableWidth;
			grp.setLayoutData(gd);
			grp.setText(_prop.msgs.searchRange);
			grp.setLayout(zeroGridLayout(1, true));
			auto checked = new LCheck;
			_checked ~= checked;
			{ mixin(S_TRACE);
				auto btns = addButtonLine(grp);
				Button createB(string text, char accr) { mixin(S_TRACE);
					auto b = new Button(btns, SWT.CHECK);
					b.setText(text ~ "(&" ~ accr ~ ")");
					checked.buttons ~= b;
					b.addSelectionListener(checked);
					return b;
				}
				_cCoupon = createB(_prop.msgs.replTextCoupon, '1');
				_cGossip = createB(_prop.msgs.replTextGossip, '2');
				_cEnd = createB(_prop.msgs.replTextEndScenario, '3');
				_cKeyCode = createB(_prop.msgs.replTextKeyCode, '4');
				_cCellName = createB(_prop.msgs.replTextCellName, '5');
				_cCardGroup = createB(_prop.msgs.replTextCardGroup, '6');
			}
			auto sep = new Label(grp, SWT.SEPARATOR | SWT.HORIZONTAL);
			sep.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			{ mixin(S_TRACE);
				auto btns = addButtonLine(grp);
				checked.createAlls(btns, _prop.msgs.allCheck);
			}
		}

		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.replForCoupon);
		tab.setControl(comp);
		_tabCoupon = tab;

		auto gd = new GridData(GridData.FILL_BOTH);
		comp2.setLayoutData(gd);
		_comps[tab] = comp2;
	}
	void constructUnuse(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(zeroGridLayout(1, true));
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.replUnuseTarget);
			grp.setLayout(zeroGridLayout(1, true));
			auto checked = new LCheck;
			_checked ~= checked;
			{ mixin(S_TRACE);
				auto btns = addButtonLine(grp);
				Button createB(string text, char accr) { mixin(S_TRACE);
					auto b = new Button(btns, SWT.CHECK);
					b.setText(text ~ "(&" ~ accr ~ ")");
					checked.buttons ~= b;
					b.addSelectionListener(checked);
					return b;
				}
				_unuseFlag = createB(_prop.msgs.replUnuseFlag, '1');
				_unuseStep = createB(_prop.msgs.replUnuseStep, '2');
				_unuseVariant = createB(_prop.msgs.replUnuseVariant, '3');
				_unuseArea = createB(_prop.msgs.replUnuseArea, '4');
				_unuseBattle = createB(_prop.msgs.replUnuseBattle, '5');
				_unusePackage = createB(_prop.msgs.replUnusePackage, '6');
				_unuseCast = createB(_prop.msgs.replUnuseCast, '7');
				_unuseSkill = createB(_prop.msgs.replUnuseSkill, '8');
				_unuseItem = createB(_prop.msgs.replUnuseItem, '9');
				_unuseBeast = createB(_prop.msgs.replUnuseBeast, 'A');
				_unuseInfo = createB(_prop.msgs.replUnuseInfo, 'B');
				_unuseStart = createB(_prop.msgs.replUnuseStart, 'C');
				_unusePath = createB(_prop.msgs.replUnusePath, 'D');
			}
			auto sep = new Label(grp, SWT.SEPARATOR | SWT.HORIZONTAL);
			sep.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			{ mixin(S_TRACE);
				auto btns = addButtonLine(grp);
				checked.createAlls(btns, _prop.msgs.allCheck);
			}
			auto sep2 = new Label(grp, SWT.SEPARATOR | SWT.HORIZONTAL);
			sep2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			auto hint = new Label(grp, SWT.WRAP);
			hint.setText(_prop.msgs.findUnusedHint);
			hint.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
		}

		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.replForUnuse);
		tab.setControl(comp);
		_tabUnuse = tab;

		auto gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 0;
		comp2.setLayoutData(gd);
		_comps[tab] = comp2;
	}
	void constructError(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(zeroGridLayout(1, true));
		{ mixin(S_TRACE);
			auto l = new Label(comp2, SWT.WRAP);
			l.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			l.setText(_prop.msgs.replError);
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.searchErrorCondition);
			grp.setLayout(normalGridLayout(2, false));
			auto l = new Label(grp, SWT.NONE);
			l.setText(_prop.msgs.searchErrorTargetVersion);

			_targetVer = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
			_targetVer.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
			_targetVer.add(_prop.msgs.searchErrorTargetVersionNotSet);
			_targetVer.select(0);
			foreach (wsnVer, wsnName, engineName; .zip(VERSIONS, VERSION_NAMES, ENGINES)) { mixin(S_TRACE);
				_targetVer.add(.tryFormat(_prop.msgs.searchErrorTargetVersionName, wsnName, engineName));
				_verInfos ~= VerInfo(wsnName, false, wsnVer, "");
				if (wsnName == _prop.var.etc.searchErrorTargetVersion) _targetVer.select(_targetVer.getItemCount() - 1);
			}
			foreach (targVer, engineName; .zip(CLASSIC_VERSIONS, CLASSIC_ENGINES)) { mixin(S_TRACE);
				_targetVer.add(engineName);
				_verInfos ~= VerInfo(engineName, true, "", targVer);
				if (engineName == _prop.var.etc.searchErrorTargetVersion) _targetVer.select(_targetVer.getItemCount() - 1);
			}
			.listener(_targetVer, SWT.Dispose, { mixin(S_TRACE);
				auto targIndex = _targetVer.getSelectionIndex();
				_prop.var.etc.searchErrorTargetVersion = targIndex <= 0 ? "" : _verInfos[targIndex - 1].name;
			});
			auto hint = new Label(grp, SWT.NONE);
			hint.setText(_prop.msgs.searchErrorTargetVersionHint);
			auto hgd = new GridData;
			hgd.horizontalSpan = 2;
			hint.setLayoutData(hgd);
		}
		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.replForError);
		tab.setControl(comp);
		_tabError = tab;

		auto gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 0;
		comp2.setLayoutData(gd);
		_comps[tab] = comp2;
	}
	void constructGrep(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		auto comp2 = new Composite(comp, SWT.NONE);
		auto comp2gl = windowGridLayout(1, true);
		comp2gl.marginWidth = 0;
		comp2gl.marginHeight = 0;
		comp2.setLayout(comp2gl);

		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.grepText);
			grp.setLayout(normalGridLayout(2, false));
			auto fl = new Label(grp, SWT.NONE);
			fl.setText(_prop.msgs.grepFrom);
			_grepFromComp = new Composite(grp, SWT.NONE);
			_grepFromComp.setLayout(new FillLayout);
			auto gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.widthHint = _prop.var.etc.nameWidth;
			_grepFromComp.setLayoutData(gd);
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp2, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.grepTarget);
			grp.setLayout(normalGridLayout(4, false));

			_grepDir = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN);
			_grepDir.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
			createTextMenu!Combo(_comm, _prop, _grepDir, &catchMod);
			_grepDir.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			auto grepDirRef = new Button(grp, SWT.PUSH);
			grepDirRef.setText(_prop.msgs.reference);
			.listener(grepDirRef, SWT.Selection, { mixin(S_TRACE);
				selectDir(_prop, _grepDir, _prop.msgs.grepDir, _prop.msgs.grepDirDesc, _grepDir.getText());
			});
			createOpenButton(_comm, grp, { return _prop.toAppAbs(_grepDir.getText()); }, true);

			auto grepCurrent = new Button(grp, SWT.PUSH);
			grepCurrent.setText(_prop.msgs.grepCurrent);
			.listener(grepCurrent, SWT.Selection, &setGrepCurrentDir);
			_grepSubDir = new Button(grp, SWT.CHECK);
			auto gd = new GridData(GridData.HORIZONTAL_ALIGN_END);
			gd.horizontalSpan = 4;
			_grepSubDir.setLayoutData(gd);
			_grepSubDir.setText(_prop.msgs.grepSubDir);
			.listener(_grepDir, SWT.Modify, { mixin(S_TRACE);
				if (ignoreMod) return;
				_prop.var.etc.grepDir = _grepDir.getText();
			});
			.listener(_grepSubDir, SWT.Selection, { mixin(S_TRACE);
				if (ignoreMod) return;
				_prop.var.etc.grepSubDir = _grepSubDir.getSelection();
			});
		}

		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.replGrep);
		tab.setControl(comp);
		_tabGrep = tab;

		auto gd = new GridData(GridData.FILL_BOTH);
		comp2.setLayoutData(gd);
		_comps[tab] = comp2;
	}
	void setGrepCurrentDir() { mixin(S_TRACE);
		if (_summ) { mixin(S_TRACE);
			auto sc = _summ.scenarioPath.dirName();
			if (_summ.readOnlyPath != "") { mixin(S_TRACE);
				sc = _summ.readOnlyPath;
			} else if (_summ.useTemp) { mixin(S_TRACE);
				sc = _summ.origZipName.dirName();
			}
			_grepDir.setText(sc);
		}
	}

	void refFunc(bool Del, A : CWXPath)(A a) { mixin(S_TRACE);
		if (_incSearch.isSearching) { mixin(S_TRACE);
			refreshRangeTree(false);
			return;
		}
		bool recurse(TreeItem itm) { mixin(S_TRACE);
			if (a is itm.getData()) { mixin(S_TRACE);
				static if (Del) {
					itm.dispose();
				} else { mixin(S_TRACE);
					itm.setText(.text(a.id) ~ "." ~ a.name);
				}
				return true;
			} else { mixin(S_TRACE);
				foreach (child; itm.getItems()) { mixin(S_TRACE);
					if (recurse(child)) { mixin(S_TRACE);
						return true;
					}
				}
				return false;
			}
		}
		foreach (child; _range.getItems()) { mixin(S_TRACE);
			if (recurse(child)) { mixin(S_TRACE);
				return;
			}
		}
		static if (!Del) {
			// 追加
			refreshRangeTree(false);
		}
	}
	void delArea(Area a) { mixin(S_TRACE);
		refFunc!true(a);
	}
	void delBattle(Battle a) { mixin(S_TRACE);
		refFunc!true(a);
	}
	void delPackage(Package a) { mixin(S_TRACE);
		refFunc!true(a);
	}
	void delCast(CastCard a) { mixin(S_TRACE);
		refFunc!true(a);
	}
	void delSkill(CWXPath owner, SkillCard a) { mixin(S_TRACE);
		refFunc!true(a);
	}
	void delItem(CWXPath owner, ItemCard a) { mixin(S_TRACE);
		refFunc!true(a);
	}
	void delBeast(CWXPath owner, BeastCard a) { mixin(S_TRACE);
		refFunc!true(a);
	}
	void delInfo(InfoCard a) { mixin(S_TRACE);
		refFunc!true(a);
	}
	void refArea(Area a) { mixin(S_TRACE);
		if (_prop.var.etc.showAreaDirTree) {
			refreshRangeTree(false);
		} else {
			refFunc!false(a);
		}
	}
	void refBattle(Battle a) { mixin(S_TRACE);
		if (_prop.var.etc.showAreaDirTree) {
			refreshRangeTree(false);
		} else {
			refFunc!false(a);
		}
	}
	void refPackage(Package a) { mixin(S_TRACE);
		if (_prop.var.etc.showAreaDirTree) {
			refreshRangeTree(false);
		} else {
			refFunc!false(a);
		}
	}
	void refCast(CastCard a) { mixin(S_TRACE);
		refFunc!false(a);
	}
	void refSkill(SkillCard a) { mixin(S_TRACE);
		refFunc!false(a);
	}
	void refItem(ItemCard a) { mixin(S_TRACE);
		refFunc!false(a);
	}
	void refBeast(BeastCard a) { mixin(S_TRACE);
		refFunc!false(a);
	}
	void refInfo(InfoCard a) { mixin(S_TRACE);
		refFunc!false(a);
	}
	static CWXPath[] rangeTree(Summary summ) { mixin(S_TRACE);
		CWXPath[] r;
		r ~= summ;
		r ~= summ.flagDirRoot;
		foreach (a; summ.areas) r ~= a;
		foreach (a; summ.battles) r ~= a;
		foreach (a; summ.packages) r ~= a;
		foreach (a; summ.casts) { mixin(S_TRACE);
			r ~= a;
			foreach (c; a.skills) { mixin(S_TRACE);
				if (0 != c.linkId) continue;
				r ~= c;
			}
			foreach (c; a.items) { mixin(S_TRACE);
				if (0 != c.linkId) continue;
				r ~= c;
			}
			foreach (c; a.beasts) { mixin(S_TRACE);
				if (0 != c.linkId) continue;
				r ~= c;
			}
		}
		foreach (a; summ.skills) r ~= a;
		foreach (a; summ.items) r ~= a;
		foreach (a; summ.beasts) r ~= a;
		foreach (a; summ.infos) r ~= a;
		return r;
	}
	void refreshRangeTree2() { refreshRangeTree(false); }
	void refreshRangeTree(bool clearChecked) { mixin(S_TRACE);
		_range.setRedraw(false);
		scope (exit) _range.setRedraw(true);
		if (!_summ) { mixin(S_TRACE);
			_rangeTable = null;
			_range.removeAll();
			return;
		}
		if (clearChecked) _rangeTable = null;
 		bool[CWXPath] sels;
 		bool[string] selsStr;
 		foreach (itm; _range.getSelection()) { mixin(S_TRACE);
 			auto sel = cast(CWXPath)itm.getData();
 			if (sel) { mixin(S_TRACE);
				sels[sel] = true;
 			} else { mixin(S_TRACE);
 				selsStr[itm.getText().toLower()] = true;
 			}
		}
		TreeItem[] newSels;

		bool[CWXPath] rangeTable;
		scope (exit) _rangeTable = rangeTable;
		_range.removeAll();
		TreeItem add(lazy TreeItem par, string name, bool hasMatchName, string matchName, CWXPath path) { mixin(S_TRACE);
			rangeTable[path] = _rangeTable.get(path, true);
			if (hasMatchName && !_incSearch.match(matchName)) { mixin(S_TRACE);
				return null;
			}
			TreeItem itm;
			if (par) { mixin(S_TRACE);
				itm = new TreeItem(par, SWT.NONE);
			} else { mixin(S_TRACE);
				itm = new TreeItem(_range, SWT.NONE);
			}
			string text1;
			Image img1;
			getSymbols(_comm, _summ, path, false, text1, img1);
			itm.setText(name);
			itm.setImage(img1);
			itm.setData(cast(Object)path);
			itm.setChecked(_rangeTable.get(path, true));
			if (path in sels) { mixin(S_TRACE);
				newSels ~= itm;
			}
			return itm;
		}
		add(null, _prop.msgs.summary, false, null, _summ);
		add(null, _prop.msgs.flagsAndSteps, false, null, _summ.flagDirRoot);
		if (_prop.var.etc.showAreaDirTree) { mixin(S_TRACE);
			TreeItem[string] itmTable;
			auto dirSet = new HashSet!string;
			auto dirSet2 = new HashSet!string;
			void put1(string dirName) { mixin(S_TRACE);
				auto l = dirName.toLower();
				if (dirSet2.contains(l)) return;
				dirSet.add(dirName);
				dirSet2.add(l);
			}
			foreach (a; _summ.areas) { mixin(S_TRACE);
				if (_incSearch.match(a.baseName)) put1(a.dirName);
			}
			foreach (a; _summ.battles) { mixin(S_TRACE);
				if (_incSearch.match(a.baseName)) put1(a.dirName);
			}
			foreach (a; _summ.packages) { mixin(S_TRACE);
				if (_incSearch.match(a.baseName)) put1(a.dirName);
			}
			bool delegate(string, string) cmps;
			if (_prop.var.etc.logicalSort) { mixin(S_TRACE);
				cmps = (a, b) => incmp(a, b) < 0;
			} else { mixin(S_TRACE);
				cmps = (a, b) => icmp(a, b) < 0;
			}
			foreach (dirName; .sortDlg(dirSet.toArray(), cmps)) { mixin(S_TRACE);
				auto dirs = .split(dirName, "\\");
				TreeItem itm = null;
				foreach (i, dir; dirs) { mixin(S_TRACE);
					auto fPath = dirs[0 .. i + 1].join("\\");
					auto path = fPath.toLower();
					auto p = path in itmTable;
					if (p) { mixin(S_TRACE);
						itm = *p;
					} else { mixin(S_TRACE);
						TreeItem sub;
						if (itm) { mixin(S_TRACE);
							sub = new TreeItem(itm, SWT.NONE);
						} else { mixin(S_TRACE);
							sub = new TreeItem(_range, SWT.NONE);
						}
						sub.setText(dir);
						sub.setImage(_prop.images.areaDir);
						sub.setChecked(true);
						itm = sub;
						itmTable[path] = sub;
					}
				}
				itmTable[dirName.toLower()] = itm;
				if (itm && dirName.toLower() in selsStr) newSels ~= itm;
			}
			void put(A)(A[] arr) { mixin(S_TRACE);
				foreach (a; arr) { mixin(S_TRACE);
					add(itmTable[a.dirName.toLower()], .tryFormat("%s.%s", a.id, a.baseName), true, a.baseName, a);
				}
			}
			put(_summ.areas);
			put(_summ.battles);
			put(_summ.packages);
			foreach (dirName, itm; itmTable) { mixin(S_TRACE);
				if (!itm) continue;
				bool recurse(TreeItem itm) { mixin(S_TRACE);
					if (!itm.getChecked()) return false;
					foreach (child; itm.getItems()) { mixin(S_TRACE);
						if (!recurse(child)) return false;
					}
					return true;
				}
				auto checked = true;
				foreach (child; itm.getItems()) { mixin(S_TRACE);
					checked &= recurse(child);
					if (!checked) break;
				}
				itm.setChecked(checked);
			}
		} else { mixin(S_TRACE);
			foreach (a; _summ.areas) { mixin(S_TRACE);
				add(null, .tryFormat("%s.%s", a.id, a.name), true, a.name, a);
			}
			foreach (a; _summ.battles) { mixin(S_TRACE);
				add(null, .tryFormat("%s.%s", a.id, a.name), true, a.name, a);
			}
			foreach (a; _summ.packages) { mixin(S_TRACE);
				add(null, .tryFormat("%s.%s", a.id, a.name), true, a.name, a);
			}
		}
		foreach (a; _summ.casts) { mixin(S_TRACE);
			auto par = add(null, .tryFormat("%s.%s", a.id, a.name), true, a.name, a);
			void createParent() { mixin(S_TRACE);
				if (par) return;
				par = add(null, .tryFormat("%s.%s", a.id, a.name), false, null, a);
			}
			foreach (c; a.skills) { mixin(S_TRACE);
				if (0 != c.linkId) continue;
				if (_incSearch.match(c.name)) createParent();
				add(par, .tryFormat("%s.%s", c.id, c.name), true, c.name, c);
			}
			foreach (c; a.items) { mixin(S_TRACE);
				if (0 != c.linkId) continue;
				if (_incSearch.match(c.name)) createParent();
				add(par, .tryFormat("%s.%s", c.id, c.name), true, c.name, c);
			}
			foreach (c; a.beasts) { mixin(S_TRACE);
				if (0 != c.linkId) continue;
				if (_incSearch.match(c.name)) createParent();
				add(par, .tryFormat("%s.%s", c.id, c.name), true, c.name, c);
			}
		}
		foreach (a; _summ.skills) { mixin(S_TRACE);
			add(null, .tryFormat("%s.%s", a.id, a.name), true, a.name, a);
		}
		foreach (a; _summ.items) { mixin(S_TRACE);
			add(null, .tryFormat("%s.%s", a.id, a.name), true, a.name, a);
		}
		foreach (a; _summ.beasts) { mixin(S_TRACE);
			add(null, .tryFormat("%s.%s", a.id, a.name), true, a.name, a);
		}
		foreach (a; _summ.infos) { mixin(S_TRACE);
			add(null, .tryFormat("%s.%s", a.id, a.name), true, a.name, a);
		}
		_range.setSelection(newSels);
		if (_rangeAllCheck) refreshRangeAllCheck();
		_range.treeExpandedAll();
		_range.showSelection();
		_comm.refreshToolBar();
	}
	void refreshRangeAllCheck() { mixin(S_TRACE);
		foreach (checked; _rangeTable.byValue()) { mixin(S_TRACE);
			if (!checked) { mixin(S_TRACE);
				_rangeAllCheck.setSelection(false);
				return;
			}
		}
		_rangeAllCheck.setSelection(true);
	}
	class RefRangeAllCheck : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			if (SWT.CHECK != e.detail) return;
			auto par = cast(TreeItem)e.item;
			if (!par) return;
			if (auto cwxPath = cast(CWXPath)par.getData()) { mixin(S_TRACE);
				_rangeTable[cwxPath] = par.getChecked();
			}

			TreeItem[] targs;
			if (updateChecked(e)) { mixin(S_TRACE);
				targs = par.getParent().getSelection();
			} else { mixin(S_TRACE);
				targs = [par];
			}
			foreach (par2; targs) { mixin(S_TRACE);
				if (!par2.getData()) { mixin(S_TRACE);
					// アイテムが単なるコンテナであれば
					// 下のアイテムの状態を一括で切り替える
					void recurse(TreeItem itm) { mixin(S_TRACE);
						itm.setChecked(par2.getChecked());
						if (auto cwxPath = cast(CWXPath)itm.getData()) { mixin(S_TRACE);
							_rangeTable[cwxPath] = itm.getChecked();
						}
						foreach (child; itm.getItems()) recurse(child);
					}
					foreach (itm; par2.getItems()) { mixin(S_TRACE);
						recurse(itm);
					}
				}
				// 上のアイテムが単なるコンテナであれば
				// 下のアイテムの状態によってチェック状態を切り替える
				while (par2 && par2.getParentItem() && !par2.getParentItem().getData()) { mixin(S_TRACE);
					bool same = true;
					void recurse2(TreeItem itm) { mixin(S_TRACE);
						if (itm.getChecked() != par2.getChecked()) { mixin(S_TRACE);
							same = false;
							return;
						}
						foreach (child; itm.getItems()) recurse2(child);
					}
					foreach (itm; par2.getParentItem().getItems()) { mixin(S_TRACE);
						recurse2(itm);
					}
					if (!same) break;
					auto parItm = par2.getParentItem();
					parItm.setChecked(par2.getChecked());
					if (auto cwxPath = cast(CWXPath)parItm.getData()) { mixin(S_TRACE);
						_rangeTable[cwxPath] = parItm.getChecked();
					}
					par2 = par2.getParentItem();
				}
			}
			refreshRangeAllCheck();
		}
	}
	class RangeAllCheck : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			void recurse(TreeItem itm) { mixin(S_TRACE);
				itm.setChecked(_rangeAllCheck.getSelection());
				auto path = cast(CWXPath)itm.getData();
				if (path) _rangeTable[path] = itm.getChecked();
				foreach (child; itm.getItems()) { mixin(S_TRACE);
					recurse(child);
				}
			}
			foreach (child; _range.getItems()) { mixin(S_TRACE);
				recurse(child);
			}
		}
	}
	void refUndoMax() { mixin(S_TRACE);
		_undo.max = _prop.var.etc.undoMaxReplace;
	}
public:
	this (Commons comm, Props prop, Shell shell, Summary summ, void delegate() sendReloadProps) { mixin(S_TRACE);
		_uiThread = core.thread.Thread.getThis();
		_comm = comm;
		_prop = prop;
		_summ = summ;
		_undo = new UndoManager(_prop.var.etc.undoMaxReplace);
		_sendReloadProps = sendReloadProps;
		_win = new Shell(shell, SWT.SHELL_TRIM);
		_win.setText(_prop.msgs.dlgTitReplaceText);
		_win.setImage(prop.images.menu(MenuID.Find));

		_display = shell.getDisplay();

		setup();
		_win.open();
		_win.setActive();
	}
	@property
	Shell widget() { mixin(S_TRACE);
		return _win;
	}
	@property
	void summary(Summary summ) { mixin(S_TRACE);
		if (_win.isDisposed()) return;
		if (!summ) { mixin(S_TRACE);
			_win.close();
		} else { mixin(S_TRACE);
			_summ = summ;
			reset();
			refreshRangeTree(true);
			setupIDs(true, true, true);
		}
		_undo.reset();
		_comm.refreshToolBar();
	}

	void open() { mixin(S_TRACE);
		reset();
		tabChanged();
		auto tab = _tabf.getSelection();
		if (tab is _tabText) { mixin(S_TRACE);
			_from.setFocus();
		} else if (tab is _tabID) { mixin(S_TRACE);
			_idKind.setFocus();
		} else if (tab is _tabPath) { mixin(S_TRACE);
			_fromPath.setFocus();
		} else if (tab is _tabContents) { mixin(S_TRACE);
			// Nothing
		} else if (tab is _tabCoupon) { mixin(S_TRACE);
			// Nothing
		} else if (tab is _tabUnuse) { mixin(S_TRACE);
			// Nothing
		} else if (tab is _tabError) { mixin(S_TRACE);
			// Nothing
		} else if (tab is _tabGrep) { mixin(S_TRACE);
			_from.setFocus();
		} else assert (0);
	}
	void replaceText(string from, bool start = false) { mixin(S_TRACE);
		reset();
		_from.setText(from);
		_to.setText("");
		_tabf.setSelection(_tabText);
		tabChanged();
		_from.setFocus();
		if (start) { mixin(S_TRACE);
			search();
		}
	}
	void replacePath(string from, bool start = false) { mixin(S_TRACE);
		reset();
		_fromPath.setText(from);
		_toPath.setText("");
		_tabf.setSelection(_tabPath);
		tabChanged();
		_fromPath.setFocus();
		if (start) { mixin(S_TRACE);
			search();
		}
	}
	private bool canFindSelectedID() { mixin(S_TRACE);
		if (_lastFind is _tabGrep) return false;
		auto index = _result.getSelectionIndex();
		if (index == -1) return false;
		auto itm = _result.getItem(index);
		auto d = itm.getData();
		auto ps = cast(CWXPathString)d;
		auto data = ps ? ps.path : null;
		if (auto tex = cast(SimpleTextHolder)data) data = tex.owner;
		auto img = itm.getImage();
		auto str = itm.getText();
		if (auto id = cast(Area)data) { mixin(S_TRACE);
			return true;
		} else if (auto id = cast(Battle)data) { mixin(S_TRACE);
			return true;
		} else if (auto id = cast(Package)data) { mixin(S_TRACE);
			return true;
		} else if (auto id = cast(CastCard)data) { mixin(S_TRACE);
			return true;
		} else if (auto id = cast(SkillCard)data) { mixin(S_TRACE);
			if (id.cwxParent !is _comm.summary) return false;
			return true;
		} else if (auto id = cast(ItemCard)data) { mixin(S_TRACE);
			if (id.cwxParent !is _comm.summary) return false;
			return true;
		} else if (auto id = cast(BeastCard)data) { mixin(S_TRACE);
			if (id.cwxParent !is _comm.summary) return false;
			return true;
		} else if (auto id = cast(InfoCard)data) { mixin(S_TRACE);
			return true;
		} else if (auto id = cast(cwx.flag.Flag)data) { mixin(S_TRACE);
			return true;
		} else if (auto id = cast(Step)data) { mixin(S_TRACE);
			return true;
		} else if (auto id = cast(cwx.flag.Variant)data) { mixin(S_TRACE);
			return true;
		} else if (img is _prop.images.couponNormal && data is null) { mixin(S_TRACE);
			return true;
		} else if (img is _prop.images.gossip && data is null) { mixin(S_TRACE);
			return true;
		} else if (img is _prop.images.endScenario && data is null) { mixin(S_TRACE);
			return true;
		} else if (img is _prop.images.keyCode && data is null) { mixin(S_TRACE);
			return true;
		} else if (img is _prop.images.backs && data is null) { mixin(S_TRACE);
			return true;
		} else if (auto id = cast(Content)data) { mixin(S_TRACE);
			if (id.type !is CType.Start) return false;
			if (!id.tree) return false;
			return true;
		}
		return false;
	}
	private void findSelectedID() { mixin(S_TRACE);
		if (!canFindSelectedID()) return;
		auto index = _result.getSelectionIndex();
		if (index == -1) return;
		auto itm = _result.getItem(index);
		auto d = itm.getData();
		auto ps = cast(CWXPathString)d;
		auto data = ps ? ps.path : null;
		if (auto tex = cast(SimpleTextHolder)data) data = tex.owner;
		auto img = itm.getImage();
		auto str = itm.getText();
		if (auto id = cast(Area)data) { mixin(S_TRACE);
			replaceID(toAreaId(id.id), true);
		} else if (auto id = cast(Battle)data) { mixin(S_TRACE);
			replaceID(toBattleId(id.id), true);
		} else if (auto id = cast(Package)data) { mixin(S_TRACE);
			replaceID(toPackageId(id.id), true);
		} else if (auto id = cast(CastCard)data) { mixin(S_TRACE);
			replaceID(typeof(id).toID(id.id), true);
		} else if (auto id = cast(SkillCard)data) { mixin(S_TRACE);
			if (id.cwxParent !is _comm.summary) return;
			replaceID(typeof(id).toID(id.id), true);
		} else if (auto id = cast(ItemCard)data) { mixin(S_TRACE);
			if (id.cwxParent !is _comm.summary) return;
			replaceID(typeof(id).toID(id.id), true);
		} else if (auto id = cast(BeastCard)data) { mixin(S_TRACE);
			if (id.cwxParent !is _comm.summary) return;
			replaceID(typeof(id).toID(id.id), true);
		} else if (auto id = cast(InfoCard)data) { mixin(S_TRACE);
			replaceID(typeof(id).toID(id.id), true);
		} else if (auto id = cast(cwx.flag.Flag)data) { mixin(S_TRACE);
			if (auto ec = cast(LocalVariableOwner)id.useCounter.owner) { mixin(S_TRACE);
				CWXPath[] arr;
				SortableCWXPath!FlagUser.sortedPaths(id.useCounter.valueSet(toFlagId(id.path)), (p) { arr ~= p.obj.owner; });
				setFindResult(.cwxPlace(ec), arr, _prop.msgs.replLocalVariables);
			} else { mixin(S_TRACE);
				replaceID(toFlagId(id.path), true);
			}
		} else if (auto id = cast(Step)data) { mixin(S_TRACE);
			if (auto ec = cast(LocalVariableOwner)id.useCounter.owner) { mixin(S_TRACE);
				CWXPath[] arr;
				SortableCWXPath!StepUser.sortedPaths(id.useCounter.valueSet(toStepId(id.path)), (p) { arr ~= p.obj.owner; });
				setFindResult(.cwxPlace(ec), arr, _prop.msgs.replLocalVariables);
			} else { mixin(S_TRACE);
				replaceID(toStepId(id.path), true);
			}
		} else if (auto id = cast(cwx.flag.Variant)data) { mixin(S_TRACE);
			if (auto ec = cast(LocalVariableOwner)id.useCounter.owner) { mixin(S_TRACE);
				CWXPath[] arr;
				SortableCWXPath!VariantUser.sortedPaths(id.useCounter.valueSet(toVariantId(id.path)), (p) { arr ~= p.obj.owner; });
				setFindResult(.cwxPlace(ec), arr, _prop.msgs.replLocalVariables);
			} else { mixin(S_TRACE);
				replaceID(toVariantId(id.path), true);
			}
		} else if (img is _prop.images.couponNormal && data is null) { mixin(S_TRACE);
			replaceID(toCouponId(str), true);
		} else if (img is _prop.images.gossip && data is null) { mixin(S_TRACE);
			replaceID(toGossipId(str), true);
		} else if (img is _prop.images.endScenario && data is null) { mixin(S_TRACE);
			replaceID(toCompleteStampId(str), true);
		} else if (img is _prop.images.keyCode && data is null) { mixin(S_TRACE);
			replaceID(toKeyCodeId(str), true);
		} else if (img is _prop.images.backs && data is null) { mixin(S_TRACE);
			replaceID(toCellNameId(str), true);
		} else if (img is _prop.images.cards && data is null) { mixin(S_TRACE);
			replaceID(toCardGroupId(str), true);
		} else if (auto id = cast(Content)data) { mixin(S_TRACE);
			if (id.type !is CType.Start) return;
			auto tree = id.tree;
			if (!tree) return;
			CWXPath[] arr;
			SortableCWXPath!IStartUser.sortedPaths(id.tree.startUseCounter.valueSet(id.name), (p) { mixin(S_TRACE);
				arr ~= p.obj;
			});
			setFindResult(arr.length ? .cwxPlace(arr[0]) : null, arr, _prop.msgs.replStartUsers);
		}
	}
	void replaceID(ID)(ID from, bool start = false) { mixin(S_TRACE);
		reset();
		static if (is(ID:AreaId)) {
			_idKind.select(ID_AREA);
		} else static if (is(ID:BattleId)) {
			_idKind.select(ID_BATTLE);
		} else static if (is(ID:PackageId)) {
			_idKind.select(ID_PACKAGE);
		} else static if (is(ID:CastId)) {
			_idKind.select(ID_CAST);
		} else static if (is(ID:SkillId)) {
			_idKind.select(ID_SKILL);
		} else static if (is(ID:ItemId)) {
			_idKind.select(ID_ITEM);
		} else static if (is(ID:BeastId)) {
			_idKind.select(ID_BEAST);
		} else static if (is(ID:InfoId)) {
			_idKind.select(ID_INFO);
		} else static if (is(ID:FlagId)) {
			_idKind.select(ID_FLAG);
		} else static if (is(ID:StepId)) {
			_idKind.select(ID_STEP);
		} else static if (is(ID:VariantId)) {
			_idKind.select(ID_VARIANT);
		} else static if (is(ID:CouponId)) {
			_idKind.select(ID_COUPON);
		} else static if (is(ID:GossipId)) {
			_idKind.select(ID_GOSSIP);
		} else static if (is(ID:CompleteStampId)) {
			_idKind.select(ID_COMPLETE_STAMP);
		} else static if (is(ID:KeyCodeId)) {
			_idKind.select(ID_KEY_CODE);
		} else static if (is(ID:CellNameId)) {
			_idKind.select(ID_CELL_NAME);
		} else static if (is(ID:CardGroupId)) {
			_idKind.select(ID_CARD_GROUP);
		} else static assert (0);
		selIDKind();
		static if (is(typeof(from.id):ulong)) {
			bool sel = false;
			foreach (index, id; _fromIDTbl) { mixin(S_TRACE);
				if (id == from.id) { mixin(S_TRACE);
					_fromID.select(index);
					sel = true;
					break;
				}
			}
			if (!sel) return;
		} else {
			_fromID.setText(cast(string)from);
		}
		_tabf.setSelection(_tabID);
		tabChanged();
		_fromID.setFocus();
		if (start) { mixin(S_TRACE);
			search();
		}
	}

	private void openRangePath() { mixin(S_TRACE);
		auto sels = _range.getSelection();
		if (!sels.length) return;
		auto data = cast(CWXPath)sels[0].getData();
		if (!data) return;
		string path = data.cwxPath(true);
		path = cpaddattr(path, "shallow");
		path = cpaddattr(path, "only");
		auto r = _comm.openCWXPath(path, false);
		if (!r) { mixin(S_TRACE);
			MessageBox.showWarning(.tryFormat(_prop.msgs.cwxPathOpenError, path), _prop.msgs.dlgTitWarning, _win);
		}
	}
	private class OpenPath : MouseAdapter, KeyListener {
		override void mouseDoubleClick(MouseEvent e) { mixin(S_TRACE);
			if (1 == e.button) { mixin(S_TRACE);
				openRangePath();
			}
		}
		override void keyReleased(KeyEvent e) {}
		override void keyPressed(KeyEvent e) { mixin(S_TRACE);
			if (.isEnterKey(e.keyCode)) openRangePath();
		}
	}
	private void setup() { mixin(S_TRACE);
		_win.addShellListener(new SListener);
		_win.setLayout(zeroGridLayout(1, true));
		auto area = new Composite(_win, SWT.NONE);
		area.setLayoutData(new GridData(GridData.FILL_BOTH));
		area.setLayout(windowGridLayout(2, false));

		auto sash = new SplitPane(area, SWT.HORIZONTAL);
		sash.resizeControl1 = true;
		sash.setLayoutData(new GridData(GridData.FILL_BOTH));

		auto left = new Composite(sash, SWT.NONE);
		left.setLayout(windowGridLayout(1, true));
		_parent = left;
		auto right = new Composite(sash, SWT.NONE);
		right.setLayout(windowGridLayout(1, true));

		_tabf = new CTabFolder(left, SWT.BORDER);
		{ mixin(S_TRACE);
			_tabf.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			constructText(_tabf);
			constructID(_tabf);
			constructPath(_tabf);
			constructContents(_tabf);
			constructCoupon(_tabf);
			constructUnuse(_tabf);
			constructError(_tabf);
			constructGrep(_tabf);
			_tabf.addSelectionListener(new TSListener);
		}
		{ mixin(S_TRACE);
			_result = .rangeSelectableTable(left, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.VIRTUAL);
			auto gd = new GridData(GridData.FILL_BOTH);
			gd.widthHint = _prop.var.etc.searchResultTableWidth;
			gd.heightHint = _prop.var.etc.searchResultTableHeight;
			_result.setLayoutData(gd);
			_result.addMouseListener(new ML);
			_result.addKeyListener(new KL);
			.listener(_result, SWT.Selection, &_comm.refreshToolBar);
			auto menu = new Menu(_win, SWT.POP_UP);
			createMenuItem(_comm, menu, MenuID.Undo, &undo, &_undo.canUndo);
			createMenuItem(_comm, menu, MenuID.Redo, &redo, &_undo.canRedo);
			new MenuItem(menu, SWT.SEPARATOR);
			createMenuItem(_comm, menu, MenuID.CopyAsText, &copyResult, () => _result.getSelectionIndex() != -1);
			new MenuItem(menu, SWT.SEPARATOR);
			createMenuItem(_comm, menu, MenuID.SelectAll, &_result.selectAll, () => _result.getItemCount() > 0);
			new MenuItem(menu, SWT.SEPARATOR);
			createMenuItem(_comm, menu, MenuID.FindID, &findSelectedID, &canFindSelectedID);
			new MenuItem(menu, SWT.SEPARATOR);
			createMenuItem(_comm, menu, MenuID.OpenAtView, &openPath, &canOpenPath);
			_result.setMenu(menu);
			_edit = new TableTextEdit(_comm, _prop, _result, 0, &couponEditEnd, &canCouponEdit);

			.listener(_result, SWT.SetData, (e) { mixin(S_TRACE);
				if (_results.length <= e.index) return;
				_results[e.index].setData(cast(TableItem)e.item);
			});
		}
		{ mixin(S_TRACE);
			auto comp = new Composite(left, SWT.NONE);
			comp.setLayout(zeroMarginGridLayout(2, false));
			comp.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
			auto realtime = new Button(comp, SWT.CHECK);
			realtime.setText(_prop.msgs.searchResultRealtime);
			realtime.setSelection(_prop.var.etc.searchResultRealtime);
			.listener(realtime, SWT.Selection, { mixin(S_TRACE);
				_prop.var.etc.searchResultRealtime = realtime.getSelection();
				if (_prop.var.etc.searchResultRealtime) { mixin(S_TRACE);
					 _result.setItemCount(cast(int)_results.length);
					 _result.clearAll();
				}
			});
			auto openDlg = new Button(comp, SWT.CHECK);
			openDlg.setText(_prop.msgs.searchOpenDialog);
			openDlg.setToolTipText(_prop.msgs.searchOpenDialogHint);
			openDlg.setSelection(_prop.var.etc.searchOpenDialog);
			openDlg.addSelectionListener(new class SelectionAdapter {
				override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
					_prop.var.etc.searchOpenDialog = openDlg.getSelection();
				}
			});
		}
		{ mixin(S_TRACE);
			auto grp = new Group(right, SWT.NONE);
			grp.setText(_prop.msgs.searchRange);
			grp.setLayoutData(new GridData(GridData.FILL_BOTH));
			grp.setLayout(normalGridLayout(1, true));
			_range = new Tree(grp, SWT.MULTI | SWT.BORDER | SWT.CHECK);
			initTree(_comm, _range, false);
			_range.addSelectionListener(new RefRangeAllCheck);
			auto gd = new GridData(GridData.FILL_BOTH);
			gd.widthHint = 0;
			gd.heightHint = 0;
			_range.setLayoutData(gd);
			auto canIncSearch = () => _summ && (_summ.areas.length || _summ.battles.length || _summ.packages.length || _summ.casts.length || _summ.skills.length || _summ.items.length || _summ.beasts.length || _summ.infos.length);
			_incSearch = new IncSearch(_comm, _range, canIncSearch);
			refreshRangeTree(false);
			_rangeAllCheck = new Button(grp, SWT.CHECK);
			_rangeAllCheck.setText(_prop.msgs.allCheckRange);
			refreshRangeAllCheck();
			_rangeAllCheck.addSelectionListener(new RangeAllCheck);
			auto openPath = new OpenPath;
			_range.addKeyListener(openPath);
			_range.addMouseListener(openPath);
			auto menu = new Menu(_win, SWT.POP_UP);
			createMenuItem(_comm, menu, MenuID.IncSearch, &incSearch, canIncSearch);
			new MenuItem(menu, SWT.SEPARATOR);
			createMenuItem(_comm, menu, MenuID.SelectAll, { mixin(S_TRACE);
				void recurse(TreeItem itm) { mixin(S_TRACE);
					_range.select(itm);
					foreach (child; itm.getItems()) { mixin(S_TRACE);
						recurse(child);
					}
				}
				foreach (itm; _range.getItems()) { mixin(S_TRACE);
					recurse(itm);
				}
				_comm.refreshToolBar();
			}, () => _range.getAllItemCount() != _range.getSelectionCount());
			new MenuItem(menu, SWT.SEPARATOR);
			createMenuItem(_comm, menu, MenuID.OpenAtView, &openRangePath, () => _range.getSelection().length > 0 && cast(CWXPath)_range.getSelection()[0].getData());
			_range.setMenu(menu);
			_incSearch.modEvent ~= () => refreshRangeTree(false);
		}
		{ mixin(S_TRACE);
			auto sep = new Label(_win, SWT.SEPARATOR | SWT.HORIZONTAL);
			sep.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}
		{ mixin(S_TRACE);
			auto bArea = new Composite(_win, SWT.NONE);
			bArea.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			bArea.setLayout(normalGridLayout(2, false));
			_status = new Label(bArea, SWT.NONE);
			_status.setText(_prop.msgs.searchResultEmpty);
			_status.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			auto comp = new Composite(bArea, SWT.NONE);
			comp.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
			auto gl = normalGridLayout(4, true);
			gl.marginWidth = 0;
			gl.marginHeight = 0;
			comp.setLayout(gl);
			Button createButton(string text, void delegate() push) { mixin(S_TRACE);
				auto b = new Button(comp, SWT.PUSH);
				auto gd = new GridData(GridData.FILL_HORIZONTAL);
				gd.widthHint = _prop.var.etc.buttonWidth;
				b.setLayoutData(gd);
				b.setText(text);
				auto sa = new class SelectionAdapter {
					private void delegate() push;
					override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
						push();
					}
				};
				sa.push = push;
				b.addSelectionListener(sa);
				return b;
			}
			_find = createButton(_prop.msgs.search, &search);
			_comm.put(_find, &canFind);
			_replace = createButton(_prop.msgs.replace, &replace);
			_comm.put(_replace, &canReplace);
			_close = createButton(_prop.msgs.dlgTextClose, &exit);
			auto cancel = createButton(_prop.msgs.searchCancel, { mixin(S_TRACE);
				_cancel = true;
				resultRedraw(true);
			});
			_comm.put(cancel, &canCancel);
		}
		auto d = _tabf.getDisplay();
		auto keyFilter = new class Listener {
			override void handleEvent(Event e) { mixin(S_TRACE);
				auto fc = d.getFocusControl();
				if (!fc || !_tabf.isDescendant(fc)) return;
				if (.isEnterKey(e.keyCode)) { mixin(S_TRACE);
					search();
				}
			}
		};
		d.addFilter(SWT.KeyDown, keyFilter);
		.listener(_tabf, SWT.Dispose, { mixin(S_TRACE);
			d.removeFilter(SWT.KeyDown, keyFilter);
		});

		ignoreMod = true;
		scope (exit) ignoreMod = false;
		setComboItems(_from, _prop.var.etc.searchHistories.dup);
		setComboItems(_to, _prop.var.etc.replaceHistories.dup);
		setComboItems(_grepDir, _prop.var.etc.grepDirHistories.dup);
		_notIgnoreCase.setSelection(_prop.var.etc.replaceTextNotIgnoreCase);
		_exact.setSelection(_prop.var.etc.replaceTextExactMatch);
		_ignoreReturnCode.setSelection(_prop.var.etc.replaceTextIgnoreReturnCode);
		_useRegex.setSelection(_prop.var.etc.replaceTextRegExp);
		_useWildcard.setSelection(_prop.var.etc.replaceTextWildcard);
		_summary.setSelection(_prop.var.etc.replaceTextSummary);
		_scenario.setSelection(_prop.var.etc.replaceTextScenario);
		_author.setSelection(_prop.var.etc.replaceTextAuthor);
		_msg.setSelection(_prop.var.etc.replaceTextMessage);
		_cardName.setSelection(_prop.var.etc.replaceTextCardName);
		_cardDesc.setSelection(_prop.var.etc.replaceTextCardDescription);
		_event.setSelection(_prop.var.etc.replaceTextEventText);
		_varName.setSelection(_prop.var.etc.replaceTextVariableName);
		_varValue.setSelection(_prop.var.etc.replaceTextVariableValue);
		_expression.setSelection(_prop.var.etc.replaceTextExpression);
		_start.setSelection(_prop.var.etc.replaceTextStart);
		_coupon.setSelection(_prop.var.etc.replaceTextCoupon);
		_gossip.setSelection(_prop.var.etc.replaceTextGossip);
		_end.setSelection(_prop.var.etc.replaceTextEndScenario);
		_area.setSelection(_prop.var.etc.replaceTextAreaName);
		_keyCode.setSelection(_prop.var.etc.replaceTextKeyCode);
		_cellName.setSelection(_prop.var.etc.replaceTextCellName);
		_cardGroup.setSelection(_prop.var.etc.replaceTextCardGroup);
		_file.setSelection(_prop.var.etc.replaceTextFile);
		_comment.setSelection(_prop.var.etc.replaceTextComment);
		_jptx.setSelection(_prop.var.etc.replaceTextJptx);
		_textFile.setSelection(_prop.var.etc.replaceTextTextFile);
		if (_prop.var.etc.grepDir.length) { mixin(S_TRACE);
			_grepDir.setText(_prop.var.etc.grepDir);
		} else { mixin(S_TRACE);
			setGrepCurrentDir();
		}
		_grepSubDir.setSelection(_prop.var.etc.grepSubDir);

		foreach (cType; EnumMembers!CType) { mixin(S_TRACE);
			_contents[cType].setSelection(_prop.var.etc.searchContents(cType));
		}

		_cCoupon.setSelection(_prop.var.etc.replaceNameCoupon);
		_cGossip.setSelection(_prop.var.etc.replaceNameGossip);
		_cEnd.setSelection(_prop.var.etc.replaceNameEndScenario);
		_cKeyCode.setSelection(_prop.var.etc.replaceNameKeyCode);
		_cCellName.setSelection(_prop.var.etc.replaceNameCellName);
		_cCardGroup.setSelection(_prop.var.etc.replaceNameCardGroup);

		_unuseFlag.setSelection(_prop.var.etc.searchUnusedFlag);
		_unuseStep.setSelection(_prop.var.etc.searchUnusedStep);
		_unuseVariant.setSelection(_prop.var.etc.searchUnusedVariant);
		_unuseArea.setSelection(_prop.var.etc.searchUnusedArea);
		_unuseBattle.setSelection(_prop.var.etc.searchUnusedBattle);
		_unusePackage.setSelection(_prop.var.etc.searchUnusedPackage);
		_unuseCast.setSelection(_prop.var.etc.searchUnusedCast);
		_unuseSkill.setSelection(_prop.var.etc.searchUnusedSkill);
		_unuseItem.setSelection(_prop.var.etc.searchUnusedItem);
		_unuseBeast.setSelection(_prop.var.etc.searchUnusedBeast);
		_unuseInfo.setSelection(_prop.var.etc.searchUnusedInfo);
		_unuseStart.setSelection(_prop.var.etc.searchUnusedStart);
		_unusePath.setSelection(_prop.var.etc.searchUnusedPath);
		foreach (l; _checked) { mixin(S_TRACE);
			l.check();
		}
		_tabf.setSelection(0);
		if (0 <= _prop.var.etc.searchPlan && _prop.var.etc.searchPlan < _tabf.getItemCount()) { mixin(S_TRACE);
			_tabf.setSelection(_prop.var.etc.searchPlan);
		}

		.setupWeights(sash, _prop.var.etc.replaceRangeSashL, _prop.var.etc.replaceRangeSashR);

		_comm.refTableViewStyle.add(&refreshRangeTree2);
		_comm.refArea.add(&refArea);
		_comm.refBattle.add(&refBattle);
		_comm.refPackage.add(&refPackage);
		_comm.refCast.add(&refCast);
		_comm.refSkill.add(&refSkill);
		_comm.refItem.add(&refItem);
		_comm.refBeast.add(&refBeast);
		_comm.refInfo.add(&refInfo);
		_comm.delArea.add(&delArea);
		_comm.delBattle.add(&delBattle);
		_comm.delPackage.add(&delPackage);
		_comm.delCast.add(&delCast);
		_comm.delSkill.add(&delSkill);
		_comm.delItem.add(&delItem);
		_comm.delBeast.add(&delBeast);
		_comm.delInfo.add(&delInfo);
		_comm.refSearchHistories.add(&refSearchHistories);
		_comm.refContentText.add(&refContentText);
		_comm.refUndoMax.add(&refUndoMax);

		_comm.changed.add(&changed);
		_comm.refScenario.add(&summary);
		_win.addDisposeListener(new DL);

		.setupWindow(_win, _prop.var.replaceDlg);
	}
	private class DL : DisposeListener {
		override void widgetDisposed(DisposeEvent e) { mixin(S_TRACE);
			_cancel = true;
			_comm.changed.remove(&changed);
			_comm.refScenario.remove(&summary);
			_prop.var.etc.replaceTextNotIgnoreCase = _notIgnoreCase.getSelection();
			_prop.var.etc.replaceTextExactMatch = _exact.getSelection();
			_prop.var.etc.replaceTextIgnoreReturnCode = _ignoreReturnCode.getSelection();
			_prop.var.etc.replaceTextRegExp = _useRegex.getSelection();
			_prop.var.etc.replaceTextWildcard = _useWildcard.getSelection();
			_prop.var.etc.replaceTextSummary = _summary.getSelection();
			_prop.var.etc.replaceTextScenario = _scenario.getSelection();
			_prop.var.etc.replaceTextAuthor = _author.getSelection();
			_prop.var.etc.replaceTextMessage = _msg.getSelection();
			_prop.var.etc.replaceTextCardName = _cardName.getSelection();
			_prop.var.etc.replaceTextCardDescription = _cardDesc.getSelection();
			_prop.var.etc.replaceTextEventText = _event.getSelection();
			_prop.var.etc.replaceTextStart = _start.getSelection();
			_prop.var.etc.replaceTextVariableName = _varName.getSelection();
			_prop.var.etc.replaceTextVariableValue = _varValue.getSelection();
			_prop.var.etc.replaceTextExpression = _expression.getSelection();
			_prop.var.etc.replaceTextCoupon = _coupon.getSelection();
			_prop.var.etc.replaceTextGossip = _gossip.getSelection();
			_prop.var.etc.replaceTextEndScenario = _end.getSelection();
			_prop.var.etc.replaceTextAreaName = _area.getSelection();
			_prop.var.etc.replaceTextKeyCode = _keyCode.getSelection();
			_prop.var.etc.replaceTextCellName = _cellName.getSelection();
			_prop.var.etc.replaceTextCardGroup = _cardGroup.getSelection();
			_prop.var.etc.replaceTextFile = _file.getSelection();
			_prop.var.etc.replaceTextComment = _comment.getSelection();
			_prop.var.etc.replaceTextJptx = _jptx.getSelection();
			_prop.var.etc.replaceTextTextFile = _textFile.getSelection();

			foreach (cType; EnumMembers!CType) { mixin(S_TRACE);
				_prop.var.etc.searchContents(cType, _contents[cType].getSelection());
			}

			_prop.var.etc.replaceNameCoupon = _cCoupon.getSelection();
			_prop.var.etc.replaceNameGossip = _cGossip.getSelection();
			_prop.var.etc.replaceNameEndScenario = _cEnd.getSelection();
			_prop.var.etc.replaceNameKeyCode = _cKeyCode.getSelection();
			_prop.var.etc.replaceNameCellName = _cCellName.getSelection();
			_prop.var.etc.replaceNameCardGroup = _cCardGroup.getSelection();

			_prop.var.etc.searchUnusedFlag = _unuseFlag.getSelection();
			_prop.var.etc.searchUnusedStep = _unuseStep.getSelection();
			_prop.var.etc.searchUnusedVariant = _unuseVariant.getSelection();
			_prop.var.etc.searchUnusedArea = _unuseArea.getSelection();
			_prop.var.etc.searchUnusedBattle = _unuseBattle.getSelection();
			_prop.var.etc.searchUnusedPackage = _unusePackage.getSelection();
			_prop.var.etc.searchUnusedCast = _unuseCast.getSelection();
			_prop.var.etc.searchUnusedSkill = _unuseSkill.getSelection();
			_prop.var.etc.searchUnusedItem = _unuseItem.getSelection();
			_prop.var.etc.searchUnusedBeast = _unuseBeast.getSelection();
			_prop.var.etc.searchUnusedInfo = _unuseInfo.getSelection();
			_prop.var.etc.searchUnusedStart = _unuseStart.getSelection();
			_prop.var.etc.searchUnusedPath = _unusePath.getSelection();

			_comm.refTableViewStyle.remove(&refreshRangeTree2);
			_comm.refArea.remove(&refArea);
			_comm.refBattle.remove(&refBattle);
			_comm.refPackage.remove(&refPackage);
			_comm.refCast.remove(&refCast);
			_comm.refSkill.remove(&refSkill);
			_comm.refItem.remove(&refItem);
			_comm.refBeast.remove(&refBeast);
			_comm.refInfo.remove(&refInfo);
			_comm.delArea.remove(&delArea);
			_comm.delBattle.remove(&delBattle);
			_comm.delPackage.remove(&delPackage);
			_comm.delCast.remove(&delCast);
			_comm.delSkill.remove(&delSkill);
			_comm.delItem.remove(&delItem);
			_comm.delBeast.remove(&delBeast);
			_comm.delInfo.remove(&delInfo);
			_comm.refSearchHistories.remove(&refSearchHistories);
			_comm.refContentText.remove(&refContentText);
			_comm.refUndoMax.remove(&refUndoMax);
		}
	}
	private void changed() { mixin(S_TRACE);
		if (!_inUndo && !_inProc) { mixin(S_TRACE);
			_undo.reset();
		}
		auto thr = core.thread.Thread.getThis();
		if (&_uiThread !is &thr) return;
		if (_inGrep) return;
		if (_inUndo) return;
		if (_inProc) { mixin(S_TRACE);
			_cancel = true;
		} else { mixin(S_TRACE);
			_comm.refreshToolBar();
		}
	}
	private void undo() { mixin(S_TRACE);
		if (!_undo.canUndo) return;
		scope (exit) {
			_comm.refreshToolBar();
		}
		_inProc = true;
		scope (exit) _inProc = false;
		_inUndo = true;
		scope (exit) _inUndo = false;
		resultRedraw(false);
		scope (exit) resultRedraw(true);
		_undo.undo();
		if (_comm.updateJpy1Files(true)) { mixin(S_TRACE);
			_comm.sync.sync();
		}
	}
	private void redo() { mixin(S_TRACE);
		if (!_undo.canRedo) return;
		scope (exit) {
			_comm.refreshToolBar();
		}
		_inProc = true;
		scope (exit) _inProc = false;
		_inUndo = true;
		scope (exit) _inUndo = false;
		resultRedraw(false);
		scope (exit) resultRedraw(true);
		_undo.redo();
		if (_comm.updateJpy1Files(true)) { mixin(S_TRACE);
			_comm.sync.sync();
		}
	}
	/// 外部で検索した結果を表示する。
	void setFindResult(CWXPath parent, CWXPath[] paths, string kind) { mixin(S_TRACE);
		if (!_win || _win.isDisposed()) return;
		resultRedraw(false);
		scope (exit) resultRedraw(true);
		reset();
		initReplaceText();
		foreach (path; paths) { mixin(S_TRACE);
			auto addResultCWXPath = new AddResultCWXPath;
			addResultCWXPath.grepSumm = _grepSumm;
			addResultCWXPath.parent = parent;
			addResultCWXPath.path = path;
			addResultCWXPath.cwxPath = path.cwxPath(true);
			_results ~= addResultCWXPath;
		}
		refResultStatusImpl(cast(int)paths.length, kind);
		setResultStatus(_results.length);
	}
	private void search() { mixin(S_TRACE);
		auto c = _win.getDisplay().getFocusControl();
		_replMode = false;
		replaceImpl();
		if (c) .forceFocus(c, false);
	}
	private void replace() { mixin(S_TRACE);
		auto c = _win.getDisplay().getFocusControl();
		_replMode = true;
		replaceImpl();
		if (c) .forceFocus(_replace, false);
	}
	private void reset(bool removeColumns = true) { mixin(S_TRACE);
		if (!_inUndo && !_inProc) { mixin(S_TRACE);
			_undo.reset();
		}
		_result.setItemCount(0);
		_results = [];
		_grepCount = -1;
		if (removeColumns && _result.getColumnCount()) { mixin(S_TRACE);
			foreach (column; _result.getColumns()) { mixin(S_TRACE);
				column.dispose();
			}
			_result.setHeaderVisible(false);
		}
		if (_replMode) { mixin(S_TRACE);
			_status.setText(_prop.msgs.replResultEmpty);
		} else { mixin(S_TRACE);
			_status.setText(_prop.msgs.searchResultEmpty);
		}
		_lastFind = null;
		_comm.refreshToolBar();
	}
	@property
	private bool canFind() { mixin(S_TRACE);
		if (_inProc) return false;
		if (!_tabf || _tabf.isDisposed()) return false;
		auto sel = _tabf.getSelection();
		if (!sel) return false;
		if (sel is _tabText) { mixin(S_TRACE);
			return _summ && _from.getText().length > 0;
		} else if (sel is _tabID) { mixin(S_TRACE);
			return _summ && canReplID();
		} else if (sel is _tabPath) { mixin(S_TRACE);
			return _summ && _fromPath.getText().length > 0;
		} else if (sel is _tabContents) { mixin(S_TRACE);
			return _summ !is null;
		} else if (sel is _tabCoupon) { mixin(S_TRACE);
			return _summ !is null;
		} else if (sel is _tabUnuse) { mixin(S_TRACE);
			return _summ !is null;
		} else if (sel is _tabError) { mixin(S_TRACE);
			return _summ !is null;
		} else if (sel is _tabGrep) { mixin(S_TRACE);
			return true;
		} else assert (0);
	}
	@property
	private bool canReplace() { mixin(S_TRACE);
		if (_inProc) return false;
		if (!_tabf || _tabf.isDisposed()) return false;
		auto sel = _tabf.getSelection();
		if (!sel) return false;
		if (!_summ) return false;
		return canFind && ((sel is _tabText && !_ignoreReturnCode.getSelection()) || sel is _tabID || sel is _tabPath);
	}
	private bool canCancel() { mixin(S_TRACE);
		return _inProc;
	}
	private void replaceImpl() { mixin(S_TRACE);
		if (_inProc) return;
		_rUndo.length = 0;
		_after.length = 0;
		_refCall.length = 0;

		_notIgnoreCaseSel = _notIgnoreCase.getSelection();
		_exactSel = _exact.getSelection();
		_ignoreReturnCodeSel = _ignoreReturnCode.getSelection();
		_summarySel = _summary.getSelection();
		_scenarioSel = _scenario.getSelection();
		_authorSel = _author.getSelection();
		_msgSel = _msg.getSelection();
		_cardNameSel = _cardName.getSelection();
		_cardDescSel = _cardDesc.getSelection();
		_eventSel = _event.getSelection();
		_startSel = _start.getSelection();
		_varNameSel = _varName.getSelection();
		_varValueSel = _varValue.getSelection();
		_expressionSel = _expression.getSelection();
		_couponSel = _coupon.getSelection();
		_gossipSel = _gossip.getSelection();
		_endSel = _end.getSelection();
		_areaSel = _area.getSelection();
		_keyCodeSel = _keyCode.getSelection();
		_cellNameSel = _cellName.getSelection();
		_cardGroupSel = _cardGroup.getSelection();
		_fileSel = _file.getSelection();
		_commentSel = _comment.getSelection();
		_jptxSel = _jptx.getSelection();
		_textFileSel = _textFile.getSelection();
		_fromText = _from.getText();
		_toText = _to.getText();

		_flagDirOnRange = false;
		_incSearch.close();
		foreach (itm; _range.getItems()) { mixin(S_TRACE);
			if (itm.getChecked()) { mixin(S_TRACE);
				auto root = cast(FlagDir)itm.getData();
				if (root) { mixin(S_TRACE);
					_flagDirOnRange = true;
					break;
				}
			}
		}

		_cancel = false;
		if (_tabf.getSelection() is _tabText) { mixin(S_TRACE);
			replaceTextImpl();
		} else if (_tabf.getSelection() is _tabID) { mixin(S_TRACE);
			replaceIDImpl();
		} else if (_tabf.getSelection() is _tabPath) { mixin(S_TRACE);
			replacePathImpl();
		} else if (_tabf.getSelection() is _tabContents) { mixin(S_TRACE);
			searchContents();
		} else if (_tabf.getSelection() is _tabCoupon) { mixin(S_TRACE);
			searchCoupon();
		} else if (_tabf.getSelection() is _tabUnuse) { mixin(S_TRACE);
			searchUnuseImpl();
		} else if (_tabf.getSelection() is _tabError) { mixin(S_TRACE);
			searchErrorImpl();
		} else if (_tabf.getSelection() is _tabGrep) { mixin(S_TRACE);
			grepImpl();
		} else assert (0);
	}
	private bool cautionReplace(string name1, string name2) { mixin(S_TRACE);
		if (_replMode && _prop.var.etc.cautionBeforeReplace) { mixin(S_TRACE);
			auto dlg = new MessageBox(_win, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			dlg.setMessage(.tryFormat(_prop.msgs.cautionOfReplace, name1, name2));
			dlg.setText(_prop.msgs.dlgTitQuestion);
			return SWT.YES == dlg.open();
		}
		return true;
	}
	private void after() { mixin(S_TRACE);
		foreach (a; _refCall) a();
		foreach (a; _after) a();
		_comm.inReplaceText = false;
		if (_after.length) { mixin(S_TRACE);
			refContentText();
			if (_replMode) _comm.replText.call();
		}
		if (!_win.isDisposed() && _replMode && _rUndo.length) { mixin(S_TRACE);
			_undo ~= new UndoRepl(_rUndo, false, _refCall);
		}
		_rUndo = [];
		_refCall = [];
		_after = [];
		_comm.refreshToolBar();
	}
	@property
	private CWXPath[] searchRange() { mixin(S_TRACE);
		CWXPath[] r;
		void recurse(TreeItem itm) { mixin(S_TRACE);
			if (itm.getChecked()) { mixin(S_TRACE);
				auto data = cast(CWXPath)itm.getData();
				if (data) r ~= data;
			}
			foreach (child; itm.getItems()) { mixin(S_TRACE);
				recurse(child);
			}
		}
		_incSearch.close();
		foreach (itm; _range.getItems()) { mixin(S_TRACE);
			recurse(itm);
		}
		return r;
	}
	private void searchAll(CWXPath parent, CWXPath path, ref size_t count,
			void delegate(CWXPath parent, CWXPath path, ref size_t count, const(char)[] cwxPath) dlg, char[] cwxPath) { mixin(S_TRACE);
		if (cancel) return;
		while (true) {
			dlg(parent, path, count, cwxPath);
			// _rangeに含まれる要素は再帰的検索から除外する
			auto fdir = cast(FlagDir)path;
			if (fdir) { mixin(S_TRACE);
				foreach (i, o; fdir.flags) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "flag".dup, i));
				foreach (i, o; fdir.steps) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "step".dup, i));
				foreach (i, o; fdir.variants) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "variant".dup, i));
				auto subDirs = fdir.subDirs;
				if (subDirs.length == 1) { mixin(S_TRACE);
					path = subDirs[0];
					cwxPath = cpjoin2(cwxPath, "dir".dup, 0);
					continue; // 再帰回避
				} else { mixin(S_TRACE);
					foreach (i, o; subDirs) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "dir".dup, i));
				}
			}
			if (auto ec = cast(LocalVariableOwner)path) { mixin(S_TRACE);
				searchAll(parent, ec.flagDirRoot, count, dlg, cpjoin2(cwxPath, "variable".dup));
			}
			auto eto = cast(EventTreeOwner)path;
			if (eto) { mixin(S_TRACE);
				foreach (i, o; eto.trees) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "event".dup, i));
			}
			auto et = cast(EventTree)path;
			if (et) { mixin(S_TRACE);
				foreach (i, o; et.starts) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "".dup, i));
			}
			auto area = cast(Area)path;
			if (area) { mixin(S_TRACE);
				searchAll(parent, area.playerEvents, count, dlg, cpjoin2(cwxPath, "playercard".dup));
				foreach (i, o; area.cards) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "menucard".dup, i));
				foreach (i, o; area.backs) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "background".dup, i));
			}
			auto battle = cast(Battle)path;
			if (battle) { mixin(S_TRACE);
				searchAll(parent, battle.playerEvents, count, dlg, cpjoin2(cwxPath, "playercard".dup));
				foreach (i, o; battle.cards) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "enemycard".dup, i));
			}
			auto mo = cast(MotionOwner)path;
			if (mo) { mixin(S_TRACE);
				foreach (i, m; mo.motions) { mixin(S_TRACE);
					if (m.beast && 0 == m.beast.linkId) { mixin(S_TRACE);
						searchAll(parent, m.beast, count, dlg, cpjoin2(cpjoin2(cwxPath, "motion".dup, i), cpjoin2("".dup, "beastcard".dup, 0)));
					}
				}
			}
			auto c = cast(Content)path;
			if (c) { mixin(S_TRACE);
				foreach (i, o; c.dialogs) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "dialog".dup, i));
				foreach (i, o; c.backs) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "background".dup, i));
				auto next = c.next;
				if (next.length == 1) { mixin(S_TRACE);
					path = next[0];
					cwxPath = cpjoin2(cwxPath, "".dup, 0);
					continue; // 再帰回避
				} else { mixin(S_TRACE);
					foreach (i, o; next) searchAll(parent, o, count, dlg, cpjoin2(cwxPath, "".dup, i));
				}
			}
			break;
		}
	}

	@property
	private bool cancel() { mixin(S_TRACE);
		return _cancel;
	}

	private void setResultStatus(size_t count) { mixin(S_TRACE);
		if (count > 0) { mixin(S_TRACE);
			if (_replMode && _summ) { mixin(S_TRACE);
				_summ.changed();
				_comm.refUseCount.call();
			}
		}
		_inProc = false;
		_result.setItemCount(cast(int)_results.length);
		resultRedraw(true);
		refResultStatus(count, true);
	}
	private void refResultStatus(size_t count, bool force) { mixin(S_TRACE);
		if (!_prop.var.etc.searchResultRealtime) { mixin(S_TRACE);
			if (!force && 0 != (count % _prop.var.etc.searchResultRefreshCount)) return;
		}
		_display.syncExec(new class Runnable {
			void run() { mixin(S_TRACE);
				if (!_win || _win.isDisposed()) return;
				if (!_tabf.getSelection()) return;
				refResultStatusImpl(count, _tabf.getSelection().getText());
			}
		});
	}
	private void refResultStatusImpl(size_t count, string kind) { mixin(S_TRACE);
		string text;
		string num = .formatNum(count);
		if (_replMode) { mixin(S_TRACE);
			text = .tryFormat(_prop.msgs.replResult, num, kind);
		} else { mixin(S_TRACE);
			if (_grepSumm) { mixin(S_TRACE);
				text = .tryFormat(_prop.msgs.searchResultGrep2, .formatNum(_grepCount), num, _grepFile);
			} else if (_grepFile.length) { mixin(S_TRACE);
				text = .tryFormat(_prop.msgs.searchResultGrep1, .formatNum(_grepCount), num, _grepFile);
			} else if (0 <= _grepCount) { mixin(S_TRACE);
				text = .tryFormat(_prop.msgs.searchResultGrep3, .formatNum(_grepCount), num, kind);
			} else { mixin(S_TRACE);
				text = .tryFormat(_prop.msgs.searchResult, num, kind);
			}
		}
		_status.setText(text);
	}
	@property
	private bool[CWXPath] rangeTable() { mixin(S_TRACE);
		return _rangeTable;
	}
	private static bool dec(CWXPath path, in bool[CWXPath] range) { mixin(S_TRACE);
		assert (path);
		auto p = path in range;
		if (p) { mixin(S_TRACE);
			return *p;
		}
		if (!path.cwxParent) return true;
		return dec(path.cwxParent, range);
	}
	private void sortedPaths(T)(HashSet!T paths, CWXPath[] range, void delegate(SortableCWXPath!T) yield) { mixin(S_TRACE);
		if (!paths) return;
		if (paths.size < 512) { mixin(S_TRACE);
			SortableCWXPath!T.sortedPaths(paths, yield);
		} else { mixin(S_TRACE);
			// ある程度の数になるとシナリオ全体をサーチした方が速い
			T[CWXPath] owners;
			foreach (t; paths) owners[t.owner] = t;
			foreach (path; range) { mixin(S_TRACE);
				size_t count = 0;
				auto parent = .cwxPlace(path);
				searchAll(parent, path, count, (CWXPath parent, CWXPath path, ref size_t count, const(char)[] cwxPath) { mixin(S_TRACE);
					if (auto t = path in owners) { mixin(S_TRACE);
						count++;
						auto u = new SortableCWXPath!T(parent, *t, cwxPath.idup);
						yield(u);
					}
				}, path.cwxPath(true).dup);
			}
		}
	}
	private void replaceIDImpl2(ID)(ID from, ID to) { mixin(S_TRACE);
		if (!_summ) return;
		reset();
		_lastFind = _tabf.getSelection();

		_result.setHeaderVisible(true);
		auto mainColumn = new TableColumn(_result, SWT.NONE);
		mainColumn.setText(_prop.msgs.searchResultColumnMain);
		saveColumnWidth!("prop.var.etc.searchResultColumnMain")(_prop, mainColumn);
		auto subColumn = new TableColumn(_result, SWT.NONE);
		subColumn.setText(_prop.msgs.searchResultColumnParent);
		saveColumnWidth!("prop.var.etc.searchResultColumnParent")(_prop, subColumn);

		auto range = rangeTable;
		auto sRange = searchRange;

		auto uc = _summ.useCounter;
		auto users = uc.valueSet(from);
		_inProc = true;
		_comm.refreshToolBar();
		size_t count = 0;

		auto cursors = setWaitCursors(_win);
		auto thr = new core.thread.Thread({ mixin(S_TRACE);
			auto exit = new class Runnable {
				override void run() { mixin(S_TRACE);
					_inProc = false;
					if (!_win.isDisposed()) { mixin(S_TRACE);
						setResultStatus(cast(int)count);
						resetCursors(cursors);
					}
					if (count) { mixin(S_TRACE);
						_comm.replID.call();
						static if (is(ID:CouponId)) _comm.refCoupons.call();
						static if (is(ID:GossipId)) _comm.refGossips.call();
						static if (is(ID:CompleteStampId)) _comm.refCompleteStamps.call();
						static if (is(ID:KeyCodeId)) _comm.refKeyCodes.call();
						static if (is(ID:CellNameId)) _comm.refCellNames.call();
						static if (is(ID:CardGroupId)) _comm.refCardGroups.call();
					}
					after();
				}
			};
			scope (exit) _display.syncExec(exit);

			try { mixin(S_TRACE);
				// 以下は空白を許容しないため、空文字列に
				// 置換した時は配列ごと取り替える。
				//  * シナリオの開始条件
				//  * キャラクターの経歴
				//  * 口調選択クーポン
				//  * クーポン分岐のクーポン名
				//  * 評価条件
				//  * キーコード発火条件
				static if (is(ID:CouponId)) {
					CWXPath[] elems;
					string[][CWXPath] arrElem;
					Coupon[][CWXPath] csElem;
					SortableCWXPath!(typeof(users.toArray()[0]))[CWXPath] us;
				} else static if (is(ID:KeyCodeId)) {
					FKeyCode[][CWXPath] kcsElem;
					SortableCWXPath!(typeof(users.toArray()[0]))[CWXPath] us;
				}
				sortedPaths(users, sRange, (SortableCWXPath!(typeof(users.toArray()[0])) su) { mixin(S_TRACE);
					auto u = su.obj;
					if (!dec(u.owner, range)) return;
					if (_replMode) { mixin(S_TRACE);
						static if (is(ID:FlagId) || is(ID:StepId) || is(ID:VariantId)) {
							u.id = to;
							storeID(su.parent, u.owner, u, from, to, &u.id);
						} else static if (is(ID:CouponId)) {
							auto store = true;
							if (cast(string)to == "") { mixin(S_TRACE);
								if (auto c = cast(Summary)u.owner) { mixin(S_TRACE);
									if (c !in arrElem) { mixin(S_TRACE);
										elems ~= c;
										arrElem[c] = c.rCoupons;
										us[c] = su;
									}
									store = false;
								} else if (auto c = cast(CastCard)u.owner) { mixin(S_TRACE);
									if (c !in csElem) { mixin(S_TRACE);
										elems ~= c;
										csElem[c] = c.coupons.map!(a => new Coupon(a)).array();
										us[c] = su;
									}
									store = false;
								} else if (auto c = cast(SDialog)u.owner) { mixin(S_TRACE);
									if (c !in arrElem) { mixin(S_TRACE);
										elems ~= c;
										arrElem[c] = c.rCoupons;
										us[c] = su;
									}
									store = false;
								} else if (auto c = cast(Content)u.owner) { mixin(S_TRACE);
									auto d = c.detail;
									if (d.use(CArg.Coupons)) { mixin(S_TRACE);
										if (c !in csElem) { mixin(S_TRACE);
											elems ~= c;
											csElem[c] = c.coupons.map!(a => new Coupon(a)).array();
											us[c] = su;
										}
										store = false;
									}
									if (d.use(CArg.CouponNames)) { mixin(S_TRACE);
										if (c !in arrElem) { mixin(S_TRACE);
											elems ~= c;
											arrElem[c] = c.couponNames;
											us[c] = su;
										}
										store = false;
									}
								}
							}
							u.coupon = cast(string)to;
							if (store) storeID(su.parent, u.owner, u, cast(string)from, cast(string)to, &u.coupon);
						} else static if (is(ID:GossipId)) {
							u.gossip = cast(string)to;
							storeID(su.parent, u.owner, u, cast(string)from, cast(string)to, &u.gossip);
						} else static if (is(ID:CompleteStampId)) {
							u.completeStamp = cast(string)to;
							storeID(su.parent, u.owner, u, cast(string)from, cast(string)to, &u.completeStamp);
						} else static if (is(ID:KeyCodeId)) {
							auto store = true;
							if (cast(string)to == "") { mixin(S_TRACE);
								if (auto c = cast(EventTree)u.owner) { mixin(S_TRACE);
									if (c !in kcsElem) { mixin(S_TRACE);
										kcsElem[c] = c.keyCodes;
										us[c] = su;
									}
									store = false;
								}
							}
							u.keyCode = cast(string)to;
							if (store) storeID(su.parent, u.owner, u, cast(string)from, cast(string)to, &u.keyCode);
						} else static if (is(ID:CellNameId)) {
							u.cellName = cast(string)to;
							storeID(su.parent, u.owner, u, cast(string)from, cast(string)to, &u.cellName);
						} else static if (is(ID:CardGroupId)) {
							u.cardGroup = cast(string)to;
							storeID(su.parent, u.owner, u, cast(string)from, cast(string)to, &u.cardGroup);
						} else {
							u.id = to;
							storeID(su.parent, u.owner, u, from, to, &u.id);
						}
					}
					addResult(su.parent, u.owner, su.cwxPath, count);
				});
				static if (is(ID:CouponId)) {
					foreach (path; elems) { mixin(S_TRACE);
						if (auto p = path in arrElem) { mixin(S_TRACE);
							void f(SortableCWXPath!(typeof(users.toArray()[0])) su, CWXPath path, string[] arr) { mixin(S_TRACE);
								if (auto targ = cast(Summary)path) { mixin(S_TRACE);
									store(su.parent, targ, su.cwxPath, [new StrArrUndo(arr, targ.rCoupons.dup, (a) { targ.rCoupons = a.filter!(a => a != "").array(); })]);
									targ.rCoupons = targ.rCoupons.filter!(a => a != "").array();
								} else if (auto targ = cast(SDialog)path) { mixin(S_TRACE);
									store(su.parent, targ, su.cwxPath, [new StrArrUndo(arr, targ.rCoupons.dup, (a) { targ.rCoupons = a.filter!(a => a != "").array(); })]);
									targ.rCoupons = targ.rCoupons.filter!(a => a != "").array();
								} else if (auto targ = cast(Content)path) { mixin(S_TRACE);
									assert (targ.detail.use(CArg.CouponNames));
									store(su.parent, targ, su.cwxPath, [new StrArrUndo(arr, targ.couponNames.dup, (a) { targ.couponNames = a.filter!(a => a != "").array(); })]);
									targ.couponNames = targ.couponNames.filter!(a => a != "").array();
								} else assert (0);
							}
							f(us[path], path, *p);
						} else if (auto p = path in csElem) { mixin(S_TRACE);
							void f2(SortableCWXPath!(typeof(users.toArray()[0])) su, CWXPath path, Coupon[] cs) { mixin(S_TRACE);
								if (auto targ = cast(CastCard)path) { mixin(S_TRACE);
									store(su.parent, targ, su.cwxPath, [new CouponsUndo(cs, targ.coupons.dup, (a) { targ.coupons = a.filter!(a => a.name != "").array(); })]);
									targ.coupons = targ.coupons.filter!(a => a.name != "").array();
								} else if (auto targ = cast(Content)path) { mixin(S_TRACE);
									assert (targ.detail.use(CArg.Coupons));
									store(su.parent, targ, su.cwxPath, [new CouponsUndo(cs, targ.coupons.dup, (a) { targ.coupons = a.filter!(a => a.name != "").array(); })]);
									targ.coupons = targ.coupons.filter!(a => a.name != "").array();
								} else assert (0);
							}
							f2(us[path], path, *p);
						}
					}
				} else static if (is(ID:KeyCodeId)) {
					foreach (path, arr; kcsElem) { mixin(S_TRACE);
						void f(SortableCWXPath!(typeof(users.toArray()[0])) su, CWXPath path, FKeyCode[] arr) { mixin(S_TRACE);
							if (auto targ = cast(EventTree)path) { mixin(S_TRACE);
								store(su.parent, targ, su.cwxPath, [new FKeyCodesUndo(arr, targ.keyCodes.dup, (fkc) { targ.keyCodes = fkc.filter!(a => a.keyCode != "").array(); })]);
								targ.keyCodes = targ.keyCodes.filter!(a => a.keyCode != "").array();
							} else assert (0);
						}
						f(us[path], path, arr);
					}
				}
			} catch (Throwable e) {
				printStackTrace();
				debugln(e);
			}
		});
		thr.start();
	}
	private bool canReplID() {
		if (idKindIsString) { mixin(S_TRACE);
			return _fromID.getText() != "";
		} else { mixin(S_TRACE);
			return getID(_fromID, _fromIDVal, _fromIDTbl) !is 0;
		}
	}
	private ulong getID(Combo combo, Spinner spn, ulong[int] tbl) { mixin(S_TRACE);
		if (!_summ) return 0;
		if (combo.getSelectionIndex() == 0) { mixin(S_TRACE);
			return spn.getSelection();
		} else { mixin(S_TRACE);
			auto p = combo.getSelectionIndex() in tbl;
			if (!p) return 0;
			return *p;
		}
	}
	private string getIDName(Combo combo, Spinner spn) { mixin(S_TRACE);
		if (combo.getSelectionIndex() == 0) { mixin(S_TRACE);
			auto id = spn.getSelection();
			string objName = _idKind.getText();
			return .tryFormat(_prop.msgs.idValue, id, objName);
		} else { mixin(S_TRACE);
			return .tryFormat(_prop.msgs.replaceValue, combo.getText());
		}
	}
	private void replaceIDImpl() { mixin(S_TRACE);
		if (!_summ) return;
		int index = _idKind.getSelectionIndex();
		if (idKindIsString) { mixin(S_TRACE);
			string from = _fromID.getText();
			string to = _toID.getText();
			if (from == "") return;
			if (!cautionReplace(.tryFormat(_prop.msgs.replaceValue, from), to != "" ? .tryFormat(_prop.msgs.replaceValue, to) : _prop.msgs.emptyText)) return;
			if (from == to) _replMode = false;
			switch (_idKind.getSelectionIndex()) {
			case ID_FLAG: replaceIDImpl2(toFlagId(from), toFlagId(to)); break;
			case ID_STEP: replaceIDImpl2(toStepId(from), toStepId(to)); break;
			case ID_VARIANT: replaceIDImpl2(toVariantId(from), toVariantId(to)); break;
			case ID_COUPON: replaceIDImpl2(toCouponId(from), toCouponId(to)); break;
			case ID_GOSSIP: replaceIDImpl2(toGossipId(from), toGossipId(to)); break;
			case ID_COMPLETE_STAMP: replaceIDImpl2(toCompleteStampId(from), toCompleteStampId(to)); break;
			case ID_KEY_CODE: replaceIDImpl2(toKeyCodeId(from), toKeyCodeId(to)); break;
			case ID_CELL_NAME: replaceIDImpl2(toCellNameId(from), toCellNameId(to)); break;
			case ID_CARD_GROUP: replaceIDImpl2(toCardGroupId(from), toCardGroupId(to)); break;
			default: assert (0);
			}
		} else { mixin(S_TRACE);
			ulong from = getID(_fromID, _fromIDVal, _fromIDTbl);
			ulong to = getID(_toID, _toIDVal, _toIDTbl);
			if (0 == from) return;
			if (!cautionReplace(getIDName(_fromID, _fromIDVal), getIDName(_toID, _toIDVal))) return;
			if (from == to) _replMode = false;
			switch (_idKind.getSelectionIndex()) {
			case ID_AREA: replaceIDImpl2(toAreaId(from), toAreaId(to)); break;
			case ID_BATTLE: replaceIDImpl2(toBattleId(from), toBattleId(to)); break;
			case ID_PACKAGE: replaceIDImpl2(toPackageId(from), toPackageId(to)); break;
			case ID_CAST: replaceIDImpl2(toCastId(from), toCastId(to)); break;
			case ID_SKILL: replaceIDImpl2(toSkillId(from), toSkillId(to)); break;
			case ID_ITEM: replaceIDImpl2(toItemId(from), toItemId(to)); break;
			case ID_BEAST: replaceIDImpl2(toBeastId(from), toBeastId(to)); break;
			case ID_INFO: replaceIDImpl2(toInfoId(from), toInfoId(to)); break;
			default: assert (0);
			}
		}
	}
	private void replacePathImpl() { mixin(S_TRACE);
		if (!_summ) return;
		if (!_fromPath.getText().length) return;
		string toText = _toPath.getText();
		string fromName = .tryFormat(_prop.msgs.replaceValue, _fromPath.getText());
		string toName = toText.length ? .tryFormat(_prop.msgs.replaceValue, toText) : _prop.msgs.emptyPath;
		if (!cautionReplace(fromName, toName)) return;
		auto fromT = _fromPath.getText();
		auto toT = _toPath.getText();
		if (_summ.loadScaledImage) { mixin(S_TRACE);
			auto fromT2 = fromT.noScaledPath;
			if (fromT2 != "") fromT = fromT2;
		}
		auto from = toPathId(fromT);
		auto to = toPathId(toT);
		if (from == to) _replMode = false;
		reset();
		_lastFind = _tabf.getSelection();

		_result.setHeaderVisible(true);
		auto mainColumn = new TableColumn(_result, SWT.NONE);
		mainColumn.setText(_prop.msgs.searchResultColumnMain);
		saveColumnWidth!("prop.var.etc.searchResultColumnMain")(_prop, mainColumn);
		auto subColumn = new TableColumn(_result, SWT.NONE);
		subColumn.setText(_prop.msgs.searchResultColumnParent);
		saveColumnWidth!("prop.var.etc.searchResultColumnParent")(_prop, subColumn);

		auto range = rangeTable;
		auto sRange = searchRange;

		auto uc = _summ.useCounter;
		_inProc = true;
		_comm.refreshToolBar();
		size_t count = 0;

		auto cursors = setWaitCursors(_win);
		auto thr = new core.thread.Thread({ mixin(S_TRACE);
			string[2][] fromTos;
			auto exit = new class Runnable {
				override void run() { mixin(S_TRACE);
					_inProc = false;
					if (!_win.isDisposed()) { mixin(S_TRACE);
						setResultStatus(cast(int)count);
						resetCursors(cursors);
					}
					if (count) { mixin(S_TRACE);
						foreach (fromTo; .uniq(fromTos)) { mixin(S_TRACE);
							_comm.replPath.call(fromTo[0], fromTo[1]);
						}
					}
					if (_comm.updateJpy1Files(true)) { mixin(S_TRACE);
						_comm.sync.sync();
					}
					after();
				}
			};
			scope (exit) _display.syncExec(exit);

			try { mixin(S_TRACE);
				auto wildcard = Wildcard((cast(string)from).encodePath(), 0 == filenameCharCmp('A', 'a'));
				auto users = new HashSet!PathUser;
				foreach (key; uc.keys!PathId) { mixin(S_TRACE);
					if (wildcard.match((cast(string)key).encodePath())) { mixin(S_TRACE);
						foreach (u; uc.values(key)) { mixin(S_TRACE);
							if (!(cast(Jpy1Sec)u || cast(Jpdc)u) && !dec(u.owner, range)) continue;
							users.add(u);
						}
					}
				}
				sortedPaths(users, sRange, (SortableCWXPath!(typeof(users.toArray()[0])) su) { mixin(S_TRACE);
					auto u = su.obj;
					if (_replMode) { mixin(S_TRACE);
						auto id = u.path;
						if (!(u.id = to)) return;
						void store(PathUser u, string id) { mixin(S_TRACE);
							storeID!(PathUser, PathId)(su.parent, cast(CWXPath)u.owner, u, toPathId(id), to, (id) { mixin(S_TRACE);
								u.id = id;
							});
						}
						store(u, id);
						fromTos ~= [cast(string)id, cast(string)to];
					}
					addResult(su.parent, u.owner, su.cwxPath, count);
				});
			} catch (Throwable e) {
				printStackTrace();
				debugln(e);
			}
		});
		thr.start();
	}

	private void couponEditEnd(TableItem itm, int column, string text) { mixin(S_TRACE);
		_inProc = true;
		scope (exit) _inProc = false;
		if (itm.getImage() is _prop.images.couponNormal) { mixin(S_TRACE);
			renameCoupon(_comm, _summ, itm, toCouponId(itm.getText()), toCouponId(text), _undo, rangeTable, true, _result, this, _results);
			_comm.refCoupons.call();
		} else if (itm.getImage() is _prop.images.gossip) { mixin(S_TRACE);
			renameCoupon(_comm, _summ, itm, toGossipId(itm.getText()), toGossipId(text), _undo, rangeTable, true, _result, this, _results);
			_comm.refGossips.call();
		} else if (itm.getImage() is _prop.images.endScenario) { mixin(S_TRACE);
			renameCoupon(_comm, _summ, itm, toCompleteStampId(itm.getText()), toCompleteStampId(text), _undo, rangeTable, true, _result, this, _results);
			_comm.refCompleteStamps.call();
		} else if (itm.getImage() is _prop.images.keyCode) { mixin(S_TRACE);
			renameCoupon(_comm, _summ, itm, toKeyCodeId(itm.getText()), toKeyCodeId(text), _undo, rangeTable, true, _result, this, _results);
			_comm.refKeyCodes.call();
		} else if (itm.getImage() is _prop.images.backs) { mixin(S_TRACE);
			renameCoupon(_comm, _summ, itm, toCellNameId(itm.getText()), toCellNameId(text), _undo, rangeTable, true, _result, this, _results);
			_comm.refCellNames.call();
		} else if (itm.getImage() is _prop.images.cards) { mixin(S_TRACE);
			renameCoupon(_comm, _summ, itm, toCardGroupId(itm.getText()), toCardGroupId(text), _undo, rangeTable, true, _result, this, _results);
			_comm.refCardGroups.call();
		}
		_summ.changed();
		_comm.replText.call();
	}
	private bool canCouponEdit(TableItem itm, int column) { mixin(S_TRACE);
		return _lastFind is _tabCoupon;
	}
	static struct CouponParams {
		Image image;
		string name;
		size_t count;
	}
	static class AddResultCouponParams : AddResult {
		CouponParams params;
		override void run() { }
		override void setData(TableItem itm) { mixin(S_TRACE);
			itm.setImage(params.image);
			itm.setText(params.name);
			itm.setText(1, params.count.text());
		}
	}
	static class CouponUndo(User, KeyType) : Undo {
		private Commons _comm;
		private Summary _summ;
		private CouponParams[] _results;
		private KeyType _oldVal, _newVal;
		private ReplaceDialog _dlg;
		private bool _callRef;
		User[] users;
		this (Commons comm, Summary summ, KeyType oldVal, KeyType newVal, ReplaceDialog dlg, bool callRef) { mixin(S_TRACE);
			_comm = comm;
			_summ = summ;
			_oldVal = oldVal;
			_newVal = newVal;
			_dlg = dlg;
			_callRef = callRef;
			save();
		}
		private void save() { mixin(S_TRACE);
			if (!_dlg) return;
			_results = [];
			foreach (itm; _dlg._result.getItems()) { mixin(S_TRACE);
				_results ~= CouponParams(itm.getImage(), itm.getText(), itm.getText(1).to!uint());
			}
		}
		private void impl() { mixin(S_TRACE);
			if (_dlg) _dlg.resultRedraw(false);
			scope(exit) {
				if (_dlg) _dlg.resultRedraw(true);
			}
			auto results = _results;
			save();

			foreach (u; users) { mixin(S_TRACE);
				UseCounter.replaceID(_oldVal, u);
			}

			if (_dlg) { mixin(S_TRACE);
				_dlg._result.setItemCount(cast(int)results.length);
				_dlg._result.clearAll();
				_dlg._results = [];
				foreach (i, r; results) { mixin(S_TRACE);
					auto a = new AddResultCouponParams;
					a.params = r;
					_dlg._results ~= a;
				}
			}

			.swap(_oldVal, _newVal);
			if (_dlg) { mixin(S_TRACE);
				_dlg.refResultStatus(_dlg._result.getItemCount(), false);
			}
			if (_callRef) { mixin(S_TRACE);
				_comm.replText.call();
				_summ.changed();

				static if (is(KeyType:CouponId)) { mixin(S_TRACE);
					_comm.refCoupons.call();
				} else static if (is(KeyType:GossipId)) { mixin(S_TRACE);
					_comm.refGossips.call();
				} else static if (is(KeyType:CompleteStampId)) { mixin(S_TRACE);
					_comm.refCompleteStamps.call();
				} else static if (is(KeyType:KeyCodeId)) { mixin(S_TRACE);
					_comm.refKeyCodes.call();
				} else static if (is(KeyType:CellNameId)) { mixin(S_TRACE);
					_comm.refCellNames.call();
				} else static if (is(KeyType:CardGroupId)) { mixin(S_TRACE);
					_comm.refCardGroups.call();
				} else static assert (0);
			}
		}
		void undo() { mixin(S_TRACE);
			impl();
		}
		void redo() { mixin(S_TRACE);
			impl();
		}
		void dispose() { }
	}
	static auto renameCoupon(KeyType, RType)(Commons comm, Summary summ, TableItem itm, KeyType oldVal, KeyType newVal, UndoManager undoManager, in bool[CWXPath] rangeT, bool useRangeT, Table list, ReplaceDialog dlg, RType[] nameList, bool callRef = true) { mixin(S_TRACE);
		if (cast(string)oldVal == cast(string)newVal) return null;
		if (cast(string)newVal == "") return null;
		auto uc = summ.useCounter;
		auto undo = new CouponUndo!(ForeachType!(typeof(uc.values(oldVal))), KeyType)(comm, summ, oldVal, newVal, dlg, callRef);
		foreach (u; uc.values(oldVal)) { mixin(S_TRACE);
			if (!useRangeT || dec(u.owner, rangeT)) { mixin(S_TRACE);
				UseCounter.replaceID(newVal, u);
				undo.users ~= u;
				itm.setText(0, cast(string)newVal);
			}
		}
		foreach (u; uc.sub.values(oldVal)) { mixin(S_TRACE);
			if (!useRangeT || !u.ucOwner || dec(u.ucOwner, rangeT)) { mixin(S_TRACE);
				UseCounter.replaceID(newVal, u);
				undo.users ~= u;
			}
		}
		if (undoManager) undoManager ~= undo;
		// 変更の結果、他のキーと同一の名前になったら統合する
		auto i1 = list.indexOf(itm);
		foreach (i, itm2; list.getItems()) { mixin(S_TRACE);
			if (itm is itm2) continue;
			if (itm.getImage() !is itm2.getImage()) continue;
			if (itm.getText() == itm2.getText()) { mixin(S_TRACE);
				list.select(cast(int)i);
				list.showSelection();
				static if (is(RType:AddResult)) {
					(cast(AddResultUse)nameList[i]).use += (cast(AddResultUse)nameList[i1]).use;
				}
				list.clear(cast(int)i, list.getItemCount() - 1);
				list.setItemCount(list.getItemCount() - 1);
				std.algorithm.remove(nameList, i);
				break;
			}
		}
		return undo;
	}
	private void searchCouponImpl(KeyType)(in KeyType[] keys, UseCounter uc, in bool[CWXPath] rangeT, Image delegate() image, ref size_t count) { mixin(S_TRACE);
		foreach (key; std.algorithm.sort(keys.dup)) { mixin(S_TRACE);
			if (cancel) break;
			uint use = 0;
			foreach (u; uc.values(key)) { mixin(S_TRACE);
				if (dec(u.owner, rangeT)) { mixin(S_TRACE);
					use++;
				}
			}
			if (use) { mixin(S_TRACE);
				addResult(key, use, image, count);
			}
		}
	}
	private void searchCoupon() { mixin(S_TRACE);
		if (!_summ) return;
		size_t count = 0;
		reset();
		_lastFind = _tabf.getSelection();

		_result.setHeaderVisible(true);
		auto mainColumn = new TableColumn(_result, SWT.NONE);
		mainColumn.setText(_prop.msgs.searchResultColumnCoupon);
		saveColumnWidth!("prop.var.etc.searchResultColumnMain")(_prop, mainColumn);
		auto subColumn = new TableColumn(_result, SWT.NONE);
		subColumn.setText(_prop.msgs.searchResultColumnCouponCount);
		saveColumnWidth!("prop.var.etc.searchResultColumnCouponCount")(_prop, subColumn);

		bool cCouponSel = _cCoupon.getSelection();
		bool cKeyCodeSel = _cKeyCode.getSelection();
		bool cGossipSel = _cGossip.getSelection();
		bool cEndSel = _cEnd.getSelection();
		bool cCellName = _cCellName.getSelection();
		bool cCardGroup = _cCardGroup.getSelection();

		auto rangeT = rangeTable;
		auto uc = _summ.useCounter;
		void search() { mixin(S_TRACE);
			if (cCouponSel) { mixin(S_TRACE);
				searchCouponImpl(uc.keys!CouponId, uc, rangeT, &_prop.images.couponNormal, count);
			}
			if (cGossipSel) { mixin(S_TRACE);
				searchCouponImpl(uc.keys!GossipId, uc, rangeT, &_prop.images.gossip, count);
			}
			if (cEndSel) { mixin(S_TRACE);
				searchCouponImpl(uc.keys!CompleteStampId, uc, rangeT, &_prop.images.endScenario, count);
			}
			if (cKeyCodeSel) { mixin(S_TRACE);
				searchCouponImpl(uc.keys!KeyCodeId, uc, rangeT, &_prop.images.keyCode, count);
			}
			if (cCellName) { mixin(S_TRACE);
				searchCouponImpl(uc.keys!CellNameId, uc, rangeT, &_prop.images.backs, count);
			}
			if (cCardGroup) { mixin(S_TRACE);
				searchCouponImpl(uc.keys!CardGroupId, uc, rangeT, &_prop.images.cards, count);
			}
		}

		_inProc = true;
		_comm.refreshToolBar();
		auto cursors = setWaitCursors(_win);
		auto thr = new core.thread.Thread({ mixin(S_TRACE);
			auto exit = new class Runnable {
				override void run() { mixin(S_TRACE);
					_inProc = false;
					if (!_win.isDisposed()) { mixin(S_TRACE);
						setResultStatus(count);
						resetCursors(cursors);
					}
					after();
				}
			};
			scope (exit) _display.syncExec(exit);
			try { mixin(S_TRACE);
				search();
			} catch (Throwable e) {
				printStackTrace();
				debugln(e);
			}
		});
		thr.start();
	}

	private void searchContents() { mixin(S_TRACE);
		if (!_summ) return;
		size_t count = 0;
		reset();
		_lastFind = _tabf.getSelection();

		_result.setHeaderVisible(true);
		auto mainColumn = new TableColumn(_result, SWT.NONE);
		mainColumn.setText(_prop.msgs.searchResultColumnMain);
		saveColumnWidth!("prop.var.etc.searchResultColumnMain")(_prop, mainColumn);
		auto subColumn = new TableColumn(_result, SWT.NONE);
		subColumn.setText(_prop.msgs.searchResultColumnParent);
		saveColumnWidth!("prop.var.etc.searchResultColumnParent")(_prop, subColumn);

		auto range = searchRange;

		_inProc = true;
		_comm.refreshToolBar();
		auto cursors = setWaitCursors(_win);
		bool[CType] contents;
		foreach (type; EnumMembers!CType) { mixin(S_TRACE);
			contents[type] = _contents[type].getSelection();
		}
		auto thr = new core.thread.Thread({ mixin(S_TRACE);
			auto exit = new class Runnable {
				override void run() { mixin(S_TRACE);
					_inProc = false;
					if (!_win.isDisposed()) { mixin(S_TRACE);
						setResultStatus(count);
						resetCursors(cursors);
					}
					after();
				}
			};
			scope (exit) _display.syncExec(exit);
			try { mixin(S_TRACE);
				foreach (path; range) { mixin(S_TRACE);
					searchAll(.cwxPlace(path), path, count, (CWXPath parent, CWXPath path, ref size_t count, const(char)[] cwxPath) { mixin(S_TRACE);
						if (cancel) return;
						auto c = cast(Content)path;
						if (!c) return;
						assert (c.type in _contents);
						if (!contents[c.type]) return;
						addResult(parent, path, cwxPath.idup, count);
					}, path.cwxPath(true).dup);
				}
			} catch (Throwable e) {
				printStackTrace();
				debugln(e);
			}
		});
		thr.start();
	}

	private void searchUnuseImpl2(string ToId, T)(T[] all, ref size_t count) { mixin(S_TRACE);
		if (!_summ) return;
		foreach (o; all) { mixin(S_TRACE);
			if (cancel) break;
			if (_summ.useCounter.get(mixin(ToId)) == 0) { mixin(S_TRACE);
				addResult(.cwxPlace(o), o, o.cwxPath(true), count);
			}
		}
	}
	private void searchUnuseImpl() { mixin(S_TRACE);
		if (!_summ) return;
		_replMode = false;
		size_t count = 0;
		reset();
		_lastFind = _tabf.getSelection();

		_result.setHeaderVisible(true);
		auto mainColumn = new TableColumn(_result, SWT.NONE);
		mainColumn.setText(_prop.msgs.searchResultColumnMain);
		saveColumnWidth!("prop.var.etc.searchResultColumnMain")(_prop, mainColumn);
		auto subColumn = new TableColumn(_result, SWT.NONE);
		subColumn.setText(_prop.msgs.searchResultColumnParent);
		saveColumnWidth!("prop.var.etc.searchResultColumnParent")(_prop, subColumn);

		auto range = searchRange;

		bool unuseFlagSel = _unuseFlag.getSelection();
		bool unuseStepSel = _unuseStep.getSelection();
		bool unuseVariantSel = _unuseVariant.getSelection();
		bool unuseAreaSel = _unuseArea.getSelection();
		bool unuseBattleSel = _unuseBattle.getSelection();
		bool unusePackageSel = _unusePackage.getSelection();
		bool unuseCastSel = _unuseCast.getSelection();
		bool unuseSkillSel = _unuseSkill.getSelection();
		bool unuseItemSel = _unuseItem.getSelection();
		bool unuseBeastSel = _unuseBeast.getSelection();
		bool unuseInfoSel = _unuseInfo.getSelection();
		bool unuseStartSel = _unuseStart.getSelection();
		bool unusePathSel = _unusePath.getSelection();

		void search() { mixin(S_TRACE);
			if (unuseFlagSel) { mixin(S_TRACE);
				cwx.flag.Flag[] fs;
				.sortedWithPath(_summ.flagDirRoot.allFlags, _prop.var.etc.logicalSort, (cwx.flag.Flag f) { fs ~= f; });
				searchUnuseImpl2!("toFlagId(o.path)")(fs, count);
			}
			if (unuseStepSel) { mixin(S_TRACE);
				Step[] fs;
				.sortedWithPath(_summ.flagDirRoot.allSteps, _prop.var.etc.logicalSort, (Step f) { fs ~= f; });
				searchUnuseImpl2!("toStepId(o.path)")(fs, count);
			}
			if (unuseVariantSel) { mixin(S_TRACE);
				cwx.flag.Variant[] fs;
				.sortedWithPath(_summ.flagDirRoot.allVariants, _prop.var.etc.logicalSort, (cwx.flag.Variant f) { fs ~= f; });
				searchUnuseImpl2!("toVariantId(o.path)")(fs, count);
			}
			if (unuseAreaSel) { mixin(S_TRACE);
				searchUnuseImpl2!("toAreaId(o.id)")(_summ.areas, count);
			}
			if (unuseBattleSel) { mixin(S_TRACE);
				searchUnuseImpl2!("toBattleId(o.id)")(_summ.battles, count);
			}
			if (unusePackageSel) { mixin(S_TRACE);
				searchUnuseImpl2!("toPackageId(o.id)")(_summ.packages, count);
			}
			if (unuseCastSel) { mixin(S_TRACE);
				searchUnuseImpl2!("toCastId(o.id)")(_summ.casts, count);
			}
			if (unuseSkillSel) { mixin(S_TRACE);
				searchUnuseImpl2!("toSkillId(o.id)")(_summ.skills, count);
			}
			if (unuseItemSel) { mixin(S_TRACE);
				searchUnuseImpl2!("toItemId(o.id)")(_summ.items, count);
			}
			if (unuseBeastSel) { mixin(S_TRACE);
				searchUnuseImpl2!("toBeastId(o.id)")(_summ.beasts, count);
			}
			if (unuseInfoSel) { mixin(S_TRACE);
				searchUnuseImpl2!("toInfoId(o.id)")(_summ.infos, count);
			}
			if (unuseStartSel) { mixin(S_TRACE);
				foreach (path; range) { mixin(S_TRACE);
					searchAll(.cwxPlace(path), path, count, (CWXPath parent, CWXPath path, ref size_t count, const(char)[] cwxPath) { mixin(S_TRACE);
						if (cancel) return;
						auto et = cast(EventTree)path;
						if (et) { mixin(S_TRACE);
							foreach (i, s; et.starts[1 .. $]) { mixin(S_TRACE);
								if (et.startUseCounter.get(toStartId(s.name)) == 0) { mixin(S_TRACE);
									addResult(parent, s, cpjoin(cwxPath.idup, "", i + 1).idup, count);
								}
							}
						}
					}, path.cwxPath(true).dup);
				}
			}
			if (unusePathSel) { mixin(S_TRACE);
				auto files = _summ.notUsedFiles(_comm.skin, _prop.var.etc.ignorePaths, _prop.var.etc.logicalSort);
				foreach (file; files) { mixin(S_TRACE);
					if (cancel) break;
					addResult(encodePath(file), count);
				}
			}
		}

		_inProc = true;
		_comm.refreshToolBar();
		auto cursors = setWaitCursors(_win);
		auto thr = new core.thread.Thread({ mixin(S_TRACE);
			auto exit = new class Runnable {
				override void run() { mixin(S_TRACE);
					_inProc = false;
					if (!_win.isDisposed()) { mixin(S_TRACE);
						setResultStatus(count);
						resetCursors(cursors);
					}
					after();
				}
			};
			scope (exit) _display.syncExec(exit);
			try { mixin(S_TRACE);
				search();
			} catch (Throwable e) {
				printStackTrace();
				debugln(e);
			}
		});
		thr.start();
	}
	private void searchErrorImpl() { mixin(S_TRACE);
		if (!_summ) return;
		size_t count = 0;
		auto froot = _summ.flagDirRoot;
		auto sPath = _summ.scenarioPath;
		auto skin = _comm.skin;
		auto isClassic = _summ.legacy;
		auto wsnVer = _summ.dataVersion;
		auto targVer = _prop.var.etc.targetVersion;
		auto targIndex = _targetVer.getSelectionIndex();
		if (0 < targIndex) { mixin(S_TRACE);
			auto verInfo = _verInfos[targIndex - 1];
			isClassic = verInfo.isClassic;
			wsnVer = verInfo.wsnVer;
			targVer = verInfo.targVer;
		}

		reset();
		_lastFind = _tabf.getSelection();

		_result.setHeaderVisible(true);
		auto mainColumn = new TableColumn(_result, SWT.NONE);
		mainColumn.setText(_prop.msgs.searchResultColumnError);
		saveColumnWidth!("prop.var.etc.searchResultColumnMain")(_prop, mainColumn);
		auto subColumn = new TableColumn(_result, SWT.NONE);
		subColumn.setText(_prop.msgs.searchResultColumnParent);
		saveColumnWidth!("prop.var.etc.searchResultColumnParent")(_prop, subColumn);
		auto descColumn = new TableColumn(_result, SWT.NONE);
		descColumn.setText(_prop.msgs.searchResultColumnErrorDesc);
		saveColumnWidth!("prop.var.etc.searchResultColumnErrorDesc")(_prop, descColumn);

		auto range = searchRange;

		void search(CWXPath path) { mixin(S_TRACE);
			searchAll(.cwxPlace(path), path, count, (CWXPath parent, CWXPath path, ref size_t count, const(char)[] cwxPath) { mixin(S_TRACE);
				if (cancel) return;
				auto warnings = .warnings(_prop.parent, skin, _summ, path, isClassic, wsnVer, targVer);
				foreach (warning; warnings) { mixin(S_TRACE);
					addResult(parent, path, cwxPath.idup, count, warning);
				}
			}, path.cwxPath(true).dup);
		}
		void searchFileErrors() { mixin(S_TRACE);
			string sPath = _summ.scenarioPath;
			string[string] digests;
			void recurse(string file) { mixin(S_TRACE);
				try { mixin(S_TRACE);
					bool dir = .isDir(file);
					if (_summ.isSystemFile(file, dir)) return;
					if (containsPath(_prop.var.etc.ignorePaths, file.baseName())) return;
					if (dir) { mixin(S_TRACE);
						foreach (w; .sjisWarnings(_prop.parent, _summ, file.baseName(), _prop.msgs.dirName)) { mixin(S_TRACE);
							addResult(abs2rel(file, sPath), count, w);
						}
						foreach (string sub; clistdir(file)) { mixin(S_TRACE);
							recurse(file.buildPath(sub));
						}
					} else { mixin(S_TRACE);
						foreach (w; .sjisWarnings(_prop.parent, _summ, file.baseName(), _prop.msgs.fileName)) { mixin(S_TRACE);
							addResult(abs2rel(file, sPath), count, w);
						}

						auto size = file.getSize();
						if (!size) { mixin(S_TRACE);
							addResult(abs2rel(file, sPath), count, _prop.msgs.searchErrorEmptyFile);
							return;
						}

						foreach (w; .fileExtensionWarnings(_prop.parent, file)) { mixin(S_TRACE);
							addResult(abs2rel(file, sPath), count, w);
						}

						auto digest = file.fileToMD5Digest();
						if (!digest.length) return;
						digest = .format("%s-%s", digest, size);

						if (auto p = digest in digests) { mixin(S_TRACE);
							addResult(abs2rel(file, sPath), count, .tryFormat(_prop.msgs.searchErrorDupFile, .encodePath(abs2rel(*p, sPath))));
							return;
						}
						digests[digest] = file;

						try { mixin(S_TRACE);
							switch (file.extension().toLower()) {
							case ".jpy1": mixin(S_TRACE);
								Jpy1.load(_prop.parent, _summ ? _summ.scenarioPath : "", file);
								break;
							case ".jptx": mixin(S_TRACE);
								Jptx.load(_prop.parent, file);
								break;
							case ".jpdc": mixin(S_TRACE);
								Jpdc.load(_prop.parent, _summ ? _summ.scenarioPath : "", file);
								break;
							default:
								break;
							}
						} catch (EffectBoosterError e) { mixin(S_TRACE);
							printStackTrace();
							debugln(e);
							foreach (err; e.errors) { mixin(S_TRACE);
								addResult(abs2rel(file, sPath), count, .tryFormat(_prop.msgs.jpyErrorInfoWithoutFile, err.msg, err.line));
							}
						}
					}
				} catch (Exception e) {
					printStackTrace();
					debugln(e);
				}
			}
			recurse(sPath);
		}

		_inProc = true;
		_comm.refreshToolBar();
		auto cursors = setWaitCursors(_win);
		auto thr = new core.thread.Thread({ mixin(S_TRACE);
			auto exit = new class Runnable {
				override void run() { mixin(S_TRACE);
					_inProc = false;
					if (!_win.isDisposed()) { mixin(S_TRACE);
						setResultStatus(count);
						resetCursors(cursors);
					}
					after();
				}
			};
			scope (exit) _display.syncExec(exit);
			try { mixin(S_TRACE);
				foreach (path; range) { mixin(S_TRACE);
					search(path);
				}
				searchFileErrors();
			} catch (Throwable e) {
				printStackTrace();
				debugln(e);
			}
		});
		thr.start();
	}
	private void replaceTextImpl(CWXPath parent, CWXPath c, ref size_t count, const(char)[] cwxPath) { mixin(S_TRACE);
		if (cancel) return;
		auto icwxPath = cwxPath.idup;
		bool oldIgnoreMod = ignoreMod;
		ignoreMod = true;
		scope (exit) ignoreMod = oldIgnoreMod;
		size_t dmy = 0;
		auto commentable = cast(Commentable)c;
		if (_commentSel && commentable) { mixin(S_TRACE);
			Undo[] uArr;
			auto r = repl(parent, null, "", commentable.comment, &commentable.comment, count, uArr);
			if (r) { mixin(S_TRACE);
				if (_replMode) store(parent, c, icwxPath, uArr);
				addResultComment(parent, c, icwxPath, count, commentable.comment);
			}
		}
		auto eto = cast(EventTreeOwner)c;
		if (_commentSel && eto) { mixin(S_TRACE);
			Undo[] uArr;
			auto r = repl(parent, null, "", eto.commentForEvents, &eto.commentForEvents, count, uArr);
			if (r) { mixin(S_TRACE);
				if (_replMode) store(parent, c, icwxPath, uArr);
				addResultComment(parent, c, icwxPath, count, eto.commentForEvents);
			}
		}
		auto summ = cast(Summary)c;
		if (summ) { mixin(S_TRACE);
			bool sr = false;
			Undo[] uArr;
			if (_fileSel) { mixin(S_TRACE);
				sr |= replImagePaths(summ.imagePaths, &summ.imagePaths, count, uArr);
			}
			if (_summarySel) { mixin(S_TRACE);
				sr |= repl(parent, null, "", summ.desc, &summ.desc, count, uArr);
			}
			if (_scenarioSel) { mixin(S_TRACE);
				sr |= repl(parent, null, "", summ.scenarioName, &summ.scenarioName, count, uArr);
			}
			if (_authorSel) { mixin(S_TRACE);
				sr |= repl(parent, null, "", summ.author, &summ.author, count, uArr);
			}
			if (_couponSel) { mixin(S_TRACE);
				sr |= replRqCoupons(parent, null, "", summ, icwxPath, count, uArr);
			}
			if (sr) { mixin(S_TRACE);
				if (_replMode) store(parent, summ, icwxPath, uArr);
				addResult(parent, summ, icwxPath, dmy);
			}
		}
		auto cc = cast(CastCard)c;
		if (cc) { mixin(S_TRACE);
			Undo[] uArr;
			bool r = replCard!(CastCard)(parent, null, "", cc, icwxPath, count, uArr);
			r |= replCoupons(parent, null, "", cc, count, uArr);
			if (r) { mixin(S_TRACE);
				if (_replMode) store(parent, cc, icwxPath, uArr);
				addResult(parent, cc, icwxPath, dmy);
				if (_replMode) _refCall ~= { _comm.refCast.call(cc); };
			}
		}
		Undo[] nArr;
		auto eff = cast(EffectCard)c;
		if (eff) { mixin(S_TRACE);
			auto r = replCard(parent, eff, icwxPath, eff, icwxPath, count, nArr);
			if (r && _replMode) { mixin(S_TRACE);
				_refCall ~= { mixin(S_TRACE);
					if (auto aa = cast(SkillCard)eff) { mixin(S_TRACE);
						_comm.refSkill.call(aa);
					} else if (auto aa = cast(ItemCard)eff) { mixin(S_TRACE);
						_comm.refItem.call(aa);
					} else if (auto aa = cast(BeastCard)eff) { mixin(S_TRACE);
						_comm.refBeast.call(aa);
					} else assert (0);
				};
			}
		}
		auto info = cast(InfoCard)c;
		if (info) { mixin(S_TRACE);
			auto r = replCard(parent, info, icwxPath, info, icwxPath, count, nArr);
			if (r && _replMode) { mixin(S_TRACE);
				_refCall ~= { _comm.refInfo.call(info); };
			}
		}
		auto a = cast(AbstractArea)c;
		if (a) { mixin(S_TRACE);
			if (_areaSel) { mixin(S_TRACE);
				auto r = repl(parent, a, icwxPath, a.name, &a.name, count, nArr);
				if (r && _replMode) { mixin(S_TRACE);
					_refCall ~= { mixin(S_TRACE);
						if (auto aa = cast(Area)a) { mixin(S_TRACE);
							_comm.refArea.call(aa);
						} else if (auto aa = cast(Battle)a) { mixin(S_TRACE);
							_comm.refBattle.call(aa);
						} else if (auto aa = cast(Package)a) { mixin(S_TRACE);
							_comm.refPackage.call(aa);
						} else assert (0);
					};
				}
			}
		}
		auto menu = cast(MenuCard)c;
		if (menu) { mixin(S_TRACE);
			replCard(parent, menu, icwxPath, menu, icwxPath, count, nArr);
		}
		auto back = cast(BgImage)c;
		if (back) { mixin(S_TRACE);
			replBgImage(parent, back, icwxPath, back, count, nArr);
		}
		auto fDir = cast(FlagDir)c;
		if (fDir && _varNameSel) { mixin(S_TRACE);
			Undo[] uArr = new Undo[0];
			bool r = replFlagDirName(parent, fDir, count, uArr);
			if (r) { mixin(S_TRACE);
				if (_replMode) store(parent, fDir, icwxPath, uArr);
				addResult(parent, fDir, icwxPath, dmy);
			}
		}
		auto f = cast(cwx.flag.Flag)c;
		if (f && (_varNameSel || _varValueSel)) { mixin(S_TRACE);
			Undo[] uArr = new Undo[0];
			bool r = false;
			if (_varNameSel) r |= replFlagName!(cwx.flag.Flag)(parent, f.parent, f, count, uArr);
			if (_varValueSel) r |= repl(parent, null, "", f.on, &f.on, count, uArr);
			if (_varValueSel) r |= repl(parent, null, "", f.off, &f.off, count, uArr);
			if (r) { mixin(S_TRACE);
				if (_replMode) store(parent, f, icwxPath, uArr);
				addResult(parent, f, icwxPath, dmy);
				if (_replMode) { mixin(S_TRACE);
					_refCall ~= { mixin(S_TRACE);
						_comm.refFlagAndStep.call([f], [], []);
					};
				}
			}
		}
		auto s = cast(Step)c;
		if (s && (_varNameSel || _varValueSel)) { mixin(S_TRACE);
			Undo[] uArr;
			bool r = false;
			if (_varNameSel) r |= replFlagName!Step(parent, s.parent, s, count, uArr);
			if (_varValueSel) { mixin(S_TRACE);
				foreach (i, v; s.values) { mixin(S_TRACE);
					r |= repl(parent, null, "", v, (string t) { s.setValue(cast(int)i, t); }, count, uArr);
				}
			}
			if (r) { mixin(S_TRACE);
				if (_replMode) store(parent, s, icwxPath, uArr);
				addResult(parent, s, icwxPath, dmy);
				if (_replMode) { mixin(S_TRACE);
					_refCall ~= { mixin(S_TRACE);
						_comm.refFlagAndStep.call([], [s], []);
					};
				}
			}
		}
		auto v = cast(cwx.flag.Variant)c;
		if (v && (_varNameSel || _varValueSel)) { mixin(S_TRACE);
			Undo[] uArr;
			bool r = false;
			if (_varNameSel) r |= replFlagName!(cwx.flag.Variant)(parent, v.parent, v, count, uArr);
			if (_varValueSel && v.type is VariantType.String) { mixin(S_TRACE);
				r |= repl(parent, null, "", v.strVal, &v.value, count, uArr);
			}
			if (r) { mixin(S_TRACE);
				if (_replMode) store(parent, v, icwxPath, uArr);
				addResult(parent, v, icwxPath, dmy);
				if (_replMode) { mixin(S_TRACE);
					_refCall ~= { mixin(S_TRACE);
						_comm.refFlagAndStep.call([], [], [v]);
					};
				}
			}
		}
		auto et = cast(EventTree) c;
		if (et) { mixin(S_TRACE);
			replFKeyCode(parent, et, icwxPath, et, icwxPath, count, nArr);
		}
		auto content = cast(Content) c;
		if (content) { mixin(S_TRACE);
			replContent(parent, content, icwxPath, count);
		}
	}
	private void initText(string from, string to) { mixin(S_TRACE);
		if (_useRegex.getSelection()) { mixin(S_TRACE);
			try { mixin(S_TRACE);
				_regex = .regex!(dstring)(toUTF32(from), _notIgnoreCase.getSelection() ? "gm" : "gim");
				_regexTarg = true;
				_toTemp = toUTF32(to);
			} catch (Exception e) {
				printStackTrace();
				debugln(e);
				MessageBox.showWarning
					(_prop.msgs.regexError ~ "\n" ~ e.msg,
					_prop.msgs.dlgTitWarning, _win);
				return;
			}
		} else if (_useWildcard.getSelection()) { mixin(S_TRACE);
			_wildcard = Wildcard(from, !_notIgnoreCase.getSelection());
		} else { mixin(S_TRACE);
			if (from == to) _replMode = false;
		}
	}
	private void exitText() { mixin(S_TRACE);
		if (!_win || _win.isDisposed()) return;
		_wildcard = null;
		_regex = typeof(_regex).init;
		_regexTarg = false;
		_toTemp = ""d;
	}
	private static bool addHist(Combo combo, void delegate(string[]) set,
			string[] delegate() get, int max, string text) { mixin(S_TRACE);
		if (text.length) { mixin(S_TRACE);
			string[] list = get();
			if (.contains(list, text)) { mixin(S_TRACE);
				list = cwx.utils.remove(list, text);
			}
			list = [text] ~ list;
			if (list.length > max) { mixin(S_TRACE);
				list = list[0 .. $ - 1];
			}
			set(list);
			setComboItems(combo, list);
			combo.select(0);
			return true;
		}
		return false;
	}
	private void initReplaceText() {
		_result.setHeaderVisible(true);
		auto mainColumn = new TableColumn(_result, SWT.NONE);
		mainColumn.setText(_prop.msgs.searchResultColumnMain);
		saveColumnWidth!("prop.var.etc.searchResultColumnMain")(_prop, mainColumn);
		auto subColumn = new TableColumn(_result, SWT.NONE);
		subColumn.setText(_prop.msgs.searchResultColumnParent);
		saveColumnWidth!("prop.var.etc.searchResultColumnParent")(_prop, subColumn);
	}
	private void replaceTextImpl() { mixin(S_TRACE);
		if (!_summ) return;
		string from = _from.getText();
		string to = _to.getText();
		if (!from.length) return;
		string fromName = from.length ? .tryFormat(_prop.msgs.replaceValue, from) : _prop.msgs.emptyText;
		string toName = to.length ? .tryFormat(_prop.msgs.replaceValue, to) : _prop.msgs.emptyText;
		if (!cautionReplace(fromName, toName)) return;
		initText(from, to);

		size_t count = 0;
		reset();
		_lastFind = _tabf.getSelection();

		initReplaceText();

		auto range = searchRange;
		_inProc = true;
		_comm.refreshToolBar();
		bool addH = addHist(_from, (string[] s) { _prop.var.etc.searchHistories = s; },
			{ return _prop.var.etc.searchHistories.dup; },
			_prop.var.etc.searchHistoryMax, from);
		if (_replMode) { mixin(S_TRACE);
			addH |= addHist(_to, (string[] s) { _prop.var.etc.replaceHistories = s; },
				{ return _prop.var.etc.replaceHistories.dup; },
				_prop.var.etc.searchHistoryMax, to);
		}
		_comm.refSearchHistories.call(this);
		if (addH && _sendReloadProps) _sendReloadProps();

		void search() { mixin(S_TRACE);
			foreach (path; range) { mixin(S_TRACE);
				searchAll(.cwxPlace(path), path, count, &replaceTextImpl, path.cwxPath(true).dup);
			}
			if (_jptxSel || _textFile) { mixin(S_TRACE);
				auto update = false;
				foreach (string file; .dirEntries(_summ.scenarioPath, SpanMode.depth, false)) { mixin(S_TRACE);
					if (cancel) break;
					auto ext = .extension(file);
					if (_jptxSel && cfnmatch(ext, ".jptx")) { mixin(S_TRACE);
						try { mixin(S_TRACE);
							bool isSJIS;
							auto errInfo = new EffectBoosterError;
							string value = readJPYFile(file, _prop.parent, errInfo, isSJIS);
							auto jText = jptxText(value);
							Undo[] uArr;
							string file2 = file;
							bool r = repl(null, null, "", jText, (string jText) { mixin(S_TRACE);
								string value = jptxText(value, jText);
								try { mixin(S_TRACE);
									writeJPYFile(file2, value, isSJIS, _comm.sync);
								} catch (Exception e) {
									printStackTrace();
									debugln(e);
								}
							}, count, uArr, true);
							if (r) { mixin(S_TRACE);
								update = true;
								file = abs2rel(file, _summ.scenarioPath);
								size_t dmy = 0;
								addResult(file, dmy);
								store(file, uArr);
							}
						} catch (Exception e) {
							printStackTrace();
							debugln(e);
						}
					}
					if (_textFileSel) { mixin(S_TRACE);
						foreach (txtExt; _prop.var.etc.plainTextFileExtensions) { mixin(S_TRACE);
							if (.cfnmatch(ext, txtExt)) { mixin(S_TRACE);
								bool isSJIS;
								auto value = readTextFile(file, isSJIS);
								Undo[] uArr;
								auto file2 = file;
								bool r = repl(null, null, "", value, (string value) { mixin(S_TRACE);
									try { mixin(S_TRACE);
										writeTextFile(file2, value, isSJIS, _comm.sync);
									} catch (Exception e) {
										printStackTrace();
										debugln(e);
									}
								}, count, uArr, true);
								if (r) { mixin(S_TRACE);
									update = true;
									file = abs2rel(file, _summ.scenarioPath);
									size_t dmy = 0;
									addResult(file, dmy);
									store(file, uArr);
								}
								break;
							}
						}
					}
				}
				if (update) { mixin(S_TRACE);
					_comm.sync.sync();
				}
			}
		}

		if (_replMode) _comm.inReplaceText = true;
		auto cursors = setWaitCursors(_win);
		auto thr = new core.thread.Thread({ mixin(S_TRACE);
			auto exit = new class Runnable {
				override void run() { mixin(S_TRACE);
					_inProc = false;
					exitText();
					if (!_win.isDisposed()) { mixin(S_TRACE);
						setResultStatus(count);
						resetCursors(cursors);
					}
					if (_replMode && !_after.length) { mixin(S_TRACE);
						_comm.inReplaceText = false;
						_comm.replText.call();
					}
					after();
				}
			};
			scope (exit) _display.syncExec(exit);

			try { mixin(S_TRACE);
				search();
			} catch (Throwable e) {
				printStackTrace();
				debugln(e);
			}
		});
		thr.start();
	}
	private void grepImpl() { mixin(S_TRACE);
		string from = _from.getText();
		auto dirBase = _grepDir.getText();
		auto dir = _prop.toAppAbs(dirBase);
		initText(from, "");
		_replMode = false;

		size_t count = 0;
		reset();
		_grepCount = 0;
		_lastFind = _tabf.getSelection();

		_result.setHeaderVisible(true);
		auto mainColumn = new TableColumn(_result, SWT.NONE);
		mainColumn.setText(_prop.msgs.searchResultColumnMain);
		saveColumnWidth!("prop.var.etc.searchResultColumnMain")(_prop, mainColumn);
		auto subColumn = new TableColumn(_result, SWT.NONE);
		subColumn.setText(_prop.msgs.searchResultColumnParent);
		saveColumnWidth!("prop.var.etc.searchResultColumnParent")(_prop, subColumn);
		auto scColumn = new TableColumn(_result, SWT.NONE);
		scColumn.setText(_prop.msgs.searchResultColumnScenario);
		saveColumnWidth!("prop.var.etc.searchResultColumnScenario")(_prop, scColumn);

		if (!dir.exists()) return;
		LoadOption opt;
		opt.doubleIO = _prop.var.etc.doubleIO;
		opt.cardOnly = false;
		opt.textOnly = true;
		opt.expandXMLs = false;
		opt.summaryOnly = true;
		opt.numberStepToVariantThreshold = _prop.var.etc.numberStepToVariantThreshold;
		foreach (b; _noSummText) { mixin(S_TRACE);
			if (b.getSelection()) { mixin(S_TRACE);
				opt.summaryOnly = false;
				break;
			}
		}
		if (0 == from.length) { mixin(S_TRACE);
			opt.summaryOnly = true;
		}
		bool subDir = _grepSubDir.getSelection();

		void findSumm(string summFile) { mixin(S_TRACE);
			if (cancel) return;
			_grepFile = summFile;
			scope (exit) _grepFile = "";
			refResultStatus(count, true);
			string[] errorFiles;
			Skin defSkin = .findSkin2(_prop, _prop.var.etc.defaultSkin, _prop.var.etc.defaultSkinName);
			auto summ = Summary.loadScenarioFromFile(_prop.parent, opt, errorFiles, summFile, _prop.tempPath, defSkin);
			if (!summ) return;
			_grepSkin = null;
			_grepSumm = summ;
			_grepCount++;
			scope (exit) {
				summ.delTemp(true);
				_grepSkin = null;
				_grepSumm = null;
				core.memory.GC.collect();
				core.memory.GC.minimize();
			}
			if (!_fromText.length) { mixin(S_TRACE);
				addResult(.cwxPlace(summ), summ, summ.cwxPath(true), count);
				return;
			}
			refResultStatus(count, true);
			auto range = rangeTree(summ);
			foreach (path; range) { mixin(S_TRACE);
				if (cancel) return;
				searchAll(.cwxPlace(path), path, count, &replaceTextImpl, path.cwxPath(true).dup);
			}
			if (_jptxSel || _textFile) { mixin(S_TRACE);
				foreach (string file; .dirEntries(summ.scenarioPath, SpanMode.depth, false)) { mixin(S_TRACE);
					if (cancel) break;
					auto ext = .extension(file);
					if (_jptxSel && cfnmatch(ext, ".jptx")) { mixin(S_TRACE);
						bool isSJIS;
						auto errInfo = new EffectBoosterError;
						try { mixin(S_TRACE);
							string value = readJPYFile(file, _prop.parent, errInfo, isSJIS);
							auto jText = jptxText(value);
							Undo[] uArr;
							bool r = repl(null, null, "", jText, null, count, uArr, true);
							if (r) { mixin(S_TRACE);
								file = abs2rel(file, summ.scenarioPath);
								size_t dmy = 0;
								addResult(file, dmy);
							}
						} catch (Exception e) {
							printStackTrace();
							debugln(e);
						}
					}
					if (_textFileSel) { mixin(S_TRACE);
						foreach (txtExt; _prop.var.etc.plainTextFileExtensions) { mixin(S_TRACE);
							if (.cfnmatch(ext, txtExt)) { mixin(S_TRACE);
								bool isSJIS;
								auto value = readTextFile(file, isSJIS);
								Undo[] uArr;
								bool r = repl(null, null, "", value, null, count, uArr, true);
								if (r) { mixin(S_TRACE);
									file = abs2rel(file, summ.scenarioPath);
									size_t dmy = 0;
									addResult(file, dmy);
								}
								break;
							}
						}
					}
				}
			}
			refResultStatus(count, true);
		}
		void recurse(string dir, uint rec) { mixin(S_TRACE);
			if (cancel) return;
			if (dir.buildPath("Summary.xml").exists() || dir.buildPath("Summary.wsm").exists()) { mixin(S_TRACE);
				try { mixin(S_TRACE);
					findSumm(dir);
				} catch (Exception e) {
					printStackTrace();
					debugln(dir);
					debugln(e);
				}
			}
			if (!subDir && 0 != rec) return;
			foreach (file; clistdir(dir)) { mixin(S_TRACE);
				if (cancel) break;
				try { mixin(S_TRACE);
					file = dir.buildPath(file);
					if (file.isDir()) { mixin(S_TRACE);
						if (subDir || 0 == rec) recurse(file, rec + 1);
					} else { mixin(S_TRACE);
						auto ext = file.extension();
						if (cfnmatch(ext, ".wsn") || cfnmatch(ext, ".zip") || cfnmatch(ext, ".lzh") || cfnmatch(ext, ".lha") || (canUncab && cfnmatch(ext, ".cab"))) { mixin(S_TRACE);
							findSumm(file);
						}
					}
				} catch (Exception e) {
					printStackTrace();
					debugln(file);
					debugln(e);
				}
			}
		}
		_inProc = true;
		_inGrep = true;
		_comm.refreshToolBar();
		bool addH = addHist(_from, (string[] s) { _prop.var.etc.searchHistories = s; },
			{ return _prop.var.etc.searchHistories.dup; },
			_prop.var.etc.searchHistoryMax, from);
		addH |= addHist(_grepDir, (string[] s) { _prop.var.etc.grepDirHistories = s; },
			{ return _prop.var.etc.grepDirHistories.dup; },
			_prop.var.etc.searchHistoryMax, dirBase);
		_comm.refSearchHistories.call(this);
		if (addH && _sendReloadProps) _sendReloadProps();
		auto cursors = setWaitCursors(_win);
		auto thr = new core.thread.Thread({ mixin(S_TRACE);
			auto exit = new class Runnable {
				override void run() { mixin(S_TRACE);
					_inProc = false;
					_inGrep = false;
					exitText();
					if (!_win.isDisposed()) { mixin(S_TRACE);
						setResultStatus(count);
						resetCursors(cursors);
					}
					after();
				}
			};
			scope (exit) _display.syncExec(exit);
			try { mixin(S_TRACE);
				recurse(dir, 0);
			} catch (Throwable e) {
				printStackTrace();
				debugln(e);
			}
		});
		thr.start();
	}
	private void refSearchHistories(Object sender) { mixin(S_TRACE);
		if (sender is this) return;
		updateFromHistory();
		updateToHistory();
		setComboItems(_grepDir, _prop.var.etc.grepDirHistories.dup);
	}
	private void updateFromHistory() {
		string ft = _from.getText();
		_from.removeAll();
		foreach (hist; _prop.var.etc.searchHistories) {
			if (!_fromIncSearch.match(hist)) continue;
			_from.add(hist);
		}
		_from.setText(ft);
	}
	private void updateToHistory() {
		string tt = _to.getText();
		_to.removeAll();
		foreach (hist; _prop.var.etc.replaceHistories) {
			if (!_toIncSearch.match(hist)) continue;
			_to.add(hist);
		}
		_to.setText(tt);
	}
	private Regex!(dchar) _regex;
	private bool _regexTarg = false;
	private Wildcard _wildcard = null;
	private dstring _toTemp = ""d;
	private string fTextRepl(string s) { mixin(S_TRACE);
		if (_regexTarg) { mixin(S_TRACE);
			return toUTF8(std.regex.replace(toUTF32(s), _regex, _toTemp));
		}
		string to = _toText;
		if (_wildcard) { mixin(S_TRACE);
			return _wildcard.replace(s, to);
		}
		string from = _fromText;
		if (_notIgnoreCaseSel) { mixin(S_TRACE);
			if (_exactSel) { mixin(S_TRACE);
				return 0 == cmp(s, from) ? to : from;
			} else { mixin(S_TRACE);
				return .replace(s, from, to);
			}
		} else { mixin(S_TRACE);
			if (_exactSel) { mixin(S_TRACE);
				return 0 == icmp(s, from) ? to : from;
			} else { mixin(S_TRACE);
				return .ireplace(s, from, to);
			}
		}
	}
	private size_t fTextCount(string s) { mixin(S_TRACE);
		if (_ignoreReturnCodeSel) s = .singleLine(s);
		if (_regexTarg) { mixin(S_TRACE);
			size_t c = 0;
			try { mixin(S_TRACE);
				foreach (m; std.regex.match(toUTF32(s), _regex)) { mixin(S_TRACE);
					c++;
				}
			} catch (Throwable e) {
				printStackTrace();
 				debugln(_fromText, " -> ", s);
				throw e;
			}
			return c;
		}
		if (_wildcard) { mixin(S_TRACE);
			return _wildcard.count(s);
		}
		string from = _fromText;
		if (_notIgnoreCaseSel) { mixin(S_TRACE);
			if (_exactSel) { mixin(S_TRACE);
				return 0 == cmp(s, from) ? 1 : 0;
			} else { mixin(S_TRACE);
				return .count(s, from);
			}
		} else { mixin(S_TRACE);
			if (_exactSel) { mixin(S_TRACE);
				return 0 == icmp(s, from) ? 1 : 0;
			} else { mixin(S_TRACE);
				return .icount(s, from);
			}
		}
	}
	private void exit() { mixin(S_TRACE);
		_win.close();
	}
	private bool _replMode = false;

	private bool canOpenPath() { mixin(S_TRACE);
		auto sels = _result.getSelection();
		if (!sels.length) return false;
		foreach (itm; sels) { mixin(S_TRACE);
			auto d = itm.getData();
			if (cast(CWXPathString)d !is null || cast(FilePathString)d !is null) { mixin(S_TRACE);
				return true;
			}
		}
		return false;
	}
	private void openPath(SelectionEvent e) { mixin(S_TRACE);
		openPath((e.stateMask & SWT.SHIFT) != 0);
	}
	private void openPath(bool shift) { mixin(S_TRACE);
		auto i = _result.getSelectionIndex();
		if (-1 == i) return;
		foreach (itm; [_result.getItem(i)] ~ _result.getItems()) { mixin(S_TRACE);
			auto d = itm.getData();
			auto rp = cast(CWXPathString)d;
			if (rp) { mixin(S_TRACE);
				if (auto jpy1 = cast(Jpy1Sec)rp.path) { mixin(S_TRACE);
					_comm.openFilePath(.abs2rel(jpy1.fPath, jpy1.sPath), false, true);
					return;
				} else if (auto jpdc = cast(Jpdc)rp.path) { mixin(S_TRACE);
					_comm.openFilePath(.abs2rel(jpdc.jpdcPath, jpdc.sPath), false, true);
					return;
				}
				auto path = rp.array;
				auto searchOpenDialog = _prop.var.etc.searchOpenDialog;
				if (shift) searchOpenDialog = !searchOpenDialog;
				if (searchOpenDialog) { mixin(S_TRACE);
					path = cpaddattr(path, "opendialog");
				}
				path = cpaddattr(path, "only");
				if (rp.scPath !is null) { mixin(S_TRACE);
					exec("\"" ~ _prop.parent.appPath ~ "\" \"" ~ rp.scPath ~ "\" " ~ path);
					return;
				}
				if (!_summ) return;
				try { mixin(S_TRACE);
					if (_comm.openCWXPath(path, false)) { mixin(S_TRACE);
						if (!searchOpenDialog) _win.setActive();
						return;
					}
				} catch (Exception e) {
					printStackTrace();
					debugln(e);
				}
				MessageBox.showWarning(.tryFormat(_prop.msgs.cwxPathOpenError, path), _prop.msgs.dlgTitWarning, _win);
				return;
			}
			auto p = cast(FilePathString)d;
			if (p) { mixin(S_TRACE);
				auto path = nabs(std.path.buildPath(p.scPath, p.array));
				if (p.scPath !is null && !(_summ && .nabs(p.scPath) == .nabs(_summ.scenarioPath))) { mixin(S_TRACE);
					exec("\"" ~ _prop.parent.appPath ~ "\" -selectfile \"" ~ p.array ~ "\" \"" ~ p.scPath ~ "\"");
					return;
				}
				if (_comm.openFilePath(p.array, false, true)) { mixin(S_TRACE);
					_win.setActive();
					return;
				}
				MessageBox.showWarning(.tryFormat(_prop.msgs.filePathOpenError, path), _prop.msgs.dlgTitWarning, _win);
				return;
			}
		}
	}
	Image fimage(string file, Skin skin) { mixin(S_TRACE);
		return .fimage(_prop, file, skin);
	}
	private void copyResult() { mixin(S_TRACE);
		string[] t;
		foreach (itm; _result.getSelection()) { mixin(S_TRACE);
			string[] line;
			foreach (i; 0 .. _result.getColumnCount()) { mixin(S_TRACE);
				line ~= itm.getText(i).replace("\n", .newline);
			}
			t ~= std.string.join(line, "\t");
		}
		if (!t.length) return;
		auto text = new ArrayWrapperString(std.string.join(t, .newline));
		_comm.clipboard.setContents([text], [TextTransfer.getInstance()]);
		_comm.refreshToolBar();
	}
	private void addResult(string path, ref size_t count, string desc = "") { mixin(S_TRACE);
		if (cancel) return;
		count++;
		auto addResultPath = new AddResultPath;
		if (!_grepSkin) _grepSkin = findSkin(_comm, _prop, _grepSumm);
		addResultPath.grepSumm = _grepSumm;
		addResultPath.grepSkin = _grepSkin;
		addResultPath.path = path;
		addResultPath.desc = desc;
		addResultPath.count = count;
		_display.syncExec(addResultPath);
	}
	private void refContentText() { mixin(S_TRACE);
		if (_win.isDisposed()) return;
		foreach (itm; _result.getItems()) { mixin(S_TRACE);
			auto c = cast(CWXPathString)itm.getData();
			if (c) { mixin(S_TRACE);
				string text1, text2;
				Image img1, img2;
				auto includePath = !cast(Content)c.path && cast(Content)c.path.cwxParent;
				getPathParams(_summ, c.parent, c.path, text1, text2, img1, img2, includePath ? c.path.cwxParent : null);
				itm.setImage(0, img1);
				itm.setText(0, text1);
				itm.setImage(1, img2);
				itm.setText(1, text2);
			}
		}
	}
	private void getPathParams(Summary grepSumm, CWXPath parent, CWXPath path, out string text, out string text2, out Image img, out Image img2, CWXPath includeRoutePath) { mixin(S_TRACE);
		if (!path) return;
		auto summ = grepSumm ? grepSumm : _summ;
		getSymbols(_comm, summ, path, true, text, img);
		if (parent && path.cwxParent && !cast(Summary)path.cwxParent) { mixin(S_TRACE);
			if (_prop.var.etc.showRouteOfSearchResult) { mixin(S_TRACE);
				// 所属先を最上位からの経路で表示する
				auto par = path.cwxParent;
				CWXPath[] route;
				if (includeRoutePath) route ~= includeRoutePath;
				while (par && !cast(Summary)par) { mixin(S_TRACE);
					if (auto c = cast(Content)par) { mixin(S_TRACE);
						if (c.type !is CType.Start) { mixin(S_TRACE);
							par = par.cwxParent;
							continue;
						}
					} else if (auto dir = cast(FlagDir)par) { mixin(S_TRACE);
						if (cast(FlagDir)dir.cwxParent) { mixin(S_TRACE);
							par = par.cwxParent;
							continue;
						}
					}
					route ~= par;
					par = par.cwxParent;
				}
				char[] routeStr;
				CWXPath before = null;
				foreach_reverse (i, p; route) { mixin(S_TRACE);
					string text3;
					Image img3;
					getSymbols(_comm, summ, p, false, text3, img3);
					if (routeStr.length) { mixin(S_TRACE);
						routeStr ~= " > ";
					} else { mixin(S_TRACE);
						img2 = img3;
					}
					routeStr ~= text3;
					before = p;
				}
				text2 = .assumeUnique(routeStr);
			} else { mixin(S_TRACE);
				// 所属先を最上位だけ表示する
				getSymbols(_comm, summ, parent, false, text2, img2);
			}
		}
	}
	private void addResult(CWXPath parent, CWXPath path, string cwxPath, ref size_t count, string desc = "") { mixin(S_TRACE);
		if (cancel) return;
		count++;
		auto addResultCWXPath = new AddResultCWXPath;
		addResultCWXPath.grepSumm = _grepSumm;
		addResultCWXPath.parent = parent;
		addResultCWXPath.path = path;
		addResultCWXPath.cwxPath = cwxPath;
		addResultCWXPath.desc = desc;
		addResultCWXPath.count = count;
		_display.syncExec(addResultCWXPath);
	}
	private void addResult(string name, Image delegate() image, ref size_t count) { mixin(S_TRACE);
		if (cancel) return;
		count++;
		auto addResultMsg = new AddResultMsg;
		addResultMsg.name = name;
		addResultMsg.image = image;
		addResultMsg.count = count;
		_display.syncExec(addResultMsg);
	}
	private void addResult(string name, uint use, Image delegate() image, ref size_t count) { mixin(S_TRACE);
		if (cancel) return;
		count++;
		auto addResultUse = new AddResultUse;
		addResultUse.name = name;
		addResultUse.use = use;
		addResultUse.image = image;
		addResultUse.count = count;
		_display.syncExec(addResultUse);
	}
	private void addResultComment(CWXPath parent, CWXPath path, string cwxPath, ref size_t count, string comment) { mixin(S_TRACE);
		if (cancel) return;
		count++;
		auto add = new AddResultComment;
		add.grepSumm = _grepSumm;
		add.parent = parent;
		add.path = path;
		add.cwxPath = cwxPath;
		add.comment = comment;
		add.count = count;
		_display.syncExec(add);
	}

	private bool repl(CWXPath parent, CWXPath path, lazy string cwxPath, string text, void delegate(string) set, ref size_t count, ref Undo[] uArr, bool storeToArr = false, bool isVariablePath = false) { mixin(S_TRACE);
		auto isLocal = isVariablePath && _prop.sys.isLocalVariable(text);
		auto origText = text;
		if (isLocal) text = text[(FlagDir.SEPARATOR ~ _prop.sys.localVariablePrefix ~ FlagDir.SEPARATOR).length .. $];

		auto c = fTextCount(text);
		count += c;
		if (c > 0) { mixin(S_TRACE);
			string n;
			if (_replMode && set) { mixin(S_TRACE);
				n = fTextRepl(text);
				if (isLocal) n = FlagDir.SEPARATOR ~ _prop.sys.localVariablePrefix ~ FlagDir.SEPARATOR ~ n;
				if (!path || storeToArr) uArr ~= new StrUndo(origText, n, set);
				set(n);
			}
			if (path) { mixin(S_TRACE);
				if (_replMode && set && !storeToArr) store(parent, path, cwxPath, origText, n, set);
				size_t dmy = 0;
				addResult(parent, path, cwxPath, dmy);
			}
			return true;
		}
		return false;
	}
	private class CardImagesUndo : TUndo!(CardImage[]) {
		this (CardImage[] old, CardImage[] n, void delegate(in CardImage[]) set) { mixin(S_TRACE);
			super (old.dup, n.dup, (v) { set(v); }, (CardImage[] v) { return v.dup; });
		}
	}
	private bool replImagePaths(CardImage[] imgPaths, void delegate(in CardImage[]) set, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		bool r = false;
		imgPaths = imgPaths.dup;
		auto o = imgPaths.dup;
		foreach (i, imgPath; imgPaths) { mixin(S_TRACE);
			if (imgPath.type !is CardImageType.File) continue;
			if (isBinImg(imgPath.path)) continue;
			Undo[] uArr2;
			r |= replFilePath(imgPath.path, (text) {
				imgPaths[i] = new CardImage(text, imgPath.positionType);
			}, count, uArr2);
		}
		if (r) { mixin(S_TRACE);
			uArr ~= new CardImagesUndo(o, imgPaths, set);
			set(imgPaths);
		}
		return r;
	}
	private bool replFilePath(string text, void delegate(string) set, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		auto c = fTextCount(encodePath(text));
		count += c;
		if (c > 0) { mixin(S_TRACE);
			if (_replMode) { mixin(S_TRACE);
				auto o = decodePath(text);
				string n = fTextRepl(o);
				uArr ~= new StrUndo(o, n, set);
				set(n);
			}
			return true;
		}
		return false;
	}

	private bool replFlagDirName(CWXPath parent, FlagDir dir, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		if (dir.localOwner && !dir.parent) { mixin(S_TRACE);
			return false;
		}
		string text = dir.name;
		auto c = fTextCount(text);
		count += c;
		if (c > 0) { mixin(S_TRACE);
			if (_replMode) { mixin(S_TRACE);
				auto parentDir = dir.parent;
				auto flags = dir.allFlags;
				auto steps = dir.allSteps;
				auto variants = dir.allVariants;
				auto oldFPaths = .map!(a => a.path)(flags).array();
				auto oldSPaths = .map!(a => a.path)(steps).array();
				auto oldVPaths = .map!(a => a.path)(variants).array();
				string[] newFPaths;
				string[] newSPaths;
				string[] newVPaths;
				string n = fTextRepl(text);
				uArr ~= new StrUndo(text, n, (string name) { mixin(S_TRACE);
					if (parentDir) { mixin(S_TRACE);
						dir.name = parentDir.validName(name);
					} else { mixin(S_TRACE);
						dir.name = name;
					}
					if (!_prop.sys.isLocalVariable(dir.path)) return;
					foreach (oldPath, newPath; .zip(oldFPaths, newFPaths)) { mixin(S_TRACE);
						auto oldID = cwx.flag.Flag.toID(oldPath);
						auto newID = cwx.flag.Flag.toID(newPath);
						dir.useCounter.deleteID(newID);
						dir.useCounter.createID(oldID);
					}
					foreach (oldPath, newPath; .zip(oldSPaths, newSPaths)) { mixin(S_TRACE);
						auto oldID = Step.toID(oldPath);
						auto newID = Step.toID(newPath);
						dir.useCounter.deleteID(newID);
						dir.useCounter.createID(oldID);
					}
					foreach (oldPath, newPath; .zip(oldVPaths, newVPaths)) { mixin(S_TRACE);
						auto oldID = cwx.flag.Variant.toID(oldPath);
						auto newID = cwx.flag.Variant.toID(newPath);
						dir.useCounter.deleteID(newID);
						dir.useCounter.createID(oldID);
					}
					std.algorithm.swap(oldFPaths, newFPaths);
					std.algorithm.swap(oldSPaths, newSPaths);
					std.algorithm.swap(oldVPaths, newVPaths);
				});
				dir.name = parentDir.validName(n);
				_refCall ~= { mixin(S_TRACE);
					_comm.refFlagAndStep.call(flags, steps, variants);
				};
				newFPaths = .map!(a => a.path)(flags).array();
				newSPaths = .map!(a => a.path)(steps).array();
				newVPaths = .map!(a => a.path)(variants).array();
				_after ~= { mixin(S_TRACE);
					foreach (oldPath, newPath; .zip(oldFPaths, newFPaths)) { mixin(S_TRACE);
						auto oldID = cwx.flag.Flag.toID(oldPath);
						auto newID = cwx.flag.Flag.toID(newPath);
						foreach (v; dir.useCounter.values(oldID)) { mixin(S_TRACE);
							storeID(parent, null, v, oldID, newID, &v.id);
						}
						dir.useCounter.change(oldID, newID);
					}
					foreach (oldPath, newPath; .zip(oldSPaths, newSPaths)) { mixin(S_TRACE);
						auto oldID = Step.toID(oldPath);
						auto newID = Step.toID(newPath);
						foreach (v; dir.useCounter.values(oldID)) { mixin(S_TRACE);
							storeID(parent, null, v, oldID, newID, &v.id);
						}
						dir.useCounter.change(oldID, newID);
					}
					foreach (oldPath, newPath; .zip(oldVPaths, newVPaths)) { mixin(S_TRACE);
						auto oldID = cwx.flag.Variant.toID(oldPath);
						auto newID = cwx.flag.Variant.toID(newPath);
						foreach (v; dir.useCounter.values(oldID)) { mixin(S_TRACE);
							storeID(parent, null, v, oldID, newID, &v.id);
						}
						dir.useCounter.change(oldID, newID);
					}
				};
			}
			return true;
		}
		return false;
	}

	private bool replFlagName(F)(CWXPath parent, FlagDir parentDir, F flag, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		string text = flag.name;
		auto c = fTextCount(text);
		count += c;
		if (c > 0) { mixin(S_TRACE);
			if (_replMode) { mixin(S_TRACE);
				string oldPath = flag.path;
				string newPath = "";
				string n = fTextRepl(text);
				uArr ~= new StrUndo(text, n, (string name) { mixin(S_TRACE);
					auto parent = flag.parent;
					if (parent) { mixin(S_TRACE);
						flag.name = parent.validName(name);
					} else { mixin(S_TRACE);
						flag.name = name;
					}
					if (_prop.sys.isLocalVariable(oldPath)) { mixin(S_TRACE);
						auto oldID = F.toID(oldPath);
						auto newID = F.toID(newPath);
						flag.useCounter.deleteID(newID);
						flag.useCounter.createID(oldID);
					}
					std.algorithm.swap(oldPath, newPath);
				});
				flag.name = parentDir.validName(n);
				newPath = flag.path;
				_after ~= { mixin(S_TRACE);
					auto oldID = F.toID(oldPath);
					auto newID = F.toID(newPath);
					foreach (v; flag.useCounter.values(oldID)) { mixin(S_TRACE);
						storeID(parent, null, v, oldID, newID, &v.id);
					}
					flag.useCounter.change(oldID, newID);
				};
			}
			return true;
		}
		return false;
	}

	private bool replRqCoupons(C)(CWXPath parent, CWXPath path, string pathPath, C targ, string targPath, ref size_t count, ref Undo[] uArr, bool storeToArr = false) { mixin(S_TRACE);
		string[] coupons = targ.rCoupons;
		string[] old = coupons.dup;
		bool r = false;
		foreach (i, cp; coupons) { mixin(S_TRACE);
			Undo[] nArr;
			r |= repl(parent, null, "", cp, (string t) { cp = t; }, count, nArr);
			if (_replMode) coupons[i] = cp;
		}
		if (r) { mixin(S_TRACE);
			if (_replMode) { mixin(S_TRACE);
				if (!path || storeToArr) uArr ~= new StrArrUndo(old, coupons.dup, (arr) { targ.rCoupons = arr.filter!(a => a != "").array(); });
				targ.rCoupons = coupons.filter!(a => a != "").array();
			}
			if (path) { mixin(S_TRACE);
				if (_replMode && !storeToArr) store(parent, path, pathPath, old, coupons.dup, (arr) { targ.rCoupons = arr.filter!(a => a != "").array(); });
				size_t dmy = 0;
				addResult(parent, targ, targPath, dmy);
			}
			return true;
		}
		return false;
	}

	private bool replKeyCode(C)(CWXPath parent, CWXPath path, lazy string pathPath, C targ, string targPath, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		if (_keyCodeSel) { mixin(S_TRACE);
			auto kcs = targ.keyCodes.dup;
			auto old = targ.keyCodes.dup;
			bool r = false;
			Undo[] nArr;
			foreach (i, kc; kcs) { mixin(S_TRACE);
				r |= repl(parent, null, "", kc, (string t) { kc = t; }, count, nArr);
				if (_replMode) kcs[i] = kc;
			}
			if (r) { mixin(S_TRACE);
				if (_replMode) { mixin(S_TRACE);
					if (!path) uArr ~= new StrArrUndo(old, kcs.dup, &targ.keyCodes);
					targ.keyCodes = kcs;
				}
				if (path) { mixin(S_TRACE);
					auto p = pathPath;
					if (_replMode) store(parent, path, p, old, kcs.dup, &targ.keyCodes);
					size_t dmy = 0;
					addResult(parent, path, p, dmy);
				}
				return true;
			}
		}
		return false;
	}
	private bool replFKeyCode(C)(CWXPath parent, CWXPath path, string pathPath, C targ, string targPath, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		if (_keyCodeSel) { mixin(S_TRACE);
			auto kcs = targ.keyCodes.dup;
			auto old = targ.keyCodes.dup;
			bool r = false;
			Undo[] nArr;
			foreach (i, fkc; kcs) { mixin(S_TRACE);
				auto kc = _prop.sys.convFireKeyCode(fkc);
				r |= repl(parent, null, "", kc, (string t) { kc = t; }, count, nArr);
				if (_replMode) kcs[i] = _prop.sys.toFKeyCode(kc);
			}
			if (r) { mixin(S_TRACE);
				if (_replMode) { mixin(S_TRACE);
					if (!path) uArr ~= new FKeyCodesUndo(old, kcs.dup, (fkc) { targ.keyCodes = fkc.filter!(a => a.keyCode != "").array(); });
					targ.keyCodes = kcs.filter!(a => a.keyCode != "").array();
				}
				if (path) { mixin(S_TRACE);
					if (_replMode) store(parent, path, pathPath, old, kcs.dup, (fkc) { targ.keyCodes = fkc.filter!(a => a.keyCode != "").array(); });
					size_t dmy = 0;
					addResult(parent, path, pathPath, dmy);
				}
				return true;
			}
		}
		return false;
	}
	private bool replCoupons(C)(CWXPath parent, CWXPath path, lazy string pathPath, C targ, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		if (_couponSel) { mixin(S_TRACE);
			auto coupons = targ.coupons.dup;
			auto old = targ.coupons.dup;
			bool r = false;
			Undo[] nArr;
			foreach (i, coupon; coupons) { mixin(S_TRACE);
				r |= repl(parent, null, "", coupon.name, (string t) { coupon = new Coupon(t, coupon.value); }, count, nArr);
				if (_replMode) coupons[i] = coupon;
			}
			if (r) { mixin(S_TRACE);
				if (_replMode) { mixin(S_TRACE);
					if (!path) uArr ~= new CouponsUndo(old, coupons.dup, (arr) { targ.coupons = arr.filter!(a => a.name != "").array(); });
					targ.coupons = coupons.filter!(a => a.name != "").array();
				}
				if (path) { mixin(S_TRACE);
					auto p = pathPath;
					if (_replMode) store(parent, path, p, old, coupons.dup, (arr) { targ.coupons = arr.filter!(a => a.name != "").array(); });
					size_t dmy = 0;
					addResult(parent, path, p, dmy);
				}
				return true;
			}
		}
		return false;
	}
	private bool replCouponNames(C)(CWXPath parent, CWXPath path, lazy string pathPath, C targ, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		if (_couponSel) { mixin(S_TRACE);
			auto cs = targ.couponNames.dup;
			auto old = targ.couponNames.dup;
			bool r = false;
			Undo[] nArr;
			foreach (i, c; cs) { mixin(S_TRACE);
				r |= repl(parent, null, "", c, (string t) { c = t; }, count, nArr);
				if (_replMode) cs[i] = c;
			}
			if (r) { mixin(S_TRACE);
				if (_replMode) { mixin(S_TRACE);
					if (!path) uArr ~= new StrArrUndo(old, cs.dup, (arr) { targ.couponNames = arr.filter!(a => a != "").array(); });
					targ.couponNames = cs.filter!(a => a != "").array();
				}
				if (path) { mixin(S_TRACE);
					auto p = pathPath;
					if (_replMode) store(parent, path, p, old, cs.dup, (arr) { targ.couponNames = arr.filter!(a => a != "").array(); });
					size_t dmy = 0;
					addResult(parent, path, p, dmy);
				}
				return true;
			}
		}
		return false;
	}

	private bool replBgImage(CWXPath parent, CWXPath path, string pathPath, BgImage back, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		bool r = false;
		Undo[] uArr2;
		if (_varNameSel) { mixin(S_TRACE);
			if (_flagDirOnRange) { mixin(S_TRACE);
				r |= repl(parent, null, "", back.flag, null, count, uArr2);
			} else { mixin(S_TRACE);
				r |= repl(parent, null, "", back.flag, &back.flag, count, uArr2);
			}
		}
		if (_cellNameSel) { mixin(S_TRACE);
			r |= repl(parent, null, "", back.cellName, &back.cellName, count, uArr2);
		}
		auto ic = cast(ImageCell)back;
		if (ic && _fileSel) { mixin(S_TRACE);
			r |= replFilePath(ic.path, &ic.path, count, uArr2);
		}
		auto tc = cast(TextCell)back;
		if (tc) { mixin(S_TRACE);
			if (_jptxSel) { mixin(S_TRACE);
				r |= repl(parent, null, "", tc.text, &tc.text, count, uArr2, true);
			}
			r |= replFlagsInText(parent, tc, count, uArr2);
		}
		if (r && path) { mixin(S_TRACE);
			if (_replMode) store(parent, path, pathPath, uArr2);
			size_t dmy = 0;
			addResult(parent, path, pathPath, dmy);
		} else { mixin(S_TRACE);
			uArr ~= uArr2;
		}
		return r;
	}
	private bool replCard(C)(CWXPath parent, CWXPath path, string pathPath, C card, string cardPath, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		bool r = false;
		static if (is(typeof(card.linkId))) {
			if (card.linkId != 0) return r;
		}
		Undo[] uArr2;
		if (_cardNameSel) { mixin(S_TRACE);
			r |= repl(parent, null, "", card.name, &card.name, count, uArr2);
		}
		if (_cardDescSel) { mixin(S_TRACE);
			r |= repl(parent, null, "", card.desc, &card.desc, count, uArr2);
		}
		static if (is(C:EffectCard)) {
			if (_scenarioSel) { mixin(S_TRACE);
				r |= repl(parent, null, "", card.scenario, &card.scenario, count, uArr2);
			}
			if (_authorSel) { mixin(S_TRACE);
				r |= repl(parent, null, "", card.author, &card.author, count, uArr2);
			}
		}
		static if (is(C:AbstractSpCard)) {
			if (_cardGroupSel) { mixin(S_TRACE);
				r |= repl(parent, null, "", card.cardGroup, &card.cardGroup, count, uArr2);
			}
		}
		if (_varNameSel) { mixin(S_TRACE);
			static if (is(C:AbstractSpCard)) {
				if (_flagDirOnRange) { mixin(S_TRACE);
					r |= repl(parent, null, "", card.flag, null, count, uArr2);
				} else { mixin(S_TRACE);
					r |= repl(parent, null, "", card.flag, &card.flag, count, uArr2);
				}
			}
		}
		if (_fileSel) { mixin(S_TRACE);
			r |= replImagePaths(card.paths, &card.paths, count, uArr2);
		}
		static if (is(C : EffectCard)) {
			r |= replKeyCode!(C)(parent, null, "", card, cardPath, count, uArr2);
		}
		if (r && path) { mixin(S_TRACE);
			if (_replMode) store(parent, path, pathPath, uArr2);
			size_t dmy = 0;
			addResult(parent, path, pathPath, dmy);
		} else { mixin(S_TRACE);
			uArr ~= uArr2;
		}
		return r;
	}
	bool replFontsInText(CWXPath parent, ITextHolder th, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		Undo[] nArr;
		bool r = false;
		string old = th.text;
		if (_fileSel) { mixin(S_TRACE);
			auto ps = th.fontsInText;
			foreach (i, p; ps) { mixin(S_TRACE);
				auto c = decodeFontPath(p);
				string ext = .extension(p);
				r |= repl(parent, null, "", .to!string(c), (string s) { mixin(S_TRACE);
					dstring ds = .to!dstring(s);
					if (!ds.length) return;
					auto path = .encodeFontPath(ds[0], ext);
					if (path == "") return;
					th.changeInText(i, toPathId(path));
				}, count, nArr);
			}
			uArr ~= new StrUndo(old, th.text, &th.text);
		}
		return r;
	}
	bool replFlagsInText(CWXPath parent, ISimpleTextHolder th, ref size_t count, ref Undo[] uArr) { mixin(S_TRACE);
		Undo[] nArr;
		bool r = false;
		if (_varNameSel && !_msgSel) { mixin(S_TRACE);
			// 状態変数名が置換対象に入っている場合、
			// Flag/Step/VariantについてはUseCounter経由で置換される
			auto fps = th.flagsInText;
			foreach (i, p; fps) { mixin(S_TRACE);
				if (_flagDirOnRange) { mixin(S_TRACE);
					r |= repl(parent, null, "", p, null, count, nArr);
				} else { mixin(S_TRACE);
					r |= repl(parent, null, "", p, (string n) { th.changeInText(i, toFlagId(p)); }, count, nArr, false, true);
				}
			}
			auto sps = th.stepsInText;
			foreach (i, p; sps) { mixin(S_TRACE);
				if (_flagDirOnRange) { mixin(S_TRACE);
					r |= repl(parent, null, "", p, null, count, nArr);
				} else { mixin(S_TRACE);
					r |= repl(parent, null, "", p, (string n) { th.changeInText(i, toStepId(p)); }, count, nArr, false, true);
				}
			}
			auto vps = th.variantsInText;
			foreach (i, p; vps) { mixin(S_TRACE);
				if (_flagDirOnRange) { mixin(S_TRACE);
					r |= repl(parent, null, "", p, null, count, nArr);
				} else { mixin(S_TRACE);
					r |= repl(parent, null, "", p, (string n) { th.changeInText(i, toVariantId(p)); }, count, nArr, false, true);
				}
			}
		}
		return r;
	}
	void replContent(CWXPath parent, Content e, string cwxPath, ref size_t count) { mixin(S_TRACE);
		auto eo = e.parent;
		auto d = e.detail;
		assert (!eo || eo.detail.owner);
		bool r = false;
		Undo[] uArr2;
		if ((_eventSel && eo && eo.detail.nextType == CNextType.Text)
				|| (_couponSel && eo && eo.detail.nextType == CNextType.Coupon)) { mixin(S_TRACE);
			r |= repl(parent, null, "", e.name, name => e.setName(_prop.parent, name), count, uArr2);
		}
		if (_varNameSel) { mixin(S_TRACE);
			// 状態変数名が置換対象に入っている場合、
			// Flag/Step/VariantについてはUseCounter経由で置換される
			if (d.use(CArg.Flag)) { mixin(S_TRACE);
				if (_flagDirOnRange) { mixin(S_TRACE);
					r |= repl(parent, null, "", e.flag, null, count, uArr2);
				} else { mixin(S_TRACE);
					r |= repl(parent, null, "", e.flag, &e.flag, count, uArr2, false, true);
				}
			}
			if (d.use(CArg.Step)) { mixin(S_TRACE);
				if (_flagDirOnRange) { mixin(S_TRACE);
					r |= repl(parent, null, "", e.step, null, count, uArr2);
				} else { mixin(S_TRACE);
					r |= repl(parent, null, "", e.step, &e.step, count, uArr2, false, true);
				}
			}
			if (d.use(CArg.Variant)) { mixin(S_TRACE);
				if (_flagDirOnRange) { mixin(S_TRACE);
					r |= repl(parent, null, "", e.variant, null, count, uArr2);
				} else { mixin(S_TRACE);
					r |= repl(parent, null, "", e.variant, &e.variant, count, uArr2, false, true);
				}
			}
			if (d.use(CArg.Flag2)) { mixin(S_TRACE);
				if (_flagDirOnRange) { mixin(S_TRACE);
					r |= repl(parent, null, "", e.flag2, null, count, uArr2);
				} else { mixin(S_TRACE);
					r |= repl(parent, null, "", e.flag2, &e.flag2, count, uArr2, false, true);
				}
			}
			if (d.use(CArg.Step2)) { mixin(S_TRACE);
				if (_flagDirOnRange) { mixin(S_TRACE);
					r |= repl(parent, null, "", e.step2, null, count, uArr2);
				} else { mixin(S_TRACE);
					r |= repl(parent, null, "", e.step2, &e.step2, count, uArr2, false, true);
				}
			}
		}
		if (_startSel) { mixin(S_TRACE);
			// スタートへのリンク・コールはSUseCounter経由で置換されるため
			// ここでは置換しない
			if (d.use(CArg.Start)) r |= repl(parent, null, "", e.start, null, count, uArr2);
			if (e.type == CType.Start) { mixin(S_TRACE);
				r |= repl(parent, null, "", e.name, name => e.setName(_prop.parent, name), count, uArr2);
			}
		}
		if (_couponSel) { mixin(S_TRACE);
			if (d.use(CArg.Coupon)) r |= repl(parent, null, "", e.coupon, &e.coupon, count, uArr2);
			if (d.use(CArg.Coupons)) r |= replCoupons(parent, null, "", e, count, uArr2);
			if (d.use(CArg.CouponNames)) r |= replCouponNames(parent, null, "", e, count, uArr2);
			if (d.use(CArg.HoldingCoupon)) r |= repl(parent, null, "", e.holdingCoupon, &e.holdingCoupon, count, uArr2);
		}
		if (_gossipSel) { mixin(S_TRACE);
			if (d.use(CArg.Gossip)) r |= repl(parent, null, "", e.gossip, &e.gossip, count, uArr2);
		}
		if (_endSel) { mixin(S_TRACE);
			if (d.use(CArg.CompleteStamp)) r |= repl(parent, null, "", e.completeStamp, &e.completeStamp, count, uArr2);
		}
		if (_msgSel) { mixin(S_TRACE);
			if (d.use(CArg.Text)) r |= repl(parent, null, "", e.text, &e.text, count, uArr2);
		}
		if (d.use(CArg.Text)) r |= replFontsInText(parent, e, count, uArr2);
		if (d.use(CArg.Text)) r |= replFlagsInText(parent, e, count, uArr2);
		bool rDlg = false;
		if (_msgSel || _couponSel || _fileSel || _varNameSel) { mixin(S_TRACE);
			auto dlgs = e.dialogs;
			foreach (i, dlg; dlgs) { mixin(S_TRACE);
				Undo[] uArrDlg;
				auto put = dlg;
				auto putPath = cpjoin(cwxPath, "dialog", i);
				auto dlgPath = putPath;
				if (_msgSel && repl(parent, dlg, dlgPath, dlg.text, &dlg.text, count, uArrDlg, true)) { mixin(S_TRACE);
					rDlg = true;
					put = null;
				}
				if (_couponSel && replRqCoupons!(typeof(dlg))(parent, put, putPath, dlg, dlgPath, count, uArrDlg, true)) { mixin(S_TRACE);
					rDlg = true;
					put = null;
				}
				bool dr = replFontsInText(parent, dlg, count, uArrDlg);
				dr |= replFlagsInText(parent, dlg, count, uArrDlg);
				if (dr) { mixin(S_TRACE);
					size_t dmy = 0;
					if (put) addResult(parent, put, putPath, dmy);
					rDlg = true;
					put = null;
				}
				if (_replMode && !put) { mixin(S_TRACE);
					store(parent, dlg, dlgPath, uArrDlg);
				}
			}
		}
		if (_fileSel) { mixin(S_TRACE);
			if (d.use(CArg.TalkerC)) r |= replImagePaths(e.cardPaths, (paths) { e.cardPaths = paths; }, count, uArr2);
			if (d.use(CArg.BgmPath)) r |= replFilePath(e.bgmPath, &e.bgmPath, count, uArr2);
			if (d.use(CArg.InitialSoundPath)) r |= replFilePath(e.initialSoundPath, &e.initialSoundPath, count, uArr2);
			if (d.use(CArg.SoundPath)) r |= replFilePath(e.soundPath, &e.soundPath, count, uArr2);
		}
		if (_keyCodeSel) { mixin(S_TRACE);
			if (d.use(CArg.KeyCode)) r |= repl(parent, null, "", e.keyCode, &e.keyCode, count, uArr2);
			if (d.use(CArg.KeyCodes)) r |= replKeyCode(parent, e, e.cwxPath(true), e, "", count, uArr2);
		}
		if (_cellNameSel) { mixin(S_TRACE);
			if (d.use(CArg.CellName)) r |= repl(parent, null, "", e.cellName, &e.cellName, count, uArr2);
		}
		if (_cardGroupSel) { mixin(S_TRACE);
			if (d.use(CArg.CardGroup)) r |= repl(parent, null, "", e.cardGroup, &e.cardGroup, count, uArr2);
		}
		if (_expressionSel) { mixin(S_TRACE);
			if (d.use(CArg.Expression)) r |= repl(parent, null, "", e.expression, &e.expression, count, uArr2);
		}
		if (r) { mixin(S_TRACE);
			if (_replMode) { mixin(S_TRACE);
				store(parent, e, cwxPath, uArr2);
			}
			size_t dmy = 0;
			addResult(parent, e, cwxPath, dmy);
		}
	}
}

class SortableCWXPath(T) {
	CWXPath parent = null;
	T obj = null;
	string cwxPath;
	string[] path;
	this (T obj) { mixin(S_TRACE);
		this.obj = obj;
		parent = .cwxPlace(obj);
		cwxPath = obj.cwxPath(true);
		path= cwxPath.cpsplit();
	}
	this (CWXPath parent, T obj, string cwxPath) { mixin(S_TRACE);
		this.parent = parent;
		this.obj = obj;
		this.cwxPath = cwxPath;
	}

	static void sortedPaths(HashSet!T paths, void delegate(SortableCWXPath!T) yield) { mixin(S_TRACE);
		auto array = .map!(a => new SortableCWXPath!T(a))(paths.toArray()).array();
		foreach (u; std.algorithm.sort!((a, b) => .cpcmp(a.path, b.path) < 0)(array)) { mixin(S_TRACE);
			yield(u);
		}
	}
}
