
module cwx.editor.gui.dwt.summarydialog;

import cwx.utils;
import cwx.usecounter;
import cwx.summary;
import cwx.area;
import cwx.event;
import cwx.skin;
import cwx.card;
import cwx.structs;
import cwx.types;
import cwx.path;
import cwx.imagesize;
import cwx.warning;

import cwx.editor.gui.dwt.dskin;
import cwx.editor.gui.dwt.dutils;
import cwx.editor.gui.dwt.dprops;
import cwx.editor.gui.dwt.commons;
import cwx.editor.gui.dwt.materialselect;
import cwx.editor.gui.dwt.imageselect;
import cwx.editor.gui.dwt.centerlayout;
import cwx.editor.gui.dwt.customtext;
import cwx.editor.gui.dwt.customtable;
import cwx.editor.gui.dwt.absdialog;
import cwx.editor.gui.dwt.splitpane;
import cwx.editor.gui.dwt.properties;
import cwx.editor.gui.dwt.dmenu;
import cwx.editor.gui.dwt.incsearch;
import cwx.editor.gui.dwt.chooser;

import std.conv;
import std.string;
import std.path;
import std.typecons;

import org.eclipse.swt.all;

public:

/// シナリオの概略を設定するダイアログ。
class SummaryDialog : AbsDialog {
private:
	int _readOnly = SWT.NONE;
	Commons _comm;
	Props _prop;
	Summary _summ;

	SummaryPreview _summImage = null;
	Composite _imgArea;
	uint _lastImageScale = 0;

	Text _sname;
	ImageSelect!(MtType.CARD) _imgPath;
	FixedWidthText!Text _desc;
	Text _author;
	Spinner _levMin, _levMax;
	AreaChooser!(Area, true) _startArea;
	Spinner _rCouponNum;
	Text _rCoupons;
	Button _typeSkin;
	Button _typeClassic;
	Combo _type;
	Tuple!(string, "type", string, "name", string, "path")[] _skinInfo;
	bool _hasLegacySkin;
	ClassicEngine[] _classicEngines;
	Combo _dataVersion;
	Button _loadScaledImage;
	bool _loadScaledImageValue;
	SplitPane _tab2Sash, _tab3Sash;
	// TODO Tag
	// TODO Label

	IncSearch _skinIncSearch;
	Skin _selectedSkin = null;

	Skin _summSkin;
	@property
	Skin summSkin() { mixin(S_TRACE);
		return _summSkin ? _summSkin : _comm.skin;
	}

	void refreshWarning() { mixin(S_TRACE);
		if (!_sname || !_desc || !_author || !_rCoupons) return;
		string[] ws;
		if (_dataVersion && _summ && _imgPath) { mixin(S_TRACE);
			auto ver = dataVersion;
			ws ~= _imgPath.warningsWith(_summ.legacy, ver, selectedSkin);
			if (_loadScaledImage.getSelection() && !.isTargetVersion(ver, "2")) { mixin(S_TRACE);
				ws ~= _prop.msgs.warningLoadScaledImage;
			}
		}
		ws ~= .sjisWarnings(_prop.parent, _summ, _sname.getText(), _prop.msgs.title);
		ws ~= .sjisWarnings(_prop.parent, _summ, _author.getText(), _prop.msgs.author);
		if ((_levMin.getSelection() != 0 || _levMax.getSelection() != 0) && _summ.legacy) { mixin(S_TRACE);
			if (_levMin.getSelection() == 0) { mixin(S_TRACE);
				ws ~= _prop.msgs.warningNoLevelMin;
			} else if (_levMax.getSelection() == 0) { mixin(S_TRACE);
				ws ~= _prop.msgs.warningNoLevelMax;
			}
		}
		if ((_levMin.getSelection() != 0 && _levMax.getSelection() != 0) && _levMin.getSelection() > _levMax.getSelection()) { mixin(S_TRACE);
			ws ~= _prop.msgs.searchErrorReversalLevel;
		}
		ws ~= .sjisWarnings(_prop.parent, _summ, _desc.getRRText(), _prop.msgs.desc);
		ws ~= .sjisWarnings(_prop.parent, _summ, _rCoupons.getText(), _prop.msgs.rCoupons);
		warning = ws;
	}
	string dataVersion() { mixin(S_TRACE);
		return VERSIONS[_dataVersion.getSelectionIndex()];
	}

	void levMaxEnter(int enter) { mixin(S_TRACE);
		if (_readOnly) return;
		if (enter > 0 && _levMin.getSelection() != 0 && enter < _levMin.getSelection()) { mixin(S_TRACE);
			_levMin.setSelection(enter);
		}
	}
	void levMinEnter(int enter) { mixin(S_TRACE);
		if (_readOnly) return;
		if (enter > 0 && _levMax.getSelection() != 0 && enter > _levMax.getSelection()) { mixin(S_TRACE);
			_levMax.setSelection(enter);
		}
	}

	@property
	Skin selectedSkin() { mixin(S_TRACE);
		int i = _type.getSelectionIndex();
		if (i == -1) return _selectedSkin;
		if (_typeSkin.getSelection()) { mixin(S_TRACE);
			auto info = _skinInfo[i];
			foreach (file, skin; skinTable(_prop)) { mixin(S_TRACE);
				if (info.path == skin.path) { mixin(S_TRACE);
					return skin;
				}
			}
		} else if (_typeClassic.getSelection()) { mixin(S_TRACE);
			if (_hasLegacySkin) { mixin(S_TRACE);
				if (i == 0) { mixin(S_TRACE);
					return .findSkin(_comm, _prop, _summ, null, "", "", false);
				}
				i--;
			}
			return .createClassicSkin(_prop, _classicEngines[i]);
		}
		return .findSkin2(_prop, _prop.var.etc.defaultSkin, _prop.var.etc.defaultSkinName);
	}
	void constructImage(Composite area) { mixin(S_TRACE);
		_imgArea = area;

		auto aComp = addition();
		aComp.setLayout(normalGridLayout(1, true));
		auto prev = new Button(aComp, SWT.TOGGLE);
		auto pgd = new GridData;
		pgd.widthHint = _prop.var.etc.buttonWidth;
		prev.setLayoutData(pgd);
		prev.setText(_prop.msgs.messagePreview);
		prev.setSelection(_prop.var.etc.showSummaryPreview);
		.listener(prev, SWT.Selection, { mixin(S_TRACE);
			showImagePreview(prev.getSelection());
		});
		showImagePreview(_prop.var.etc.showSummaryPreview, false);
	}
	void showImagePreview(bool visible, bool regWin = true) { mixin(S_TRACE);
		if (getShell().isVisible()) getShell().setRedraw(false);
		scope (exit) {
			if (getShell().isVisible()) getShell().setRedraw(true);
		}

		int w;
		if (visible) { mixin(S_TRACE);
			if (_summImage) return;
			_summImage = new SummaryPreview(_comm, _imgArea, SWT.NONE, () => _loadScaledImage.getSelection(), &dataVersion);
			_summImage.setImageSelect(&_sname.getText, &_imgPath.images, &selectedSkin, { return _desc.getRRText(); }, &_levMin.getSelection, &_levMax.getSelection);
			_summImage.setLayoutData(new GridData(GridData.FILL_VERTICAL));
			w = _summImage.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
		} else { mixin(S_TRACE);
			if (!_summImage) return;
			w = _summImage.getSize().x;
			if (_summImage) _summImage.dispose();
			_summImage = null;
		}
		_prop.var.etc.showSummaryPreview = visible;

		if (regWin) { mixin(S_TRACE);
			auto ws = getShell().getSize();
			if (visible) { mixin(S_TRACE);
				ws.x += w;
			} else { mixin(S_TRACE);
				ws.x -= w;
			}
			getShell().setSize(ws);
		}
	}
	void refImageScale() { mixin(S_TRACE);
		auto lis = _lastImageScale;
		_lastImageScale = _prop.var.etc.imageScale;
		_imgPath.setPreviewSize(_prop.s(_prop.looks.cardSize.width), _prop.s(_prop.looks.cardSize.height), _prop.s(CInsets(0, 0, 0, 0)));

		if (!_summImage) return;
		if (getShell().isVisible()) getShell().setRedraw(false);
		scope (exit) {
			if (getShell().isVisible()) getShell().setRedraw(true);
		}
		if (lis == _prop.var.etc.imageScale) { mixin(S_TRACE);
			clearBuf();
			refreshPreview();
		} else { mixin(S_TRACE);
			showImagePreview(false);
			showImagePreview(true);
		}
	}
	void refreshPreview() { mixin(S_TRACE);
		if (_summImage) { mixin(S_TRACE);
			_summImage.redrawImage();
		}
	}
	void clearBuf() { mixin(S_TRACE);
		if (_summImage) { mixin(S_TRACE);
			_summImage.clearBuf();
		}
	}
	private void setCDataX(Control c, GridData data) { mixin(S_TRACE);
		auto p = c.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		data.widthHint = p.x;
		c.setLayoutData(data);
	}
	private void setCDataXY(Control c, GridData data) { mixin(S_TRACE);
		auto p = c.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		data.widthHint = p.x;
		data.heightHint = p.y;
		c.setLayoutData(data);
	}
	void constructTab1(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		{ mixin(S_TRACE);
			_tab2Sash = new SplitPane(comp, SWT.HORIZONTAL);
			_tab2Sash.resizeControl1 = true;
			_tab2Sash.setLayoutData(new GridData(GridData.FILL_BOTH));
			auto skin = summSkin;
			{ mixin(S_TRACE);
				_imgPath = new ImageSelect!(MtType.CARD)(_tab2Sash, _readOnly, _comm, _prop, _summ,
					_prop.s(_prop.looks.cardSize.width), _prop.s(_prop.looks.cardSize.height), _prop.s(CInsets(0, 0, 0, 0)),
					CardImagePosition.TopLeft, true, { mixin(S_TRACE);
						return .toExportedImageNameWithoutCardName(_prop.parent, _sname.getText(), _author.getText());
					}, &clearBuf);
				mod(_imgPath);
				_imgPath.modEvent ~= &refreshWarning;
				_imgPath.images = _summ.imagePaths;
				_imgPath.modEvent ~= &refreshPreview;
				_imgPath.updateImageEvent ~= &refreshPreview;
			}
			{ mixin(S_TRACE);
				auto comp2 = new Composite(_tab2Sash, SWT.NONE);
				comp2.setLayout(zeroGridLayout(1, true));
				{ mixin(S_TRACE);
					auto grp = centerGroup(comp2, _prop.msgs.title, true, false, new GridData(GridData.FILL_BOTH));
					grp.setLayout(normalGridLayout(1, true));
					_sname = new Text(grp, SWT.BORDER | _readOnly);
					createTextMenu!Text(_comm, _prop, _sname, &catchMod);
					mod(_sname);
					setCDataX(_sname, new GridData(GridData.FILL_HORIZONTAL));
					_sname.setText(_summ.scenarioName);
					checker(_sname);
					.listener(_sname, SWT.Modify, &refreshPreview);
					.listener(_sname, SWT.Modify, &refreshWarning);
				}
				{ mixin(S_TRACE);
					auto grp = centerGroup(comp2, _prop.msgs.author, true, false, new GridData(GridData.FILL_BOTH));
					grp.setLayout(normalGridLayout(1, true));
					_author = new Text(grp, SWT.BORDER | _readOnly);
					createTextMenu!Text(_comm, _prop, _author, &catchMod);
					mod(_author);
					setCDataX(_author, new GridData(GridData.FILL_HORIZONTAL));
					_author.setText(_summ.author);
					.listener(_author, SWT.Modify, &refreshWarning);
				}
				{ mixin(S_TRACE);
					auto grp = centerGroup(comp2, _prop.msgs.targetLevel, false, false, new GridData(GridData.FILL_BOTH));
					grp.setLayout(normalGridLayout(4, false));
					_levMin = new Spinner(grp, SWT.BORDER | _readOnly);
					initSpinner(_levMin);
					mod(_levMin);
					_levMin.setSelection(_summ.levelMin);
					_levMin.setMinimum(0);
					_levMin.setMaximum(_prop.var.etc.levelMax);
					if (!_readOnly) new SpinnerEdit(_levMin, &levMinEnter);
					auto lbl = new Label(grp, SWT.NONE);
					lbl.setText(_prop.msgs.levSep);
					lbl.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
					_levMax = new Spinner(grp, SWT.BORDER | _readOnly);
					initSpinner(_levMax);
					mod(_levMax);
					_levMax.setSelection(_summ.levelMax);
					_levMax.setMinimum(0);
					_levMax.setMaximum(_prop.var.etc.levelMax);
					if (!_readOnly) { mixin(S_TRACE);
						new SpinnerEdit(_levMax, &levMaxEnter);
					}
					if (!_readOnly) { mixin(S_TRACE);
						.listener(_levMin, SWT.Modify, &refreshPreview);
						.listener(_levMin, SWT.Modify, &refreshWarning);
						.listener(_levMax, SWT.Modify, &refreshPreview);
						.listener(_levMax, SWT.Modify, &refreshWarning);
					}

					auto hint = new Label(grp, SWT.NONE);
					hint.setText(.tryFormat(_prop.msgs.rangeHint, _levMin.getMinimum(), _levMin.getMaximum()));
				}
			}
			.setupWeights(_tab2Sash, _prop.var.etc.summaryParamSashL, _prop.var.etc.summaryParamSashR);
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setLayout(new CenterLayout(SWT.HORIZONTAL));
			grp.setText(_prop.msgs.desc);
			_desc = new FixedWidthText!Text(_prop.adjustFont(_prop.looks.summaryDescFont(summSkin.legacy)), _prop.looks.summaryDescLen, grp, SWT.BORDER | _readOnly);
			createTextMenu!Text(_comm, _prop, _desc.widget, &catchMod);
			mod(_desc.widget);
			_desc.widget.setLayoutData(_desc.computeTextBaseSize(_prop.looks.summaryDescLine));
			_desc.setText(_summ.desc);
			.listener(_desc.widget, SWT.Modify, &refreshPreview);
			.listener(_desc.widget, SWT.Modify, &refreshWarning);
		}
		_comm.refImageScale.add(&refImageScale);
		.listener(tabf, SWT.Dispose, { mixin(S_TRACE);
			_comm.refImageScale.remove(&refImageScale);
		});

		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.baseData);
		tab.setControl(comp);
	}
	void constructTab2(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, true));
		{ mixin(S_TRACE);
			_tab3Sash = new SplitPane(comp, SWT.HORIZONTAL);
			_tab3Sash.setLayoutData(new GridData(GridData.FILL_BOTH));
			auto skin = summSkin;
			{ mixin(S_TRACE);
				auto comp2 = new Composite(_tab3Sash, SWT.NONE);
				comp2.setLayout(zeroMarginGridLayout(1, true));
				{ mixin(S_TRACE);
					auto grp = new Group(comp2, SWT.NONE);
					grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					grp.setText(_prop.msgs.scenarioType);
					grp.setLayout(normalGridLayout(1, true));
					auto refTypes = new RefreshTypes;
					_typeSkin = new Button(grp, SWT.RADIO);
					_typeSkin.setEnabled(!_readOnly);
					_typeSkin.setText(_prop.msgs.sTypeXML);
					_typeSkin.addSelectionListener(refTypes);
					_typeClassic = new Button(grp, SWT.RADIO);
					_typeClassic.setEnabled(!_readOnly);
					_typeClassic.setText(_prop.msgs.sTypeClassic);
					_typeClassic.addSelectionListener(refTypes);
					_type = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
					_type.setEnabled(!_readOnly);
					mod(_type);
					_type.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
					_type.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					void refType() { mixin(S_TRACE);
						refreshPreview();
						refreshWarning();
						_selectedSkin = selectedSkin;
					}
					.listener(_typeSkin, SWT.Selection, &refType);
					.listener(_typeClassic, SWT.Selection, &refType);
					.listener(_type, SWT.Modify, &refreshPreview);

					_skinIncSearch = new IncSearch(_comm, _type, null);
					_skinIncSearch.modEvent ~= () => refreshTypes(null, true);
					auto menu = new Menu(_type.getShell(), SWT.POP_UP);
					createMenuItem(_comm, menu, MenuID.IncSearch, { _skinIncSearch.startIncSearch(); }, null);
					_type.setMenu(menu);

					refreshTypes(skin);
					_selectedSkin = selectedSkin;
					.listener(_type, SWT.Selection, { mixin(S_TRACE);
						_selectedSkin = selectedSkin;
					});
				}
				{ mixin(S_TRACE);
					auto grp = new Group(comp2, SWT.NONE);
					grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					grp.setText(_prop.msgs.dataVersion);
					grp.setLayout(normalGridLayout(1, true));
					_dataVersion = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
					mod(_dataVersion);
					_dataVersion.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
					_dataVersion.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					foreach (i, v; VERSIONS) { mixin(S_TRACE);
						_dataVersion.add(.tryFormat(_prop.msgs.dataVersionName, VERSION_NAMES[i], ENGINES[i]));
						if (v == _summ.dataVersion) { mixin(S_TRACE);
							_dataVersion.select(cast(int)i);
						}
					}
					if (_dataVersion.getSelectionIndex() == -1) _dataVersion.select(0);

					_loadScaledImage = new Button(grp, SWT.CHECK);
					_loadScaledImage.setText(_prop.msgs.loadScaledImage);
					_loadScaledImage.setToolTipText(_prop.msgs.loadScaledImageHint);
					mod(_loadScaledImage);
					_loadScaledImage.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
					_loadScaledImage.setSelection(_summ.loadScaledImage);
					_loadScaledImageValue = _summ.loadScaledImage;

					.listener(_dataVersion, SWT.Selection, &refreshWarning);
					.listener(_loadScaledImage, SWT.Selection, &refreshWarning);
					.listener(_loadScaledImage, SWT.Selection, { mixin(S_TRACE);
						clearBuf();
						refreshPreview();
					});
					_imgPath.loadScaledImage = () => _loadScaledImageValue;
					.listener(_loadScaledImage, SWT.Selection, { mixin(S_TRACE);
						_loadScaledImageValue = _loadScaledImage.getSelection();
						_imgPath.refreshList();
					});
				}
				{ mixin(S_TRACE);
					auto grp = new Group(comp2, SWT.NONE);
					grp.setLayoutData(new GridData(GridData.FILL_BOTH));
					grp.setText(_prop.msgs.qualification);
					grp.setLayout(normalGridLayout(2, false));
					{ mixin(S_TRACE);
						auto lblN = new Label(grp, SWT.NONE);
						lblN.setText(_prop.msgs.rCouponNum);
						_rCouponNum = new Spinner(grp, SWT.BORDER | _readOnly);
						initSpinner(_rCouponNum);
						mod(_rCouponNum);
						_rCouponNum.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
						_rCouponNum.setSelection(_summ.rCouponNum);
						_rCouponNum.setMaximum(999);
						_rCouponNum.setMinimum(0);
					}
					{ mixin(S_TRACE);
						auto lblR = new Label(grp, SWT.NONE);
						auto gd = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
						gd.horizontalSpan = 2;
						lblR.setLayoutData(gd);
						lblR.setText(_prop.msgs.rCoupons);
					}
					{ mixin(S_TRACE);
						_rCoupons = new Text(grp, SWT.BORDER | SWT.MULTI | SWT.WRAP | _readOnly);
						createTextMenu!Text(_comm, _prop, _rCoupons, &catchMod);
						mod(_rCoupons);
						_rCoupons.setTabs(_prop.var.etc.tabs);
						auto gd = new GridData(GridData.FILL_BOTH);
						gd.horizontalSpan = 2;
						setCDataXY(_rCoupons, gd);
						string buf;
						foreach (i, t; _summ.rCoupons) { mixin(S_TRACE);
							buf ~= t;
							buf ~= "\n";
						}
						_rCoupons.setText(buf);
						.listener(_rCoupons, SWT.Modify, &refreshWarning);
					}
				}
			}
			{ mixin(S_TRACE);
				auto grp = new Group(_tab3Sash, SWT.NONE);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setText(_prop.msgs.startArea);
				grp.setLayout(normalGridLayout(1, false));
				_startArea = new AreaChooser!(Area, true)(_comm, _summ, grp);
				_startArea.setEnabled(!_readOnly);
				mod(_startArea);
				setCDataXY(_startArea, new GridData(GridData.FILL_BOTH));

				_startArea.startArea = _summ.startArea;
			}
			.setupWeights(_tab3Sash, _prop.var.etc.rCouponsStartAreaSashL, _prop.var.etc.rCouponsStartAreaSashR);
		}
		auto tab = new CTabItem(tabf, SWT.NONE);
		tab.setText(_prop.msgs.etcData);
		tab.setControl(comp);
	}
	void updateDataVersion() { mixin(S_TRACE);
		_dataVersion.setEnabled(!_readOnly && !_summ.legacy);
		_loadScaledImage.setEnabled(!_readOnly && !_summ.legacy);
		refreshWarning();
	}
	class RefreshTypes : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			refreshTypes(summSkin);
		}
	}
	class Dispose : DisposeListener {
		override void widgetDisposed(DisposeEvent e) { mixin(S_TRACE);
			if (!_readOnly) { mixin(S_TRACE);
				_comm.refScenario.remove(&refScenario);
			}
			_comm.refSkin.remove(&refSkin);
			_comm.refSortCondition.remove(&refreshTypes);
			_comm.refClassicSkin.remove(&refreshTypes);
			_comm.refDataVersion.remove(&updateDataVersion);
			_comm.refTargetVersion.remove(&refreshWarning);
		}
	}

	void refScenario(Summary summ) { mixin(S_TRACE);
		if (_readOnly) return;
		forceCancel();
	}
	void refSkin(Object sender) { mixin(S_TRACE);
		if (_readOnly) return;
		refreshPreview();
		_desc.font = _prop.adjustFont(_prop.looks.summaryDescFont(summSkin.legacy));
		if (sender is this) return;
		refreshTypes();
	}
	void refreshTypes() { mixin(S_TRACE);
		refreshTypes(null);
	}
	void refreshTypes(Skin skin, bool incSearch = false) { mixin(S_TRACE);
		if (incSearch) skin = _selectedSkin;
		string selType = skin ? skin.type : _summ.type;
		string selName = skin ? skin.name : _summ.skinName;
		string selPath = skin ? skin.path : "";
		string selClassic = null;

		if (!_typeSkin.getSelection() && !_typeClassic.getSelection()) { mixin(S_TRACE);
			if (summSkin.legacy) { mixin(S_TRACE);
				_typeClassic.setSelection(true);
			} else { mixin(S_TRACE);
				_typeSkin.setSelection(true);
			}
			selType = _summ.type;
			selClassic = summSkin.legacyEngine.length ? summSkin.legacyEngine : null;
		} else if (incSearch) { mixin(S_TRACE);
			if (skin.legacy) { mixin(S_TRACE);
				selClassic = .nabs(_prop.toAppAbs(skin.engine));
			}
		} else { mixin(S_TRACE);
			if (_skinInfo.length) { mixin(S_TRACE);
				auto info = _skinInfo[_type.getSelectionIndex()];
				selType = info.type;
				selName = info.name;
				selPath = info.path;
			} else { mixin(S_TRACE);
				int i = _type.getSelectionIndex();
				if (-1 != i && i < _classicEngines.length) { mixin(S_TRACE);
					if (_hasLegacySkin) { mixin(S_TRACE);
						if (0 < i) { mixin(S_TRACE);
							selClassic = _prop.toAppAbs(_classicEngines[i - 1].enginePath);
						}
					} else { mixin(S_TRACE);
						selClassic = _prop.toAppAbs(_classicEngines[i].enginePath);
					}
				}
				if (selClassic) selClassic = .nabs(selClassic);
			}
		}
		_classicEngines = [];
		foreach (e; _prop.var.etc.classicEngines) { mixin(S_TRACE);
			if (!_skinIncSearch.match(e.name)) continue;
			_classicEngines ~= e.dup;
		}

		_type.removeAll();
		_skinInfo = [];
		void initSkin() { mixin(S_TRACE);
			// XML形式のスキン
			Skin[] skins;
			foreach (key, value; skinTable(_prop)) { mixin(S_TRACE);
				skins ~= value;
			}
			if (!skins.length) { mixin(S_TRACE);
				// スキンが無い
				skins ~= .findSkin2(_prop, _prop.var.etc.defaultSkin, _prop.var.etc.defaultSkinName);
			}
			int skinCmp(in Skin skin1, in Skin skin2) { mixin(S_TRACE);
				if (_prop.var.etc.logicalSort) { mixin(S_TRACE);
					int i = ncmp(skin1.name, skin2.name);
					if (i == 0) i = ncmp(skin1.type, skin2.type);
					return i;
				} else { mixin(S_TRACE);
					int i = cmp(skin1.name, skin2.name);
					if (i == 0) i = cmp(skin1.type, skin2.type);
					return i;
				}
			}
			skins = sort!(skinCmp)(skins);
			ptrdiff_t typeSkin = -1;
			auto i = 0;
			foreach (skin2; skins) { mixin(S_TRACE);
				auto name = skin2.name;
				auto type = skin2.type;
				if (skin2.isEmpty) { mixin(S_TRACE);
					name = _prop.var.etc.defaultSkinName;
					type = _prop.var.etc.defaultSkin;
				}
				if (!_skinIncSearch.match(name)) continue;
				_type.add(.tryFormat("%s(%s)", name, type));
				_skinInfo ~= typeof(_skinInfo[0])(type, name, skin2.path);
				if (selPath == "") { mixin(S_TRACE);
					if (type == selType) { mixin(S_TRACE);
						if (typeSkin == -1) typeSkin = i;
						if (name == selName) _type.select(cast(int)i);
					}
				} else if (skin2.path == selPath) { mixin(S_TRACE);
					_type.select(cast(int)i);
				}
				i++;
			}
			if (!incSearch && _type.getSelectionIndex() == -1) { mixin(S_TRACE);
				assert (0 < i);
				if (typeSkin == -1) { mixin(S_TRACE);
					_type.select(cast(int)typeSkin);
				} else { mixin(S_TRACE);
					_type.select(0);
				}
			}
		}
		_hasLegacySkin = false;
		string resDir, lEnginePath;
		auto curSkin = Skin.findLegacy(_summ.scenarioPath, resDir, lEnginePath, _prop.var.etc.classicEngineRegex,
			_prop.var.etc.classicDataDirRegex, _prop.var.etc.classicMatchKey, _prop.var.etc.classicEngines);
		lEnginePath = nabs(lEnginePath);
		bool cur = 0 != lEnginePath.length;
		if (_typeSkin.getSelection()) { mixin(S_TRACE);
			initSkin();
		} else { mixin(S_TRACE);
			assert (_typeClassic.getSelection());
			// クラシックエンジンのリソース
			auto i = 0;
			foreach (ce; _classicEngines) { mixin(S_TRACE);
				_type.add(ce.name);
				if (selClassic && cfnmatch(selClassic, _prop.toAppAbs(ce.enginePath))) { mixin(S_TRACE);
					_type.select(cast(int)i);
				}
				if (cur && cfnmatch(lEnginePath, _prop.toAppAbs(ce.enginePath))) { mixin(S_TRACE);
					cur = false;
					if (-1 == _type.getSelectionIndex()) _type.select(cast(int)i);
				}
				i++;
			}
			if (!incSearch) { mixin(S_TRACE);
				if (cur) { mixin(S_TRACE);
					_type.add(_prop.msgs.defaultSelection(lEnginePath), 0);
					_hasLegacySkin = true;
				}
				if (!_type.getItemCount()) { mixin(S_TRACE);
					// クラシックエンジンが無い
					_typeClassic.setSelection(false);
					_typeSkin.setSelection(true);
					initSkin();
				}
			}
		}
		if (!incSearch) { mixin(S_TRACE);
			_typeClassic.setEnabled(!_readOnly && (cur || _classicEngines.length));
			if (-1 == _type.getSelectionIndex() && _type.getItemCount()) { mixin(S_TRACE);
				_type.select(0);
			}
		}
	}

public:
	this(Commons comm, Props prop, Shell shell, Summary summ, bool readOnly) { mixin(S_TRACE);
		assert (summ !is null);
		_readOnly = readOnly ? SWT.READ_ONLY : SWT.NONE;
		_comm = comm;
		_summ = summ;
		_prop = prop;
		if (_readOnly || !_summ || _summ.scenarioPath == "") { mixin(S_TRACE);
			_summSkin = findSkin(_comm, _prop, _summ);
		}
		super(prop, shell, _readOnly, false, .tryFormat(_prop.msgs.dlgTitSummary, _summ.scenarioName),
			_prop.images.summary, true, _prop.var.summaryDlg, true);
	}

protected:
	override void setup(Composite area) { mixin(S_TRACE);
		area.setLayout(windowGridLayout(2, false));

		auto tabf = new CTabFolder(area, SWT.BORDER);
		tabf.setLayoutData(new GridData(GridData.FILL_BOTH));
		constructTab1(tabf);
		constructTab2(tabf);
		constructImage(area);
		tabf.setSelection(0);

		if (!_readOnly) { mixin(S_TRACE);
			_comm.refScenario.add(&refScenario);
		}
		_comm.refSkin.add(&refSkin);
		_comm.refSortCondition.add(&refreshTypes);
		_comm.refClassicSkin.add(&refreshTypes);
		_comm.refDataVersion.add(&updateDataVersion);
		_comm.refTargetVersion.add(&refreshWarning);
		area.addDisposeListener(new Dispose);

		void closeAdds(Summary summ) { mixin(S_TRACE);
			if (_summ is summ) forceCancel();
		}
		_comm.closeAdds.add(&closeAdds);
		.listener(getShell(), SWT.Dispose, () => _comm.closeAdds.remove(&closeAdds));

		ignoreMod = true;
		scope (exit) ignoreMod = false;

		updateDataVersion();
		_lastImageScale = _prop.var.etc.imageScale;
	}

	@property
	private string[] rCoupons() { mixin(S_TRACE);
		string[] rcs;
		foreach (s; .splitLines(_rCoupons.getText())) { mixin(S_TRACE);
			if (s.length > 0) { mixin(S_TRACE);
				rcs ~= s;
			}
		}
		return rcs;
	}

	override bool apply() { mixin(S_TRACE);
		if (_readOnly) return true;
		auto images = _imgPath.materialPath(forceApplying);
		if (images.cancel) return false;
		string oldName = _summ.scenarioName;
		string oldResDir = nabs(summSkin.resDir);
		scope (exit) {
			if (oldName != _summ.scenarioName) _comm.refScenarioName.call();
			if (!cfnmatch(oldResDir, nabs(summSkin.resDir))) _comm.refSkin.call(this);
			_comm.refUseCount.call();
		}
		_summ.setBaseParams(_sname.getText(), _author.getText());
		_summ.desc = _desc.getRRText();
		_summ.imagePaths = images.images;
		_summ.levelMin = _levMin.getSelection();
		_summ.levelMax = _levMax.getSelection();
		_summ.rCoupons = rCoupons;
		_summ.rCouponNum = _rCouponNum.getSelection();
		_summ.startArea = _startArea.startArea;
		if (_typeSkin.getSelection()) { mixin(S_TRACE);
			auto t = _skinInfo[_type.getSelectionIndex()];
			_summ.type = t.type;
			_summ.skinName = t.name;
		} else { mixin(S_TRACE);
			_summ.type = "";
			_summ.skinName = "";
		}
		auto oldSkin = _comm.skin;
		selectedSkin.initialize();
		_comm.skin = selectedSkin;
		_comm.updateSkinMaterialsExtension(_summ.useCounter, oldSkin, _comm.skin);
		_comm.refCoupons.call();
		getShell().setText(.tryFormat(_prop.msgs.dlgTitSummary, _summ.scenarioName));
		auto ver = dataVersion();
		if (_summ.dataVersion != ver) { mixin(S_TRACE);
			_summ.dataVersion = ver;
			if (!_summ.legacy) { mixin(S_TRACE);
				_comm.refDataVersion.call();
			}
		}
		auto loadScaledImage = _loadScaledImage.getSelection();
		if (_summ.loadScaledImage != loadScaledImage) { mixin(S_TRACE);
			_summ.loadScaledImage = loadScaledImage;
			_comm.refImageScale.call();
			_comm.refUseCount.call();
		}
		if (_summ.legacy && _prop.var.etc.replaceClassicResourceExtension && (!oldSkin.legacy && !oldSkin.sourceOfMaterialsIsClassicEngine) != (!_comm.skin.legacy && !_comm.skin.sourceOfMaterialsIsClassicEngine)) { mixin(S_TRACE);
			_comm.summary.changedAll();
		}
		return true;
	}
}

private class SummaryPreview : Composite {

	private Commons _comm;
	private Props _prop;
	private Summary _summ;
	private bool delegate() _loadScaledImage;
	private string delegate() _dataVersion;

	private Canvas _summImage;
	private string delegate() _sname = null;
	private CardImage[] delegate() _imgPaths = null;
	private Skin delegate() _selectedSkin = null;
	private string delegate() _desc = null;
	private int delegate() _levMin = null;
	private int delegate() _levMax = null;

	private Image _summImageBuf = null;
	private ImageDataWithScale _bufImgData = null;

	private class PListener : PaintListener {
		override void paintControl(PaintEvent e) { mixin(S_TRACE);
			if (!_imgPaths) return;
			auto d = Display.getCurrent();
			auto drawingScaleImg = _loadScaledImage() ? _prop.drawingScale : 1;
			auto drawingScale = _prop.drawingScale;
			auto size = _prop.looks.summarySize;
			size.width *= drawingScale;
			size.height *= drawingScale;
			auto rect = _summImage.getClientArea();
			scope buf = new Image(d, size.width, size.height);
			scope (exit) buf.dispose();
			scope gc = new GC(buf);
			scope (exit) gc.dispose();

			auto skin = _selectedSkin();
			auto imgPaths = _imgPaths();
			if (!_summImageBuf || .summary(skin, drawingScale) !is _bufImgData) { mixin(S_TRACE);
				if (_summImageBuf) _summImageBuf.dispose();
				_bufImgData = .summary(skin, drawingScale);
				_summImageBuf = new Image(d, _bufImgData.scaled(drawingScale));
			}

			if (_summImageBuf) gc.drawImage(_summImageBuf, 0 * drawingScale, 0 * drawingScale);

			string imgFile(in CardImage imgPath) { mixin(S_TRACE);
				auto wsnVer = _dataVersion();
				return imgPath.type is CardImageType.File ? nabs(skin.findImagePath(imgPath.path, _summ.scenarioPath, wsnVer)) : "";
			}
			auto wsnVer = _dataVersion();
			foreach (i, imgPath; imgPaths) { mixin(S_TRACE);
				final switch (imgPath.type) {
				case CardImageType.File:
					auto isSkinMaterial = false;
					auto isEngineMaterial = false;
					string p = skin.findImagePathF(imgPath.path, _summ.scenarioPath, wsnVer, isSkinMaterial, isEngineMaterial);
					auto fDrawingScale = (isSkinMaterial || isEngineMaterial) ? drawingScale : drawingScaleImg;
					if (p.length) { mixin(S_TRACE);
						auto image = new Image(d, .loadImageWithScale(_prop, skin, _summ, p, drawingScaleImg, true, false).scaled(drawingScale));
						final switch (imgPath.positionType) {
						case CardImagePosition.Center:
							auto b = image.getBounds();
							gc.drawImage(image, (cast(int)size.width - b.width) / 2, (cast(int)size.height - b.height) / 2);
							break;
						case CardImagePosition.TopLeft:
						case CardImagePosition.Default:
							gc.drawImage(image, _prop.looks.summaryImageXY.x * drawingScale, _prop.looks.summaryImageXY.y * drawingScale);
							break;
						}
						image.dispose();
					}
					break;
				case CardImageType.PCNumber:
					// Invalid data.
					break;
				case CardImageType.Talker:
					// Invalid data.
					break;
				}
			}
			{ mixin(S_TRACE);
				void drawCenterText(in CFont fontData, string text, int y) { mixin(S_TRACE);
					auto font = .createFontFromPixels(fontData, 2 <= _prop.drawingScale);
					gc.setFont(font);
					auto p = gc.stringExtent(text);
					gc.wDrawText(text, (size.width - p.x) / 2, y, true);
					font.dispose();
				}
				int alpha;
				scope c = new Color(d, dwtData(_prop.looks.summaryLevelColor, alpha));
				gc.setForeground(c);
				gc.setAlpha(alpha);
				int levL = _levMin();
				int levH = _levMax();
				if (_summ.legacy && (levH < levL || levL == 0 && levH != 0)) { mixin(S_TRACE);
					levH = levL;
				}
				string levText;
				if (levL > 0 && levL == levH) { mixin(S_TRACE);
					levText = .tryFormat(_prop.msgs.targetLevelSame, levL);
				} else if (levL > 0 && levH > 0) { mixin(S_TRACE);
					levText = .tryFormat(_prop.msgs.targetLevelHL, levL, levH);
				} else if (levL > 0 && levH == 0) { mixin(S_TRACE);
					levText = .tryFormat(_prop.msgs.targetLevelL, levL);
				} else if (levL == 0 && levH > 0) { mixin(S_TRACE);
					levText = .tryFormat(_prop.msgs.targetLevelH, levH);
				} else { mixin(S_TRACE);
					levText = "";
				}
				auto lFont = _prop.adjustFont(_prop.looks.summaryLevelFont(skin.legacy));
				lFont.point *= drawingScale;
				drawCenterText(lFont, levText, _prop.looks.summaryLevelY * drawingScale);
				c.dispose();
				gc.setAlpha(255);
				gc.setForeground(d.getSystemColor(SWT.COLOR_BLACK));
				auto tFont = _prop.adjustFont(_prop.looks.summaryTitleFont(skin.legacy));
				tFont.point *= drawingScale;
				drawCenterText(tFont, _sname(), _prop.looks.summaryTitleY * drawingScale);
				auto dFont = _prop.adjustFont(_prop.looks.summaryDescFont(skin.legacy));
				dFont.point *= drawingScale;
				auto font = .createFontFromPixels(dFont, 2 <= _prop.drawingScale);
				gc.setFont(font);
				int hig = gc.getFontMetrics().getHeight();
				int x = _prop.looks.summaryDescXY.x *= drawingScale;
				int y = _prop.looks.summaryDescXY.y *= drawingScale;
				string desc = _desc();
				if (_comm.skin.legacy) { mixin(S_TRACE);
					foreach (line; splitLines(desc)) { mixin(S_TRACE);
						gc.wDrawText(line, x, y, SWT.DRAW_DELIMITER | SWT.DRAW_TRANSPARENT);
						y += _prop.looks.summaryDescLineHeightClassic * drawingScale;
					}
				} else { mixin(S_TRACE);
					gc.wDrawText(desc, x, y, SWT.DRAW_DELIMITER | SWT.DRAW_TRANSPARENT);
				}
				gc.setFont(null);
				font.dispose();
				auto pFont = _prop.adjustFont(_prop.looks.summaryPageFont(skin.legacy));
				pFont.point *= drawingScale;
				drawCenterText(pFont, _prop.msgs.summaryPageDummy, _prop.looks.summaryPageY * drawingScale);
			}
			auto bx = (rect.width - _prop.s(_prop.looks.summarySize.width)) / 2;
			auto by = (rect.height - _prop.s(_prop.looks.summarySize.height)) / 2;
			e.gc.drawImage(buf, _prop.s(0), _prop.s(0), size.width, size.height,
				bx, by, _prop.s(_prop.looks.summarySize.width), _prop.s(_prop.looks.summarySize.height));
		}
	}

	this (Commons comm, Composite parent, int style, bool delegate() loadScaledImage, string delegate() dataVersion) { mixin(S_TRACE);
		super (parent, style);
		_comm = comm;
		_prop = comm.prop;
		_summ = comm.summary;
		_loadScaledImage = loadScaledImage;
		_dataVersion = dataVersion;

		this.setLayout(new FillLayout());

		auto grp = new Group(this, SWT.NONE);
		grp.setLayoutData(new GridData(GridData.FILL_BOTH));
		grp.setText(_prop.msgs.summaryPreview);
		grp.setLayout(new CenterLayout(SWT.HORIZONTAL | SWT.VERTICAL));

		_summImage = new Canvas(grp, SWT.BORDER | SWT.DOUBLE_BUFFERED);
		updateScale();
		_summImage.addPaintListener(new PListener);

		_comm.refSkin.add(&clearBuf);
		.listener(this, SWT.Dispose, { mixin(S_TRACE);
			_comm.refSkin.remove(&clearBuf);
			clearBuf();
		});
	}

	void updateScale() { mixin(S_TRACE);
		auto size = _prop.looks.summarySize;
		_summImage.setLayoutData(_summImage.computeSize(_prop.s(size.width), _prop.s(size.height)));
	}

	void setImageSelect(string delegate() sname, CardImage[] delegate() imgPaths, Skin delegate() selectedSkin, string delegate() desc, int delegate() levMin, int delegate() levMax) { mixin(S_TRACE);
		_sname = sname;
		_imgPaths = imgPaths;
		_selectedSkin = selectedSkin;
		_desc = desc;
		_levMin = levMin;
		_levMax = levMax;
	}

	void redrawImage() { mixin(S_TRACE);
		_summImage.redraw();
	}

	void clearBuf() { mixin(S_TRACE);
		if (_summImageBuf) _summImageBuf.dispose();
		_summImageBuf = null;
		_bufImgData = null;
	}
}
