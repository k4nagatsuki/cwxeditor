/// エディタの設定を行なうダイアログ。
module cwx.editor.gui.dwt.settingsdialog;

import cwx.event;
import cwx.graphics;
import cwx.imagesize;
import cwx.menu;
import cwx.props;
import cwx.script;
import cwx.structs;
import cwx.summary;
import cwx.types;
import cwx.utils;
import cwx.variables;

import cwx.editor.gui.sound;

import cwx.editor.gui.dwt.absdialog;
import cwx.editor.gui.dwt.autokeycode;
import cwx.editor.gui.dwt.centerlayout;
import cwx.editor.gui.dwt.commons;
import cwx.editor.gui.dwt.contentinitializer;
import cwx.editor.gui.dwt.customtable;
import cwx.editor.gui.dwt.customtext;
import cwx.editor.gui.dwt.dmenu;
import cwx.editor.gui.dwt.dockingfolder;
import cwx.editor.gui.dwt.dprops;
import cwx.editor.gui.dwt.dskin;
import cwx.editor.gui.dwt.dutils;
import cwx.editor.gui.dwt.etcsettings;
import cwx.editor.gui.dwt.history;
import cwx.editor.gui.dwt.incsearch;
import cwx.editor.gui.dwt.properties;
import cwx.editor.gui.dwt.scripterrordialog;
import cwx.editor.gui.dwt.skintypesettings;
import cwx.editor.gui.dwt.splitpane;
import cwx.editor.gui.dwt.toolspane;
import cwx.editor.gui.dwt.undo;

static import std.algorithm;
import std.algorithm : map, max, min, stripRight;
import std.array;
import std.conv;
import std.file;
import std.functional;
import std.math;
import std.path;
import std.range : iota;
import std.string;
import std.traits;

import org.eclipse.swt.all;

import java.lang.all;

enum BackupType {
	Time = 0,
	Edit = 1,
}

class SettingsDialog : AbsDialog {
private:
	Commons _comm;
	Props _prop;
	Summary _summ;
	DockingFolderCTC _dock;
	void delegate() _sendReloadProps;

	CTabItem _tabB;
	Text _enginePath;
	Button _refEnginePath;
	Button _findEnginePath;
	Text _tempDir;
	Text _backupDir;
	Button _backupEnabled;
	Button _backupIntervalTypeTime;
	Button _backupIntervalTypeEdit;
	Spinner _backupIntervalTime;
	Spinner _backupIntervalEdit;
	Spinner _backupCount;
	Button _backupRefAuthor;
	Button _autoSave;
	Button _backupArchived;
	Button _backupRef;
	Button _backupDirOpen;
	Text _backupBeforeSaveDir;
	Button _backupBeforeSaveEnabled;
	Button _backupBeforeSaveRef;
	Button _backupBeforeSaveDirOpen;
	Text _author;
	Text _newAreaName;
	Text _wallpaper;
	Combo _wallpaperStyle;
	int[int] _wallpaperStyleTbl;
	int[int] _wallpaperStyleTbl2;
	Button _clearHist;
	Spinner _histMax;
	Button _clearSHist;
	Spinner _sHistMax;
	Button _clearPHist;
	Spinner _pHistMax;
	Spinner _undoMaxMainView;
	Spinner _undoMaxEvent;
	Spinner _undoMaxReplace;
	Spinner _undoMaxEtc;

	CTabFolder _tabf;

	CTabItem _tabS;
	SkinTypeChooser _skinType;
	Button _overrideBgImages;
	Button _overrideSelections;
	Button _overrideKeyCodes;
	Button _defBgImg;
	DefBgImgDialog _defBgImgDlg = null;
	BgImageS[] _bgImagesDefault;
	BgImageSetting[] _bgImageSettings;
	string[] _standardSelections;
	string[] _standardKeyCodes;
	KeyCodeByFeature[] _keyCodesByFeatures;
	KeyCodeByMotion[] _keyCodesByMotions;
	ToolsPane!BgImageSetting _bgStgs;
	Text _selections;
	UndoManager _selectionsUndo;
	Text _keyCodes;
	UndoManager _keyCodesUndo;
	KeyCodeByFeatureView _keyCodesByFeaturesView;
	KeyCodeByMotionView _keyCodesByMotionsView;
	ElementOverrideView _elementOverrides;

	CTabItem _tabT;
	ToolsPane!OuterTool _tools;
	ToolsPane!ClassicEngine _cEngines;

	CTabItem _tabC;
	ToolsPane!ScTemplate _scTempls;
	ToolsPane!EvTemplate _evTempls;

	ContentInitialValueEditor _initializer;
	ContentInitializer[] _initializers = [];
	string[] _initializerKeys = [];
	UndoManager[] _initializerUndos = [];
	int _selectedInitializer = 0;

	CTabItem _tabE;
	Combo _drawingScale;
	Combo _imageScale;
	Combo _language;
	string[int] _msgsTableIndex;
	string[string] _msgsTableFile;
	Text _ignorePaths;

	EtcSettings _etcSettings;
	Combo _targetVersion;
	int[string] _targetVersionTbl;
	string[int] _targetVersionTbl2;
	Combo _soundPlayType;
	int[int] _soundPlayTypeTbl;
	int[int] _soundPlayTypeTbl2;
	Spinner _bgmVolume;
	Combo _soundEffectPlayType;
	int[int] _soundEffectPlayTypeTbl;
	int[int] _soundEffectPlayTypeTbl2;
	Spinner _seVolume;
	Combo _dialogStatus;
	int[int] _dialogStatusTbl;
	int[int] _dialogStatusTbl2;
	Combo _showStatusTime;
	int[int] _showStatusTimeTbl;
	int[int] _showStatusTimeTbl2;
	Text _savedSound;

	Combo _flagInitValue;
	Combo _stepInitValue;
	Text _stepValueName;

	Text _mnemonic;
	HotKeyField _hotkey;
	string[MenuID] _appliedMnemonic;
	string[MenuID] _appliedHotkey;
	Table _menu;
	Button _menuApply;
	Button _menuDel;
	class SMenuData {
		MenuID id;
		string mnemonic;
		string hotkey;
	}
	IncSearch _menuIncSearch;
	void menuIncSearch() { mixin(S_TRACE);
		_menuIncSearch.startIncSearch();
	}

	class RefE : SelectionAdapter, ModifyListener {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			refreshEnabled();
		}
		override void modifyText(ModifyEvent e) { mixin(S_TRACE);
			refreshEnabled();
		}
	}
	string dropEngine(string[] files) { mixin(S_TRACE);
		if (!files.length) return "";
		string file = files[0];
		if (cfnmatch(baseName(file), _prop.var.etc.engine)) { mixin(S_TRACE);
			return file;
		} else { mixin(S_TRACE);
			return "";
		}
	}
	string dropSysSound(string[] files) { mixin(S_TRACE);
		if (!files.length) return "";
		foreach (file; files) { mixin(S_TRACE);
			string ext = .toLower(.extension(file));
			if (.contains!("a == b", string, string)(_comm.skin.extSound, ext)) { mixin(S_TRACE);
				return file;
			}
		}
		return "";
	}

	string dropWallpaper(string[] files) { mixin(S_TRACE);
		if (!files.length) return "";
		foreach (file; files) { mixin(S_TRACE);
			if (.contains!("a == b", string, string)(_comm.skin.extImage, .toLower(.extension(file)))) { mixin(S_TRACE);
				return file;
			}
		}
		return "";
	}
	void selectEngine() { mixin(S_TRACE);
		auto ext = [_prop.var.etc.engine, "cardwirth.py"].join(";");
		selectFile(_enginePath, [.tryFormat(_prop.msgs.filterEnginePath, ext)], [ext],
			_prop.var.etc.engine, .tryFormat(_prop.msgs.dlgTitEnginePath, _prop.var.etc.engine),
			_prop.var.etc.enginePath);
	}
	void selectSysSound(Text widget) { mixin(S_TRACE);
		string[] extArr;
		foreach (sse; _comm.skin.extSound) { mixin(S_TRACE);
			extArr ~= "*" ~ sse;
		}
		string exts = std.string.join(extArr, ";");
		selectFile(widget, [.tryFormat(_prop.msgs.playableSounds, exts)], [exts],
			baseName(widget.getText()), _prop.msgs.dlgTitSystemSound,
			widget.getText());
	}
	void selectTemp() { mixin(S_TRACE);
		selectDir(_prop, _tempDir, _prop.msgs.tempDir, _prop.msgs.tempDirDesc, _prop.tempPath);
	}
	void selectBackup() { mixin(S_TRACE);
		selectDir(_prop, _backupDir, _prop.msgs.backupDir, _prop.msgs.backupDirDesc, _prop.backupPath);
	}
	void selectBackupBeforeSave() { mixin(S_TRACE);
		selectDir(_prop, _backupBeforeSaveDir, _prop.msgs.backupBeforeSaveDir, _prop.msgs.backupBeforeSaveDirDesc, _prop.backupBeforeSavePath);
	}
	void selectWallpaper() { mixin(S_TRACE);
		string[] filterName = [_prop.msgs.filterImage, _prop.msgs.filterAll];
		string[] filter = [
			"*" ~ std.string.join(_comm.skin.extImage.dup, ";*"),
			"*"
		];
		selectFile(_wallpaper, filterName, filter, _prop.var.etc.wallpaper, _prop.msgs.dlgTitWallpaper, getcwd());
	}
	class SelEngine : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { selectEngine(); }
	}
	class SelTemp : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { selectTemp(); }
	}
	class SelBackup : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { selectBackup(); }
	}
	class SelBackupBeforeSave : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { selectBackupBeforeSave(); }
	}
	class SelWallpaper : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { selectWallpaper(); }
	}
	class SelSysSound : SelectionAdapter {
		private Text _text;
		this (Text text) { mixin(S_TRACE);
			_text = text;
		}
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			selectSysSound(_text);
		}
	}
	void refHistories() { mixin(S_TRACE);
		_clearHist.setEnabled(_prop.var.etc.openHistories.length > 0);
	}
	void refSearchHistories() { mixin(S_TRACE);
		_clearSHist.setEnabled(_prop.var.etc.searchHistories.length || _prop.var.etc.replaceHistories.length);
	}
	class ClearHist : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			auto dlg = new MessageBox(_histMax.getShell(), SWT.ICON_QUESTION | SWT.OK | SWT.CANCEL);
			dlg.setText(_prop.msgs.dlgTitQuestion);
			dlg.setMessage(_prop.msgs.dlgMsgHistoryClear);
			if (SWT.OK == dlg.open()) { mixin(S_TRACE);
				_prop.var.etc.openHistories = [];
				_comm.refHistories.call();
				_prop.var.save(_dock);
				_sendReloadProps();
			}
		}
	}
	class ClearSHist : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			auto dlg = new MessageBox(_sHistMax.getShell(), SWT.ICON_QUESTION | SWT.OK | SWT.CANCEL);
			dlg.setText(_prop.msgs.dlgTitQuestion);
			dlg.setMessage(_prop.msgs.dlgMsgSearchHistoryClear);
			if (SWT.OK == dlg.open()) { mixin(S_TRACE);
				_prop.var.etc.searchHistories = [];
				_prop.var.etc.replaceHistories = [];
				_prop.var.etc.grepDirHistories = [];
				_comm.refSearchHistories.call();
				_prop.var.save(_dock);
				_sendReloadProps();
			}
		}
	}
	class ClearPHist : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			auto dlg = new MessageBox(_pHistMax.getShell(), SWT.ICON_QUESTION | SWT.OK | SWT.CANCEL);
			dlg.setText(_prop.msgs.dlgTitQuestion);
			dlg.setMessage(_prop.msgs.dlgMsgExecutedPartiesClear);
			if (SWT.OK == dlg.open()) { mixin(S_TRACE);
				_prop.var.etc.executedParties = [];
				_comm.refExecutedParties.call();
				_prop.var.save(_dock);
				_sendReloadProps();
			}
		}
	}
	void construct1(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, false));
		_tabB = new CTabItem(tabf, SWT.NONE);
		_tabB.setText(_prop.msgs.baseSettings);
		_tabB.setControl(comp);
		{ mixin(S_TRACE);
			auto comp2 = new Composite(comp, SWT.NONE);
			comp2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			comp2.setLayout(zeroMarginGridLayout(3, false));
			{ mixin(S_TRACE);
				auto grp = new Group(comp2, SWT.NONE);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setLayout(normalGridLayout(3, false));
				grp.setText(.tryFormat(_prop.msgs.enginePath, _prop.var.etc.engine));
				_enginePath = new Text(grp, SWT.BORDER);
				createTextMenu!Text(_comm, _prop, _enginePath, &catchMod);
				_enginePath.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				mod(_enginePath);
				_refEnginePath = new Button(grp, SWT.PUSH);
				_refEnginePath.setText(_prop.msgs.reference);
				_refEnginePath.addSelectionListener(new SelEngine);
				createOpenButton(_comm, grp, &_enginePath.getText, false);
				_findEnginePath = new Button(grp, SWT.CHECK);
				mod(_findEnginePath);
				_findEnginePath.setText(_prop.msgs.findEnginePath);
				auto gd = new GridData;
				gd.horizontalSpan = 3;
				_findEnginePath.setLayoutData(gd);
				setupDropFile(grp, _enginePath, &dropEngine);

				.listener(_enginePath, SWT.Modify, { mixin(S_TRACE);
					_findEnginePath.setEnabled(0 == _enginePath.getText().length);
				});
				.listener(_findEnginePath, SWT.Selection, { mixin(S_TRACE);
					_enginePath.setEnabled(!_findEnginePath.getSelection());
					_refEnginePath.setEnabled(!_findEnginePath.getSelection());
				});
			}
			{ mixin(S_TRACE);
				auto grp = new Group(comp2, SWT.NONE);
				grp.setLayoutData(new GridData(GridData.FILL_VERTICAL));
				grp.setText(_prop.msgs.previewScale);
				grp.setLayout(normalGridLayout(2, false));
				auto l1 = new Label(grp, SWT.NONE);
				l1.setText(_prop.msgs.drawingScale);
				_drawingScale = new Combo(grp, SWT.READ_ONLY | SWT.DROP_DOWN | SWT.BORDER);
				mod(_drawingScale);
				_drawingScale.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
				_drawingScale.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				auto l2 = new Label(grp, SWT.NONE);
				l2.setText(_prop.msgs.imageScale);
				_imageScale = new Combo(grp, SWT.READ_ONLY | SWT.DROP_DOWN | SWT.BORDER);
				mod(_imageScale);
				_imageScale.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
				_imageScale.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

				auto ca = _imageScale.getDisplay().getClientArea();
				int imageScale = 1;
				auto vs = _prop.looks.viewSize;
				_drawingScale.add(.tryFormat(_prop.msgs.imageScaleValue, imageScale));
				_drawingScale.select(0);
				_imageScale.add(.tryFormat(_prop.msgs.imageScaleValue, imageScale));
				_imageScale.select(0);
				imageScale *= 2;
				while (vs.width * imageScale <= ca.width && vs.height * imageScale <= ca.height) { mixin(S_TRACE);
					_drawingScale.add(.tryFormat(_prop.msgs.imageScaleValue, imageScale));
					if (imageScale == _prop.var.etc.drawingScale) { mixin(S_TRACE);
						_drawingScale.select(_drawingScale.getItemCount() - 1);
					}
					_imageScale.add(.tryFormat(_prop.msgs.imageScaleValue, imageScale));
					if (imageScale == _prop.var.etc.imageScale) { mixin(S_TRACE);
						_imageScale.select(_imageScale.getItemCount() - 1);
					}
					imageScale *= 2;
				}
			}
			{ mixin(S_TRACE);
				auto grp = new Group(comp2, SWT.NONE);
				grp.setLayoutData(new GridData(GridData.FILL_VERTICAL));
				grp.setText(_prop.msgs.languageSetting);
				grp.setLayout(normalGridLayout(1, true));
				string defLocale;
				auto msgsTable = _prop.parent.msgsTable(_prop.var.etc.languageDir, _msgsTableFile, defLocale);
				_language = new Combo(grp, SWT.READ_ONLY | SWT.DROP_DOWN | SWT.BORDER);
				mod(_language);
				_language.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
				_language.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				_language.add(_prop.msgs.defaultSelection(_prop.msgs.languageSystem));
				_language.select(0);
				string curLocale = _prop.msgs.locale;
				foreach (locale; std.algorithm.sort(msgsTable.keys)) { mixin(S_TRACE);
					_language.add(msgsTable[locale].localeName);
					int index = _language.getItemCount() - 1;
					_msgsTableIndex[index] = locale;
					if (!_prop.var.etc.useSystemLanguage && 0 == icmp(curLocale, locale)) { mixin(S_TRACE);
						_language.select(index);
					}
				}
				auto l = new Label(grp, SWT.NONE);
				l.setText(_prop.msgs.languageCaution);
			}
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setLayout(normalGridLayout(3, false));
			grp.setText(_prop.msgs.tempDir);
			_tempDir = new Text(grp, SWT.BORDER);
			createTextMenu!Text(_comm, _prop, _tempDir, &catchMod);
			_tempDir.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			mod(_tempDir);
			auto refr = new Button(grp, SWT.PUSH);
			refr.setText(_prop.msgs.reference);
			refr.addSelectionListener(new SelTemp);
			createOpenButton(_comm, grp, &_tempDir.getText, true);
			setupDropFile(grp, _tempDir, toDelegate(&dropDir));
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.backupDir);
			auto gl = normalGridLayout(3, false);
			gl.horizontalSpacing = 10.ppis;
			grp.setLayout(gl);

			{ mixin(S_TRACE);
				_backupEnabled = new Button(grp, SWT.CHECK);
				_backupEnabled.setText(_prop.msgs.backupEnabled);
				mod(_backupEnabled);
				_backupEnabled.addSelectionListener(_refe);
			}
			{ mixin(S_TRACE);
				auto comp2 = new Composite(grp, SWT.NONE);
				comp2.setLayoutData(new GridData(GridData.FILL_VERTICAL));
				comp2.setLayout(zeroMarginGridLayout(6, false));

				_backupIntervalTypeTime = new Button(comp2, SWT.RADIO);
				_backupIntervalTypeTime.setText(_prop.msgs.backupIntervalTime);
				_backupIntervalTime = new Spinner(comp2, SWT.BORDER);
				initSpinner(_backupIntervalTime);
				_backupIntervalTime.setMinimum(1);
				_backupIntervalTime.setMaximum(99);
				mod(_backupIntervalTime);
				auto l2 = new Label(comp2, SWT.CENTER);
				l2.setText(_prop.msgs.minute);
				_backupIntervalTypeTime.addSelectionListener(_refe);

				_backupIntervalTypeEdit = new Button(comp2, SWT.RADIO);
				_backupIntervalTypeEdit.setText(_prop.msgs.backupIntervalEdit);
				_backupIntervalEdit = new Spinner(comp2, SWT.BORDER);
				initSpinner(_backupIntervalEdit);
				_backupIntervalEdit.setMinimum(1);
				_backupIntervalEdit.setMaximum(9999);
				mod(_backupIntervalEdit);
				auto l3 = new Label(comp2, SWT.CENTER);
				l3.setText(_prop.msgs.count);
				_backupIntervalTypeEdit.addSelectionListener(_refe);
			}
			{ mixin(S_TRACE);
				auto comp2 = new Composite(grp, SWT.NONE);
				comp2.setLayoutData(new GridData(GridData.FILL_VERTICAL));
				comp2.setLayout(zeroMarginGridLayout(2, false));
				auto l = new Label(comp2, SWT.CENTER);
				l.setText(_prop.msgs.backupCount);
				_backupCount = new Spinner(comp2, SWT.BORDER);
				initSpinner(_backupCount);
				_backupCount.setMinimum(0);
				_backupCount.setMaximum(99);
				mod(_backupCount);
			}
			{ mixin(S_TRACE);
				auto comp2 = new Composite(grp, SWT.NONE);
				auto gd = new GridData(GridData.FILL_HORIZONTAL);
				gd.horizontalSpan = 3;
				comp2.setLayoutData(gd);
				comp2.setLayout(zeroMarginGridLayout(4, false));
				auto l = new Label(comp2, SWT.NONE);
				l.setText(_prop.msgs.backupPath);
				_backupDir = new Text(comp2, SWT.BORDER);
				createTextMenu!Text(_comm, _prop, _backupDir, &catchMod);
				_backupDir.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				mod(_backupDir);
				_backupRef = new Button(comp2, SWT.PUSH);
				_backupRef.setText(_prop.msgs.reference);
				_backupRef.addSelectionListener(new SelBackup);
				_backupDirOpen = createOpenButton(_comm, comp2, &_backupDir.getText, true);
				setupDropFile(grp, _backupDir, toDelegate(&dropDir));
			}
			{ mixin(S_TRACE);
				auto comp2 = new Composite(grp, SWT.NONE);
				auto gd = new GridData(GridData.FILL_HORIZONTAL);
				gd.horizontalSpan = 3;
				comp2.setLayoutData(gd);
				comp2.setLayout(zeroMarginGridLayout(3, false));

				_backupRefAuthor = new Button(comp2, SWT.CHECK);
				_backupRefAuthor.setText(_prop.msgs.backupRefAuthor);
				mod(_backupRefAuthor);
				_backupRefAuthor.addSelectionListener(_refe);
				_autoSave = new Button(comp2, SWT.CHECK);
				_autoSave.setText(_prop.msgs.autoSave);
				mod(_autoSave);
				_autoSave.addSelectionListener(_refe);
				_backupArchived = new Button(comp2, SWT.CHECK);
				_backupArchived.setText(_prop.msgs.backupArchived);
				mod(_backupArchived);
				_backupArchived.addSelectionListener(_refe);
			}
		}
		{ mixin(S_TRACE);
			auto grp = new Group(comp, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.backupBeforeSaveDir);
			grp.setLayout(normalGridLayout(1, false));

			{ mixin(S_TRACE);
				_backupBeforeSaveEnabled = new Button(grp, SWT.CHECK);
				_backupBeforeSaveEnabled.setText(_prop.msgs.backupBeforeSaveEnabled);
				mod(_backupBeforeSaveEnabled);
				_backupBeforeSaveEnabled.addSelectionListener(_refe);
			}
			{ mixin(S_TRACE);
				auto comp2 = new Composite(grp, SWT.NONE);
				auto gd = new GridData(GridData.FILL_HORIZONTAL);
				gd.horizontalSpan = 3;
				comp2.setLayoutData(gd);
				comp2.setLayout(zeroMarginGridLayout(4, false));
				auto l = new Label(comp2, SWT.NONE);
				l.setText(_prop.msgs.backupBeforeSavePath);
				_backupBeforeSaveDir = new Text(comp2, SWT.BORDER);
				createTextMenu!Text(_comm, _prop, _backupBeforeSaveDir, &catchMod);
				_backupBeforeSaveDir.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				mod(_backupBeforeSaveDir);
				_backupBeforeSaveRef = new Button(comp2, SWT.PUSH);
				_backupBeforeSaveRef.setText(_prop.msgs.reference);
				_backupBeforeSaveRef.addSelectionListener(new SelBackupBeforeSave);
				_backupBeforeSaveDirOpen = createOpenButton(_comm, comp2, &_backupBeforeSaveDir.getText, true);
				setupDropFile(grp, _backupBeforeSaveDir, toDelegate(&dropDir));
			}
		}
		{ mixin(S_TRACE);
			auto comp2 = new Composite(comp, SWT.NONE);
			comp2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			comp2.setLayout(zeroMarginGridLayout(2, false));
			{ mixin(S_TRACE);
				auto comp3 = new Composite(comp2, SWT.NONE);
				comp3.setLayoutData(new GridData(GridData.FILL_BOTH));
				comp3.setLayout(zeroMarginGridLayout(1, false));
				{ mixin(S_TRACE);
					auto sash = new SplitPane(comp3, SWT.HORIZONTAL);
					sash.setLayoutData(new GridData(GridData.FILL_BOTH));
					{ mixin(S_TRACE);
						auto grp = new Group(sash, SWT.NONE);
						auto cl = new CenterLayout;
						cl.fillHorizontal = true;
						grp.setLayout(cl);
						grp.setText(_prop.msgs.scenarioAuthor);
						_author = new Text(grp, SWT.BORDER);
						createTextMenu!Text(_comm, _prop, _author, &catchMod);
						mod(_author);
					}
					{ mixin(S_TRACE);
						auto grp = new Group(sash, SWT.NONE);
						auto cl = new CenterLayout;
						cl.fillHorizontal = true;
						grp.setLayout(cl);
						grp.setText(_prop.msgs.startAreaName);
						_newAreaName = new Text(grp, SWT.BORDER);
						createTextMenu!Text(_comm, _prop, _newAreaName, &catchMod);
						mod(_newAreaName);
					}
					.setupWeights(sash, _prop.var.etc.authorNewAreaNameSashL, _prop.var.etc.authorNewAreaNameSashR);
				}
				{ mixin(S_TRACE);
					auto grp = new Group(comp3, SWT.NONE);
					grp.setLayoutData(new GridData(GridData.FILL_BOTH));
					grp.setLayout(normalGridLayout(1, true));
					grp.setText(_prop.msgs.wallpaper);
					{ mixin(S_TRACE);
						auto comp4 = new Composite(grp, SWT.NONE);
						comp4.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
						comp4.setLayout(zeroMarginGridLayout(3, false));
						_wallpaper = new Text(comp4, SWT.BORDER);
						createTextMenu!Text(_comm, _prop, _wallpaper, &catchMod);
						_wallpaper.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
						mod(_wallpaper);
						auto refr = new Button(comp4, SWT.PUSH);
						refr.setText(_prop.msgs.reference);
						refr.addSelectionListener(new SelWallpaper);
						createOpenButton(_comm, comp4, &_wallpaper.getText, false);
					}
					{ mixin(S_TRACE);
						auto comp4 = new Composite(grp, SWT.NONE);
						comp4.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
						comp4.setLayout(zeroMarginGridLayout(2, false));

						int[] styles;
						string[] names;
						foreach (s; [WallpaperStyle.Center, WallpaperStyle.Tile, WallpaperStyle.ExpandFull, WallpaperStyle.Expand]) { mixin(S_TRACE);
							styles ~= cast(int)s;
							names ~= _prop.msgs.wallpaperStyleName(s);
						}
						_wallpaperStyle = createEnumC(comp4, _prop.msgs.wallpaperStyle, styles, names, _wallpaperStyleTbl, _wallpaperStyleTbl2);
					}
					setupDropFile(grp, _wallpaper, &dropWallpaper);
				}
				{ mixin(S_TRACE);
					auto grp = new Group(comp3, SWT.NONE);
					grp.setLayoutData(new GridData(GridData.FILL_BOTH));
					grp.setText(_prop.msgs.systemSounds);
					grp.setLayout(normalGridLayout(7, false));
					auto l = new Label(grp, SWT.NONE);
					l.setText(_prop.msgs.soundSaved);
					_savedSound = new Text(grp, SWT.BORDER);
					createTextMenu!Text(_comm, _prop, _savedSound, &catchMod);
					_savedSound.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					mod(_savedSound);
					auto refr = new Button(grp, SWT.PUSH);
					refr.setText(_prop.msgs.reference);
					refr.addSelectionListener(new SelSysSound(_savedSound));
					createOpenButton(_comm, grp, &_savedSound.getText, false);
					setupDropFile(grp, _savedSound, &dropSysSound);

					auto sep = new Label(grp, SWT.SEPARATOR);
					auto sgd = new GridData(GridData.FILL_VERTICAL);
					sgd.heightHint = 0;
					sep.setLayoutData(sgd);

					auto play = new Button(grp, SWT.PUSH);
					play.setToolTipText(_prop.msgs.menuText(MenuID.PlaySE));
					play.setImage(_prop.images.menu(MenuID.PlaySE));
					.listener(play, SWT.Selection, { mixin(S_TRACE);
						string file = _savedSound.getText();
						if (file.length && .exists(file)) { mixin(S_TRACE);
							playSE(file, 0, 1, lastSoundType, true);
						}
					});
					auto stop = new Button(grp, SWT.PUSH);
					stop.setToolTipText(_prop.msgs.menuText(MenuID.StopSE));
					stop.setImage(_prop.images.menu(MenuID.StopSE));
					.listener(stop, SWT.Selection, { mixin(S_TRACE);
						stopSE();
					});
				}
			}
			{ mixin(S_TRACE);
				auto comp3 = new Composite(comp2, SWT.NONE);
				comp3.setLayout(zeroMarginGridLayout(1, true));
				comp3.setLayoutData(new GridData(GridData.FILL_VERTICAL));
				{ mixin(S_TRACE);
					auto grp = new Group(comp3, SWT.NONE);
					grp.setText(_prop.msgs.historiesSettings);
					grp.setLayoutData(new GridData(GridData.FILL_BOTH));
					grp.setLayout(normalGridLayout(3, false));
					{ mixin(S_TRACE);
						auto lComp = new Composite(grp, SWT.NONE);
						auto cl = new CenterLayout(SWT.HORIZONTAL | SWT.VERTICAL, 0);
						cl.fillHorizontal = true;
						lComp.setLayout(cl);
						lComp.setLayoutData(new GridData(GridData.FILL_BOTH));
						auto l = new Label(lComp, SWT.NONE);
						l.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
						l.setText(_prop.msgs.openHistoryMax);
						_histMax = new Spinner(grp, SWT.BORDER);
						initSpinner(_histMax);
						_histMax.setMinimum(0);
						_histMax.setMaximum(99);
						mod(_histMax);
						_clearHist = new Button(grp, SWT.PUSH);
						_comm.put(_clearHist, () => 0 < _prop.var.etc.openHistories.length);
						_clearHist.setText(_prop.msgs.openHistoryClear);
						_clearHist.addSelectionListener(new ClearHist);
					}
					{ mixin(S_TRACE);
						auto lComp = new Composite(grp, SWT.NONE);
						auto cl = new CenterLayout(SWT.HORIZONTAL | SWT.VERTICAL, 0);
						cl.fillHorizontal = true;
						lComp.setLayout(cl);
						lComp.setLayoutData(new GridData(GridData.FILL_BOTH));
						auto l = new Label(lComp, SWT.NONE);
						l.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
						l.setText(_prop.msgs.searchHistoryMax);
						_sHistMax = new Spinner(grp, SWT.BORDER);
						initSpinner(_sHistMax);
						_sHistMax.setMinimum(0);
						_sHistMax.setMaximum(99);
						mod(_sHistMax);
						_clearSHist = new Button(grp, SWT.PUSH);
						_comm.put(_clearSHist, () => _prop.var.etc.searchHistories.length || _prop.var.etc.grepDirHistories.length);
						_clearSHist.setText(_prop.msgs.searchHistoryClear);
						_clearSHist.addSelectionListener(new ClearSHist);
					}
					{ mixin(S_TRACE);
						auto lComp = new Composite(grp, SWT.NONE);
						auto cl = new CenterLayout(SWT.HORIZONTAL | SWT.VERTICAL, 0);
						cl.fillHorizontal = true;
						lComp.setLayout(cl);
						lComp.setLayoutData(new GridData(GridData.FILL_BOTH));
						auto l = new Label(lComp, SWT.NONE);
						l.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
						l.setText(_prop.msgs.executedPartiesMax);
						_pHistMax = new Spinner(grp, SWT.BORDER);
						initSpinner(_pHistMax);
						_pHistMax.setMinimum(0);
						_pHistMax.setMaximum(99);
						mod(_pHistMax);
						_clearPHist = new Button(grp, SWT.PUSH);
						_comm.put(_clearPHist, () => 0 < _prop.var.etc.executedParties.length);
						_clearPHist.setText(_prop.msgs.executedPartiesClear);
						_clearPHist.addSelectionListener(new ClearPHist);
					}
				}
				{ mixin(S_TRACE);
					auto grp = new Group(comp3, SWT.NONE);
					grp.setText(_prop.msgs.undoMax);
					grp.setLayoutData(new GridData(GridData.FILL_BOTH));
					grp.setLayout(normalGridLayout(2, false));
					Spinner createUndoMax(string title) { mixin(S_TRACE);
						auto l = new Label(grp, SWT.NONE);
						l.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
						l.setText(title);
						auto spn = new Spinner(grp, SWT.BORDER);
						initSpinner(spn);
						mod(spn);
						spn.setMaximum(_prop.var.etc.undoMaxLimit);
						spn.setMinimum(0);
						return spn;
					}
					_undoMaxMainView = createUndoMax(_prop.msgs.undoMaxMainView);
					_undoMaxEvent = createUndoMax(_prop.msgs.undoMaxEvent);
					_undoMaxReplace = createUndoMax(_prop.msgs.undoMaxReplace);
					_undoMaxEtc = createUndoMax(_prop.msgs.undoMaxEtc);
				}
			}
		}
	}
	void construct2(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		_tabS = new CTabItem(tabf, SWT.NONE);
		_tabS.setText(_prop.msgs.bgImageAndSelections);
		_tabS.setControl(comp);
		comp.setLayout(normalGridLayout(1, true));

		_skinType = new SkinTypeChooser(_comm, comp, SWT.NONE, (del) { mixin(S_TRACE);
			if (!_bgStgs.noApply) return true;
			if (del) { mixin(S_TRACE);
				return true;
			}
			auto dlg = new MessageBox(getShell(), SWT.YES | SWT.NO | SWT.CANCEL | SWT.ICON_QUESTION);
			dlg.setText(_prop.msgs.dlgTitQuestion);
			auto name = _bgStgs.editingName;
			if (name == "") name = _comm.prop.msgs.noNameData;
			dlg.setMessage(.tryFormat(_prop.msgs.dlgMsgForceApplySelection, name));
			final switch (dlg.open()) {
			case SWT.YES:
				_bgStgs.forceApply();
				return true;
			case SWT.NO:
				return true;
			case SWT.CANCEL:
				return false;
			}
			assert (0);
		}, { mixin(S_TRACE);
			return SettingsWithSkinType("", true, true, true, _bgImagesDefault.dup, _bgImageSettings.dup, _standardSelections.dup, _standardKeyCodes.dup, _keyCodesByFeatures.dup, _keyCodesByMotions.dup);
		});
		_skinType.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		_skinType.selectEvent ~= &selectSkinType;

		const(string)[] standardKeyCodes() { mixin(S_TRACE);
			return _skinType.selected == -1 ? _standardKeyCodes : _skinType.settingsWithSkinTypes[_skinType.selected].standardKeyCodes;
		}
		const(ElementOverride)[] elementOverrides() { mixin(S_TRACE);
			if (_skinType.selected == -1) return [];
			return _skinType.settingsWithSkinTypes[_skinType.selected].elementOverrides;
		}

		auto sep = new Label(comp, SWT.SEPARATOR | SWT.HORIZONTAL);
		sep.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		{ mixin(S_TRACE);
			auto grp = new Group(comp, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			grp.setText(_prop.msgs.overrideDefaultSettings);
			grp.setLayout(new GridLayout(3, true));
			_overrideBgImages = new Button(grp, SWT.CHECK);
			mod(_overrideBgImages);
			_overrideBgImages.setText(_prop.msgs.overrideBgImagesSettings);
			.listener(_overrideBgImages, SWT.Selection, &updateOverrideBgImages);
			_overrideSelections = new Button(grp, SWT.CHECK);
			mod(_overrideSelections);
			_overrideSelections.setText(_prop.msgs.overrideSelectionsSettings);
			.listener(_overrideSelections, SWT.Selection, &updateOverrideSelections);
			_overrideKeyCodes = new Button(grp, SWT.CHECK);
			mod(_overrideKeyCodes);
			_overrideKeyCodes.setText(_prop.msgs.overrideKeyCodesSettings);
			.listener(_overrideKeyCodes, SWT.Selection, &updateOverrideKeyCodes);
		}

		auto sash = new SplitPane(comp, SWT.HORIZONTAL);
		sash.setLayoutData(new GridData(GridData.FILL_BOTH));
		auto back = new Composite(sash, SWT.NONE);
		back.setLayout(zeroMarginGridLayout(1, true));
		{ mixin(S_TRACE);
			auto grp = new Group(back, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			auto cl = new CenterLayout;
			cl.fillHorizontal = true;
			grp.setLayout(cl);
			grp.setText(_prop.msgs.bgImagesDefault);
			_defBgImg = new Button(grp, SWT.PUSH);
			_defBgImg.setText(_prop.msgs.setBgImagesDefault);

			void defBgSetting() { mixin(S_TRACE);
				if (_defBgImgDlg) { mixin(S_TRACE);
					_defBgImgDlg.active();
					return;
				}
				auto skinType = _skinType.selected == -1 ? _prop.var.etc.defaultSkin : _skinType.settingsWithSkinTypes[_skinType.selected].type;
				auto skin = .findSkin2(_prop, skinType, "");
				if (skin.isEmpty) { mixin(S_TRACE);
					foreach (ref ce; _comm.prop.var.etc.classicEngines) { mixin(S_TRACE);
						if (ce.type == skinType) {
							skin = .createClassicSkin(_prop, ce);
							break;
						}
					}
				}
				_defBgImgDlg = new DefBgImgDialog(_comm, _prop, getShell(), _skinType.selected == -1 ? _bgImagesDefault : _skinType.settingsWithSkinTypes[_skinType.selected].bgImagesDefault, skin);
				_defBgImgDlg.appliedEvent ~= { mixin(S_TRACE);
					if (_skinType.selected == -1) { mixin(S_TRACE);
						_bgImagesDefault = _defBgImgDlg.backs;
						_initializer.bgImagesDefault = _defBgImgDlg.backs;
					} else { mixin(S_TRACE);
						_skinType.settingsWithSkinTypes[_skinType.selected].bgImagesDefault = _defBgImgDlg.backs;
					}
					applyEnabled();
				};
				_defBgImgDlg.closeEvent ~= { mixin(S_TRACE);
					_defBgImgDlg = null;
				};
				_defBgImgDlg.open();
			}
			.listener(_defBgImg, SWT.Selection, &defBgSetting);
		}
		auto sashL = new SplitPane(back, SWT.VERTICAL);
		auto slgd = new GridData(GridData.FILL_BOTH);
		slgd.heightHint = 0;
		sashL.setLayoutData(slgd);
		{ mixin(S_TRACE);
			_bgStgs = new ToolsPane!BgImageSetting(_comm, (b) { ignoreMod = b; }, &catchMod, { mixin(S_TRACE);
				if (_skinType.selected == -1) { mixin(S_TRACE);
					_bgImageSettings = _bgStgs.array;
				} else { mixin(S_TRACE);
					_skinType.settingsWithSkinTypes[_skinType.selected].bgImageSettings = _bgStgs.array;
				}
				applyEnabled();
			}, sashL, SWT.NONE);
		}
		auto sashKC = new SplitPane(sashL, SWT.HORIZONTAL);
		{ mixin(S_TRACE);
			auto grp = new Group(sashKC, SWT.NONE);
			grp.setLayout(normalGridLayout(1, false));
			grp.setText(_prop.msgs.keyCodesByFeatures);
			_keyCodesByFeaturesView = new KeyCodeByFeatureView(_comm, grp, SWT.NONE, &standardKeyCodes, &catchMod);
			mod(_keyCodesByFeaturesView);
			_keyCodesByFeaturesView.modEvent ~= { mixin(S_TRACE);
				if (_skinType.selected == -1) { mixin(S_TRACE);
					_keyCodesByFeatures = _keyCodesByFeaturesView.values;
				} else { mixin(S_TRACE);
					_skinType.settingsWithSkinTypes[_skinType.selected].keyCodesByFeatures = _keyCodesByFeaturesView.values;
				}
			};
			_keyCodesByFeaturesView.setLayoutData(new GridData(GridData.FILL_BOTH));
		}
		{ mixin(S_TRACE);
			auto grp = new Group(sashKC, SWT.NONE);
			grp.setLayout(normalGridLayout(1, false));
			grp.setText(_prop.msgs.keyCodesByMotions);
			_keyCodesByMotionsView = new KeyCodeByMotionView(_comm, grp, SWT.NONE, &standardKeyCodes, &elementOverrides, &catchMod);
			mod(_keyCodesByMotionsView);
			_keyCodesByMotionsView.modEvent ~= { mixin(S_TRACE);
				if (_skinType.selected == -1) { mixin(S_TRACE);
					_keyCodesByMotions = _keyCodesByMotionsView.values;
				} else { mixin(S_TRACE);
					_skinType.settingsWithSkinTypes[_skinType.selected].keyCodesByMotions = _keyCodesByMotionsView.values;
				}
			};
			_keyCodesByMotionsView.setLayoutData(new GridData(GridData.FILL_BOTH));
		}
		.setupWeights(sashL, _prop.var.etc.bgImageSettingsKeyCodesSashL, _prop.var.etc.bgImageSettingsKeyCodesSashR);
		.setupWeights(sashKC, _prop.var.etc.keyCodesByFeaturesKeyCodesByMotionSashL, _prop.var.etc.keyCodesByFeaturesKeyCodesByMotionSashR);
		{ mixin(S_TRACE);
			auto sashR = new SplitPane(sash, SWT.VERTICAL);
			{ mixin(S_TRACE);
				auto grp = new Group(sashR, SWT.NONE);
				grp.setLayoutData(new GridData(GridData.FILL_BOTH));
				grp.setLayout(normalGridLayout(1, false));
				grp.setText(_prop.msgs.standardSelections);
				_selections = new Text(grp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
				_selectionsUndo = createEtcUndo(_comm, _selections);
				createTextMenu!Text(_comm, _prop, _selections, &catchMod, _selectionsUndo);
				mod(_selections);
				.listener(_selections, SWT.Modify, { mixin(S_TRACE);
					auto selections = _selections.getText().splitLines().stripRight("");
					if (_skinType.selected == -1) { mixin(S_TRACE);
						_standardSelections = selections;
					} else { mixin(S_TRACE);
						_skinType.settingsWithSkinTypes[_skinType.selected].standardSelections = selections;
					}
				});
				_selections.setTabs(_prop.var.etc.tabs);
				auto gd = new GridData(GridData.FILL_BOTH);
				gd.widthHint = _prop.var.etc.selectionWidth;
				gd.heightHint = 0;
				_selections.setLayoutData(gd);
			}
			auto sashR2 = new SplitPane(sashR, SWT.VERTICAL);
			{ mixin(S_TRACE);
				auto grp = new Group(sashR2, SWT.NONE);
				grp.setLayout(normalGridLayout(1, false));
				grp.setText(_prop.msgs.standardKeyCode);
				_keyCodes = new Text(grp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
				_keyCodesUndo = createEtcUndo(_comm, _keyCodes);
				createTextMenu!Text(_comm, _prop, _keyCodes, &catchMod, _keyCodesUndo);
				mod(_keyCodes);
				.listener(_keyCodes, SWT.Modify, { mixin(S_TRACE);
					auto keyCodes = _keyCodes.getText().splitLines().stripRight("");
					if (_skinType.selected == -1) { mixin(S_TRACE);
						_standardKeyCodes = keyCodes;
					} else { mixin(S_TRACE);
						_skinType.settingsWithSkinTypes[_skinType.selected].standardKeyCodes = keyCodes;
					}
				});
				_keyCodes.setTabs(_prop.var.etc.tabs);
				auto gd = new GridData(GridData.FILL_BOTH);
				gd.widthHint = _prop.var.etc.keyCodeWidth;
				gd.heightHint = 0;
				_keyCodes.setLayoutData(gd);
			}
			{ mixin(S_TRACE);
				auto grp = new Group(sashR2, SWT.NONE);
				grp.setLayout(normalGridLayout(1, false));
				grp.setText(_prop.msgs.elementOverrides);
				_elementOverrides = new ElementOverrideView(_comm, grp, SWT.NONE);
				mod(_elementOverrides);
				_elementOverrides.modEvent ~= { mixin(S_TRACE);
					assert (_skinType.selected != -1);
					_skinType.settingsWithSkinTypes[_skinType.selected].elementOverrides = _elementOverrides.elementOverrides;
					_keyCodesByMotionsView.clearAll();
				};
				_elementOverrides.setLayoutData(new GridData(GridData.FILL_BOTH));
			}
			.setupWeights(sashR, _prop.var.etc.selectionKeyCodeSashL, _prop.var.etc.selectionKeyCodeSashR);
			.setupWeights(sashR2, _prop.var.etc.keyCodesElementSashL, _prop.var.etc.keyCodesElementSashR);
		}
		.setupWeights(sash, _prop.var.etc.bgImageSelectionSashL, _prop.var.etc.bgImageSelectionSashR);
	}
	void selectSkinType() { mixin(S_TRACE);
		ignoreMod = true;
		scope (exit) ignoreMod = false;
		if (_defBgImgDlg) _defBgImgDlg.forceCancel();
		if (_skinType.selected == -1) { mixin(S_TRACE);
			_bgStgs.array = _bgImageSettings;
			_selections.setText(_standardSelections.join("\n").lastRet());
			_keyCodes.setText(_standardKeyCodes.join("\n").lastRet());
			_keyCodesByFeaturesView.values = _keyCodesByFeatures;
			_keyCodesByMotionsView.values = _keyCodesByMotions;
			_elementOverrides.elementOverrides = [];
			_elementOverrides.enabled = false;
		} else { mixin(S_TRACE);
			_bgStgs.array = _skinType.settingsWithSkinTypes[_skinType.selected].bgImageSettings;
			_selections.setText(_skinType.settingsWithSkinTypes[_skinType.selected].standardSelections.join("\n").lastRet());
			_keyCodes.setText(_skinType.settingsWithSkinTypes[_skinType.selected].standardKeyCodes.join("\n").lastRet());
			_keyCodesByFeaturesView.values = _skinType.settingsWithSkinTypes[_skinType.selected].keyCodesByFeatures;
			_keyCodesByMotionsView.values = _skinType.settingsWithSkinTypes[_skinType.selected].keyCodesByMotions;
			_elementOverrides.elementOverrides = _skinType.settingsWithSkinTypes[_skinType.selected].elementOverrides;
			_elementOverrides.enabled = true;
		}
		_selectionsUndo.reset();
		_keyCodesUndo.reset();

		_overrideBgImages.setSelection(_skinType.selected != -1 && _skinType.settingsWithSkinTypes[_skinType.selected].overrideBgImages);
		_overrideBgImages.setEnabled(_skinType.selected != -1);
		_overrideSelections.setSelection(_skinType.selected != -1 && _skinType.settingsWithSkinTypes[_skinType.selected].overrideSelections);
		_overrideSelections.setEnabled(_skinType.selected != -1);
		_overrideKeyCodes.setSelection(_skinType.selected != -1 && _skinType.settingsWithSkinTypes[_skinType.selected].overrideKeyCodes);
		_overrideKeyCodes.setEnabled(_skinType.selected != -1);
		updateOverrideBgImages();
		updateOverrideSelections();
		updateOverrideKeyCodes();
	}
	void updateOverrideBgImages() { mixin(S_TRACE);
		auto enabled = _skinType.selected == -1 || _overrideBgImages.getSelection();
		if (!enabled && _defBgImgDlg) _defBgImgDlg.forceCancel();
		_defBgImg.setEnabled(enabled);
		_bgStgs.enabled = enabled;
		if (_skinType.selected != -1) _skinType.settingsWithSkinTypes[_skinType.selected].overrideBgImages = enabled;
	}
	void updateOverrideSelections() { mixin(S_TRACE);
		auto enabled = _skinType.selected == -1 || _overrideSelections.getSelection();
		_selections.setEnabled(enabled);
		if (_skinType.selected != -1) _skinType.settingsWithSkinTypes[_skinType.selected].overrideSelections = enabled;
	}
	void updateOverrideKeyCodes() { mixin(S_TRACE);
		auto enabled = _skinType.selected == -1 || _overrideKeyCodes.getSelection();
		_keyCodes.setEnabled(enabled);
		_keyCodesByFeaturesView.enabled = enabled;
		_keyCodesByMotionsView.enabled = enabled;
		if (_skinType.selected != -1) _skinType.settingsWithSkinTypes[_skinType.selected].overrideKeyCodes = enabled;
	}

	void construct3(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(2, false));
		_tabT = new CTabItem(tabf, SWT.NONE);
		_tabT.setText(_prop.msgs.templates);
		_tabT.setControl(comp);

		auto sash = new SplitPane(comp, SWT.VERTICAL);
		sash.setLayoutData(new GridData(GridData.FILL_BOTH));

		_evTempls = new ToolsPane!EvTemplate(_comm, (b) { ignoreMod = b; }, &catchMod, &applyEnabled, sash, SWT.NONE);
		_evTempls.setLayoutData(new GridData(GridData.FILL_BOTH));
		_scTempls = new ToolsPane!ScTemplate(_comm, (b) { ignoreMod = b; }, &catchMod, &applyEnabled, sash, SWT.NONE);
		_scTempls.setLayoutData(new GridData(GridData.FILL_BOTH));

		.setupWeights(sash, _prop.var.etc.templatesSashL, _prop.var.etc.templatesSashR);

		auto initGrp = new Group(comp, SWT.NONE);
		initGrp.setText(_prop.msgs.contentInitializer);
		initGrp.setLayoutData(new GridData(GridData.FILL_VERTICAL));
		initGrp.setLayout(normalGridLayout(1, true));
		auto inits = new Combo(initGrp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
		mod(inits);
		inits.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		inits.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);

		inits.add(_prop.msgs.defaultSelection(_prop.msgs.baseInitializers));
		_initializerKeys ~= "Basic";
		_initializerUndos ~= new UndoManager(_comm.prop.var.etc.undoMaxEtc);
		inits.add(_prop.msgs.defaultSelection(_prop.msgs.classic));
		_initializerKeys ~= "Classic";
		_initializerUndos ~= new UndoManager(_comm.prop.var.etc.undoMaxEtc);
		_selectedInitializer = 0;
		foreach (i, ver; VERSION_NAMES) { mixin(S_TRACE);
			inits.add(ver);
			_initializerKeys ~= VERSIONS[i];
			_initializerUndos ~= new UndoManager(_comm.prop.var.etc.undoMaxEtc);
		}

		inits.select(_selectedInitializer);

		_initializer = new ContentInitialValueEditor(_comm, initGrp);
		mod(_initializer);
		auto igd = new GridData(GridData.FILL_BOTH);
		igd.heightHint = 0;
		_initializer.widget.setLayoutData(igd);
		_initializer.bgImagesDefault = _prop.var.etc.bgImagesDefault;
		_initializers = _prop.var.etc.contentInitializers.dup;
		selectInitializers2();
		.listener(inits, SWT.Selection, &selectInitializers);

		void refUndoMax() { mixin(S_TRACE);
			foreach (undo; _initializerUndos) { mixin(S_TRACE);
				undo.max = _comm.prop.var.etc.undoMaxEtc;
			}
		}
		_comm.refUndoMax.add(&refUndoMax);
		.listener(_initializer.widget, SWT.Dispose, { mixin(S_TRACE);
			_comm.refUndoMax.remove(&refUndoMax);
		});
	}
	void selectInitializers(Event e) { mixin(S_TRACE);
		updateInitializers();

		auto combo = cast(Combo)e.widget;
		_selectedInitializer = combo.getSelectionIndex();
		selectInitializers2();
	}
	void updateInitializers() { mixin(S_TRACE);
		auto key = _initializerKeys[_selectedInitializer];
		foreach (ref initializer; _initializers) { mixin(S_TRACE);
			if (initializer.dataVersion == key) { mixin(S_TRACE);
				initializer = ContentInitializer(key, _initializer.getInitializer());
				return;
			}
		}
		_initializers ~= ContentInitializer(key, _initializer.getInitializer());
	}
	void selectInitializers2() { mixin(S_TRACE);
		auto undo = _initializerUndos[_selectedInitializer];
		foreach (ref initializer; _initializers) { mixin(S_TRACE);
			if (initializer.dataVersion == _initializerKeys[_selectedInitializer]) { mixin(S_TRACE);
				if (initializer.dataVersion == "Basic") { mixin(S_TRACE);
					_initializer.setInitializer(initializer.initializer, false, LATEST_VERSION, undo);
				} else if (initializer.dataVersion == "Classic") { mixin(S_TRACE);
					_initializer.setInitializer(initializer.initializer, true, "", undo);
				} else { mixin(S_TRACE);
					_initializer.setInitializer(initializer.initializer, false, initializer.dataVersion, undo);
				}
				return;
			}
		}
		if (_initializerKeys[_selectedInitializer] == "Basic") { mixin(S_TRACE);
			_initializer.setInitializer((Content[CType]).init, false, LATEST_VERSION, undo);
		} else if (_initializerKeys[_selectedInitializer] == "Classic") { mixin(S_TRACE);
			_initializer.setInitializer((Content[CType]).init, true, "", undo);
		} else { mixin(S_TRACE);
			_initializer.setInitializer((Content[CType]).init, false, _initializerKeys[_selectedInitializer], undo);
		}
	}

	void construct4(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		comp.setLayout(normalGridLayout(1, false));
		_tabC = new CTabItem(tabf, SWT.NONE);
		_tabC.setText(_prop.msgs.outerToolsAndClassicEngines);
		_tabC.setControl(comp);

		auto sash = new SplitPane(comp, SWT.VERTICAL);
		sash.setLayoutData(new GridData(GridData.FILL_BOTH));

		_tools = new ToolsPane!OuterTool(_comm, (b) { ignoreMod = b; }, &catchMod, &applyEnabled, sash, SWT.NONE);
		_tools.setLayoutData(new GridData(GridData.FILL_BOTH));
		_cEngines = new ToolsPane!ClassicEngine(_comm, (b) { ignoreMod = b; }, &catchMod, &applyEnabled, sash, SWT.NONE);
		_cEngines.setLayoutData(new GridData(GridData.FILL_BOTH));

		.listener(_tabf, SWT.Selection, { mixin(S_TRACE);
			if (_tabf.getSelection() !is _tabT && _cEngines.ceNameWin.getVisible()) { mixin(S_TRACE);
				_cEngines.ceNameWin.setVisible(false);
				_cEngines.features.setSelection(false);
			}
		});

		.setupWeights(sash, _prop.var.etc.toolsClassicEnginesSashL, _prop.var.etc.toolsClassicEnginesSashR);
	}

	Combo createEnumC(I)(Composite grp, string title, in I[] values, in string[] names, ref int[I] tblA, ref I[int] tblB, int hSpan = 1) { mixin(S_TRACE);
		assert (values.length == names.length);
		auto l = new Label(grp, SWT.NONE);
		l.setText(title);
		auto combo = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
		mod(combo);
		auto gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = hSpan;
		combo.setLayoutData(gd);
		combo.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
		foreach (i, val; values) { mixin(S_TRACE);
			tblA[val] = cast(int)i;
			tblB[cast(int)i] = val;
			combo.add(names[cast(int)i]);
		}
		return combo;
	}
	void selectMenu() { mixin(S_TRACE);
		ignoreMod = true;
		scope (exit) ignoreMod = false;
		auto i = _menu.getSelectionIndex();
		if (-1 == i) return;
		auto itm = _menu.getItem(i);
		auto data = cast(SMenuData)itm.getData();
		_mnemonic.setText(data.mnemonic);
		_hotkey.accelerator = data.hotkey;
		_menuApply.setEnabled(false);
		_menuDel.setEnabled(_mnemonic.getText().length || _hotkey.widget.getText().length);
	}
	void applyMenu() { mixin(S_TRACE);
		ignoreMod = true;
		scope (exit) ignoreMod = false;
		auto i = _menu.getSelectionIndex();
		if (i == -1) return;
		auto itm = _menu.getItem(i);
		auto data = cast(SMenuData)itm.getData();
		data.mnemonic = _mnemonic.getText();
		data.hotkey = _hotkey.acceleratorText;
		itm.setText(MenuProps.buildMenuSample(_prop.parent, data.id, data.mnemonic, data.hotkey));
		_menuApply.setEnabled(false);
		_appliedMnemonic[data.id] = data.mnemonic;
		_appliedHotkey[data.id] = data.hotkey;
		applyEnabled();
	}
	void refreshMenu() { mixin(S_TRACE);
		auto selID = MenuID.None;
		int selIndex = _menu.getSelectionIndex();
		if (-1 != selIndex) { mixin(S_TRACE);
			selID = (cast(SMenuData)_menu.getItem(selIndex).getData()).id;
		}
		_menu.removeAll();
		foreach (id; EnumMembers!MenuID) { mixin(S_TRACE);
			if (id !is MenuID.None && !isNoKeyBindMenu(id)) { mixin(S_TRACE);
				auto pm = id in _appliedMnemonic;
				auto phk = id in _appliedHotkey;
				auto mnemonic = pm ? *pm : _prop.var.menu.mnemonic(id);
				auto hotkey = phk ? *phk : _prop.var.menu.hotkey(id);
				string name = MenuProps.buildMenuSample(_prop.parent, id, mnemonic, hotkey);
				if (!_menuIncSearch.match(name)) continue;
				auto itm = new TableItem(_menu, SWT.NONE);
				itm.setText(name);
				itm.setImage(_prop.images.menu(id));
				auto data = new SMenuData();
				data.id = id;
				data.mnemonic = mnemonic;
				data.hotkey = hotkey;
				itm.setData(data);
				if (id == selID) { mixin(S_TRACE);
					_menu.select(_menu.getItemCount() - 1);
				}
			}
		}
		if (-1 == _menu.getSelectionIndex()) _menu.select(0);
		_menu.showSelection();
		selectMenu();
	}
	class SelectMenu : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			selectMenu();
		}
	}
	class ModMenu : ModifyListener {
		override void modifyText(ModifyEvent e) { mixin(S_TRACE);
			if (ignoreMod) return;
			_menuApply.setEnabled(true);
			_menuDel.setEnabled(_mnemonic.getText().length || _hotkey.widget.getText().length);
		}
	}
	class DelMenuAccel : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			ignoreMod = true;
			scope (exit) ignoreMod = false;
			_mnemonic.setText("");
			_hotkey.widget.setText("");
			applyMenu();
			_menuDel.setEnabled(false);
		}
	}
	class ApplyMenu : SelectionAdapter {
		override void widgetSelected(SelectionEvent e) { mixin(S_TRACE);
			applyMenu();
		}
	}
	void construct5(CTabFolder tabf) { mixin(S_TRACE);
		auto comp = new Composite(tabf, SWT.NONE);
		_tabE = new CTabItem(tabf, SWT.NONE);
		_tabE.setText(_prop.msgs.etcSettings);
		_tabE.setControl(comp);
		comp.setLayout(normalGridLayout(2, false));
		{ mixin(S_TRACE);
			auto grp = new Group(comp, SWT.NONE);
			grp.setLayoutData(new GridData(GridData.FILL_VERTICAL));
			grp.setText(_prop.msgs.etcSettingsTitle);
			grp.setLayout(normalGridLayout(5, false));

			_etcSettings = new EtcSettings(_comm, grp, SWT.NONE);
			mod(_etcSettings);
			auto tgd = new GridData(GridData.FILL_BOTH);
			tgd.horizontalSpan = 5;
			tgd.heightHint = 0;
			_etcSettings.setLayoutData(tgd);

			auto sep = new Label(grp, SWT.SEPARATOR | SWT.HORIZONTAL);
			auto sepgd = new GridData(GridData.FILL_HORIZONTAL);
			sepgd.horizontalSpan = 5;
			sep.setLayoutData(sepgd);

			immutable string[] targetVersionVals = [
				"CardWirthPy",
				"1.50",
				"1.30",
				"1.29",
				"1.28",
			];
			string[] targetVersionNames = [
				_prop.msgs.cardWirthPy.value,
			] ~ .map!((s) => .tryFormat(_prop.msgs.cardWirthWithVersion, s))(targetVersionVals[1..$]).array();
			_targetVersion = createEnumC(grp, _prop.msgs.targetVersion, targetVersionVals, targetVersionNames, _targetVersionTbl, _targetVersionTbl2, 4);
			auto dummy2 = new Composite(grp, SWT.NONE);
			auto dgd2 = new GridData(GridData.FILL_HORIZONTAL);
			dgd2.widthHint = 0;
			dgd2.heightHint = 0;
			dummy2.setLayoutData(dgd2);
			auto tvHint = new Label(grp, SWT.NONE);
			tvHint.setText(_prop.msgs.targetVersionHint);
			auto tvHintGD = new GridData(GridData.FILL_HORIZONTAL);
			tvHintGD.horizontalSpan = 4;
			tvHint.setLayoutData(tvHintGD);

			version (Windows) {
				immutable int[] soundPlayTypeVals = [
					SOUND_TYPE_AUTO,
					SOUND_TYPE_SDL,
					SOUND_TYPE_MCI,
					SOUND_TYPE_APP
				];
				immutable string[] soundPlayTypeNames = [
					_prop.msgs.soundPlayTypeDef,
					_prop.msgs.soundPlayTypeSDL,
					_prop.msgs.soundPlayTypeMCI,
					_prop.msgs.soundPlayTypeApp
				];
			} else { mixin(S_TRACE);
				immutable int[] soundPlayTypeVals = [
					SOUND_TYPE_AUTO,
					SOUND_TYPE_SDL,
					SOUND_TYPE_APP
				];
				immutable string[] soundPlayTypeNames = [
					_prop.msgs.soundPlayTypeDef,
					_prop.msgs.soundPlayTypeSDL,
					_prop.msgs.soundPlayTypeApp
				];
			}
			Spinner createVolume() { mixin(S_TRACE);
				auto l1 = new Label(grp, SWT.CENTER);
				l1.setText(_prop.msgs.soundVolume);
				auto spn = new Spinner(grp, SWT.BORDER);
				initSpinner(spn);
				spn.setMinimum(0);
				spn.setMaximum(100);
				mod(spn);
				auto l2 = new Label(grp, SWT.CENTER);
				l2.setText(_prop.msgs.soundVolumePer);
				return spn;
			}
			_soundPlayType = createEnumC(grp, _prop.msgs.soundPlayType, soundPlayTypeVals, soundPlayTypeNames, _soundPlayTypeTbl, _soundPlayTypeTbl2);
			_bgmVolume = createVolume();
			string sameBGM = _prop.msgs.soundPlaySameBGM;
			_soundEffectPlayType = createEnumC(grp, _prop.msgs.soundEffectPlayType, [SOUND_TYPE_SAME_BGM] ~ soundPlayTypeVals, sameBGM ~ soundPlayTypeNames, _soundEffectPlayTypeTbl, _soundEffectPlayTypeTbl2);
			_seVolume = createVolume();
			auto dummy = new Composite(grp, SWT.NONE);
			auto dgd = new GridData(GridData.FILL_HORIZONTAL);
			dgd.widthHint = 0;
			dgd.heightHint = 0;
			dummy.setLayoutData(dgd);
			auto volc = new Label(grp, SWT.NONE);
			volc.setText(_prop.msgs.soundCaution);
			auto volcgd = new GridData(GridData.FILL_HORIZONTAL);
			volcgd.horizontalSpan = 4;
			volc.setLayoutData(volcgd);

			_dialogStatus = createEnumC(grp, _prop.msgs.dialogStatus, [
				cast(int)DialogStatus.Top,
				cast(int)DialogStatus.Under,
				cast(int)DialogStatus.UnderWithCoupon,
			], [
				_prop.msgs.dialogStatusName(DialogStatus.Top),
				_prop.msgs.dialogStatusName(DialogStatus.Under),
				_prop.msgs.dialogStatusName(DialogStatus.UnderWithCoupon),
			], _dialogStatusTbl, _dialogStatusTbl2, 4);

			_showStatusTime = createEnumC(grp, _prop.msgs.showStatusTime, [
				cast(int)ShowStatusTime.Always,
				cast(int)ShowStatusTime.WithSkin,
				cast(int)ShowStatusTime.No,
			], [
				_prop.msgs.showStatusTimeName(ShowStatusTime.Always),
				_prop.msgs.showStatusTimeName(ShowStatusTime.WithSkin),
				_prop.msgs.showStatusTimeName(ShowStatusTime.No),
			], _showStatusTimeTbl, _showStatusTimeTbl2, 4);

			Combo createInitVarCombo(string name, string[] values) { mixin(S_TRACE);
				auto l = new Label(grp, SWT.NONE);
				l.setText(name);
				auto combo = new Combo(grp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
				mod(combo);
				auto gd = new GridData(GridData.FILL_HORIZONTAL);
				gd.horizontalSpan = 4;
				combo.setLayoutData(gd);
				combo.setVisibleItemCount(_prop.var.etc.comboVisibleItemCount);
				foreach (value; values) combo.add(value);
				return combo;
			}
			_flagInitValue = createInitVarCombo(_prop.msgs.flagInitValue, [_prop.msgs.flagOn, _prop.msgs.flagOff]);
			_stepInitValue = createInitVarCombo(_prop.msgs.stepInitValue, _prop.looks.stepMaxCount.iota().map!((a) => .parseDollarParams(_prop.var.etc.stepValueName, ['N':.to!string(a)])).array());

			auto l = new Label(grp, SWT.NONE);
			l.setText(_prop.msgs.stepValueName);
			_stepValueName = new Text(grp, SWT.BORDER);
			createTextMenu!Text(_comm, _prop, _stepValueName, &catchMod);
			mod(_stepValueName);
			auto gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.horizontalSpan = 4;
			_stepValueName.setLayoutData(gd);
			.listener(_stepValueName, SWT.Modify, { mixin(S_TRACE);
				foreach (i; 0 .. _prop.looks.stepMaxCount) { mixin(S_TRACE);
					_stepInitValue.setItem(i, .parseDollarParams(_stepValueName.getText(), ['N':.to!string(i)]));
				}
			});
		}
		{ mixin(S_TRACE);
			auto sash = new SplitPane(comp, SWT.VERTICAL);
			sash.resizeControl1 = true;
			sash.setLayoutData(new GridData(GridData.FILL_BOTH));
			{ mixin(S_TRACE);
				auto grp = new Group(sash, SWT.NONE);
				grp.setText(_prop.msgs.keyBind);
				grp.setLayout(new FillLayout);
				auto menuComp = new Composite(grp, SWT.NONE);
				menuComp.setLayout(normalGridLayout(4, false));

				auto l1 = new Label(menuComp, SWT.NONE);
				l1.setText(_prop.msgs.mnemonic);
				_mnemonic = mnemonicText(menuComp, SWT.BORDER);
				createTextMenu!Text(_comm, _prop, _mnemonic, &catchMod);
				_mnemonic.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				_mnemonic.addModifyListener(new ModMenu);

				_menuApply = new Button(menuComp, SWT.PUSH);
				_menuApply.setText(_prop.msgs.apply);
				_menuApply.addSelectionListener(new ApplyMenu);
				_menuDel = new Button(menuComp, SWT.PUSH);
				_menuDel.setText(_prop.msgs.del);
				_menuDel.addSelectionListener(new DelMenuAccel);

				auto l2 = new Label(menuComp, SWT.NONE);
				l2.setText(_prop.msgs.hotkey);
				_hotkey = new HotKeyField(menuComp, SWT.BORDER);
				createTextMenu!Text(_comm, _prop, _hotkey.widget, &catchMod);
				auto hgd = new GridData(GridData.FILL_HORIZONTAL);
				hgd.horizontalSpan = 3;
				_hotkey.widget.setLayoutData(hgd);
				_hotkey.widget.addModifyListener(new ModMenu);

				_menu = .rangeSelectableTable(menuComp, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL | SWT.FULL_SELECTION);
				auto mgd = new GridData(GridData.FILL_BOTH);
				mgd.horizontalSpan = 4;
				mgd.heightHint = _prop.var.etc.menuSettingsHeight;
				_menu.setLayoutData(mgd);
				new FullTableColumn(_menu, SWT.NONE);
				_menu.addSelectionListener(new SelectMenu);

				_menuIncSearch = new IncSearch(_comm, menuComp, null);
				_menuIncSearch.modEvent ~= &refreshMenu;

				auto menu = new Menu(_menu.getShell(), SWT.POP_UP);
				createMenuItem(_comm, menu, MenuID.IncSearch, &menuIncSearch, null);
				_menu.setMenu(menu);
				refreshMenu();

				menuComp.setTabList([_menuApply, _menuDel, _menu]);
			}
			{ mixin(S_TRACE);
				auto grp = new Group(sash, SWT.NONE);
				grp.setText(_prop.msgs.ignorePaths);
				grp.setLayout(normalGridLayout(1, false));
				_ignorePaths = new Text(grp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
				createTextMenu!Text(_comm, _prop, _ignorePaths, &catchMod);
				mod(_ignorePaths);
				_ignorePaths.setTabs(_prop.var.etc.tabs);
				auto gdp = new GridData(GridData.FILL_BOTH);
				gdp.widthHint = _prop.var.etc.ignorePathsWidth;
				gdp.heightHint = 0;
				_ignorePaths.setLayoutData(gdp);
			}
			.setupWeights(sash, _prop.var.etc.ignoreMenuSashL, _prop.var.etc.ignoreMenuSashR);
		}
	}
	private RefE _refe;
	private void refreshScenario(Summary summ) { mixin(S_TRACE);
		_summ = summ;
	}
	private void refreshEnabled() { mixin(S_TRACE);
		_backupDir.setEnabled(_backupEnabled.getSelection());
		_backupIntervalTypeTime.setEnabled(_backupEnabled.getSelection());
		_backupIntervalTypeEdit.setEnabled(_backupEnabled.getSelection());
		_backupIntervalTime.setEnabled(_backupEnabled.getSelection() && _backupIntervalTypeTime.getSelection());
		_backupIntervalEdit.setEnabled(_backupEnabled.getSelection() && _backupIntervalTypeEdit.getSelection());
		_backupCount.setEnabled(_backupEnabled.getSelection());
		_backupRef.setEnabled(_backupEnabled.getSelection());
		_backupRefAuthor.setEnabled(_backupEnabled.getSelection());
		_autoSave.setEnabled(_backupEnabled.getSelection());
		_backupArchived.setEnabled(_backupEnabled.getSelection());

		_backupBeforeSaveDir.setEnabled(_backupBeforeSaveEnabled.getSelection());
		_backupBeforeSaveRef.setEnabled(_backupBeforeSaveEnabled.getSelection());
	}
	void refEngineEnabled() { mixin(S_TRACE);
		_findEnginePath.setEnabled(0 == _enginePath.getText().length);
		auto enabled = !_findEnginePath.getSelection() || _enginePath.getText().length;
		_enginePath.setEnabled(enabled);
		_refEnginePath.setEnabled(enabled);
	}
public:
	this (Commons comm, Props prop, Shell shell, DockingFolderCTC dock, Summary summ, void delegate() sendReloadProps) { mixin(S_TRACE);
		super(prop, shell, false, prop.msgs.dlgTitSettings, prop.images.menu(MenuID.Settings), true, prop.var.settingsDlg, true);
		_comm = comm;
		_prop = prop;
		_dock = dock;
		_summ = summ;
		_sendReloadProps = sendReloadProps;
	}

protected:
	override void setup(Composite area) { mixin(S_TRACE);
		area.setLayout(windowGridLayout(1, true));
		_comm.refScenario.add(&refreshScenario);
		_comm.refHistories.add(&refHistories);
		_comm.refSearchHistories.add(&refSearchHistories);
		area.addDisposeListener(new class DisposeListener {
			override void widgetDisposed(DisposeEvent e) { mixin(S_TRACE);
				_comm.refScenario.remove(&refreshScenario);
				_comm.refHistories.remove(&refHistories);
				_comm.refSearchHistories.remove(&refSearchHistories);
			}
		});
		_tabf = new CTabFolder(area, SWT.BORDER);
		_tabf.setLayoutData(new GridData(GridData.FILL_BOTH));
		_refe = new RefE;
		construct1(_tabf);
		construct2(_tabf);
		construct3(_tabf);
		construct4(_tabf);
		construct5(_tabf);
		_tabf.setSelection(0);
		_bgStgs.setup(_prop.var.etc.bgImageSettings, _prop.var.etc.bgImageSettingsSashL, _prop.var.etc.bgImageSettingsSashR);
		_cEngines.setup(_prop.var.etc.classicEngines, _prop.var.etc.classicEnginesSashL, _prop.var.etc.classicEnginesSashR);
		_tools.setup(_prop.var.etc.outerTools, _prop.var.etc.outerToolsSashL, _prop.var.etc.outerToolsSashR);
		_scTempls.setup(_prop.var.etc.scenarioTemplates, _prop.var.etc.scenarioTemplatesSashL, _prop.var.etc.scenarioTemplatesSashR);
		_evTempls.setup(_prop.var.etc.eventTemplates, _prop.var.etc.eventTemplatesSashL, _prop.var.etc.eventTemplatesSashR);
		_tools.setShortcutWeights(_prop.var.etc.outerToolShortcutSashL, _prop.var.etc.outerToolShortcutSashR);
		_cEngines.setShortcutWeights(_prop.var.etc.classicEngineShortcutSashL, _prop.var.etc.classicEngineShortcutSashR);
		_evTempls.setShortcutWeights(_prop.var.etc.eventTemplateShortcutSashL, _prop.var.etc.eventTemplateShortcutSashR);

		_enginePath.setText(_prop.var.etc.enginePath);
		_findEnginePath.setSelection(_prop.var.etc.findEnginePath);
		refEngineEnabled();

		_tempDir.setText(_prop.var.etc.tempPath);

		_backupDir.setText(_prop.var.etc.backupPath);
		_backupEnabled.setSelection(_prop.var.etc.backupEnabled);
		_backupIntervalTypeTime.setSelection(_prop.var.etc.backupIntervalType != BackupType.Edit);
		_backupIntervalTypeEdit.setSelection(_prop.var.etc.backupIntervalType == BackupType.Edit);
		_backupIntervalTime.setSelection(_prop.var.etc.backupInterval);
		_backupIntervalEdit.setSelection(_prop.var.etc.backupIntervalEdit);
		_backupCount.setSelection(_prop.var.etc.backupCount);
		_backupRefAuthor.setSelection(_prop.var.etc.backupRefAuthor);
		_autoSave.setSelection(_prop.var.etc.autoSave);
		_backupArchived.setSelection(_prop.var.etc.backupArchived);

		_backupBeforeSaveDir.setText(_prop.var.etc.backupBeforeSavePath);
		_backupBeforeSaveEnabled.setSelection(_prop.var.etc.backupBeforeSaveEnabled);

		_author.setText(_prop.var.etc.defaultAuthor);
		_newAreaName.setText(_prop.var.etc.newAreaName);
		_wallpaper.setText(_prop.var.etc.wallpaper);
		auto wsp = _prop.var.etc.wallpaperStyle in _wallpaperStyleTbl;
		if (wsp) { mixin(S_TRACE);
			_wallpaperStyle.select(*wsp);
		} else { mixin(S_TRACE);
			_wallpaperStyle.select(WallpaperStyle.Tile);
		}
		_histMax.setSelection(_prop.var.etc.historyMax);
		_sHistMax.setSelection(_prop.var.etc.searchHistoryMax);
		_pHistMax.setSelection(_prop.var.etc.executedPartiesMax);
		_undoMaxMainView.setSelection(_prop.var.etc.undoMaxMainView);
		_undoMaxEvent.setSelection(_prop.var.etc.undoMaxEvent);
		_undoMaxReplace.setSelection(_prop.var.etc.undoMaxReplace);
		_undoMaxEtc.setSelection(_prop.var.etc.undoMaxEtc);
		string ipbuf = "";
		foreach (path; _prop.var.etc.ignorePaths) { mixin(S_TRACE);
			ipbuf ~= path ~ "\n";
		}
		_ignorePaths.setText(ipbuf);

		_bgImagesDefault = _comm.prop.var.etc.bgImagesDefault.dup;
		_bgImageSettings = _comm.prop.var.etc.bgImageSettings.dup;
		_standardSelections = _comm.prop.var.etc.standardSelections.dup;
		_standardKeyCodes = _comm.prop.var.etc.standardKeyCodes.dup;
		_keyCodesByFeatures = _comm.prop.var.etc.keyCodesByFeatures.dup;
		_keyCodesByMotions = _comm.prop.var.etc.keyCodesByMotions.dup;
		selectSkinType();

		auto tVer = _prop.var.etc.targetVersion in _targetVersionTbl;
		if (tVer) { mixin(S_TRACE);
			_targetVersion.select(*tVer);
		} else { mixin(S_TRACE);
			_targetVersion.select(0);
		}
		auto sptp = _prop.var.etc.soundPlayType in _soundPlayTypeTbl;
		if (sptp) { mixin(S_TRACE);
			_soundPlayType.select(*sptp);
		} else { mixin(S_TRACE);
			_soundPlayType.select(0);
		}
		_bgmVolume.setSelection(_prop.var.etc.bgmVolume);
		auto septp = _prop.var.etc.soundEffectPlayType in _soundEffectPlayTypeTbl;
		if (septp) { mixin(S_TRACE);
			_soundEffectPlayType.select(*septp);
		} else { mixin(S_TRACE);
			_soundEffectPlayType.select(0);
		}
		_seVolume.setSelection(_prop.var.etc.seVolume);
		auto dsp = _prop.var.etc.dialogStatus in _dialogStatusTbl;
		if (dsp) { mixin(S_TRACE);
			_dialogStatus.select(*dsp);
		} else { mixin(S_TRACE);
			_dialogStatus.select(DialogStatus.Top);
		}
		auto sst = _prop.var.etc.showStatusTime in _showStatusTimeTbl;
		if (sst) { mixin(S_TRACE);
			_showStatusTime.select(*sst);
		} else { mixin(S_TRACE);
			_showStatusTime.select(ShowStatusTime.Always);
		}
		_savedSound.setText(_prop.var.etc.savedSound);

		_flagInitValue.select(_prop.var.etc.flagInitValue ? 0 : 1);
		_stepInitValue.select(.max(0, .min(_prop.looks.stepMaxCount - 1, _prop.var.etc.stepInitValue)));
		_stepValueName.setText(_prop.var.etc.stepValueName);

		refreshEnabled();
	}

	@property
	const
	override
	bool noScenario() { return true; }

	override bool apply() { mixin(S_TRACE);
		void err(CTabItem tab, Text t, string msg) { mixin(S_TRACE);
			auto dlg = new MessageBox(t.getShell(), SWT.ICON_WARNING | SWT.OK);
			dlg.setText(_prop.msgs.dlgTitWarning);
			dlg.setMessage(msg);
			dlg.open();
			tab.getParent().setSelection(tab);
			t.setFocus();
		}
		string engine;
		try { mixin(S_TRACE);
			engine = _enginePath.getText();
		} catch (Exception e) { mixin(S_TRACE);
			printStackTrace();
			debugln(e);
			err(_tabB, _enginePath, .tryFormat(_prop.msgs.errorEnginePath, _prop.var.etc.engine));
			return false;
		}
		if (engine.length) { mixin(S_TRACE);
			if (!.exists(engine) || .isDir(engine)) { mixin(S_TRACE);
				err(_tabB, _enginePath, .tryFormat(_prop.msgs.errorEnginePath, _prop.var.etc.engine));
				return false;
			}
		}
		string temp;
		try { mixin(S_TRACE);
			temp = _tempDir.getText();
		} catch (Exception e) { mixin(S_TRACE);
			printStackTrace();
			debugln(e);
			err(_tabB, _tempDir, _prop.msgs.errorTempPath);
			return false;
		}
		string backup;
		try { mixin(S_TRACE);
			backup = _backupDir.getText();
		} catch (Exception e) { mixin(S_TRACE);
			printStackTrace();
			debugln(e);
			err(_tabB, _backupDir, _prop.msgs.errorBackupPath);
			return false;
		}
		string backupBeforeSave;
		try { mixin(S_TRACE);
			backupBeforeSave = _backupBeforeSaveDir.getText();
		} catch (Exception e) { mixin(S_TRACE);
			printStackTrace();
			debugln(e);
			err(_tabB, _backupBeforeSaveDir, _prop.msgs.errorBackupBeforeSavePath);
			return false;
		}
		string[] noApplyNames;
		if (_bgStgs.noApply) noApplyNames ~= _bgStgs.boxName;
		if (_tools.noApply) noApplyNames ~= _tools.boxName;
		if (_cEngines.noApply) noApplyNames ~= _cEngines.boxName;
		if (_scTempls.noApply) noApplyNames ~= _scTempls.boxName;
		if (_evTempls.noApply) noApplyNames ~= _evTempls.boxName;
		if (noApplyNames.length) { mixin(S_TRACE);
			auto dlg = new MessageBox(getShell(), SWT.YES | SWT.NO | SWT.CANCEL | SWT.ICON_QUESTION);
			dlg.setText(_prop.msgs.dlgTitQuestion);
			if (1 < noApplyNames.length) { mixin(S_TRACE);
				dlg.setMessage(.tryFormat(_prop.msgs.dlgMsgForceApply, std.string.join(noApplyNames, ", ")));
			} else { mixin(S_TRACE);
				dlg.setMessage(.tryFormat(_prop.msgs.dlgMsgForceApplySingle, noApplyNames[0]));
			}
			final switch (dlg.open()) {
			case SWT.YES:
				if (_bgStgs.noApply) _bgStgs.forceApply();
				if (_tools.noApply) _tools.forceApply();
				if (_cEngines.noApply) _cEngines.forceApply();
				if (_scTempls.noApply) _scTempls.forceApply();
				if (_evTempls.noApply) _evTempls.forceApply();
				break;
			case SWT.NO:
				break;
			case SWT.CANCEL:
				return false;
			}
		}

		auto oldStgs = OldSettings(_prop);
		scope (exit) {
			oldStgs.raiseEvent(_comm);
		}
		_prop.var.etc.enginePath = engine;
		_prop.var.etc.findEnginePath = _findEnginePath.getSelection();

		_prop.var.etc.drawingScale = cast(uint).pow(2, _drawingScale.getSelectionIndex());
		_prop.var.etc.imageScale = cast(uint).pow(2, _imageScale.getSelectionIndex());

		if (_language.getSelectionIndex() <= 0) { mixin(S_TRACE);
			_prop.var.etc.languageFile = "";
			_prop.var.etc.useSystemLanguage = true;
		} else { mixin(S_TRACE);
			_prop.var.etc.languageFile = _msgsTableFile[_msgsTableIndex[_language.getSelectionIndex()]];
			_prop.var.etc.useSystemLanguage = false;
		}
		_prop.var.etc.tempPath = temp;

		_prop.var.etc.backupPath = backup;
		_prop.var.etc.backupEnabled = _backupEnabled.getSelection();
		if (_backupIntervalTypeEdit.getSelection()) { mixin(S_TRACE);
			_prop.var.etc.backupIntervalType = cast(int)BackupType.Edit;
		} else { mixin(S_TRACE);
			_prop.var.etc.backupIntervalType = cast(int)BackupType.Time;
		}
		_prop.var.etc.backupInterval = _backupIntervalTime.getSelection();
		_prop.var.etc.backupIntervalEdit = _backupIntervalEdit.getSelection();
		_prop.var.etc.backupCount = _backupCount.getSelection();
		_prop.var.etc.backupRefAuthor = _backupRefAuthor.getSelection();
		_prop.var.etc.autoSave = _autoSave.getSelection();
		_prop.var.etc.backupArchived = _backupArchived.getSelection();

		_prop.var.etc.backupBeforeSavePath = backupBeforeSave;
		_prop.var.etc.backupBeforeSaveEnabled = _backupBeforeSaveEnabled.getSelection();

		_prop.var.etc.defaultAuthor = _author.getText();
		_prop.var.etc.newAreaName = _newAreaName.getText();
		_prop.var.etc.wallpaper = _wallpaper.getText();
		_prop.var.etc.wallpaperStyle = cast(WallpaperStyle)_wallpaperStyleTbl2[_wallpaperStyle.getSelectionIndex()];
		_prop.var.etc.historyMax = _histMax.getSelection();
		_prop.var.etc.searchHistoryMax = _sHistMax.getSelection();
		_prop.var.etc.executedPartiesMax = _pHistMax.getSelection();
		_prop.var.etc.undoMaxMainView = _undoMaxMainView.getSelection();
		_prop.var.etc.undoMaxEvent = _undoMaxEvent.getSelection();
		_prop.var.etc.undoMaxReplace = _undoMaxReplace.getSelection();
		_prop.var.etc.undoMaxEtc = _undoMaxEtc.getSelection();
		string[] ipLines = splitLines(_ignorePaths.getText());
		if (ipLines.length > 0) { mixin(S_TRACE);
			ptrdiff_t i;
			for (i = ipLines.length - 1; i >= 0 && ipLines[i].length == 0; i--) { }
			_prop.var.etc.ignorePaths = ipLines[0 .. i + 1];
		} else { mixin(S_TRACE);
			_prop.var.etc.ignorePaths = [];
		}

		_etcSettings.apply();

		_prop.var.etc.soundPlayType = _soundPlayTypeTbl2[_soundPlayType.getSelectionIndex()];
		_prop.var.etc.targetVersion = _targetVersionTbl2[_targetVersion.getSelectionIndex()];
		_prop.var.etc.bgmVolume = _bgmVolume.getSelection();
		_prop.var.etc.soundEffectPlayType = _soundEffectPlayTypeTbl2[_soundEffectPlayType.getSelectionIndex()];
		_prop.var.etc.seVolume = _seVolume.getSelection();
		_prop.var.etc.dialogStatus = cast(DialogStatus)_dialogStatusTbl2[_dialogStatus.getSelectionIndex()];
		_prop.var.etc.showStatusTime = cast(ShowStatusTime)_showStatusTimeTbl2[_showStatusTime.getSelectionIndex()];
		_prop.var.etc.savedSound = _savedSound.getText();
		if (_prop.var.etc.historyMax < _prop.var.etc.openHistories.length) { mixin(S_TRACE);
			_prop.var.etc.openHistories = .removeHistoryNonExisting(_comm, _prop.var.etc.removeScenarioHistoryNonExistingWithPriority, _prop.var.etc.openHistories, _prop.var.etc.historyMax);
		}
		if (_prop.var.etc.historyMax < _prop.var.etc.importHistory.length) { mixin(S_TRACE);
			_prop.var.etc.importHistory = .removeHistoryNonExisting(_comm, _prop.var.etc.removeScenarioHistoryNonExistingWithPriority, _prop.var.etc.importHistory, _prop.var.etc.historyMax);
		}
		if (_prop.var.etc.searchHistoryMax < _prop.var.etc.searchHistories.length) { mixin(S_TRACE);
			_prop.var.etc.searchHistories = _prop.var.etc.searchHistories[0 .. _prop.var.etc.searchHistoryMax].dup;
		}
		if (_prop.var.etc.searchHistoryMax < _prop.var.etc.grepDirHistories.length) { mixin(S_TRACE);
			_prop.var.etc.grepDirHistories = _prop.var.etc.grepDirHistories[0 .. _prop.var.etc.searchHistoryMax].dup;
		}
		if (_prop.var.etc.executedPartiesMax < _prop.var.etc.executedParties.length) { mixin(S_TRACE);
			_prop.var.etc.executedParties = .removeHistoryNonExisting(_comm, _prop.var.etc.removePartyHistoryNonExistingWithPriority, _prop.var.etc.executedParties, _prop.var.etc.executedPartiesMax);
		}

		_prop.var.etc.bgImagesDefault = _bgImagesDefault.dup;
		_prop.var.etc.bgImageSettings = _bgImageSettings.dup;
		_prop.var.etc.standardSelections = _standardSelections.dup;
		_prop.var.etc.standardKeyCodes = _standardKeyCodes.dup;
		_prop.var.etc.keyCodesByFeatures = _keyCodesByFeatures.dup;
		_prop.var.etc.keyCodesByMotions = _keyCodesByMotions.dup;
		_prop.var.etc.settingsWithSkinTypes.values = _skinType.settingsWithSkinTypes.dup;

		_prop.var.etc.flagInitValue = _flagInitValue.getSelectionIndex() == 0;
		_prop.var.etc.stepInitValue = _stepInitValue.getSelectionIndex();
		_prop.var.etc.stepValueName = _stepValueName.getText();

		_prop.var.etc.outerTools = _tools.array;
		_prop.var.etc.classicEngines = _cEngines.array;
		_prop.var.etc.eventTemplates = _evTempls.array;
		_prop.var.etc.scenarioTemplates = _scTempls.array;

		updateInitializers();
		_prop.var.etc.contentInitializers = [];
		foreach (ref initializer; _initializers) { mixin(S_TRACE);
			if (initializer.initializer.length) _prop.var.etc.contentInitializers.value ~= initializer;
		}

		foreach (menuId, mnemonic; _appliedMnemonic) { mixin(S_TRACE);
			assert (menuId in _appliedHotkey);
			_prop.var.menu.mnemonic(menuId, mnemonic);
			_prop.var.menu.hotkey(menuId, _appliedHotkey[menuId]);
		}
		_appliedMnemonic = null;
		_appliedHotkey = null;
		if (_summ && findCWPy(_prop, _summ.useTemp ? _summ.zipName : _summ.scenarioPath)) { mixin(S_TRACE);
			_enginePath.setText(_prop.var.etc.enginePath);
			_findEnginePath.setSelection(_prop.var.etc.findEnginePath);
			refEngineEnabled();
		}
		_prop.var.save(_dock);
		_sendReloadProps();
		return true;
	}
}

struct OldSettings {
	Props prop;
	uint drawingScale;
	uint imageScale;
	string targetVersion;
	string oldEnginePath;
	bool findEnginePath;
	string oldWallpaper;
	int oldWallpaperStyle;
	const string[] oldSelections;
	const string[] oldKeyCodes;
	const OuterTool[] tools;
	const ClassicEngine[] cEngines;
	const EvTemplate[] eventTemplates;
	const string[] oldIgnorePaths;
	bool oldSmoothingCard;
	bool oldLogicalSort;
	const OpenHistory[] oldOpenHistories;
	const OpenHistory[] importHistory;
	uint historyMax;
	const string[] oldSearchHistories;
	const string[] oldReplaceHistories;
	const string[] oldGrepDirHistories;
	int oldUndoMaxMainView;
	int oldUndoMaxEvent;
	int oldUndoMaxReplace;
	int oldUndoMaxEtc;
	int oldDialogStatus;
	int showStatusTime;
	string[MenuID] oldMnemonic;
	string[MenuID] oldHotkey;
	bool floatMessagePreview;
	bool useMessageWindowColorInTextContentDialog;
	int bgmVolume;
	int seVolume;
	int soundPlayType;
	int soundEffectPlayType;
	bool showHandMark;
	bool showEventTreeMark;
	bool showCardListHeader;
	bool showCardListTitle;
	bool showSkillCardLevel;
	bool ignoreEmptyStart;
	bool classicStyleTree;
	bool radarStyleParams;
	bool showAptitudeOnCastCardEditor;
	bool showSpNature;
	bool showVariableValuesInEventText;
	bool useNamesAfterStandard;
	bool selectVariableWithTree;
	bool straightEventTreeView;
	bool forceIndentBranchContent;
	bool showTerminalMark;
	bool showTargetStartLineNumber;
	bool showSummaryInAreaTable;
	bool showAreaDirTree;
	bool showContentsGroupName;
	bool showEventContentDescription;
	bool contentsFloat;
	bool contentsAutoHide;
	bool showCloseButtonAllTab;
	bool showMotionDescription;
	bool xmlFileNameIsIDOnly;
	bool replaceClassicResourceExtension;
	bool saveInnerImagePath;
	bool showSummaryPreview;
	bool showMessagePreview;
	ToolBarSettings mainToolBar;
	string stepValueName;
	bool showCurrentValueOnTopAlways;
	bool switchFixedWithCheckBox;
	bool resizingImageWithRatio;
	bool drawXORSelectionLine;
	bool showSceneViewSelectionFilter;
	bool saveSkinName;
	bool showItemNumberOfSceneAndEventView;
	ExecutionParty lastExecutedParty;
	ExecutionParty[] executedParties;
	uint executedPartiesMax;
	bool useCoolBar;
	KeyCodeByFeature[] keyCodesByFeatures;
	KeyCodeByMotion[] keyCodesByMotions;
	BgImageSetting[] bgImageSettings;
	SettingsWithSkinType[] settingsWithSkinTypes;
	this (Props prop) { mixin(S_TRACE);
		this.prop = prop;
		this.drawingScale = prop.var.etc.drawingScale;
		this.imageScale = prop.var.etc.imageScale;
		this.targetVersion = prop.var.etc.targetVersion;
		this.oldEnginePath = prop.var.etc.enginePath;
		this.oldWallpaper = prop.var.etc.wallpaper;
		this.oldWallpaperStyle = prop.var.etc.wallpaperStyle;
		this.oldSelections = prop.var.etc.standardSelections;
		this.oldKeyCodes = prop.var.etc.standardKeyCodes;
		this.tools = prop.var.etc.outerTools;
		this.cEngines = prop.var.etc.classicEngines;
		this.eventTemplates = prop.var.etc.eventTemplates;
		this.oldIgnorePaths = prop.var.etc.ignorePaths;
		this.oldSmoothingCard = prop.var.etc.smoothingCard;
		this.switchFixedWithCheckBox = prop.var.etc.switchFixedWithCheckBox;
		this.resizingImageWithRatio = prop.var.etc.resizingImageWithRatio;
		this.drawXORSelectionLine = prop.var.etc.drawXORSelectionLine;
		this.showSceneViewSelectionFilter = prop.var.etc.showSceneViewSelectionFilter;
		this.oldLogicalSort = prop.var.etc.logicalSort;
		this.oldOpenHistories = prop.var.etc.openHistories;
		this.importHistory = prop.var.etc.importHistory;
		this.historyMax = prop.var.etc.historyMax;
		this.executedPartiesMax = prop.var.etc.executedPartiesMax;
		this.oldSearchHistories = prop.var.etc.searchHistories;
		this.oldGrepDirHistories = prop.var.etc.grepDirHistories;
		this.oldReplaceHistories = prop.var.etc.replaceHistories;
		this.oldUndoMaxMainView = prop.var.etc.undoMaxMainView;
		this.oldUndoMaxEvent = prop.var.etc.undoMaxEvent;
		this.oldUndoMaxReplace = prop.var.etc.undoMaxReplace;
		this.oldUndoMaxEtc = prop.var.etc.undoMaxEtc;
		this.oldDialogStatus = prop.var.etc.dialogStatus;
		this.showStatusTime = prop.var.etc.showStatusTime;
		foreach (id; EnumMembers!MenuID) { mixin(S_TRACE);
			if (isNoKeyBindMenu(id)) continue;
			oldMnemonic[id] = prop.var.menu.mnemonic(id);
			oldHotkey[id] = prop.var.menu.hotkey(id);
		}
		this.floatMessagePreview = prop.var.etc.floatMessagePreview;
		this.useMessageWindowColorInTextContentDialog = prop.var.etc.useMessageWindowColorInTextContentDialog;
		this.bgmVolume = prop.var.etc.bgmVolume;
		this.seVolume = prop.var.etc.seVolume;
		this.soundPlayType = prop.var.etc.soundPlayType;
		this.soundEffectPlayType = prop.var.etc.soundEffectPlayType;
		this.showHandMark = prop.var.etc.showHandMark;
		this.showEventTreeMark = prop.var.etc.showEventTreeMark;
		this.showCardListHeader = prop.var.etc.showCardListHeader;
		this.showCardListTitle = prop.var.etc.showCardListTitle;
		this.showSkillCardLevel = prop.var.etc.showSkillCardLevel;
		this.ignoreEmptyStart = prop.var.etc.ignoreEmptyStart;
		this.classicStyleTree = prop.var.etc.classicStyleTree;
		this.radarStyleParams = prop.var.etc.radarStyleParams;
		this.showAptitudeOnCastCardEditor = prop.var.etc.showAptitudeOnCastCardEditor;
		this.showSpNature = prop.var.etc.showSpNature;
		this.showVariableValuesInEventText = prop.var.etc.showVariableValuesInEventText;
		this.useNamesAfterStandard = prop.var.etc.useNamesAfterStandard;
		this.selectVariableWithTree = prop.var.etc.selectVariableWithTree;
		this.straightEventTreeView = prop.var.etc.straightEventTreeView;
		this.forceIndentBranchContent = prop.var.etc.forceIndentBranchContent;
		this.showTerminalMark = prop.var.etc.showTerminalMark;
		this.showTargetStartLineNumber = prop.var.etc.showTargetStartLineNumber;
		this.showSummaryInAreaTable = prop.var.etc.showSummaryInAreaTable;
		this.showAreaDirTree = prop.var.etc.showAreaDirTree;
		this.showContentsGroupName = prop.var.etc.showContentsGroupName;
		this.showEventContentDescription = prop.var.etc.showEventContentDescription;
		this.contentsFloat = prop.var.etc.contentsFloat;
		this.contentsAutoHide = prop.var.etc.contentsAutoHide;
		this.showCloseButtonAllTab = prop.var.etc.showCloseButtonAllTab;
		this.mainToolBar = prop.var.etc.mainToolBar;
		this.showMotionDescription = prop.var.etc.showMotionDescription;
		this.xmlFileNameIsIDOnly = prop.var.etc.xmlFileNameIsIDOnly;
		this.replaceClassicResourceExtension = prop.var.etc.replaceClassicResourceExtension;
		this.saveInnerImagePath = prop.var.etc.saveInnerImagePath;
		this.showSummaryPreview = prop.var.etc.showSummaryPreview;
		this.showMessagePreview = prop.var.etc.showMessagePreview;
		this.stepValueName = prop.var.etc.stepValueName;
		this.showCurrentValueOnTopAlways = prop.var.etc.showCurrentValueOnTopAlways;
		this.saveSkinName = prop.var.etc.saveSkinName;
		this.showItemNumberOfSceneAndEventView = prop.var.etc.showItemNumberOfSceneAndEventView;
		this.lastExecutedParty = prop.var.etc.lastExecutedParty;
		this.executedParties = prop.var.etc.executedParties;
		this.executedPartiesMax = prop.var.etc.executedPartiesMax;
		this.useCoolBar = prop.var.etc.useCoolBar;
		this.keyCodesByFeatures = prop.var.etc.keyCodesByFeatures;
		this.bgImageSettings = prop.var.etc.bgImageSettings;
		this.settingsWithSkinTypes = prop.var.etc.settingsWithSkinTypes;
	}
	void raiseEvent(Commons comm) { mixin(S_TRACE);
		bool refSkin = false;
		if (imageScale != prop.var.etc.imageScale) { mixin(S_TRACE);
			if (showSummaryPreview) { mixin(S_TRACE);
				auto oldW = cast(int)prop.looks.summarySize.width * imageScale;
				auto newW = cast(int)prop.looks.summarySize.width * prop.var.etc.imageScale;
				prop.var.summaryDlg.width = prop.var.summaryDlg.width + (newW - oldW);
			}
			if (showMessagePreview && !floatMessagePreview) { mixin(S_TRACE);
				auto b = prop.looks.messageBounds;
				auto oldW = cast(int)b.width * imageScale;
				auto newW = cast(int)b.width * prop.var.etc.imageScale;
				prop.var.msgDlg.width = prop.var.msgDlg.width + (newW - oldW);
			}
			if (floatMessagePreview) {
				auto b = prop.looks.messageBounds;
				auto oldW = cast(int)b.width * imageScale;
				auto newW = cast(int)b.width * prop.var.etc.imageScale;
				prop.var.msgPrev.width = prop.var.msgPrev.width + (newW - oldW);
				auto oldH = cast(int)b.height * imageScale;
				auto newH = cast(int)b.height * prop.var.etc.imageScale;
				prop.var.msgPrev.height = prop.var.msgPrev.height + (newH - oldH);
			}

			comm.refImageScale.call();
		} else if (drawingScale != prop.var.etc.drawingScale) { mixin(S_TRACE);
			comm.refImageScale.call();
		}
		if (targetVersion != prop.var.etc.targetVersion) { mixin(S_TRACE);
			comm.refTargetVersion.call();
		}
		if (comm.summary && oldEnginePath != prop.var.etc.enginePath) { mixin(S_TRACE);
			refSkin = true;
		}
		if (oldWallpaper != prop.var.etc.wallpaper || oldWallpaperStyle != prop.var.etc.wallpaperStyle) { mixin(S_TRACE);
			comm.refreshWallpaper(prop);
			comm.refWallpaper.call();
		}
		if (tools != prop.var.etc.outerTools) { mixin(S_TRACE);
			comm.refOuterTools.call();
		}
		if (cEngines != prop.var.etc.classicEngines) { mixin(S_TRACE);
			refSkin = true;
		}
		if (eventTemplates != prop.var.etc.eventTemplates) { mixin(S_TRACE);
			comm.refEventTemplates.call();
		}
		if (oldIgnorePaths != prop.var.etc.ignorePaths) { mixin(S_TRACE);
			comm.refIgnorePaths.call();
		}
		if (oldSmoothingCard != prop.var.etc.smoothingCard || this.showStatusTime != prop.var.etc.showStatusTime) { mixin(S_TRACE);
			comm.refCardState.call();
		}
		if (switchFixedWithCheckBox != prop.var.etc.switchFixedWithCheckBox) { mixin(S_TRACE);
			comm.refSwitchFixedWithCheckBox.call();
		}
		if (resizingImageWithRatio != prop.var.etc.resizingImageWithRatio || drawXORSelectionLine != prop.var.etc.drawXORSelectionLine || showSceneViewSelectionFilter != prop.var.etc.showSceneViewSelectionFilter) { mixin(S_TRACE);
			comm.refImagePaneSelectionFilter.call();
		}
		if (oldLogicalSort != prop.var.etc.logicalSort) { mixin(S_TRACE);
			comm.refSortCondition.call();
		}
		if (refSkin) { mixin(S_TRACE);
			comm.skin = findSkin(comm, prop, comm.summary, null, "", comm.skin.legacyEngine, false);
			comm.refSkin.call();
		}
		comm.refClassicSkin.call();
		if (oldOpenHistories != prop.var.etc.openHistories) { mixin(S_TRACE);
			comm.refHistories.call();
		}
		if (importHistory != prop.var.etc.importHistory) { mixin(S_TRACE);
			comm.refImportHistory.call();
		}
		if (historyMax != prop.var.etc.historyMax) { mixin(S_TRACE);
			comm.refHistoryMax.call();
		}
		if (oldSearchHistories != prop.var.etc.searchHistories || oldReplaceHistories != prop.var.etc.replaceHistories || oldGrepDirHistories != prop.var.etc.grepDirHistories) { mixin(S_TRACE);
			comm.refSearchHistories.call();
		}
		if (oldUndoMaxMainView != prop.var.etc.undoMaxMainView
				|| oldUndoMaxEvent != prop.var.etc.undoMaxEvent
				|| oldUndoMaxReplace != prop.var.etc.undoMaxReplace
				|| oldUndoMaxEtc != prop.var.etc.undoMaxEtc) { mixin(S_TRACE);
			comm.refUndoMax.call();
		}
		if (oldDialogStatus != prop.var.etc.dialogStatus || stepValueName != prop.var.etc.stepValueName) { mixin(S_TRACE);
			comm.refContentText.call();
		}
		bool updateTool = false;
		foreach (id; EnumMembers!MenuID) { mixin(S_TRACE);
			if (isNoKeyBindMenu(id)) continue;
			if (oldMnemonic[id] != prop.var.menu.mnemonic(id) || oldHotkey[id] != prop.var.menu.hotkey(id)) { mixin(S_TRACE);
				comm.refMenu.call(id);
			}
			if (oldHotkey[id] != prop.var.menu.hotkey(id)) { mixin(S_TRACE);
				updateTool = true;
			}
		}
		if (updateTool) comm.updateToolBarText();
		if (this.floatMessagePreview != prop.var.etc.floatMessagePreview) { mixin(S_TRACE);
			int wg;
			if (prop.var.etc.floatMessagePreview) { mixin(S_TRACE);
				wg = -prop.s(prop.looks.messageBounds.width);
			} else { mixin(S_TRACE);
				wg = prop.s(prop.looks.messageBounds.width);
			}
			if (prop.var.etc.showMessagePreview) { mixin(S_TRACE);
				prop.var.msgDlg.width = .max(1, prop.var.msgDlg.width + wg);
			}
			if (prop.var.etc.showDialogPreview) { mixin(S_TRACE);
				prop.var.speakDlg.width = .max(1, prop.var.speakDlg.width + wg);
			}
			comm.refFloatMessagePreview.call();
		}
		if (this.useMessageWindowColorInTextContentDialog != prop.var.etc.useMessageWindowColorInTextContentDialog) { mixin(S_TRACE);
			comm.refUseMessageWindowColorInTextContentDialog.call();
		}
		if (this.bgmVolume != prop.var.etc.bgmVolume) { mixin(S_TRACE);
			.bgmVolume = prop.var.etc.bgmVolume;
		}
		if (this.seVolume != prop.var.etc.seVolume) { mixin(S_TRACE);
			.seVolume = prop.var.etc.seVolume;
		}
		if (this.soundPlayType != prop.var.etc.soundPlayType || this.soundEffectPlayType != prop.var.etc.soundEffectPlayType) { mixin(S_TRACE);
			comm.refSoundType.call();
		}
		if (this.showHandMark != prop.var.etc.showHandMark || this.showEventTreeMark != prop.var.etc.showEventTreeMark
				|| this.showSkillCardLevel != prop.var.etc.showSkillCardLevel || this.ignoreEmptyStart != prop.var.etc.ignoreEmptyStart
				|| this.showStatusTime != prop.var.etc.showStatusTime) { mixin(S_TRACE);
			comm.refCardImageStatus.call();
		}
		if (this.classicStyleTree != prop.var.etc.classicStyleTree) { mixin(S_TRACE);
			comm.refEventTreeStyle.call();
		}
		if (this.radarStyleParams != prop.var.etc.radarStyleParams) { mixin(S_TRACE);
			comm.refRadarStyle.call();
		}
		if (this.showAptitudeOnCastCardEditor != prop.var.etc.showAptitudeOnCastCardEditor) { mixin(S_TRACE);
			comm.refShowAptitudes.call();
		}
		if (this.radarStyleParams != prop.var.etc.radarStyleParams || this.showAptitudeOnCastCardEditor != prop.var.etc.showAptitudeOnCastCardEditor) { mixin(S_TRACE);
			comm.refCastCardParameterEditStyle.call();
		}
		if (this.showSpNature != prop.var.etc.showSpNature || this.useNamesAfterStandard != prop.var.etc.useNamesAfterStandard || this.showCurrentValueOnTopAlways != prop.var.etc.showCurrentValueOnTopAlways) { mixin(S_TRACE);
			comm.refCoupons.call();
		}
		if (this.showCurrentValueOnTopAlways != prop.var.etc.showCurrentValueOnTopAlways) { mixin(S_TRACE);
			comm.refGossips.call();
		}
		if (this.showCurrentValueOnTopAlways != prop.var.etc.showCurrentValueOnTopAlways) { mixin(S_TRACE);
			comm.refCompleteStamps.call();
		}
		if (this.useNamesAfterStandard != prop.var.etc.useNamesAfterStandard || this.showCurrentValueOnTopAlways != prop.var.etc.showCurrentValueOnTopAlways) { mixin(S_TRACE);
			comm.refKeyCodes.call();
		}
		if (this.showCurrentValueOnTopAlways != prop.var.etc.showCurrentValueOnTopAlways) { mixin(S_TRACE);
			comm.refCellNames.call();
		}
		if (this.showVariableValuesInEventText != prop.var.etc.showVariableValuesInEventText) { mixin(S_TRACE);
			comm.refPreviewValues.call();
		}
		if (this.showCardListHeader != prop.var.etc.showCardListHeader) { mixin(S_TRACE);
			comm.refShowCardListHeader.call();
		}
		if (this.showCardListTitle != prop.var.etc.showCardListTitle) { mixin(S_TRACE);
			comm.refShowCardListTitle.call();
		}
		if (this.selectVariableWithTree != prop.var.etc.selectVariableWithTree) { mixin(S_TRACE);
			comm.refVarSelectStyle.call();
		}
		if (this.straightEventTreeView != prop.var.etc.straightEventTreeView || this.forceIndentBranchContent != prop.var.etc.forceIndentBranchContent) { mixin(S_TRACE);
			comm.refEventTreeViewStyle.call();
		}
		if (this.showTerminalMark != prop.var.etc.showTerminalMark || this.showTargetStartLineNumber != prop.var.etc.showTargetStartLineNumber) { mixin(S_TRACE);
			comm.refEventEditorStyle.call();
		}
		if (this.showSummaryInAreaTable != prop.var.etc.showSummaryInAreaTable) { mixin(S_TRACE);
			comm.refAreaTable.call();
		}
		if (this.showAreaDirTree != prop.var.etc.showAreaDirTree) { mixin(S_TRACE);
			comm.refTableViewStyle.call();
		}
		if (this.contentsFloat != prop.var.etc.contentsFloat || this.contentsAutoHide != prop.var.etc.contentsAutoHide
				|| this.showContentsGroupName != prop.var.etc.showContentsGroupName
				|| this.showEventContentDescription != prop.var.etc.showEventContentDescription
				|| this.useCoolBar != prop.var.etc.useCoolBar) { mixin(S_TRACE);
			comm.refContentsToolBoxStyle.call();
		}
		if (this.showMotionDescription != prop.var.etc.showMotionDescription) { mixin(S_TRACE);
			comm.refUpdateMotionBarStyle.call();
		}
		if (this.showCloseButtonAllTab != prop.var.etc.showCloseButtonAllTab && comm.mainWin.dock) { mixin(S_TRACE);
			comm.mainWin.dock.updateCloseButtons();
		}
		if (this.mainToolBar != prop.var.etc.mainToolBar || this.useCoolBar != prop.var.etc.useCoolBar) { mixin(S_TRACE);
			comm.mainWin.updateMainToolBar();
		}
		if (this.showItemNumberOfSceneAndEventView != prop.var.etc.showItemNumberOfSceneAndEventView) { mixin(S_TRACE);
			comm.refMenuCardAndBgImageList.call();
		}
		if (lastExecutedParty != prop.var.etc.lastExecutedParty || executedParties != prop.var.etc.executedParties || executedPartiesMax != prop.var.etc.executedPartiesMax) { mixin(S_TRACE);
			comm.refExecutedParties.call();
		}
		if (executedPartiesMax != prop.var.etc.executedPartiesMax) { mixin(S_TRACE);
			comm.refPartyHistoryMax.call();
		}
		if (comm.summary && !comm.summary.legacy && this.xmlFileNameIsIDOnly != prop.var.etc.xmlFileNameIsIDOnly) { mixin(S_TRACE);
			if (comm.summary.expandXMLs) { mixin(S_TRACE);
				comm.summary.changedAll();
			} else { mixin(S_TRACE);
				comm.summary.changed();
			}
		}
		if (this.saveSkinName != prop.var.etc.saveSkinName) { mixin(S_TRACE);
			comm.summary.changed();
		}
		if (comm.summary && comm.summary.legacy && this.saveInnerImagePath != prop.var.etc.saveInnerImagePath) { mixin(S_TRACE);
			comm.summary.changedAll();
		}
		if (comm.summary && comm.summary.legacy && !comm.skin.legacy && !comm.skin.sourceOfMaterialsIsClassicEngine && this.replaceClassicResourceExtension != prop.var.etc.replaceClassicResourceExtension) { mixin(S_TRACE);
			comm.summary.changedAll();
		}

		auto refSettingsWithSkinTypes = this.settingsWithSkinTypes != prop.var.etc.settingsWithSkinTypes;

		if (refSettingsWithSkinTypes || this.bgImageSettings != prop.var.etc.bgImageSettings) { mixin(S_TRACE);
			comm.refBgImageSettings.call();
		}
		if (refSettingsWithSkinTypes || oldSelections != prop.var.etc.standardSelections || this.showCurrentValueOnTopAlways != prop.var.etc.showCurrentValueOnTopAlways) { mixin(S_TRACE);
			comm.refStandardSelections.call();
		}
		if (refSettingsWithSkinTypes || oldKeyCodes != prop.var.etc.standardKeyCodes) { mixin(S_TRACE);
			comm.refStandardKeyCodes.call();
		}
		if (refSettingsWithSkinTypes || this.keyCodesByFeatures != prop.var.etc.keyCodesByFeatures) { mixin(S_TRACE);
			comm.refKeyCodesByFeatures.call();
		}
		if (refSettingsWithSkinTypes) { mixin(S_TRACE);
			comm.refSettingsWithSkinTypes.call();
			comm.refElementOverrides.call();
		}
	}
}
