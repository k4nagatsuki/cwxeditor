/// CWXEditorの変数。
module cwx.variables;

import cwx.xml;
import cwx.structs;
import cwx.settings;
import cwx.types;
import cwx.event;
import cwx.perf;
import cwx.enumutils;

import std.conv;
import std.path;

class FlexEtcProps : Properties {
	auto languageDir = Prop!(string, true)("languageDir", "lang");
	auto languageFile = Prop!(string)("languageFile", "");
	auto useSystemLanguage = Prop!(bool)("useSystemLanguage", true);

	auto warningWindowsHandleCount = Prop!(uint, false)("warningWindowsHandleCount", 1536);
	auto liftWarningWindowsHandleCount = Prop!(uint, false)("liftWarningWindowsHandleCount", 1536);
	
	auto targetVersion = Prop!(string)("targetVersion", "1.50");
	auto imeMode = Prop!(int)("imeMode", 0);
	auto imageScale = Prop!(uint)("imageScale", 1, 2016091800);
	auto drawingScale = Prop!(uint)("drawingScale", 1);
	auto tabs = Prop!(uint, true)("tabs", 4);
	auto toolsLock = Prop!(bool)("toolsLock", false);
	auto toolsOrder = Prop!(int[])("toolsOrder", []);
	auto toolsWrapIndices = Prop!(int[])("toolsWrapIndices", []);
	auto comboVisibleItemCount = Prop!(int, true)("comboVisibleItemCount", 20);
	auto directorySashL = Prop!(int)("directorySashL", 2);
	auto directorySashR = Prop!(int)("directorySashR", 5);
	auto directorySashV = Prop!(bool)("directorySashV", false);
	auto filesSortColumn = Prop!(int)("filesSortColumn", 1);
	auto filesSortDirection = Prop!(int)("filesSortDirection", SortDir.Up);
	auto fileNameColumn = Prop!(int, false, true)("fileNameColumn", 150);
	auto fileExtColumn = Prop!(int, false, true)("fileExtColumn", 60);
	auto fileCountColumn = Prop!(int, false, true)("fileCountColumn", 60);
	auto areaIdColumn = Prop!(int, false, true)("areaIdColumn", 50);
	auto areaNameColumn = Prop!(int, false, true)("areaNameColumn", 190);
	auto areaCountColumn = Prop!(int, false, true)("areaCountColumn", 60);
	auto importAreaIdColumn = Prop!(int, false, true)("importAreaIdColumn", 50);
	auto importAreaNameColumn = Prop!(int, false, true)("importAreaNameColumn", 190);
	auto importAreaCountColumn = Prop!(int, false, true)("importAreaCountColumn", 60);
	auto areasSortColumn = Prop!(int)("areasSortColumn", 0);
	auto areasSortDirection = Prop!(int)("areasSortDirection", SortDir.Up);
	auto importAreasSortColumn = Prop!(int)("importAreasSortColumn", 0);
	auto importAreasSortDirection = Prop!(int)("importAreasSortDirection", SortDir.Up);
	auto summaryParamSashL = Prop!(int)("summaryParamSashL", 2, 2012101100);
	auto summaryParamSashR = Prop!(int)("summaryParamSashR", 1, 2012101100);
	auto rCouponsStartAreaSashL = Prop!(int)("rCouponsStartAreaSashL", 1, 2012101100);
	auto rCouponsStartAreaSashR = Prop!(int)("rCouponsStartAreaSashR", 1, 2012101100);
	auto areaViewL = Prop!(int)("areaViewL", 1);
	auto areaViewR = Prop!(int)("areaViewR", 4);
	auto battleViewL = Prop!(int)("battleViewL", 1);
	auto battleViewR = Prop!(int)("battleViewR", 4);
	auto bgImageViewL = Prop!(int)("bgImageViewL", 1);
	auto bgImageViewR = Prop!(int)("bgImageViewR", 4);
	auto areaViewImageFlagL = Prop!(int)("areaViewImageFlagL", 4);
	auto areaViewImageFlagR = Prop!(int)("areaViewImageFlagR", 1);
	auto battleViewImageFlagL = Prop!(int)("battleViewImageFlagL", 4);
	auto battleViewImageFlagR = Prop!(int)("battleViewImageFlagR", 1);
	auto bgImageViewImageFlagL = Prop!(int)("bgImageViewImageFlagL", 4);
	auto bgImageViewImageFlagR = Prop!(int)("bgImageViewImageFlagR", 1);
	auto partyCardAlpha = Prop!(int, true)("partyCardAlpha", 176);
	auto viewPartyCardsArea = Prop!(bool)("viewPartyCardsArea", true);
	auto viewPartyCardsBattle = Prop!(bool)("viewPartyCardsBattle", true);
	auto viewEnemyCardDebug = Prop!(bool)("viewEnemyCardDebug", false);
	auto viewPartyCardsEvent = Prop!(bool)("viewPartyCardsEvent", true);
	auto messageAlpha = Prop!(int, true)("messageAlpha", 176);
	auto viewMessageArea = Prop!(bool)("viewMessageArea", false);
	auto viewMessageBattle = Prop!(bool)("viewMessageBattle", false);
	auto viewMessageEvent = Prop!(bool)("viewMessageEvent", false);
	auto viewReferenceCards = Prop!(bool)("viewReferenceCards", true);
	auto fixedImagesMenuCards = Prop!(bool)("fixedImagesMenuCards", false);
	auto fixedImagesCells = Prop!(bool)("fixedImagesCells", false);
	auto fixedImagesBackground = Prop!(bool)("fixedImagesBackground", true);
	auto fixedImagesBattle = Prop!(bool)("fixedImagesBattle", false);
	auto fixedImagesEvent = Prop!(bool)("fixedImagesEvent", false);
	auto fixedImagesEventBackground = Prop!(bool)("fixedImagesEventBackground", true);
	auto fixedImagesDefaultBackground = Prop!(bool)("fixedImagesDefaultBackground", true);
	auto viewCards = Prop!(bool)("viewCards", true);
	auto viewBgImages = Prop!(bool)("viewBgImages", true);
	auto showGrid = Prop!(bool)("showGrid", false);
	auto areaSashT = Prop!(int)("areaSashT", 5);
	auto areaSashB = Prop!(int)("areaSashB", 4);
	auto areaSashL = Prop!(int)("areaSashL", 1);
	auto areaSashR = Prop!(int)("areaSashR", 4);
	auto importAreaSashL = Prop!(int)("importAreaSashL", 1);
	auto importAreaSashR = Prop!(int)("importAreaSashR", 3);
	auto areaSashV = Prop!(bool)("areaSashV", false);
	auto importAreaSashV = Prop!(bool)("importAreaSashV", true);
	auto flagSashL = Prop!(int)("flagSashL", 1);
	auto flagSashR = Prop!(int)("flagSashR", 4);
	auto flagSashV = Prop!(bool)("flagSashV", false);
	auto flagsWidth = Prop!(int, true, true)("flagsWidth", 150);
	auto flagsHeight = Prop!(int, true, true)("flagsHeight", 200);
	auto menuCardSashL = Prop!(int)("menuCardSashL", 5, 2012101100);
	auto menuCardSashR = Prop!(int)("menuCardSashR", 3, 2012101100);
	auto enemyCardSashL = Prop!(int)("enemyCardSashL", 3);
	auto enemyCardSashR = Prop!(int)("enemyCardSashR", 5);
	auto backSashL = Prop!(int)("backSashL", 5);
	auto backSashR = Prop!(int)("backSashR", 3);
	auto bgImageSampleWidth = Prop!(int, true, true)("bgImageSampleWidth", 150);
	auto bgImageSampleHeight = Prop!(int, true, true)("bgImageSampleHeight", 150);
	auto cellPCNumberWidth = Prop!(int, true)("cellPCNumberWidth", 150, false);
	auto textCellVSashT = Prop!(int)("textCellVSashT", 2);
	auto textCellVSashB = Prop!(int)("textCellVSashB", 1);
	auto textCellHSashL = Prop!(int)("textCellHSashL", 2);
	auto textCellHSashR = Prop!(int)("textCellHSashR", 1);
	auto textCellPreviewSashL = Prop!(int)("textCellPreviewSashL", 1);
	auto textCellPreviewSashR = Prop!(int)("textCellPreviewSashR", 1);
	auto textCellPreviewWidth = Prop!(int, true, true)("textCellPreviewWidth", 100);
	auto textCellPreviewHeight = Prop!(int, true, true)("textCellPreviewHeight", 100);
	auto textCellBoxWidth = Prop!(int, true, true)("textCellBoxWidth", 100);
	auto textCellBoxHeight = Prop!(int, true, true)("textCellBoxHeight", 50);
	auto cardIdColumn = Prop!(int, false, true)("cardIdColumn", 50);
	auto cardNameColumn = Prop!(int, false, true)("cardNameColumn", 100);
	auto cardNumberColumn = Prop!(int, false, true)("cardNumberColumn", 60);
	auto cardDescriptionColumn = Prop!(int, false, true)("cardDescriptionColumn", 280);
	auto cardCountColumn = Prop!(int, false, true)("cardCountColumn", 60);
	auto handCardIdColumn = Prop!(int, false, true)("handCardIdColumn", 50);
	auto handCardNameColumn = Prop!(int, false, true)("handCardNameColumn", 100);
	auto handCardNumberColumn = Prop!(int, false, true)("handCardNumberColumn", 60);
	auto handCardDescriptionColumn = Prop!(int, false, true)("handCardDescriptionColumn", 50);
	auto importCardIdColumn = Prop!(int, false, true)("importCardIdColumn", 50);
	auto importCardNameColumn = Prop!(int, false, true)("importCardNameColumn", 100);
	auto importCardNumberColumn = Prop!(int, false, true)("importCardNumberColumn", 60);
	auto importCardDescriptionColumn = Prop!(int, false, true)("importCardDescriptionColumn", 50);
	auto importCardCountColumn = Prop!(int, false, true)("importCardCountColumn", 30);
	auto importHandCardIdColumn = Prop!(int, false, true)("importHandCardIdColumn", 50);
	auto importHandCardNameColumn = Prop!(int, false, true)("importHandCardNameColumn", 100);
	auto importHandCardNumberColumn = Prop!(int, false, true)("importHandCardNumberColumn", 60);
	auto importHandCardDescriptionColumn = Prop!(int, false, true)("importHandCardDescriptionColumn", 50);
	auto mainCardsSortColumn = Prop!(int)("mainCardsSortColumn", 0);
	auto mainCardsSortDirection = Prop!(int)("mainCardsSortDirection", SortDir.Up);
	auto handCardsSortColumn = Prop!(int)("handCardsSortColumn", 0);
	auto handCardsSortDirection = Prop!(int)("handCardsSortDirection", SortDir.Up);
	auto importCardsSortColumn = Prop!(int)("importCardsSortColumn", 0);
	auto importCardsSortDirection = Prop!(int)("importCardsSortDirection", SortDir.Up);
	auto importHandCardsSortColumn = Prop!(int)("importHandCardsSortColumn", 0);
	auto importHandCardsSortDirection = Prop!(int)("importHandCardsSortDirection", SortDir.Up);
	auto linkCardMaskColor = Prop!(CRGB)("linkCardMaskColor", CRGB(0, 255, 0, 64), true);
	auto negativeCardNameBorder = Prop!(int)("negativeCardNameBorder", 116, true);
	auto cardNameBorderingColorForWhite = Prop!(CRGB, true)("cardNameBorderingColorForWhite", CRGB(0, 0, 0, 64));
	auto cardNameBorderingColorForBlack = Prop!(CRGB, true)("cardNameBorderingColorForBlack", CRGB(255, 255, 255, 64));
	auto couponWidth = Prop!(int, true, true)("couponWidth", 250);
	auto couponHeight = Prop!(int, true, true)("couponHeight", 80);
	auto couponValueColumn = Prop!(int, true, true)("couponValueColumn", 40);
	auto couponEventL = Prop!(int)("couponEventL", 1);
	auto couponEventR = Prop!(int)("couponEventR", 2);
	auto idColumn = Prop!(int, false, true)("idColumn", 50);
	auto valueNumberColumn = Prop!(int, false, true)("valueNumberColumn", 50);
	auto nameTableWidth = Prop!(int, true, true)("nameTableWidth", 250);
	auto nameTableHeight = Prop!(int, true, true)("nameTableHeight", 250);
	auto flagEventSashL = Prop!(int)("flagEventSashL", 3);
	auto flagEventSashR = Prop!(int)("flagEventSashR", 2);
	auto nameWidth = Prop!(int, true, true)("nameWidth", 300);
	auto firesWidth = Prop!(int, true, true)("firesWidth", 120);
	auto flagNameWidth = Prop!(int, true, true)("flagNameWidth", 150);
	auto flagInitWidth = Prop!(int, true, true)("flagInitWidth", 50);
	auto flagValueWidth = Prop!(int, true, true)("flagValueWidth", 50);
	auto flagNameColumn = Prop!(int, false, true)("flagNameColumn", 150);
	auto flagInitColumn = Prop!(int, false, true)("flagInitColumn", 90);
	auto variableInitializationColumn = Prop!(int, false, true)("variableInitializationColumn", 80);
	auto flagCountColumn = Prop!(int, false, true)("flagCountColumn", 60);
	auto localFlagNameColumn = Prop!(int, false, true)("localFlagNameColumn", 100);
	auto localFlagInitColumn = Prop!(int, false, true)("localFlagInitColumn", 80);
	auto localVariableInitializationColumn = Prop!(int, false, true)("localVariableInitializationColumn", 80);
	auto localFlagCountColumn = Prop!(int, false, true)("localFlagCountColumn", 60);
	auto localVariablesSashL = Prop!(int)("localVariablesSashL", 1);
	auto localVariablesSashR = Prop!(int)("localVariablesSashR", 4);
	auto limitForNumberOfVariant = Prop!(uint, true)("limitForNumberOfVariant", 12);
	auto filesWidth = Prop!(int, true, true)("filesWidth", 150);
	auto filesHeight = Prop!(int, true, true)("filesHeight", 150);
	auto talkersWidth = Prop!(int, true, true)("talkersWidth", 100);
	auto motionsWidth = Prop!(int, true, true)("motionsWidth", 150);
	auto incrementalSearchBoxWidth = Prop!(int, true, true)("incrementalSearchBoxWidth", 100);
	auto showMainToolBar = Prop!(bool, true)("showMainToolBar", true, 2017042400);
	auto showSceneToolBar = Prop!(bool, true)("showSceneToolBar", true, 2017042400);
	auto showEventToolBar = Prop!(bool, true)("showEventToolBar", true, 2017042400);
	auto flagCombiSashL = Prop!(int)("flagCombiSashL", 1);
	auto flagCombiSashR = Prop!(int)("flagCombiSashR", 1);
	auto useCoolBar = Prop!(bool)("useCoolBar", true);
	auto mainToolBarCustomSashL = Prop!(int)("mainToolBarCustomSashL", 1);
	auto mainToolBarCustomSashR = Prop!(int)("mainToolBarCustomSashR", 1);
	auto stepTopSashL = Prop!(int)("stepTopSashL", 3, 2019042700);
	auto stepTopSashR = Prop!(int)("stepTopSashR", 4, 2019042700);
	auto flagTopSashL = Prop!(int)("flagTopSashL", 3);
	auto flagTopSashR = Prop!(int)("flagTopSashR", 2);
	auto toolTipWidth = Prop!(int, true, true)("toolTipWidth", 300);
	auto colorExampleWidth = Prop!(int, true, true)("colorExampleWidth", 50);
	auto buttonWidth = Prop!(int, true, true)("buttonWidth", 100);
	auto radioGroupSeparatorWidth = Prop!(int, true, true)("radioGroupSeparatorWidth", 15);
	auto couponValueColumnWidth = Prop!(int, true, true)("couponValueColumnWidth", 40);
	auto idNameColumn = Prop!(int, false, true)("idNameColumn", 330);
	auto idCountColumn = Prop!(int, false, true)("idCountColumn", 60);
	auto couponSortColumn = Prop!(int)("couponSortColumn", 0);
	auto couponSortDirection = Prop!(int)("couponSortDirection", SortDir.Up);
	auto gossipSortColumn = Prop!(int)("gossipSortColumn", 0);
	auto gossipSortDirection = Prop!(int)("gossipSortDirection", SortDir.Up);
	auto completeStampSortColumn = Prop!(int)("completeStampSortColumn", 0);
	auto completeStampSortDirection = Prop!(int)("completeStampSortDirection", SortDir.Up);
	auto keyCodeSortColumn = Prop!(int)("keyCodeSortColumn", 0);
	auto keyCodeSortDirection = Prop!(int)("keyCodeSortDirection", SortDir.Up);
	auto cellNameSortColumn = Prop!(int)("cellNameSortColumn", 0);
	auto cellNameSortDirection = Prop!(int)("cellNameSortDirection", SortDir.Up);
	auto cardGroupSortColumn = Prop!(int)("cardGroupSortColumn", 0);
	auto cardGroupSortDirection = Prop!(int)("cardGroupSortDirection", SortDir.Up);
	auto authorNewAreaNameSashL = Prop!(int)("authorNewAreaNameSashL", 1);
	auto authorNewAreaNameSashR = Prop!(int)("authorNewAreaNameSashR", 1);
	auto historyScenarioNameColumn = Prop!(int, false, true)("historyScenarioNameColumn", 150);
	auto historyScenarioPathColumn = Prop!(int, false, true)("historyScenarioPathColumn", 300);
	auto executionPartyNameColumn = Prop!(int, false, true)("executionPartyNameColumn", 150);
	auto executionPartyYadoColumn = Prop!(int, false, true)("executionPartyYadoColumn", 150);
	auto executionPartyEngineColumn = Prop!(int, false, true)("executionPartyEngineColumn", 150);
	auto expressionAndTargetSashT = Prop!(int)("expressionAndTargetSashT", -1);
	auto expressionAndTargetSashB = Prop!(int)("expressionAndTargetSashB", -1);
	auto expressionFunctionAndArgsSashT = Prop!(int)("expressionFunctionAndArgsSashT", 3);
	auto expressionFunctionAndArgsSashB = Prop!(int)("expressionFunctionAndArgsSashB", 2);
	auto functionCallEditorWidth = Prop!(int, false, true)("functionCallEditorWidth", 350);
	auto functionCallEditorHeight = Prop!(int, false, true)("functionCallEditorHeight", 400);
	auto functionArgumentNameColumn = Prop!(int, false, true)("functionArgumentNameColumn", 100);
	auto functionArgumentTypeColumn = Prop!(int, false, true)("functionArgumentTypeColumn", 80);
	auto functionArgumentValueColumn = Prop!(int, false, true)("functionArgumentValueColumn", 120);
	auto expressionCheckingDelay = Prop!(uint, true)("expressionCheckingDelay", 500);
	auto keyCodesByFeaturesFeatureColumn = Prop!(int, false, true)("keyCodesByFeaturesFeatureColumn", 95);
	auto keyCodesByFeaturesKeyCodeColumn = Prop!(int, false, true)("keyCodesByFeaturesKeyCodeColumn", 90);
	auto keyCodesByMotionsMotionColumn = Prop!(int, false, true)("keyCodesByMotionsMotionColumn", 85);
	auto keyCodesByMotionsElementColumn = Prop!(int, false, true)("keyCodesByMotionsElementColumn", 85);
	auto keyCodesByMotionsKeyCodeColumn = Prop!(int, false, true)("keyCodesByMotionsKeyCodeColumn", 90);
	auto elementOverrideNameColumn = Prop!(int, false, true)("elementOverrideNameColumn", 50);
	auto elementOverrideTargetTypeColumn = Prop!(int, false, true)("elementOverrideTargetTypeColumn", 110);
	auto keyCodesElementSashL = Prop!(int)("keyCodesElementSashL", 3);
	auto keyCodesElementSashR = Prop!(int)("keyCodesElementSashR", 2);

	auto partyMax = Prop!(uint, true)("partyMax", 6);
	auto cardScaleMax = Prop!(int)("cardScaleMax", 300);
	auto cardScaleMin = Prop!(int)("cardScaleMin", 50);
	auto layerMax = Prop!(int)("layerMax", 9999);
	auto posLeftMax = Prop!(uint, true)("posLeftMax", 9999);
	auto posTopMax = Prop!(uint, true)("posTopMax", 9999);
	auto backWidthMax = Prop!(uint, true)("backWidthMax", 9999);
	auto backHeightMax = Prop!(uint, true)("backHeightMax", 9999);
	auto levelMax = Prop!(uint, true)("levelMax", 15);
	auto castLevelMax = Prop!(uint, true)("castLevelMax", 99);
	auto lifeMax = Prop!(uint, true)("lifeMax", 9999);
	auto couponValueMax = Prop!(uint, true)("couponValueMax", 999);
	auto physicalMax = Prop!(uint, true)("physicalMax", 15);
	auto mentalMax = Prop!(uint, true)("mentalMax", 4);
	auto skillLevelMax = Prop!(uint, true)("skillLevelMax", 999);
	auto useCountMax = Prop!(uint, true)("useCountMax", 999);
	auto priceMax = Prop!(uint, true)("priceMax", 999999);
	auto enhanceMax = Prop!(uint, true)("enhanceMax", 10);
	auto roundMax = Prop!(uint, true)("roundMax", 999);
	auto paralyzeMax = Prop!(uint, true)("paralyzeMax", 40);
	auto poisonMax = Prop!(uint, true)("poisonMax", 40);
	auto cardNumberMax = Prop!(uint, true)("cardNumberMax", 99);
	auto waitMax = Prop!(uint, true)("waitMax", 1000);
	auto uValueMax = Prop!(uint, true)("uValueMax", 999);
	auto skillPowerMax = Prop!(uint, true)("skillPowerMax", 9);
	auto beastMaxNest = Prop!(uint, true)("beastMaxNest", 99);

	auto flagInitValue = Prop!(bool)("flagInitValue", true);
	auto stepInitValue = Prop!(int)("stepInitValue", 0);
	auto stepCountMax = Prop!(uint)("stepCountMax", 10000, true);
	auto stepValueName = Prop!(string)("stepValueName", "Step - $N");

	auto imageListWidth = Prop!(int, false, true)("imageListWidth", 380);
	auto imageListHeight = Prop!(int, false, true)("imageListHeight", 300);
	auto imageListForBgImagesWidth = Prop!(int, false, true)("imageListForBgImagesWidth", 380);
	auto imageListForBgImagesHeight = Prop!(int, false, true)("imageListForBgImagesHeight", 300);
	auto layerListWidth = Prop!(int, false, true)("layerListWidth", 300);
	auto layerListHeight = Prop!(int, false, true)("layerListHeight", 350);
	auto cardLife = Prop!(bool)("cardLife", true);
	auto cardDetails = Prop!(bool)("cardDetails", false);
	auto cardsMarginX = Prop!(int, true, true)("cardsMarginX", 5);
	auto cardsSpaceX = Prop!(int, true, true)("cardsSpaceX", 8);
	auto cardsMarginY = Prop!(int, true, true)("cardsMarginY", 5);
	auto cardsSpaceY = Prop!(int, true, true)("cardsSpaceY", 8);
	auto cardsTitleSpace = Prop!(int, true, true)("cardsTitleSpace", 2);
	auto cardsFocusLinePadding = Prop!(int, true, true)("cardsFocusLinePadding", 2);
	auto cardsDefaultWrap = Prop!(int, true)("cardsDefaultWrap", 4);
	auto seKeyCodeSashL = Prop!(int)("seKeyCodeSashL", 4, 2012101100);
	auto seKeyCodeSashR = Prop!(int)("seKeyCodeSashR", 7, 2012101100);
	auto effectContentSEKeyCodeSashL = Prop!(int)("effectContentSEKeyCodeSashL", 4);
	auto effectContentSEKeyCodeSashR = Prop!(int)("effectContentSEKeyCodeSashR", 7);
	auto talkMainSashL = Prop!(int)("talkMainSashL", 3);
	auto talkMainSashR = Prop!(int)("talkMainSashR", 7);
	auto talkLeftSashL = Prop!(int)("talkLeftSashL", 4);
	auto talkLeftSashR = Prop!(int)("talkLeftSashR", 5);
	auto msgBackR = Prop!(int, true)("msgBackR", 0);
	auto msgBackG = Prop!(int, true)("msgBackG", 0);
	auto msgBackB = Prop!(int, true)("msgBackB", 128);
	auto msgForeR = Prop!(int, true)("msgForeR", 255);
	auto msgForeG = Prop!(int, true)("msgForeG", 255);
	auto msgForeB = Prop!(int, true)("msgForeB", 255);
	auto textTabs = Prop!(int, true)("textTabs", 4);
	auto messageLineColor1 = Prop!(CRGB, true)("messageLineColor1", CRGB(0, 0, 0));
	auto messageLineColor2 = Prop!(CRGB, true)("messageLineColor2", CRGB(128, 0, 0));
	auto messageBackColor = Prop!(CRGB, true)("messageBackColor", CRGB(0, 0, 128));
	auto messageForeColor = Prop!(CRGB, true)("messageForeColor", CRGB(255, 255, 255));
	auto messageHemColor = Prop!(CRGB, true)("messageHemColor", CRGB(0, 0, 0));
	auto gridX = Prop!(uint)("gridX", 10);
	auto gridY = Prop!(uint)("gridY", 10);
	auto gridRange = Prop!(uint)("gridRange", 5);
	auto gridColor = Prop!(CRGB, true)("gridColor", CRGB(64, 64, 64));
	auto gridHighlightColor = Prop!(CRGB, true)("gridHighlightColor", CRGB(192, 192, 192));
	auto enhanceMaxVal = Prop!(uint, true)("enhanceMaxVal", 10);
	auto enhanceHighVal = Prop!(uint, true)("enhanceHighVal", 7);
	auto enhanceMiddleVal = Prop!(uint, true)("enhanceMiddleVal", 4);
	auto enhanceColorMax = Prop!(CRGB, true)("enhanceColorHigh", CRGB(255, 0, 0));
	auto enhanceColorHigh = Prop!(CRGB, true)("enhanceColorHigh", CRGB(175, 0, 0));
	auto enhanceColorMiddle = Prop!(CRGB, true)("enhanceColorMiddle", CRGB(127, 0, 0));
	auto enhanceColorLow = Prop!(CRGB, true)("enhanceColorLow", CRGB(79, 0, 0));
	auto penaltyColorMax = Prop!(CRGB, true)("penaltyColorHigh", CRGB(0, 0, 51));
	auto penaltyColorHigh = Prop!(CRGB, true)("penaltyColorHigh", CRGB(0, 0, 85));
	auto penaltyColorMiddle = Prop!(CRGB, true)("penaltyColorMiddle", CRGB(0, 0, 136));
	auto penaltyColorLow = Prop!(CRGB, true)("penaltyColorLow", CRGB(0, 0, 187));
	auto textCellDefaultWidth = Prop!(int, true)("textCellDefaultWidth", 100);
	auto textCellDefaultHeight = Prop!(int, true)("textCellDefaultHeight", 100);
	auto textCellDefaultFontClassic = Prop!(string, true)("textCellDefaultFontClassic", "ＭＳ ゴシック");
	auto textCellDefaultFont = Prop!(string, true)("textCellDefaultFont", "IPA ゴシック");
	auto textCellDefaultFontSize = Prop!(int, true)("textCellDefaultFontSize", 18);
	auto textCellDefaultColor = Prop!(CRGB, true)("textCellDefaultColor", CRGB(0, 0, 0, 255));
	auto textCellDefaultBorderingColor = Prop!(CRGB, true)("textCellDefaultBorderingColor", CRGB(255, 255, 255, 255));
	auto fontSizeMax = Prop!(uint, true)("fontSizeMax", 999);
	auto borderingWidthMax = Prop!(uint, true)("borderingWidthMax", 99);
	auto colorCellDefaultWidth = Prop!(int, true)("colorCellDefaultWidth", 100);
	auto colorCellDefaultHeight = Prop!(int, true)("colorCellDefaultHeight", 100);
	auto colorCellDefaultColor1 = Prop!(CRGB, true)("colorCellDefaultColor1", CRGB(255, 255, 255, 255));
	auto colorCellDefaultColor2 = Prop!(CRGB, true)("colorCellDefaultColor2", CRGB(255, 255, 255, 255));

	auto noFileName = Prop!(string, true)("noFileName", "_");

	auto contentsOrder = Prop!(int[])("contentsOrder", []);
	auto contentsLock = Prop!(bool)("contentsLock", false);
	auto contentsWrapIndices = Prop!(int[])("contentsWrapIndices", [4, 6, 8]);
	auto contentsAutoOpen = Prop!(bool)("contentsAutoOpen", true);
	auto contentsPutMode = Prop!(int)("contentsPutMode", 0);
	auto contentsInsertFirst = Prop!(bool)("contentsInsertFirst", false);
	auto showContentsGroupName = Prop!(bool)("showContentsGroupName", true);
	auto showEventContentDescription = Prop!(bool)("showEventContentDescription", true);
	auto showMotionDescription = Prop!(bool)("showMotionDescription", true);
	auto contentsFloat = Prop!(bool)("contentsFloat", false);
	auto contentsAutoHide = Prop!(bool)("contentsAutoHide", false);
	auto comboListVisible = Prop!(bool)("comboListVisible", true);
	auto showCurrentValueOnTopAlways = Prop!(bool)("showCurrentValueOnTopAlways", false);
	auto showContentsBoxHeightWhenNoToolBar = Prop!(int, true)("showContentsBoxHeightWhenNoToolBar", 8);
	auto showSummaryInAreaTable = Prop!(bool)("showSummaryInAreaTable", true);
	auto saveTableViewSelection = Prop!(bool)("saveTableViewSelection", true);
	auto clickIsOpenEvent = Prop!(bool)("clickIsOpenEvent", false);
	auto smoothingCard = Prop!(bool)("smoothingCard", true);
	auto ignorePathsWidth = Prop!(int, true, true)("ignorePathsWidth", 50);
	auto menuSettingsHeight = Prop!(int, true, true)("menuSettingsHeight", 150);
	auto settingListWidth = Prop!(int, true, true)("settingListWidth", 150);
	auto settingListHeight = Prop!(int, true, true)("settingListHeight", 150);

	auto importOptionMaterials = Prop!(int)("importOptionMaterials", ImportTypeReference1.NoOverwrite);
	auto importOptionVariables = Prop!(int)("importOptionVariables", ImportTypeReference1.Rename);
	auto importOptionCasts = Prop!(int)("importOptionCasts", ImportTypeReference2.Rename);
	auto importOptionSkills = Prop!(int)("importOptionSkills", ImportTypeReference2.Rename);
	auto importOptionItems = Prop!(int)("importOptionItems", ImportTypeReference2.Rename);
	auto importOptionBeasts = Prop!(int)("importOptionBeasts", ImportTypeReference2.Rename);
	auto importOptionInfos = Prop!(int)("importOptionInfos", ImportTypeReference2.Rename);
	auto importOptionAreas = Prop!(int)("importOptionAreas", ImportTypeReference2.Rename);
	auto importOptionBattles = Prop!(int)("importOptionBattles", ImportTypeReference2.Rename);
	auto importOptionPackages = Prop!(int)("importOptionPackages", ImportTypeReference2.Rename);
	auto importOptionIncludedFiles = Prop!(int)("importOptionIncludedFiles", ImportTypeIncluded.AsIs);
	auto importOptionIncludedFilesWithoutIncluding = Prop!(int)("importOptionIncludedFilesWithoutIncluding", ImportTypeIncluded.Exclude);
	auto importOptionIncludedBgImages = Prop!(int)("importOptionIncludedBgImages", ImportTypeIncluded.AsIs);
	auto importOptionHands = Prop!(int)("importOptionHands", ImportTypeIncluded.AsIs);
	auto importOptionBeastsInMotions = Prop!(int)("importOptionBeastsInMotions", ImportTypeIncluded.AsIs);
	auto importOptionOverwriteScenarioInfo = Prop!(bool)("importOptionOverwriteScenarioInfo", false);
	auto wallpaper = Prop!(string)("wallpaper", "");
	auto wallpaperStyle = Prop!(int)("wallpaperStyle", WallpaperStyle.Tile);
	auto wallColorR = Prop!(int)("wallColorR", 0);
	auto wallColorG = Prop!(int)("wallColorG", 0);
	auto wallColorB = Prop!(int)("wallColorB", 128);
	auto bgImagesDefault = Prop!(BgImageS[])("bgImagesDefault", [BgImageS("MapOfWirth", 0, 0, 632, 420, false)]);
	auto bgImageSettingsSashL = Prop!(int)("bgImageSettingsSashL", 1);
	auto bgImageSettingsSashR = Prop!(int)("bgImageSettingsSashR", 1);
	auto selectionKeyCodeSashL = Prop!(int)("selectionKeyCodeSashL", 2);
	auto selectionKeyCodeSashR = Prop!(int)("selectionKeyCodeSashR", 5);
	auto bgImageSettingsKeyCodesSashL = Prop!(int)("bgImageSettingsKeyCodesSashL", 3);
	auto bgImageSettingsKeyCodesSashR = Prop!(int)("bgImageSettingsKeyCodesSashR", 2);
	auto keyCodesByFeaturesKeyCodesByMotionSashL = Prop!(int)("keyCodesByFeaturesKeyCodesByMotionSashL", 3);
	auto keyCodesByFeaturesKeyCodesByMotionSashR = Prop!(int)("keyCodesByFeaturesKeyCodesByMotionSashR", 4);
	auto bgImageSelectionSashL = Prop!(int)("bgImageSelectionSashL", 8);
	auto bgImageSelectionSashR = Prop!(int)("bgImageSelectionSashR", 3);
	auto outerToolsSashL = Prop!(int)("outerToolsSashL", 1);
	auto outerToolsSashR = Prop!(int)("outerToolsSashR", 2);
	auto outerToolShortcutSashL = Prop!(int)("outerToolShortcutSashL", 1, 2014103100);
	auto outerToolShortcutSashR = Prop!(int)("outerToolShortcutSashR", 4, 2014103100);
	auto classicEnginesSashL = Prop!(int)("classicEnginesSashL", 1);
	auto classicEnginesSashR = Prop!(int)("classicEnginesSashR", 2);
	auto classicEnginesNameTypeSashL = Prop!(int)("classicEnginesNameTypeSashL", 4);
	auto classicEnginesNameTypeSashR = Prop!(int)("classicEnginesNameTypeSashR", 3);
	auto classicEngineShortcutSashL = Prop!(int)("classicEngineShortcutSashL", 1, 2014103100);
	auto classicEngineShortcutSashR = Prop!(int)("classicEngineShortcutSashR", 4, 2014103100);
	auto featureDefaultNameWidth = Prop!(int, false, true)("featureDefaultNameWidth", 100);
	auto featureVariantNameWidth = Prop!(int, false, true)("featureVariantNameWidth", 100);
	auto featureManualNameWidth = Prop!(int, false, true)("featureManualNameWidth", 100);
	auto eventTemplatesSashL = Prop!(int)("eventTemplatesSashL", 1);
	auto eventTemplatesSashR = Prop!(int)("eventTemplatesSashR", 2);
	auto eventTemplatesOfScenarioSashL = Prop!(int)("eventTemplatesOfScenarioSashL", 1);
	auto eventTemplatesOfScenarioSashR = Prop!(int)("eventTemplatesOfScenarioSashR", 2);
	auto eventTemplateShortcutSashL = Prop!(int)("eventTemplateShortcutSashL", 1, 2014103100);
	auto eventTemplateShortcutSashR = Prop!(int)("eventTemplateShortcutSashR", 4, 2014103100);
	auto eventTemplateOfScenarioShortcutSashL = Prop!(int)("eventTemplateOfScenarioShortcutSashL", 1, 2014103100);
	auto eventTemplateOfScenarioShortcutSashR = Prop!(int)("eventTemplateOfScenarioShortcutSashR", 5, 2014103100);
	auto scenarioTemplatesSashL = Prop!(int)("scenarioTemplatesSashL", 1);
	auto scenarioTemplatesSashR = Prop!(int)("scenarioTemplatesSashR", 2);
	auto templatesSashL = Prop!(int)("templatesSashL", 1);
	auto templatesSashR = Prop!(int)("templatesSashR", 1);
	auto toolsClassicEnginesSashL = Prop!(int)("toolsClassicEnginesSashL", 1);
	auto toolsClassicEnginesSashR = Prop!(int)("toolsClassicEnginesSashR", 1);
	auto selectionWidth = Prop!(int, true, true)("selectionWidth", 100);
	auto keyCodeWidth = Prop!(int, true, true)("keyCodeWidth", 100);
	auto roundWidth = Prop!(int, true, true)("roundWidth", 80);
	auto scenarioPath = Prop!(string)("scenarioPath", "");
	auto tempPath = Prop!(string)("tempPath", "temp");
	auto backupPath = Prop!(string)("backupPath", "backup");
	auto backupEnabled = Prop!(bool)("backupEnabled", true);
	auto backupIntervalType = Prop!(int)("backupIntervalType", 0);
	auto backupInterval = Prop!(int)("backupInterval", 15);
	auto backupIntervalEdit = Prop!(int)("backupIntervalEdit", 20);
	auto backupCount = Prop!(int)("backupCount", 10);
	auto autoSave = Prop!(bool)("autoSave", false);
	auto backupRefAuthor = Prop!(bool)("backupRefAuthor", false);
	auto backupBeforeSaveEnabled = Prop!(bool)("backupBeforeSaveEnabled", true);
	auto backupBeforeSavePath = Prop!(string)("backupBeforeSavePath", "backup");
	auto backupBeforeSaveDir = Prop!(string, true)("backupBeforeSaveDir", "files");
	auto backupArchived = Prop!(bool)("backupArchived", true);
	auto ignoreMenuSashL = Prop!(int)("ignoreMenuSashL", 2);
	auto ignoreMenuSashR = Prop!(int)("ignoreMenuSashR", 1);
	auto highValueOfImageControl = Prop!(uint, true)("highValueOfImageControl", 10);
	auto showDuplicateViewInToolBar = Prop!(bool, true)("showDuplicateViewInToolBar", false);

	auto openHistories = Prop!(OpenHistory[])("openHistories", []);
	auto scenarioBookmarks = Prop!(OpenHistory[])("scenarioBookmarks", []);
	auto historyMax = Prop!(int)("historyMax", 10);
	auto historySnipLength = Prop!(int)("historySnipLength", 30);
	auto removeScenarioHistoryNonExistingWithPriority = Prop!(bool, true)("removeScenarioHistoryNonExistingWithPriority", true);
	auto lastScenario = Prop!(string)("lastScenario", "");
	auto importHistory = Prop!(OpenHistory[])("importHistory", []);
	auto importBookmarks = Prop!(OpenHistory[])("importBookmarks", []);
	auto searchResultTableWidth = Prop!(int, true, true)("searchResultTableWidth", 400);
	auto searchResultTableHeight = Prop!(int, true, true)("searchResultTableHeight", 200);
	version (Windows) {
		auto engine = Prop!(string, true)("engine", "CardWirthPy.exe");
	} else {
		auto engine = Prop!(string, true)("engine", "CardWirthPy");
	}
	auto engineScript = Prop!(string, true)("engineScript", "cardwirth.py");
	auto enginePath = Prop!(string)("enginePath", "");
	auto pythonCommand = Prop!(string, true)("pythonCommand", "python");
	auto lastExecutedParty = Prop!(ExecutionParty)("lastExecutedParty", ExecutionParty.init);
	auto executedParties = Prop!(ExecutionParty[])("executedParties", []);
	auto executedPartyBookmarks = Prop!(ExecutionParty[])("executedPartyBookmarks", []);
	auto executedPartiesMax = Prop!(uint)("executedPartiesMax", 10);
	auto removePartyHistoryNonExistingWithPriority = Prop!(bool, true)("removePartyHistoryNonExistingWithPriority", true);
	auto dataDir = Prop!(string, true)("dataDir", "Data");
	auto findEnginePath = Prop!(bool)("findEnginePath", true);
	auto defaultSkin = Prop!(string, true)("defaultSkin", "MedievalFantasy");
	auto defaultSkinName = Prop!(string, true)("defaultSkinName", "Classic");
	auto defaultAuthor = Prop!(string)("defaultAuthor", "");
	auto lastSkinType = Prop!(string)("lastSkinType", "");
	auto lastSkinName = Prop!(string)("lastSkinName", "");
	auto saveSkinName = Prop!(bool)("saveSkinName", true);
	auto replaceClassicResourceExtension = Prop!(bool)("replaceClassicResourceExtension", true);
	auto tableExtension = Prop!(string, true)("tableExtension", ".bmp");
	auto midiExtension = Prop!(string, true)("midiExtension", ".mid");
	auto waveExtension = Prop!(string, true)("waveExtension", ".wav");

	auto classicEngineRegex = Prop!(string)("classicEngineRegex", "^(CW´|.+Wirth(_.+|Next)?)\\.exe$");
	auto classicDataDirRegex = Prop!(string)("classicDataDirRegex", "^Data|D_[A-Z]1|[A-Z]_dt$");
	auto classicMatchKey = Prop!(string, true)("classicMatchKey", "Table" ~ dirSeparator ~ "MapOfWirth.BMP");

	auto expandXMLs = Prop!(bool)("expandXMLs", false);
	auto xmlCopy = Prop!(bool)("xmlCopy", false);
	auto showSpNature = Prop!(bool)("showSpNature", false);
	auto saveChangedOnlyWsn = Prop!(bool)("saveChangedOnlyWsn", true);
	auto saveChangedOnly = Prop!(bool)("saveChangedOnly", false);
	auto xmlFileNameIsIDOnly = Prop!(bool)("xmlFileNameIsIDOnly", false);
	auto archiveInNewThread = Prop!(bool)("archiveInNewThread", true);
	auto saveInnerImagePath = Prop!(bool)("saveInnerImagePath", false);
	auto linkCard = Prop!(bool)("linkCard", false);
	auto traceDirectories = Prop!(bool)("traceDirectories", true);
	auto logicalSort = Prop!(bool)("logicalSort", true);
	auto copyDesc = Prop!(bool)("copyDesc", false);
	auto refCardsAtEditBgImage = Prop!(bool)("refCardsAtEditBgImage", true);
	auto showImagePreview = Prop!(bool)("showImagePreview", true);
	auto maskCardImagePreview = Prop!(bool)("maskCardImagePreview", true);
	auto ignoreBackgroundInRange = Prop!(bool)("ignoreBackgroundInRange", true);
	auto classicStyleTree = Prop!(bool)("classicStyleTree", false);
	auto adjustContentName = Prop!(bool)("adjustContentName", true);
	auto showHandMark = Prop!(bool)("showHandMark", true);
	auto showHandMarkAlways = Prop!(bool)("showHandMarkAlways", true);
	auto showEventTreeMark = Prop!(bool)("showEventTreeMark", true);
	auto showEventTreeMarkAlways = Prop!(bool)("showEventTreeMarkAlways", true);
	auto ignoreEmptyStart = Prop!(bool)("ignoreEmptyStart", true);
	auto showSkillCardLevel = Prop!(bool)("showSkillCardLevel", true);
	auto switchTabWheel = Prop!(bool)("switchTabWheel", true);
	auto closeTabWithMiddleClick = Prop!(bool)("closeTabWithMiddleClick", true);
	auto openTabAtRightOfCurrentTab = Prop!(bool)("openTabAtRightOfCurrentTab", true);
	auto showCloseButtonAllTab = Prop!(bool)("showCloseButtonAllTab", false);
	auto cautionBeforeReplace = Prop!(bool)("cautionBeforeReplace", false);
	auto startIncrementalSearchWhenKeyDown = Prop!(bool)("startIncrementalSearchWhenKeyDown", true);
	auto radarStyleParams = Prop!(bool)("radarStyleParams", true);
	auto showAptitudeOnCastCardEditor = Prop!(bool)("showAptitudeOnCastCardEditor", true);
	auto showCardListHeader = Prop!(bool)("showCardListHeader", true);
	auto showCardListTitle = Prop!(bool)("showCardListTitle", true);
	auto applyDialogsBeforeSave = Prop!(bool)("applyDialogsBeforeSave", true);
	auto useNamesAfterStandard = Prop!(bool)("useNamesAfterStandard", false);
	auto expandChooserItems = Prop!(bool)("expandChooserItems", true);
	auto restorePositionOfEventTreeView = Prop!(bool)("restorePositionOfEventTreeView", true);
	auto selectVariableWithTree = Prop!(bool)("selectVariableWithTree", true);
	auto useCurrentStartName = Prop!(bool)("useCurrentStartName", true);
	auto autoUpdateJpy1File = Prop!(bool)("autoUpdateJpy1File", false);
	auto straightEventTreeView = Prop!(bool)("straightEventTreeView", true);
	auto eventTreeSlope = Prop!(int)("eventTreeSlope", 16);
	auto forceIndentBranchContent = Prop!(bool)("forceIndentBranchContent", true);
	auto showTerminalMark = Prop!(bool)("showTerminalMark", true);
	auto showTargetStartLineNumber = Prop!(bool)("showTargetStartLineNumber", true);
	auto clickIconIsStartEdit = Prop!(bool)("clickIconIsStartEdit", false);
	auto showAreaDirTree = Prop!(bool)("showAreaDirTree", true);
	auto incrementNewAreaName = Prop!(bool)("incrementNewAreaName", true);
	auto saveNeedChanged = Prop!(bool)("saveNeedChanged", true);
	auto canVanishWorkAreaInMainWindow = Prop!(bool)("canVanishWorkAreaInMainWindow", false);
	auto switchFixedWithCheckBox = Prop!(bool)("switchFixedWithCheckBox", true);
	auto resizingImageWithRatio = Prop!(bool)("resizingImageWithRatio", true);
	auto drawXORSelectionLine = Prop!(bool)("drawXORSelectionLine", false);
	auto showSceneViewSelectionFilter = Prop!(bool)("showSceneViewSelectionFilter", true);
	auto showItemNumberOfSceneAndEventView = Prop!(bool)("showItemNumberOfSceneAndEventView", true);
	auto showCardStatusUnderPointer = Prop!(bool)("showCardStatusUnderPointer", true);
	auto showStatusTime = Prop!(int)("showStatusTime", ShowStatusTime.Always);

	auto showEventTreeLineNumber = Prop!(bool)("showEventTreeLineNumber", true);
	auto showEventTreeDetail = Prop!(bool)("showEventTreeDetail", true);
	auto editTriggerType = Prop!(int)("editTriggerType", EditTrigger.Slow);
	auto specifySelectedEventTree = Prop!(bool)("specifySelectedEventTree", true);

	auto spinnerUpDownWithWheel = Prop!(bool)("spinnerUpDownWithWheel", true);

	auto soundPlayType = Prop!(int)("soundPlayType", 0);
	auto soundEffectPlayType = Prop!(int)("soundEffectPlayType", -1);
	auto bgmVolume = Prop!(int)("bgmVolume", 100);
	auto seVolume = Prop!(int)("seVolume", 100);
	auto cardWirthPyDefaultSoundFont = Prop!(string, true)("cardWirthPyDefaultSoundFont", "Data/SoundFont/005.6mg_Aspirin_Stereo_V1.2_Bank.sf2");

	auto loopCountMax = Prop!(uint)("loopCountMax", 100, true);
	auto fadeInMax = Prop!(uint)("fadeInMax", 1000, true);
	auto excludeCardSizeImage = Prop!(bool)("excludeCardSizeImage", true);

	auto selectionColumnsMax = Prop!(uint)("selectionColumnsMax", 4, true);

	auto showInheritBackground = Prop!(bool, true)("showInheritBackground", true);
	auto detailAreaWidth = Prop!(int, false, true)("detailAreaWidth", 200);

	auto searchPlan = Prop!(int)("searchPlan", 0);
	auto searchIDKind = Prop!(int)("searchIDKind", 0);
	auto searchHistories = Prop!(string[])("searchHistories", []);
	auto replaceHistories = Prop!(string[])("replaceHistories", []);
	auto searchHistoryMax = Prop!(int)("searchHistoryMax", 50);
	auto replaceRangeSashL = Prop!(int)("replaceRangeSashL", 3, 2012090100);
	auto replaceRangeSashR = Prop!(int)("replaceRangeSashR", 1, 2012090100);
	auto replaceTextNotIgnoreCase = Prop!(bool)("replaceTextNotIgnoreCase", false);
	auto replaceTextRegExp = Prop!(bool)("replaceTextRegExp", false);
	auto replaceTextWildcard = Prop!(bool)("replaceTextWildcard", false);
	auto replaceTextExactMatch = Prop!(bool)("replaceTextExactMatch", false);
	auto replaceTextIgnoreReturnCode = Prop!(bool)("replaceTextIgnoreReturnCode", false);
	auto grepDir = Prop!(string)("grepDir", "");
	auto grepDirHistories = Prop!(string[])("grepDirHistories", []);
	auto grepSubDir = Prop!(bool)("grepSubDir", true);
	auto searchResultRefreshCount = Prop!(int, true)("searchResultRefreshCount", 100);
	auto searchResultRealtime = Prop!(bool)("searchResultRealtime", false);
	auto showRouteOfSearchResult = Prop!(bool, true)("showRouteOfSearchResult", true);
	auto searchErrorTargetVersion = Prop!(string)("searchErrorTargetVersion", "");

	auto replaceTextSummary = Prop!(bool)("replaceTextSummary", true);
	auto replaceTextScenario = Prop!(bool)("replaceTextScenario", false);
	auto replaceTextAuthor = Prop!(bool)("replaceTextAuthor", false);
	auto replaceTextMessage = Prop!(bool)("replaceTextMessage", true);
	auto replaceTextCardName = Prop!(bool)("replaceTextCardName", true);
	auto replaceTextCardDescription = Prop!(bool)("replaceTextCardDescription", true);
	auto replaceTextEventText = Prop!(bool)("replaceTextEventText", true);
	auto replaceTextStart = Prop!(bool)("replaceTextStart", true);
	auto replaceTextVariableName = Prop!(bool)("replaceTextVariableName", true);
	auto replaceTextVariableValue = Prop!(bool)("replaceTextVariableValue", true);
	auto replaceTextExpression = Prop!(bool)("replaceTextExpression", true);
	auto replaceTextCoupon = Prop!(bool)("replaceTextCoupon", true);
	auto replaceTextGossip = Prop!(bool)("replaceTextGossip", true);
	auto replaceTextEndScenario = Prop!(bool)("replaceTextEndScenario", true);
	auto replaceTextAreaName = Prop!(bool)("replaceTextAreaName", true);
	auto replaceTextKeyCode = Prop!(bool)("replaceTextKeyCode", true);
	auto replaceTextCellName = Prop!(bool)("replaceTextCellName", true);
	auto replaceTextCardGroup = Prop!(bool)("replaceTextCardGroup", true);
	auto replaceTextFile = Prop!(bool)("replaceTextFile", false);
	auto replaceTextComment = Prop!(bool)("replaceTextComment", true);
	auto replaceTextJptx = Prop!(bool)("replaceTextJptx", false);
	auto replaceTextTextFile = Prop!(bool)("replaceTextTextFile", false);

	auto plainTextFileExtensions = Prop!(string[])("plainTextFileExtensions", [".txt", ".ini", ".sli"]);

	auto replaceNameCoupon = Prop!(bool)("replaceNameCoupon", true);
	auto replaceNameGossip = Prop!(bool)("replaceNameGossip", true);
	auto replaceNameEndScenario = Prop!(bool)("replaceNameEndScenario", true);
	auto replaceNameKeyCode = Prop!(bool)("replaceNameKeyCode", true);
	auto replaceNameCellName = Prop!(bool)("replaceNameCellName", true);
	auto replaceNameCardGroup = Prop!(bool)("replaceNameCardGroup", true);

	mixin EnumToMembers!(CType, cType => "auto searchContents" ~ .text(cType)
		~ " = Prop!(bool)(\"searchContents" ~ .text(cType) ~ "\", false);\n");

	const
	bool searchContents(CType id) { mixin(S_TRACE);
		mixin(EnumToStringSwitch!(CType, "searchContents"));
	}
	void searchContents(CType id, bool value) { mixin(S_TRACE);
		mixin(EnumToStringSwitch!(CType, "searchContents", "value"));
	}

	auto searchUnusedFlag = Prop!(bool)("searchUnusedFlag", true);
	auto searchUnusedStep = Prop!(bool)("searchUnusedStep", true);
	auto searchUnusedVariant = Prop!(bool)("searchUnusedVariant", true);
	auto searchUnusedArea = Prop!(bool)("searchUnusedArea", true);
	auto searchUnusedBattle = Prop!(bool)("searchUnusedBattle", true);
	auto searchUnusedPackage = Prop!(bool)("searchUnusedPackage", true);
	auto searchUnusedCast = Prop!(bool)("searchUnusedCast", true);
	auto searchUnusedSkill = Prop!(bool)("searchUnusedSkill", true);
	auto searchUnusedItem = Prop!(bool)("searchUnusedItem", true);
	auto searchUnusedBeast = Prop!(bool)("searchUnusedBeast", true);
	auto searchUnusedInfo = Prop!(bool)("searchUnusedInfo", true);
	auto searchUnusedStart = Prop!(bool)("searchUnusedStart", true);
	auto searchUnusedPath = Prop!(bool)("searchUnusedPath", true);
	auto searchOpenDialog = Prop!(bool)("searchOpenDialog", false);

	auto searchResultColumnMain = Prop!(int, false, true)("searchResultColumnMain", 400);
	auto searchResultColumnParent = Prop!(int, false, true)("searchResultColumnParent", 200);
	auto searchResultColumnCouponCount = Prop!(int, false, true)("searchResultColumnCouponCount", 60);
	auto searchResultColumnErrorDesc = Prop!(int, false, true)("searchResultColumnErrorDesc", 500);
	auto searchResultColumnScenario = Prop!(int, false, true)("searchResultColumnScenario", 500);

	auto incrementalSearchType = Prop!(int)("incrementalSearchType", 0);

	auto usedCouponToCombo = Prop!(bool, true)("usedCouponToCombo", true);

	auto numberStepToVariantThreshold = Prop!(uint, true)("numberStepToVariantThreshold", 100);

	auto newScenarioName = Prop!(string, true)("newScenarioName", "新規シナリオ");
	auto newAreaName = Prop!(string)("newAreaName", "開始エリア");

	auto flagTrues = Prop!(string[])("flagTrues", ["TRUE", "表示", "ON", "有", "可", "済み"], true);
	auto flagFalses = Prop!(string[])("flagFalses", ["FALSE", "非表示", "OFF", "無", "不可", "まだ"], true);

	auto bgImageSettings = Prop!(BgImageSetting[])("bgImageSettings", [
		BgImageSetting("冒険者の宿", 116, 15, 400, 260, false, LAYER_BACK_CELL),
		BgImageSetting("冒険者の宿(フレーム)", 116, 14, 400, 261, true, LAYER_BACK_CELL),
		BgImageSetting("フル", 0, 0, 632, 420, false, LAYER_BACK_CELL),
		BgImageSetting("フル(マスク)", 0, 0, 632, 420, true, LAYER_BACK_CELL),
		BgImageSetting("カード", 0, 0, 74, 94, true, LAYER_BACK_CELL),
		BgImageSetting("冒険者カード", 0, 0, 95, 130, false, LAYER_BACK_CELL),
		BgImageSetting("ゲームオーバー", 116, 55, 400, 260, false, LAYER_BACK_CELL),
		BgImageSetting("Qubes 地面", 160, 80, 320, 160, true, LAYER_BACK_CELL),
		BgImageSetting("Qubes 左後", 80, 0, 240, 160, true, LAYER_BACK_CELL),
		BgImageSetting("Qubes 右後", 320, 0, 240, 160, true, LAYER_BACK_CELL),
		BgImageSetting("Qubes 左前", 80, 80, 240, 200, true, LAYER_BACK_CELL),
		BgImageSetting("Qubes 右前", 320, 80, 240, 200, true, LAYER_BACK_CELL)
	]);
	auto standardCoupons = Prop!(string[])("standardCoupons", [
		"：Ｒ", "＿１", "＿２", "＿３", "＿４", "＿５", "＿６", "＿消滅予約", "：レベル補正中"
	], true);
	auto standardCouponsForValued = Prop!(string[])("standardCouponsForValued", [
		"＿１", "＿２", "＿３", "＿４", "＿５", "＿６"
	], true);
	auto standardKeyCodes = Prop!(string[])("standardKeyCodes", [
		"攻撃",
		"治療",
		"魔法",
		"召喚獣",
		"気功法",
		"遠距離攻撃",
		"神聖な攻撃",
		"魔法による攻撃",
		"炎による攻撃",
		"冷気による攻撃",
		"暗殺",
		"精神を回復",
		"中毒を解除",
		"麻痺を解除",
		"眠り",
		"麻痺",
		"中毒",
		"呪縛",
		"沈黙",
		"召喚",
		"鑑定",
		"解錠",
		"呪縛を解除",
		"沈黙を解除",
		"魔法を解除",
		"",
		"魔力感知",
		"生命感知",
		"魔法の鍵",
		"解読",
		"石化",
		"石化を解除",
		"蝙蝠変化",
		"明かり",
		"目つぶし",
		"魅了",
		"透明",
		"召喚獣を付与",
		"暴露",
		"即死",
		"一撃必殺",
		"対象消去",
		"恐慌",
		"不浄な攻撃",
		"呪い",
		"飛行",
		"浮遊",
		"灯火",
		"",
		"フェイント",
		"防御",
		"逃走",
		"カード交換",
		"ペナルティ",
		"リサイクル",
		"",
		"一撃",
		"守備",
	]);
	auto keyCodesByFeatures = Prop!(KeyCodeByFeature[])("keyCodesByFeatures", [
		KeyCodeByFeature("魔法である", "魔法"),
		KeyCodeByFeature("気功法である", "気功法"),
		KeyCodeByFeature("遠距離攻撃である", "遠距離攻撃"),
		KeyCodeByFeature("不意打ちである", "暗殺"),
		KeyCodeByFeature("召喚獣である", "召喚獣"),
		KeyCodeByFeature("観察・鑑定を行う", "鑑定"),
		KeyCodeByFeature("解錠を行う", "解錠"),
		KeyCodeByFeature("魔法を解く", "魔法を解除"),
	]);
	auto keyCodesByMotions = Prop!(KeyCodeByMotion[])("keyCodesByMotions", [
		KeyCodeByMotion(MType.Damage, false, Element.All, "攻撃"),
		KeyCodeByMotion(MType.Absorb, false, Element.All, "攻撃"),
		KeyCodeByMotion(MType.Heal, false, Element.All, "治療"),
		KeyCodeByMotion(MType.VanishTarget, true, Element.Miracle, "神聖な攻撃"),
		KeyCodeByMotion(MType.Damage, true, Element.Miracle, "魔法による攻撃"),
		KeyCodeByMotion(MType.Absorb, true, Element.Miracle, "魔法による攻撃"),
		KeyCodeByMotion(MType.Damage, true, Element.Magic, "魔法による攻撃"),
		KeyCodeByMotion(MType.Absorb, true, Element.Magic, "魔法による攻撃"),
		KeyCodeByMotion(MType.Damage, true, Element.Fire, "炎による攻撃"),
		KeyCodeByMotion(MType.Absorb, true, Element.Fire, "炎による攻撃"),
		KeyCodeByMotion(MType.Damage, true, Element.Ice, "冷気による攻撃"),
		KeyCodeByMotion(MType.Absorb, true, Element.Ice, "冷気による攻撃"),
		KeyCodeByMotion(MType.Normal, false, Element.All, "精神を回復"),
		KeyCodeByMotion(MType.DisPoison, false, Element.All, "中毒を解除"),
		KeyCodeByMotion(MType.DisParalyze, false, Element.All, "麻痺を解除"),
		KeyCodeByMotion(MType.Sleep, false, Element.All, "眠り"),
		KeyCodeByMotion(MType.Paralyze, false, Element.All, "麻痺"),
		KeyCodeByMotion(MType.Poison, false, Element.All, "中毒"),
		KeyCodeByMotion(MType.Bind, false, Element.All, "呪縛"),
		KeyCodeByMotion(MType.Silence, false, Element.All, "沈黙"),
		KeyCodeByMotion(MType.DisBind, false, Element.All, "呪縛を解除"),
		KeyCodeByMotion(MType.DisSilence, false, Element.All, "沈黙を解除"),
		KeyCodeByMotion(MType.VanishTarget, false, Element.All, "対象消去"),
	]);
	auto standardSelections = Prop!(string[])("standardSelections", [
		"はい",
		"いいえ",
		"キャンセル",
		"Yes",
		"No",
		"Cancel",
	]);

	auto settingsWithSkinTypes = Prop!(SettingsWithSkinTypeList)("settingsWithSkinTypes", SettingsWithSkinTypeList([
		SettingsWithSkinType("Modern", false, false, true, [ ], [ ], [ ], [
			"攻撃",
			"治療",
			"召喚獣",
			"遠距離攻撃",
			"魔法による攻撃",
			"炎による攻撃",
			"冷気による攻撃",
			"暗殺",
			"精神を回復",
			"中毒を解除",
			"麻痺を解除",
			"眠り",
			"麻痺",
			"中毒",
			"呪縛",
			"沈黙",
			"召喚",
			"鑑定",
			"解錠",
			"呪縛を解除",
			"沈黙を解除",
			"魔法を解除",
			"",
			"超能力",
			"探偵の心得",
			"機械",
			"故障",
			"ハッキング",
			"学問",
			"学術",
			"能力変化",
			"防御力増大",
			"抵抗力増大",
			"行動力増大",
			"回避力増大",
			"魔物殺し",
			"銃による攻撃",
			"暴露",
			"隠れる",
			"尾行",
			"窃盗",
			"カード偽造",
			"魔法を無効化",
			"精神力を回復",
			"精神力回復",
			"仮想体",
			"",
			"フェイント",
			"防御",
			"逃走",
			"カード交換",
			"ペナルティ",
			"リサイクル",
			"",
			"一撃",
			"守備",
		], [
			KeyCodeByFeature("遠距離攻撃である", "遠距離攻撃"),
			KeyCodeByFeature("超能力である", "超能力"),
			KeyCodeByFeature("探偵の技術である", "探偵の心得"),
			KeyCodeByFeature("銃による攻撃である", "銃による攻撃"),
			KeyCodeByFeature("機械を攻撃する", "故障"),
			KeyCodeByFeature("機械を操作する", "機械"),
			KeyCodeByFeature("コンピュータを攻撃する", "ハッキング"),
			KeyCodeByFeature("対魔物の攻撃である", "魔物殺し"),
			KeyCodeByFeature("学問である", "学問"),
			KeyCodeByFeature("学術である", "学術"),
			KeyCodeByFeature("不意打ちである", "暗殺"),
			KeyCodeByFeature("観察・鑑定を行う", "鑑定"),
			KeyCodeByFeature("姿を隠す", "隠れる"),
			KeyCodeByFeature("尾行を行う", "尾行"),
			KeyCodeByFeature("窃盗を行う", "窃盗"),
			KeyCodeByFeature("カードを偽造する", "カード偽造"),
			KeyCodeByFeature("召喚獣である", "召喚獣"),
			KeyCodeByFeature("仮想体である", "仮想体"),
			KeyCodeByFeature("防御力を強化する", "防御力増大"),
			KeyCodeByFeature("抵抗力を強化する", "抵抗力増大"),
			KeyCodeByFeature("行動力を強化する", "行動力増大"),
			KeyCodeByFeature("回避力を強化する", "回避力増大"),
		], [
			KeyCodeByMotion(MType.Damage, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Absorb, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Heal, false, Element.All, "治療"),
			KeyCodeByMotion(MType.Damage, true, Element.Miracle, "魔法による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Miracle, "魔法による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Magic, "魔法による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Magic, "魔法による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Fire, "炎による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Fire, "炎による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Ice, "冷気による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Ice, "冷気による攻撃"),
			KeyCodeByMotion(MType.Normal, false, Element.All, "精神を回復"),
			KeyCodeByMotion(MType.DisPoison, false, Element.All, "中毒を解除"),
			KeyCodeByMotion(MType.DisParalyze, false, Element.All, "麻痺を解除"),
			KeyCodeByMotion(MType.Sleep, false, Element.All, "眠り"),
			KeyCodeByMotion(MType.Paralyze, false, Element.All, "麻痺"),
			KeyCodeByMotion(MType.Poison, false, Element.All, "中毒"),
			KeyCodeByMotion(MType.Bind, false, Element.All, "呪縛"),
			KeyCodeByMotion(MType.Silence, false, Element.All, "沈黙"),
			KeyCodeByMotion(MType.DisBind, false, Element.All, "呪縛を解除"),
			KeyCodeByMotion(MType.DisSilence, false, Element.All, "沈黙を解除"),
			KeyCodeByMotion(MType.FaceUp, false, Element.All, "暴露"),
			KeyCodeByMotion(MType.AntiMagic, false, Element.All, "魔法を無効化"),
			KeyCodeByMotion(MType.GetSkillPower, false, Element.All, "精神力を回復"),
			KeyCodeByMotion(MType.GetSkillPower, false, Element.All, "精神力回復"),
			KeyCodeByMotion(MType.EnhanceAction, false, Element.All, "能力変化"),
			KeyCodeByMotion(MType.EnhanceAvoid, false, Element.All, "能力変化"),
			KeyCodeByMotion(MType.EnhanceResist, false, Element.All, "能力変化"),
			KeyCodeByMotion(MType.EnhanceDefense, false, Element.All, "能力変化"),
		]),
		SettingsWithSkinType("Monsters", true, false, true, [
			BgImageS("MapOfWirth", 0, 0, 632, 420, false),
		], [
			BgImageSetting("妖魔の森", 116, 15, 400, 260, false, LAYER_BACK_CELL),
			BgImageSetting("妖魔の森(フレーム)", 116, 14, 400, 261, true, LAYER_BACK_CELL),
			BgImageSetting("フル", 0, 0, 632, 420, false, LAYER_BACK_CELL),
			BgImageSetting("フル(マスク)", 0, 0, 632, 420, true, LAYER_BACK_CELL),
			BgImageSetting("カード", 0, 0, 74, 94, true, LAYER_BACK_CELL),
			BgImageSetting("妖魔カード", 0, 0, 95, 130, false, LAYER_BACK_CELL),
			BgImageSetting("ゲームオーバー", 116, 55, 400, 260, false, LAYER_BACK_CELL),
			BgImageSetting("Qubes 地面", 160, 80, 320, 160, true, LAYER_BACK_CELL),
			BgImageSetting("Qubes 左後", 80, 0, 240, 160, true, LAYER_BACK_CELL),
			BgImageSetting("Qubes 右後", 320, 0, 240, 160, true, LAYER_BACK_CELL),
			BgImageSetting("Qubes 左前", 80, 80, 240, 200, true, LAYER_BACK_CELL),
			BgImageSetting("Qubes 右前", 320, 80, 240, 200, true, LAYER_BACK_CELL)
		], [ ], [
			"攻撃",
			"撹乱",
			"防御",
			"体力を回復",
			"魔法",
			"召喚獣",
			"気功法",
			"遠距離攻撃",
			"神聖な攻撃",
			"魔法による攻撃",
			"音による攻撃",
			"視覚による攻撃",
			"暗殺",
			"対象消去",
			"精神を回復",
			"中毒を解除",
			"麻痺を解除",
			"眠り",
			"麻痺",
			"中毒",
			"呪縛",
			"沈黙",
			"召喚",
			"鑑定",
			"解錠",
			"人間言語",
			"呪縛を解除",
			"沈黙を解除",
			"魔法を解除",
			"",
			"誘惑",
			"盗む",
			"煙幕",
			"",
			"フェイント",
			"防御",
			"逃走",
			"カード交換",
			"ペナルティ",
			"リサイクル",
			"",
			"一撃",
			"守備",
		], [
			KeyCodeByFeature("魔法である", "魔法"),
			KeyCodeByFeature("気功法である", "気功法"),
			KeyCodeByFeature("遠距離攻撃である", "遠距離攻撃"),
			KeyCodeByFeature("不意打ちである", "暗殺"),
			KeyCodeByFeature("召喚獣である", "召喚獣"),
			KeyCodeByFeature("観察・鑑定を行う", "鑑定"),
			KeyCodeByFeature("解錠を行う", "解錠"),
			KeyCodeByFeature("魔法を解く", "魔法を解除"),
			KeyCodeByFeature("誘惑を行う", "誘惑"),
			KeyCodeByFeature("人間の言語を使用する", "人間言語"),
			KeyCodeByFeature("窃盗を行う", "盗む"),
			KeyCodeByFeature("煙幕を張る", "煙幕"),
		], [
			KeyCodeByMotion(MType.Damage, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Absorb, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Heal, false, Element.All, "体力を回復"),
			KeyCodeByMotion(MType.VanishTarget, true, Element.Miracle, "神聖な攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Miracle, "魔法による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Miracle, "魔法による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Magic, "魔法による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Magic, "魔法による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Fire, "音による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Fire, "音による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Ice, "視覚による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Ice, "視覚による攻撃"),
			KeyCodeByMotion(MType.Normal, false, Element.All, "精神を回復"),
			KeyCodeByMotion(MType.DisPoison, false, Element.All, "中毒を解除"),
			KeyCodeByMotion(MType.DisParalyze, false, Element.All, "麻痺を解除"),
			KeyCodeByMotion(MType.Sleep, false, Element.All, "眠り"),
			KeyCodeByMotion(MType.Paralyze, false, Element.All, "麻痺"),
			KeyCodeByMotion(MType.Poison, false, Element.All, "中毒"),
			KeyCodeByMotion(MType.Bind, false, Element.All, "呪縛"),
			KeyCodeByMotion(MType.Silence, false, Element.All, "沈黙"),
			KeyCodeByMotion(MType.DisBind, false, Element.All, "呪縛を解除"),
			KeyCodeByMotion(MType.DisSilence, false, Element.All, "沈黙を解除"),
			KeyCodeByMotion(MType.Confuse, false, Element.All, "撹乱"),
			KeyCodeByMotion(MType.VanishTarget, false, Element.All, "対象消去"),
		], [
			ElementOverride(Element.Fire, "presetimage://elm_hearing.png", "音", ""),
			ElementOverride(Element.Ice, "presetimage://elm_vision.png", "視覚", ""),
		]),
		SettingsWithSkinType("Oedo", true, true, true, [
			BgImageS("MapOfWirth", 0, 0, 632, 420, false),
		], [
			BgImageSetting("万長屋", 116, 15, 400, 260, false, LAYER_BACK_CELL),
			BgImageSetting("万長屋(フレーム)", 116, 14, 400, 261, true, LAYER_BACK_CELL),
			BgImageSetting("フル", 0, 0, 632, 420, false, LAYER_BACK_CELL),
			BgImageSetting("フル(マスク)", 0, 0, 632, 420, true, LAYER_BACK_CELL),
			BgImageSetting("カード", 0, 0, 74, 94, true, LAYER_BACK_CELL),
			BgImageSetting("仕事人カード", 0, 0, 95, 130, false, LAYER_BACK_CELL),
			BgImageSetting("ゲームオーバー", 116, 55, 400, 260, false, LAYER_BACK_CELL),
			BgImageSetting("Qubes 地面", 160, 80, 320, 160, true, LAYER_BACK_CELL),
			BgImageSetting("Qubes 左後", 80, 0, 240, 160, true, LAYER_BACK_CELL),
			BgImageSetting("Qubes 右後", 320, 0, 240, 160, true, LAYER_BACK_CELL),
			BgImageSetting("Qubes 左前", 80, 80, 240, 200, true, LAYER_BACK_CELL),
			BgImageSetting("Qubes 右前", 320, 80, 240, 200, true, LAYER_BACK_CELL)
		], [
			"是",
			"認",
			"否",
			"きゃんせる",
			"はい",
			"いいえ",
			"取り消し",
		], [
			"攻撃",
			"撹乱",
			"防御",
			"治療",
			"魔法",
			"召喚獣",
			"気功法",
			"遠距離攻撃",
			"神聖な攻撃",
			"魔法による攻撃",
			"炎による攻撃",
			"冷気による攻撃",
			"暗殺",
			"対象消去",
			"精神を回復",
			"中毒を解除",
			"麻痺を解除",
			"眠り",
			"麻痺",
			"中毒",
			"呪縛",
			"沈黙",
			"召喚",
			"鑑定",
			"解錠",
			"呪縛を解除",
			"沈黙を解除",
			"魔法を解除",
			"明かり",
			"",
			"フェイント",
			"逃走",
			"カード交換",
			"ペナルティ",
			"リサイクル",
			"",
			"一撃",
			"守備",
		], [
			KeyCodeByFeature("魔法である", "魔法"),
			KeyCodeByFeature("気功法である", "気功法"),
			KeyCodeByFeature("遠距離攻撃である", "遠距離攻撃"),
			KeyCodeByFeature("不意打ちである", "暗殺"),
			KeyCodeByFeature("防御を行う", "防御"),
			KeyCodeByFeature("召喚獣である", "召喚獣"),
			KeyCodeByFeature("観察・鑑定を行う", "鑑定"),
			KeyCodeByFeature("解錠を行う", "解錠"),
			KeyCodeByFeature("魔法を解く", "魔法を解除"),
			KeyCodeByFeature("光源になる", "明かり"),
		], [
			KeyCodeByMotion(MType.Damage, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Absorb, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Confuse, false, Element.All, "撹乱"),
			KeyCodeByMotion(MType.Heal, false, Element.All, "治療"),
			KeyCodeByMotion(MType.VanishTarget, true, Element.Miracle, "神聖な攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Miracle, "魔法による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Miracle, "魔法による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Magic, "魔法による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Magic, "魔法による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Fire, "炎による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Fire, "炎による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Ice, "冷気による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Ice, "冷気による攻撃"),
			KeyCodeByMotion(MType.Normal, false, Element.All, "精神を回復"),
			KeyCodeByMotion(MType.DisPoison, false, Element.All, "中毒を解除"),
			KeyCodeByMotion(MType.DisParalyze, false, Element.All, "麻痺を解除"),
			KeyCodeByMotion(MType.Sleep, false, Element.All, "眠り"),
			KeyCodeByMotion(MType.Paralyze, false, Element.All, "麻痺"),
			KeyCodeByMotion(MType.Poison, false, Element.All, "中毒"),
			KeyCodeByMotion(MType.Bind, false, Element.All, "呪縛"),
			KeyCodeByMotion(MType.Silence, false, Element.All, "沈黙"),
			KeyCodeByMotion(MType.DisBind, false, Element.All, "呪縛を解除"),
			KeyCodeByMotion(MType.DisSilence, false, Element.All, "沈黙を解除"),
			KeyCodeByMotion(MType.VanishTarget, false, Element.All, "対象消去"),
		]),
		SettingsWithSkinType("School", false, false, true, [ ], [ ], [ ], [
			"攻撃",
			"撹乱",
			"防御",
			"治療",
			"魔法",
			"召喚獣",
			"気功法",
			"遠距離攻撃",
			"神聖な攻撃",
			"魔法による攻撃",
			"炎による攻撃",
			"冷気による攻撃",
			"水による攻撃",
			"雷による攻撃",
			"暗殺",
			"対象消去",
			"精神を回復",
			"中毒を解除",
			"麻痺を解除",
			"眠り",
			"麻痺",
			"中毒",
			"呪縛",
			"沈黙",
			"召喚",
			"鑑定",
			"解錠",
			"呪縛を解除",
			"沈黙を解除",
			"魔法を解除",
			"飛行",
			"跳躍",
			"魔力感知",
			"生命感知",
			"解読",
			"話術",
			"挑発",
			"魅了",
			"盗む",
			"明かり",
			"食料",
			"",
			"フェイント",
			"逃走",
			"カード交換",
			"ペナルティ",
			"リサイクル",
			"",
			"一撃",
			"守備",
		], [
			KeyCodeByFeature("魔法である", "魔法"),
			KeyCodeByFeature("気功法である", "気功法"),
			KeyCodeByFeature("遠距離攻撃である", "遠距離攻撃"),
			KeyCodeByFeature("水による攻撃である", "水による攻撃"),
			KeyCodeByFeature("雷による攻撃である", "雷による攻撃"),
			KeyCodeByFeature("不意打ちである", "暗殺"),
			KeyCodeByFeature("防御を行う", "防御"),
			KeyCodeByFeature("召喚獣である", "召喚獣"),
			KeyCodeByFeature("観察・鑑定を行う", "鑑定"),
			KeyCodeByFeature("解錠を行う", "解錠"),
			KeyCodeByFeature("魔法を解く", "魔法を解除"),
			KeyCodeByFeature("飛行能力を得る", "飛行"),
			KeyCodeByFeature("跳躍する", "跳躍"),
			KeyCodeByFeature("魔力を感知する", "魔力感知"),
			KeyCodeByFeature("生命を感知する", "生命感知"),
			KeyCodeByFeature("解読を行う", "解読"),
			KeyCodeByFeature("話術である", "話術"),
			KeyCodeByFeature("挑発を行う", "挑発"),
			KeyCodeByFeature("魅了する", "魅了"),
			KeyCodeByFeature("盗みを行う", "盗む"),
			KeyCodeByFeature("光源になる", "明かり"),
			KeyCodeByFeature("食料品になる", "食料"),
		], [
			KeyCodeByMotion(MType.Damage, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Absorb, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Confuse, false, Element.All, "撹乱"),
			KeyCodeByMotion(MType.Heal, false, Element.All, "治療"),
			KeyCodeByMotion(MType.VanishTarget, true, Element.Miracle, "神聖な攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Miracle, "魔法による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Miracle, "魔法による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Magic, "魔法による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Magic, "魔法による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Fire, "炎による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Fire, "炎による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Ice, "冷気による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Ice, "冷気による攻撃"),
			KeyCodeByMotion(MType.Normal, false, Element.All, "精神を回復"),
			KeyCodeByMotion(MType.DisPoison, false, Element.All, "中毒を解除"),
			KeyCodeByMotion(MType.DisParalyze, false, Element.All, "麻痺を解除"),
			KeyCodeByMotion(MType.Sleep, false, Element.All, "眠り"),
			KeyCodeByMotion(MType.Paralyze, false, Element.All, "麻痺"),
			KeyCodeByMotion(MType.Poison, false, Element.All, "中毒"),
			KeyCodeByMotion(MType.Bind, false, Element.All, "呪縛"),
			KeyCodeByMotion(MType.Silence, false, Element.All, "沈黙"),
			KeyCodeByMotion(MType.DisBind, false, Element.All, "呪縛を解除"),
			KeyCodeByMotion(MType.DisSilence, false, Element.All, "沈黙を解除"),
			KeyCodeByMotion(MType.VanishTarget, false, Element.All, "対象消去"),
		]),
		SettingsWithSkinType("ScienceFiction", false, false, true, [ ], [ ], [ ], [
			"攻撃",
			"手札の交換",
			"撹乱",
			"守備",
			"治療",
			"遠距離攻撃",
			"魔法による攻撃",
			"炎による攻撃",
			"冷気による攻撃",
			"暗殺",
			"対象消去",
			"精神を回復",
			"中毒を解除",
			"麻痺を解除",
			"眠り",
			"麻痺",
			"中毒",
			"呪縛",
			"沈黙",
			"鑑定",
			"解錠",
			"呪縛を解除",
			"沈黙を解除",
			"",
			"狙撃",
			"隠形",
			"擬態",
			"全体攻撃",
			"銃声",
			"異能力",
			"サイバー攻撃",
			"システム撹乱",
			"システム破壊",
			"システム解析",
			"システム掌握",
			"",
			"フェイント",
			"逃走",
			"カード交換",
			"ペナルティ",
			"リサイクル",
			"",
			"一撃",
		], [
			KeyCodeByFeature("銃声が鳴る", "銃声"),
			KeyCodeByFeature("異能力である", "異能力"),
			KeyCodeByFeature("遠距離攻撃である", "遠距離攻撃"),
			KeyCodeByFeature("全体攻撃である", "全体攻撃"),
			KeyCodeByFeature("狙い撃つ", "狙撃"),
			KeyCodeByFeature("不意打ちである", "暗殺"),
			KeyCodeByFeature("防御を行う", "守備"),
			KeyCodeByFeature("撹乱を行う", "撹乱"),
			KeyCodeByFeature("姿を隠す", "隠形"),
			KeyCodeByFeature("擬態する", "擬態"),
			KeyCodeByFeature("調査を行う", "調査"),
			KeyCodeByFeature("鑑定を行う", "鑑定"),
			KeyCodeByFeature("解錠を行う", "解錠"),
			KeyCodeByFeature("システムを撹乱する", "システム撹乱"),
			KeyCodeByFeature("システムを破壊する", "システム破壊"),
			KeyCodeByFeature("システムを解析する", "システム解析"),
			KeyCodeByFeature("システムを掌握する", "システム掌握"),
		], [
			KeyCodeByMotion(MType.Damage, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Absorb, false, Element.All, "攻撃"),
			KeyCodeByMotion(MType.Heal, false, Element.All, "治療"),
			KeyCodeByMotion(MType.Normal, false, Element.All, "精神を回復"),
			KeyCodeByMotion(MType.Damage, true, Element.Fire, "炎による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Fire, "炎による攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Ice, "冷気による攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Ice, "冷気による攻撃"),
			KeyCodeByMotion(MType.DisPoison, false, Element.All, "中毒を解除"),
			KeyCodeByMotion(MType.DisParalyze, false, Element.All, "麻痺を解除"),
			KeyCodeByMotion(MType.Sleep, false, Element.All, "眠り"),
			KeyCodeByMotion(MType.Paralyze, false, Element.All, "麻痺"),
			KeyCodeByMotion(MType.Poison, false, Element.All, "中毒"),
			KeyCodeByMotion(MType.Bind, false, Element.All, "呪縛"),
			KeyCodeByMotion(MType.Silence, false, Element.All, "沈黙"),
			KeyCodeByMotion(MType.DisBind, false, Element.All, "呪縛を解除"),
			KeyCodeByMotion(MType.DisSilence, false, Element.All, "沈黙を解除"),
			KeyCodeByMotion(MType.Damage, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Absorb, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Paralyze, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Poison, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.LoseSkillPower, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Sleep, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Confuse, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Overheat, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Panic, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Bind, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Silence, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.FaceUp, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.VanishTarget, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.VanishBeast, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.DealConfuseCard, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.CancelAction, true, Element.Miracle, "サイバー攻撃"),
			KeyCodeByMotion(MType.Damage, true, Element.Miracle, "システム破壊"),
			KeyCodeByMotion(MType.Absorb, true, Element.Miracle, "システム破壊"),
			KeyCodeByMotion(MType.Confuse, true, Element.Miracle, "システム撹乱"),
			KeyCodeByMotion(MType.FaceUp, true, Element.Miracle, "システム解析"),
			KeyCodeByMotion(MType.VanishTarget, false, Element.All, "対象消去"),
		], [
			ElementOverride(Element.Miracle, "presetimage://elm_electronic.png", "電子", "電子制御された存在"),
		]),
	]));

	version (Windows) {
		auto outerTools = Prop!(OuterTool[])("outerTools", [
			OuterTool("メモ帳", "notepad $F", "", "", ""),
			OuterTool("ペイント", "mspaint $F", "", "", "")
		]);
	} else {
		auto outerTools = Prop!(OuterTool[])("outerTools", []);
	}
	version (Windows) {
		auto ignorePaths = Prop!(string[])("ignorePaths", [".*", "Thumbs.db"]);
	} else {
		auto ignorePaths = Prop!(string[])("ignorePaths", [".*"]);
	}

	auto selectedArchiveFilter = Prop!(string)("selectedArchiveFilter", ".zip");

	auto classicEngines = Prop!(ClassicEngine[])("classicEngines", []);
	auto addNewClassicEngine = Prop!(bool)("addNewClassicEngine", true);
	auto skinTypesByClassicEngines = Prop!(SkinTypeByClassicEngine[], true)("skinTypesByClassicEngines", [
		SkinTypeByClassicEngine("School", "^s_c_wirth\\.exe$", ""),
		SkinTypeByClassicEngine("Modern", "^modernwirth\\.exe$", ""),
		SkinTypeByClassicEngine("Monsters", "^darkwirth\\.exe$", ""),
		SkinTypeByClassicEngine("Oedo", "^oedowirth\\.exe$", ""),
		SkinTypeByClassicEngine("ScienceFiction", "", "^.*sfv$"),
	]);

	auto drawCountOfUseOfStart = Prop!(bool)("drawCountOfUseOfStart", true);
	auto drawContentTreeLine = Prop!(bool)("drawContentTreeLine", true);
	auto drawContentWarnings = Prop!(bool)("drawContentWarnings", true);
	auto drawContentIconOnDetail = Prop!(bool)("drawContentIconOnDetail", true);
	auto commentBoxDistance = Prop!(int, true, true)("commentBoxDistance", 50);
	auto warningImageWidth = Prop!(int, true, true)("warningImageWidth", 200);
	auto warningImageWidthForTable = Prop!(int, true, true)("warningImageWidthForTable", 80);
	auto warningImageWidthForTree = Prop!(int, true, true)("warningImageWidthForTree", 80);
	auto warningImageWidthForCardList = Prop!(int, true, true)("warningImageWidthForCardList", 50);
	auto warningImageColor = Prop!(CRGB, true)("warningImageColor", CRGB(255, 128, 128));

	auto doubleIO = Prop!(bool)("doubleIO", true);
	auto cautionToScenarioLoadErrors = Prop!(bool)("cautionToScenarioLoadErrors", true);
	auto reconstruction = Prop!(bool)("reconstruction", true);
	auto branchRandomFailureFormat = Prop!(bool)("branchRandomFailureFormat", true);
	auto openLastScenario = Prop!(bool)("openLastScenario", true);
	auto imageCache = Prop!(bool)("imageCache", true);

	auto savedSound = Prop!(string)("savedSound", "");

	auto connContentTools = Prop!(bool)("connContentTools", true);

	auto previewAlpha = Prop!(int, true)("previewAlpha", 255);
	auto previewMaxWidth = Prop!(int, true, true)("previewMaxWidth", 150);
	auto previewMaxHeight = Prop!(int, true, true)("previewMaxHeight", 150);

	auto undoMaxMainView = Prop!(int)("undoMaxMainView", 1024);
	auto undoMaxEvent = Prop!(int)("undoMaxEvent", 1024);
	auto undoMaxEtc = Prop!(int)("undoMaxEtc", 1024);
	auto undoMaxLimit = Prop!(int)("undoMaxLimit", short.max);
	auto undoMaxReplace = Prop!(int)("undoMaxReplace", 16);

	auto dialogStatus = Prop!(int)("dialogStatus", DialogStatus.Top);

	auto showInputGuide = Prop!(bool)("showInputGuide", true);

	auto showSummaryPreview = Prop!(bool)("showSummaryPreview", true);

	auto floatMessagePreview = Prop!(bool)("floatMessagePreview", false);
	auto useMessageWindowColorInTextContentDialog = Prop!(bool)("useMessageWindowColorInTextContentDialog", true);
	auto showDialogPreview = Prop!(bool)("showDialogPreview", true);
	auto showMessagePreview = Prop!(bool)("showMessagePreview", true);
	auto messageVarKindColumn = Prop!(int, false, true)("messageVarKindColumn", 200);
	auto messageVarValueColumn = Prop!(int, false, true)("messageVarValueColumn", 250);
	auto textVarKindColumn = Prop!(int, false, true)("textVarKindColumn", 160);
	auto textVarValueColumn = Prop!(int, false, true)("textVarValueColumn", 180);
	auto messageVarTableHeight = Prop!(int, true, true)("messageVarTableHeight", 300);
	auto messageVarSelected = Prop!(string)("messageVarSelected", "[選択中----14]");
	auto messageVarUnselected = Prop!(string)("messageVarUnselected", "[選択外----14]");
	auto messageVarRandom = Prop!(string)("messageVarRandom", "[ランダム--14]");
	auto messageVarCard = Prop!(string)("messageVarCard", "[カード--12]");
	auto messageVarRef = Prop!(string)("messageVarRef", "[話者------14]");
	auto messageVarTeam = Prop!(string)("messageVarTeam", "[チーム名------------------30]");
	auto messageVarYado = Prop!(string)("messageVarYado", "[宿屋名--------18]");
	auto messageVarSelectedPlayerCardNumber = Prop!(uint)("messageVarSelectedPlayerCardNumber", 0); // Wsn.2
	auto messageVarPlayerCardName = Prop!(string[])("messageVarPlayerCardName", [
		"[PC名1-----14]",
		"[PC名2-----14]",
		"[PC名3-----14]",
		"[PC名4-----14]",
		"[PC名5-----14]",
		"[PC名6-----14]",
	]); // Wsn.2
	auto messageVarPlayerCardNameDefault = Prop!(string, true)("messageVarPlayerCardNameDefault", "[PC名%N-----14]");
	auto selectedPlayerCardName = Prop!(uint)("selectedPlayerCardName", 1);
	auto showVariableValuesInEventText = Prop!(bool)("showVariableValuesInEventText", false);
	auto editSelectionWithCombo = Prop!(bool)("editSelectionWithCombo", true);

	auto scenarioTemplates = Prop!(ScTemplate[])("scenarioTemplates", []);
	auto defaultScenarioTemplate = Prop!(string)("defaultScenarioTemplate", "");
	auto defaultIsTemplate = Prop!(bool)("defaultIsTemplate", false);
	auto createScenarioDir = Prop!(bool)("createScenarioDir", true);

	auto eventTemplates = Prop!(EvTemplate[])("eventTemplates", []);
	auto scriptVarTableHeight = Prop!(int, true, true)("scriptVarTableHeight", 300);

	auto archivePath = Prop!(string)("archivePath", "");

	auto contentConversionGroups = Prop!(ContentConversionGroup[])("contentConversionGroups", [
		ContentConversionGroup([CType.LinkStart, CType.CallStart, CType.LinkPackage, CType.CallPackage]),
		ContentConversionGroup([CType.ShowParty, CType.HideParty]),
		ContentConversionGroup([CType.ChangeBgImage, CType.MoveBgImage, CType.ReplaceBgImage, CType.LoseBgImage, CType.Redisplay]),
		ContentConversionGroup([CType.BranchSelect, CType.BranchRandomSelect]),
		ContentConversionGroup([CType.TalkMessage, CType.TalkDialog]),
		ContentConversionGroup([CType.BranchAbility, CType.BranchLevel, CType.BranchStatus, CType.BranchPartyNumber, CType.BranchKeyCode]),
		ContentConversionGroup([CType.BranchArea, CType.BranchBattle, CType.BranchIsBattle, CType.BranchRound]),
		ContentConversionGroup([CType.BranchRandom, CType.BranchMultiRandom]),
		ContentConversionGroup([CType.BranchFlag, CType.SetFlag, CType.ReverseFlag, CType.SubstituteFlag, CType.CheckFlag, CType.MoveCard]),
		ContentConversionGroup([CType.BranchMultiStep, CType.BranchStep, CType.SetStep, CType.SetStepUp, CType.SetStepDown, CType.SubstituteStep, CType.BranchStepCmp, CType.CheckStep]),
		ContentConversionGroup([CType.CheckFlag, CType.CheckStep]),
		ContentConversionGroup([CType.BranchCast, CType.GetCast, CType.LoseCast]),
		ContentConversionGroup([CType.BranchItem, CType.GetItem, CType.LoseItem]),
		ContentConversionGroup([CType.BranchSkill, CType.GetSkill, CType.LoseSkill]),
		ContentConversionGroup([CType.BranchBeast, CType.GetBeast, CType.LoseBeast]),
		ContentConversionGroup([CType.BranchItem, CType.BranchSkill, CType.BranchBeast, CType.BranchKeyCode]),
		ContentConversionGroup([CType.GetItem, CType.GetSkill, CType.GetBeast]),
		ContentConversionGroup([CType.LoseItem, CType.LoseSkill, CType.LoseBeast]),
		ContentConversionGroup([CType.BranchInfo, CType.GetInfo, CType.LoseInfo]),
		ContentConversionGroup([CType.BranchMoney, CType.GetMoney, CType.LoseMoney]),
		ContentConversionGroup([CType.BranchCoupon, CType.GetCoupon, CType.LoseCoupon, CType.BranchMultiCoupon]),
		ContentConversionGroup([CType.BranchGossip, CType.GetGossip, CType.LoseGossip]),
		ContentConversionGroup([CType.BranchCompleteStamp, CType.GetCompleteStamp, CType.LoseCompleteStamp]),
		ContentConversionGroup([CType.ChangeArea, CType.StartBattle]),
		ContentConversionGroup([CType.End, CType.EndBadEnd]),
		ContentConversionGroup([CType.Effect, CType.PlayBgm, CType.PlaySound]),
		ContentConversionGroup([CType.BranchVariant, CType.SetVariant, CType.CheckVariant]),
	]);

	auto contentInitializers = Prop!(ContentInitializer[])("contentInitializers", []);

	auto mainToolBar = Prop!(ToolBarSettings)("mainToolBar", ToolBarSettings([
		[
			Tool(MenuID.New),
			Tool(MenuID.Open),
			Tool(MenuID.Save),
		],
		[
			Tool(MenuID.Undo),
			Tool(MenuID.Redo),
		],
		[
			Tool(MenuID.Cut),
			Tool(MenuID.Copy),
			Tool(MenuID.Paste),
		],
		[
			Tool(MenuID.Up),
			Tool(MenuID.Down),
		],
		[
			Tool(MenuID.Find),
		],
		[
			Tool(MenuID.NewAreaDir),
			Tool(MenuID.NewArea),
			Tool(MenuID.NewBattle),
			Tool(MenuID.NewPackage),
			Tool(),
			Tool(MenuID.NewFlagDir),
			Tool(MenuID.NewFlag),
			Tool(MenuID.NewStep),
			Tool(MenuID.NewVariant),
		],
		[
			Tool(MenuID.ShowCardProp),
			Tool(MenuID.ShowCardDetail),
			Tool(),
			Tool(MenuID.NewCast),
			Tool(MenuID.NewSkill),
			Tool(MenuID.NewItem),
			Tool(MenuID.NewBeast),
			Tool(MenuID.NewInfo),
		],
		[
			Tool(MenuID.OpenDir),
		],
		[
			Tool(MenuID.ExecEngine),
			Tool(MenuID.ExecEngineWithParty),
			Tool(),
			Tool(MenuID.OpenImportSource),
			Tool(),
			Tool(MenuID.Settings),
		],
	]));

	/// 不使用。設定引き継ぎのために残してある
	auto gentleAngleEventTree = Prop!(bool)("gentleAngleEventTree", false, true);

	mixin XMLFuncs!(FlexEtcProps);
}
